# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in C:\Android\sdk/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}
#-libraryjars libs/okhttputils-2_3_8.jar
#-keep class com.zhy.http.okhttp.** { *; }
-libraryjars ../onesdk/libs/utdid4all-1.0.4.jar

-keep class com.alibaba.sdk.android.**{*;}
-keep class com.ut.**{*;}
-keep class com.ta.**{*;}
-keep class org.litepal.**{*;}
-keep class com.android.**{*;}
-keep class com.squareup.**{*;}
-keep class org.greenrobot.**{*;}
-keep class com.google.**{*;}
-keep class com.alibaba.**{*;}
-keep class android.support.**{*;}
-keep class com.alibaba.**{*;}
-keep class com.zhy.http.**{*;}
-keep class com.android.print.**{*;}
-keep class java.nio.file.*
-keep class org.codehaus.mojo.animal_sniffer.IgnoreJRERequirement
-keep class okio.**{*;}
-keep class retrofit.Platform$Java8
-keep class sun.misc.**{*;}
-keep class com.jauker.**{*;}
-keep class org.angmarch.**{*;}
-keep class me.shenfan.**{*;}

-dontwarn com.alibaba.**
-dontwarn com.zhy.http.**
-dontwarn com.android.print.**
-dontwarn java.nio.file.*
-dontwarn org.codehaus.mojo.animal_sniffer.IgnoreJRERequirement
-dontwarn okio.**
-dontwarn retrofit.Platform$Java8
-dontwarn sun.misc.**
-keepclassmembers class rx.internal.util.unsafe.*ArrayQueue*Field* {
   long producerIndex;
   long consumerIndex;
}
-keepclassmembers class rx.internal.util.unsafe.BaseLinkedQueueProducerNodeRef {
    rx.internal.util.atomic.LinkedQueueNode producerNode;
}
-keepclassmembers class rx.internal.util.unsafe.BaseLinkedQueueConsumerNodeRef {
    rx.internal.util.atomic.LinkedQueueNode consumerNode;
}

-keep class com.warmsoft.pos.litepal.data.**{*;}
-keep class com.warmsoft.pos.bean.**{*;}
-keepattributes Signature
-keepattributes *Annotation*
-keep class sun.misc.Unsafe { *; }

-keepclassmembers class ** {
    @org.greenrobot.eventbus.Subscribe <methods>;
}
-keep enum org.greenrobot.eventbus.ThreadMode { *; }

# Only required if you use AsyncExecutor
-keepclassmembers class * extends org.greenrobot.eventbus.util.ThrowableFailureEvent {
    <init>(java.lang.Throwable);
}




