package com.warmsoft.pos.support.listview.bean;

public class Type {
    private int id;
    private String typename;
    private int icon;
    private float select;

    public Type(int id, String typename, int icon, float s) {
        super();
        this.id = id;
        this.typename = typename;
        this.icon = icon;
        this.select = s;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTypename() {
        return typename;
    }

    public void setTypename(String typename) {
        this.typename = typename;
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    public float getSelect() {
        return select;
    }

    public void setSelect(float select) {
        this.select = select;
    }
}
