package com.warmsoft.pos.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;

import com.warmsoft.pos.R;
import com.warmsoft.pos.bean.PetInfoModel;
import com.warmsoft.pos.dialog.DeletePetInfoDialog;
import com.warmsoft.pos.litepal.data.PetCategory;

import org.angmarch.views.NiceSpinner;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by laole918 on 2016/3/19.
 */
public class AddPetRecyclerViewAdapter extends RecyclerView.Adapter<AddPetRecyclerViewAdapter.RecyclerViewHolder> {

    private Context mContext;
    List<PetInfoModel> petInfoModelList = new ArrayList<>();
    protected List<PetCategory> petCategories = new ArrayList<>();
    private int selectPosition = -1;

    public AddPetRecyclerViewAdapter(Context context, List<PetInfoModel> list, List<PetCategory> categories) {
        this.mContext = context;
        this.petInfoModelList = list;
        this.petCategories = categories;
    }

    public void setData(List<PetInfoModel> list) {
        this.petInfoModelList = list;
        notifyDataSetChanged();
    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerViewHolder holder = new RecyclerViewHolder(LayoutInflater.from(
                mContext).inflate(R.layout.item_for_add_pet_info, parent,
                false));
        return holder;
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, final int position) {

        if (holder.etName.getTag() instanceof TextWatcher) {
            holder.etName.removeTextChangedListener((TextWatcher) (holder.etName.getTag()));
        }

        if (holder.etAge.getTag() instanceof TextWatcher) {
            holder.etAge.removeTextChangedListener((TextWatcher) (holder.etAge.getTag()));
        }

        final PetInfoModel model = petInfoModelList.get(position);
        String petName = model.getPetName();
        if (TextUtils.isEmpty(petName)) {
            holder.etName.setText("");
        } else {
            holder.etName.setText(petName);
        }

        String petAge = model.getAge();
        if (TextUtils.isEmpty(petAge)) {
            holder.etAge.setText("");
        } else {
            holder.etAge.setText(petAge);
        }

        if (selectPosition == position) {
            if (!holder.etName.isFocused()) {
                holder.etName.requestFocus();
            }
            CharSequence name = holder.etName.getText();
            holder.etName.setSelection(TextUtils.isEmpty(name) ? 0 : name.length());

            if (!holder.etAge.isFocused()) {
                holder.etAge.requestFocus();
            }
            CharSequence age = holder.etAge.getText();
            holder.etAge.setSelection(TextUtils.isEmpty(age) ? 0 : age.length());
        } else {
            if (holder.etName.isFocused()) {
                holder.etName.clearFocus();
            }

            if (holder.etAge.isFocused()) {
                holder.etAge.clearFocus();
            }
        }

        holder.etName.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    selectPosition = position;
                }
                return false;
            }
        });

        holder.etAge.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    selectPosition = position;
                }
                return false;
            }
        });

        final TextWatcher etNameWatcher = new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (TextUtils.isEmpty(s)) {
                    model.setPetName(null);
                } else {
                    model.setPetName(String.valueOf(s));
                }
            }
        };
        holder.etName.addTextChangedListener(etNameWatcher);
        holder.etName.setTag(etNameWatcher);


        final TextWatcher etAgeWatcher = new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (TextUtils.isEmpty(s)) {
                    model.setAge(null);
                } else {
                    model.setAge(String.valueOf(s));
                }
            }
        };
        holder.etAge.addTextChangedListener(etAgeWatcher);
        holder.etAge.setTag(etAgeWatcher);

        //目录显示
        if (petCategories.size() > 0) {
            holder.nsCatgory.setSelectedIndex(0);
            holder.nsCatgory.setTextColor(mContext.getResources().getColor(R.color.black));
            holder.nsCatgory.attachDataSource(parseSpinnerPetsCatgoryDisplay(petCategories));

            if (TextUtils.isEmpty(petCategories.get(0).getName())) {
                model.setCatgory(null);
                model.setKindOF("0");
            } else {
                model.setCatgory(String.valueOf(petCategories.get(0).getName()));
                model.setKindOF(String.valueOf(petCategories.get(0).getCategory_id()));
            }

            holder.nsCatgory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    if (TextUtils.isEmpty(petCategories.get(i).getName())) {
                        petInfoModelList.get(position).setKindOF("0");
                        petInfoModelList.get(position).setCatgory("");
                        model.setKindOF("0");
                        model.setCatgory(null);
                    } else {
                        model.setCatgory(String.valueOf(petCategories.get(i).getName()));
                        model.setKindOF(String.valueOf(petCategories.get(i).getCategory_id()));

                        petInfoModelList.get(position).setKindOF(petCategories.get(i).getCategory_id());
                        petInfoModelList.get(position).setCatgory(petCategories.get(i).getName());
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });
        }

        holder.ivClearSpecial.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new DeletePetInfoDialog((Activity) mContext, "温馨提示", "确定要删除这条宠物信息吗?", "确认", new DeletePetInfoDialog.DialogListener() {

                    @Override
                    public void noteDialogConfirm() {
                        //Toast.makeText(mContext, "已经删除了...." + position + "----" + petInfoModelList.get(position).getPetName(), Toast.LENGTH_SHORT).show();

                        if (petInfoModelList != null && petInfoModelList.size() > 0) {
                            petInfoModelList.remove(position);//删除后重新刷新
                            setData(petInfoModelList);
                        }
                    }
                });
            }
        });
    }

    @Override
    public int getItemCount() {
        return petInfoModelList.size();
    }

    class RecyclerViewHolder extends RecyclerView.ViewHolder {
        //        public TextView tvCount;
        public EditText etName;
        public EditText etAge;
        public NiceSpinner nsCatgory;
        public ImageView ivClearSpecial;

        public RecyclerViewHolder(View itemView) {
            super(itemView);
            etName = (EditText) itemView.findViewById(R.id.id_for_name_value);
//            tvCount = (TextView) itemView.findViewById(R.id.id_for_pet_count);
            etAge = (EditText) itemView.findViewById(R.id.id_for_catgory_value);
            nsCatgory = (NiceSpinner) itemView.findViewById(R.id.id_for_pet_catgory);
            ivClearSpecial = (ImageView) itemView.findViewById(R.id.id_for_clean_special_pet);
        }
    }

    private List<String> parseSpinnerPetsCatgoryDisplay(List<PetCategory> list) {

        List<String> l = new ArrayList<>();
        for (PetCategory s : list) {
            l.add(s.getName());
        }

        return l;
    }
}
