package com.warmsoft.pos.wxpay;

import java.io.Serializable;

/**
 * Created by sangxiewu on 16/10/20.
 */
public class WePaySettingsModel implements Serializable {
    int code;
    String message;
    DataInfo Data;

    public static class DataInfo implements Serializable {
        int Id;
        int OrgId;

        String Appid;
        String Mchid;
        String KEY;
        String AppSecret;
        String SSLSecret;
        String SSLSecret_Password;
        String ValidatePhone;

        public int getId() {
            return Id;
        }

        public void setId(int id) {
            Id = id;
        }

        public int getOrgId() {
            return OrgId;
        }

        public void setOrgId(int orgId) {
            OrgId = orgId;
        }

        public String getAppid() {
            return Appid;
        }

        public void setAppid(String appid) {
            Appid = appid;
        }

        public String getMchid() {
            return Mchid;
        }

        public void setMchid(String mchid) {
            Mchid = mchid;
        }

        public String getKEY() {
            return KEY;
        }

        public void setKEY(String KEY) {
            this.KEY = KEY;
        }

        public String getAppSecret() {
            return AppSecret;
        }

        public void setAppSecret(String appSecret) {
            AppSecret = appSecret;
        }

        public String getSSLSecret() {
            return SSLSecret;
        }

        public void setSSLSecret(String SSLSecret) {
            this.SSLSecret = SSLSecret;
        }

        public String getSSLSecret_Password() {
            return SSLSecret_Password;
        }

        public void setSSLSecret_Password(String SSLSecret_Password) {
            this.SSLSecret_Password = SSLSecret_Password;
        }

        public String getValidatePhone() {
            return ValidatePhone;
        }

        public void setValidatePhone(String validatePhone) {
            ValidatePhone = validatePhone;
        }
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataInfo getData() {
        return Data;
    }

    public void setData(DataInfo data) {
        Data = data;
    }
}
