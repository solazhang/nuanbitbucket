package com.warmsoft.pos.litepal.data;

import org.litepal.crud.DataSupport;

import java.io.Serializable;
import java.util.Date;

/**
 * 作者: lijinliu on 2016/8/17.
 * 邮箱：jinliu.li@warmsoft.com
 * 描述: 定单信息
 */
public class OrderInfoData extends DataSupport implements Serializable {

    private int id;
    private int orderNo;//本地定单号
    private Date orderDate;//定单日期
    private int opId;//收银员Id
    private String opName;//收银员
    private int PetId;//宠物ID
    private String PetName;//宠物名称
    private int CustomerId;//客户ID
    private String memberCardNo;//会员卡号
    private String CustomerName;//客户名称
    private String CellPhone;//电话
    private int OrgId;//机构ID
    private String OrgName;//机构名称
    private float countGoods;//商品数量
    private float countAmount;//总金额
    private float cashAmount;//现金收取金额
    private float bankCardAAmount;//银行卡1
    private float bankCardBAmount;//银行卡1
    private float webChatAmount;//微信收取金额
    private float aliPayAmount;//支付宝收取金额
    private float accountAmount;//账户余额
    private float prePayAmount;//押金
    private float memberCardAmount;//会员卡
    private float keepingAccountAmout;//记账
    private float dicountRate = 1;//整单折扣率,默认是1
    private float discountAmount;//抹零金额
    private float changeAmount;//找零
    private String status = "0";//定单状态0新开单，1已下单,2重新打开，3结算成功,4 离线下单 5 离线结算
    public boolean isNew = false;//待付款数据解析时用于是update还是save
    public boolean isSelected = false;//待付款中选中

    private String noes;
    private String time;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(int orderNo) {
        this.orderNo = orderNo;
    }

    public Date getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(Date orderDate) {
        this.orderDate = orderDate;
    }

    public int getOpId() {
        return opId;
    }

    public void setOpId(int opId) {
        this.opId = opId;
    }

    public String getOpName() {
        return opName;
    }

    public void setOpName(String opName) {
        this.opName = opName;
    }

    public int getPetId() {
        return PetId;
    }

    public void setPetId(int petId) {
        PetId = petId;
    }

    public String getPetName() {
        return PetName;
    }

    public void setPetName(String petName) {
        PetName = petName;
    }

    public int getCustomerId() {
        return CustomerId;
    }

    public void setCustomerId(int customerId) {
        CustomerId = customerId;
    }

    public String getMemberCardNo() {
        return memberCardNo;
    }

    public void setMemberCardNo(String memberCardNo) {
        this.memberCardNo = memberCardNo;
    }

    public String getCustomerName() {
        return CustomerName;
    }

    public void setCustomerName(String customerName) {
        CustomerName = customerName;
    }

    public String getCellPhone() {
        return CellPhone;
    }

    public void setCellPhone(String cellPhone) {
        CellPhone = cellPhone;
    }

    public int getOrgId() {
        return OrgId;
    }

    public void setOrgId(int orgId) {
        OrgId = orgId;
    }

    public String getOrgName() {
        return OrgName;
    }

    public void setOrgName(String orgName) {
        OrgName = orgName;
    }

    public float getCountGoods() {
        return countGoods;
    }

    public void setCountGoods(float countGoods) {
        this.countGoods = countGoods;
    }

    public float getCountAmount() {
        return countAmount;
    }

    public void setCountAmount(float countAmount) {
        this.countAmount = countAmount;
    }

    public float getCashAmount() {
        return cashAmount;
    }

    public void setCashAmount(float cashAmount) {
        this.cashAmount = cashAmount;
    }

    public float getBankCardAAmount() {
        return bankCardAAmount;
    }

    public void setBankCardAAmount(float bankCardAAmount) {
        this.bankCardAAmount = bankCardAAmount;
    }

    public float getBankCardBAmount() {
        return bankCardBAmount;
    }

    public void setBankCardBAmount(float bankCardBAmount) {
        this.bankCardBAmount = bankCardBAmount;
    }

    public float getWebChatAmount() {
        return webChatAmount;
    }

    public void setWebChatAmount(float webChatAmount) {
        this.webChatAmount = webChatAmount;
    }

    public float getAliPayAmount() {
        return aliPayAmount;
    }

    public void setAliPayAmount(float aliPayAmount) {
        this.aliPayAmount = aliPayAmount;
    }

    public float getAccountAmount() {
        return accountAmount;
    }

    public void setAccountAmount(float accountAmount) {
        this.accountAmount = accountAmount;
    }

    public float getKeepingAccountAmout() {
        return keepingAccountAmout;
    }

    public void setKeepingAccountAmout(float keepingAccountAmout) {
        this.keepingAccountAmout = keepingAccountAmout;
    }

    public float getPrePayAmount() {
        return prePayAmount;
    }

    public void setPrePayAmount(float prePayAmount) {
        this.prePayAmount = prePayAmount;
    }

    public float getMemberCardAmount() {
        return memberCardAmount;
    }

    public void setMemberCardAmount(float memberCardAmount) {
        this.memberCardAmount = memberCardAmount;
    }

    public float getDicountRate() {
        return dicountRate;
    }

    public void setDicountRate(float dicountRate) {
        this.dicountRate = dicountRate;
    }

    public float getDiscountAmount() {
        return discountAmount;
    }

    public void setDiscountAmount(float discountAmount) {
        this.discountAmount = discountAmount;
    }

    public float getChangeAmount() {
        return changeAmount;
    }

    public void setChangeAmount(float changeAmount) {
        this.changeAmount = changeAmount;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getNoes() {
        return noes;
    }

    public void setNoes(String noes) {
        this.noes = noes;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}