package com.warmsoft.pos.layout.view.settlement;

import android.app.Activity;
import android.content.Context;
import android.support.v4.util.ArrayMap;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.warmsoft.pos.R;
import com.warmsoft.pos.adapter.entiy.PayInfoAdapter;
import com.warmsoft.pos.dialog.BillingOrderDialog;
import com.warmsoft.pos.bean.AmountsModel;
import com.warmsoft.pos.dialog.NomelNoteDialog;
import com.warmsoft.pos.enumutil.OrderStatusEnum;
import com.warmsoft.pos.enumutil.PayChanel;
import com.warmsoft.pos.listener.LayoutSettleListener;
import com.warmsoft.pos.litepal.data.CustomerList;
import com.warmsoft.pos.litepal.data.LitepalHelper;
import com.warmsoft.pos.litepal.data.OrderInfoData;
import com.warmsoft.pos.litepal.data.PayAmountData;
import com.warmsoft.pos.net.RetrofitManager;
import com.warmsoft.pos.protocol.GetCustomeAmontProto;
import com.warmsoft.pos.util.Constant;
import com.warmsoft.pos.util.LogUtil;
import com.warmsoft.pos.util.OrderHelper;
import com.warmsoft.pos.util.SelectMemberUtil;
import com.warmsoft.pos.util.Util;

import org.litepal.crud.DataSupport;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * 作者: lijinliu on 2016/8/10.
 * 邮箱：jinliu.li@warmsoft.com
 * 描述: 支付方式： view_seetlement_paychanel.xml
 */
public class SettlementPayChanel implements View.OnClickListener {

    private Activity mActivity;
    private Context mContext;
    private LayoutSettleListener mListner;
    private OrderHelper orderHelper;
    private LitepalHelper litepal;

    protected TextView tvOrderSize;
    protected TextView tvOrderAmount;
    protected TextView tvNeedPay;
    protected TextView tvSpentAmount;
    protected TextView tvOrderPayinfo;
    protected TextView tvMemberNum;
    protected TextView tvMemberJiFeng;
    protected TextView tvMemberLevel;

    protected TextView tvMemberPrice;
    protected TextView tvMemberAmount;
    protected TextView tvMemberPrePay;

    protected ListView listView;
    protected PayInfoAdapter adapter;
    protected List<PayAmountData> datas = new ArrayList<PayAmountData>();

    protected Button btnClearPaysInfo;
    protected Button btnSwingMember;
    protected Button btnBilling;//记账
    protected Button btnPayByDeposit;//押金支付
    protected Button btnPayBYCash;
    protected Button btnPayBYBankCard;
    protected Button btnPayByVipCard;
    protected Button btnPayByAccount;
    protected Button btnPayByAlipay;
    protected Button btnPayByWechat;
    protected Button btnPayByOther;
    protected FrameLayout flRefreshMember;

    public ImageView ivProgressbar;
    Animation operatingAnim;

    public SettlementPayChanel(View rootView, Activity activity, LayoutSettleListener listener) {
        this.mActivity = activity;
        this.mContext = mActivity.getApplicationContext();
        this.mListner = listener;
        litepal = LitepalHelper.getInstance();

        tvOrderSize = (TextView) rootView.findViewById(R.id.tv_settle_order_size);//定单项
        tvOrderAmount = (TextView) rootView.findViewById(R.id.tv_settle_order_amount);//定单金额
        tvNeedPay = (TextView) rootView.findViewById(R.id.tv_settle_need_payed);//应收
        tvSpentAmount = (TextView) rootView.findViewById(R.id.tv_settle_spent_amount);//消费金额
        listView = (ListView) rootView.findViewById(R.id.list_sellte_payinfo);//收款列表
        tvOrderPayinfo = (TextView) rootView.findViewById(R.id.tv_settle_main_payinfo);//实收多少多少，找零多少多少
        tvMemberNum = (TextView) rootView.findViewById(R.id.tv_settle_member_num);//会员卡号
        tvMemberJiFeng = (TextView) rootView.findViewById(R.id.tv_settle_member_jifeng);//会员积分
        tvMemberLevel = (TextView) rootView.findViewById(R.id.id_for_member_level);//会员积分
        tvMemberPrice = (TextView) rootView.findViewById(R.id.tv_settle_member_price);//会员价

        tvMemberAmount = (TextView) rootView.findViewById(R.id.tv_settle_member_amount);//会员余额
        tvMemberPrePay = (TextView) rootView.findViewById(R.id.id_for_pre_pay);//会员余额

        btnClearPaysInfo = (Button) rootView.findViewById(R.id.btn_settle_clear_payed);//
        btnSwingMember = (Button) rootView.findViewById(R.id.btn_settle_swing_member);//
        btnPayBYCash = (Button) rootView.findViewById(R.id.btn_settle_payby_cash);//
        btnPayBYBankCard = (Button) rootView.findViewById(R.id.btn_settle_payby_bankcard);//

        btnBilling = (Button) rootView.findViewById(R.id.btn_settle_billing);//记账
        btnPayByDeposit = (Button) rootView.findViewById(R.id.btn_settle_payby_deposit);//押金支付
        btnPayByVipCard = (Button) rootView.findViewById(R.id.btn_settle_payby_vipcard);//会员卡
        btnPayByAccount = (Button) rootView.findViewById(R.id.btn_settle_payby_account);//会员账号
        btnPayByAlipay = (Button) rootView.findViewById(R.id.btn_settle_payby_alipay);//
        btnPayByWechat = (Button) rootView.findViewById(R.id.btn_settle_payby_wechat);//
        btnPayByOther = (Button) rootView.findViewById(R.id.btn_settle_payby_others);//

        flRefreshMember = (FrameLayout) rootView.findViewById(R.id.id_for_member_info);
        ivProgressbar = (ImageView) rootView.findViewById(R.id.id_for_member_info_iv);

        initEvents();
        initDefaultData();
        {
            adapter = new PayInfoAdapter(mContext, datas);
            listView.setAdapter(adapter);
        }
    }

    private void initEvents() {
        operatingAnim = AnimationUtils.loadAnimation(mContext, R.anim.rotate_animation);
        LinearInterpolator lin = new LinearInterpolator();
        operatingAnim.setInterpolator(lin);

        btnClearPaysInfo.setOnClickListener(this);
        btnSwingMember.setOnClickListener(this);
        btnPayBYCash.setOnClickListener(this);
        btnPayBYBankCard.setOnClickListener(this);

        btnBilling.setOnClickListener(this);
        btnPayByDeposit.setOnClickListener(this);
        btnPayByVipCard.setOnClickListener(this);
        btnPayByAccount.setOnClickListener(this);
        btnPayByAlipay.setOnClickListener(this);
        btnPayByWechat.setOnClickListener(this);
        btnPayByOther.setOnClickListener(this);

        flRefreshMember.setOnClickListener(this);
    }

    public void initData() {
        orderHelper = OrderHelper.getInstance(mContext);
        OrderInfoData orderData = orderHelper.getOrderData();
        if (orderData != null) {
            tvOrderSize.setText(orderHelper.getOrderSize() + "项");
            tvOrderAmount.setText(orderData.getCountAmount() + "");//定单金额
            tvNeedPay.setText(orderHelper.getNeedAmount() + "");//应收
            tvSpentAmount.setText(orderData.getCountAmount() + "");//消费金额
            refershList(orderData.getOrderNo() + "");
            refershMemberInfo(orderData.getCustomerId());
        } else {
            initDefaultData();
        }
        checkMemberAmount();
    }

    public void initDefaultData() {
        tvOrderSize.setText("0项");
        tvOrderAmount.setText("0");//定单金额
        tvNeedPay.setText("0");//应收
        tvSpentAmount.setText("0");//消费金额
        tvOrderPayinfo.setText("");//实收多少多少，找零多少多少
        tvMemberNum.setText("会员: ");
        tvMemberJiFeng.setText("积分: ");
        tvMemberLevel.setText("等级: ");
        tvMemberAmount.setText("余额: ");
    }

    //已收款刷新
    public float refershList(String orderNo) {
        datas = litepal.queryPaysData(orderNo);
        float amout = 0.00f;
        if (datas != null && datas.size() > 0) {
            adapter.setData(datas);
            {
                float moling = 0;
                for (PayAmountData payData : datas) {
                    if (payData.getPayChanel() == PayChanel.PAY_BY_DISCOUNT) {
                        moling = payData.getPayAmount();
                    } else if (payData.getPayChanel() != PayChanel.PAY_BY_KEEPACCCOUNT) {//记账金额不算入实收
                        amout += payData.getPayAmount();
                    }
                }
                float change = 0.00f;
                if (amout > datas.get(0).getOrderAmount()) {
                    change = amout - datas.get(0).getOrderAmount();
                }
                if (moling > 0) {
                    tvOrderPayinfo.setText(String.format(Constant.ORDER_PAY_INFOB, Util.subFloat(amout) + "", Util.subFloat(moling) + ""));
                } else {
                    tvOrderPayinfo.setText(String.format(Constant.ORDER_PAY_INFOA, Util.subFloat(amout) + "", Util.subFloat(change) + ""));
                }
            }
        } else {
            tvOrderPayinfo.setText("");
            adapter.setData(new ArrayList<PayAmountData>());
        }
        return amout;
    }

    public void refershMemberInfo(int customerid) {
        CustomerList customer = litepal.queryCustomInfo(customerid + "");
        if (customer != null) {
            tvMemberNum.setText(String.format("会员: %1$s", customer.getCardNumber()));
            tvMemberJiFeng.setText(String.format("积分: %1$s", customer.getScore()));
            tvMemberLevel.setText(String.format("等级: %1$s", customer.getCardTypeName()));

            if (SelectMemberUtil.getInstance().getDataInfo() != null) {
                tvMemberAmount.setText(String.format("账户余额: %1$s", SelectMemberUtil.getInstance().getDataInfo().getAccount()));
                tvMemberPrice.setText(String.format("卡内余额: %1$s", SelectMemberUtil.getInstance().getDataInfo().getMemberCard()));
                tvMemberPrePay.setText(String.format("押金: %1$s", SelectMemberUtil.getInstance().getDataInfo().getPrePay()));
            } else {
                tvMemberAmount.setText(String.format("账户余额: %1$s", "0"));
                tvMemberPrice.setText(String.format("卡内余额:", "0"));
                tvMemberPrePay.setText(String.format("押金:", "0"));
            }

        } else {
            tvMemberNum.setText(String.format("会员: %1$s", ""));
            tvMemberJiFeng.setText(String.format("积分: %1$s", "0"));
            tvMemberLevel.setText(String.format("等级: %1$s", ""));
            tvMemberAmount.setText(String.format("账户余额: %1$s", "0"));
            tvMemberPrice.setText(String.format("卡内余额: %1$s", "0"));
            tvMemberPrePay.setText(String.format("押金: %1$s", "0"));
        }
    }

    public void refreshAmount(AmountsModel.DataInfo dataInfo, int customerid) {
        List<CustomerList> customerLists = DataSupport.where(" CustomerId=?", "" + customerid).find(CustomerList.class);
        if (customerLists != null && customerLists.size() > 0) {
            tvMemberNum.setText(String.format("会员: %1$s", customerLists.get(0).getCardNumber()));
            tvMemberJiFeng.setText(String.format("积分: %1$s", customerLists.get(0).getScore()));
            tvMemberLevel.setText(String.format("等级: %1$s", customerLists.get(0).getCardTypeName()));

            if (dataInfo != null) {
                String amount = String.valueOf(dataInfo.getAccount());
                String cardAmount = String.valueOf(dataInfo.getMemberCard());
                String prePay = String.valueOf(dataInfo.getPrePay());

                tvMemberAmount.setText(String.format("账户余额: %1$s", amount));
                tvMemberPrice.setText(String.format("卡内余额: %1$s", cardAmount));
                tvMemberPrePay.setText(String.format("押金: %1$s", prePay));

                LogUtil.error("refreshAmount getAccount " + String.valueOf(dataInfo.getAccount()));
                LogUtil.error("refreshAmount getMemberCard " + String.valueOf(dataInfo.getMemberCard()));
                LogUtil.error("refreshAmount getPrePay " + String.valueOf(dataInfo.getPrePay()));

            } else {
                tvMemberAmount.setText(String.format("账户余额: %1$s", "0"));
                tvMemberPrice.setText(String.format("卡内余额: %1$s", "0"));
                tvMemberPrePay.setText(String.format("押金: %1$s", "0"));
            }

        } else {
            tvMemberNum.setText(String.format("会员: %1$s", ""));
            tvMemberJiFeng.setText(String.format("积分: %1$s", "0"));
            tvMemberLevel.setText(String.format("等级: %1$s", ""));
            tvMemberAmount.setText(String.format("账户余额: %1$s", "0"));
            tvMemberPrice.setText(String.format("卡内余额: %1$s", "0"));
            tvMemberPrePay.setText(String.format("押金: %1$s", "0"));
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_settle_clear_payed:
                if (mListner != null) {
                    mListner.clearAllPays();
                }
                break;
            case R.id.btn_settle_swing_member:
                if (mListner != null) {
                    mListner.swingMember();
                }
                break;
            case R.id.btn_settle_payby_cash:
                if (mListner != null) {
                    mListner.collectMoney(PayChanel.PAY_BY_CASH);
                }
                break;
            case R.id.btn_settle_payby_bankcard:
                if (mListner != null) {
                    mListner.collectMoney(PayChanel.PAY_BY_BANKCARD);
                }
                break;
            case R.id.btn_settle_billing://记账
                orderHelper = OrderHelper.getInstance(mContext);
                if (orderHelper.getOrderStaus() == OrderStatusEnum.HandIn) {
                    new BillingOrderDialog(mActivity, orderHelper.getNeedAmount(), new BillingOrderDialog.DialogListener() {
                        @Override
                        public void keyConfirm() {
                            orderHelper.keepingAccounts();
                            if (mListner != null) {
                                mListner.keepingAccount();
                            }
                        }
                    });
                } else {
                    new NomelNoteDialog(mActivity, OrderStatusEnum.getEnumDesc(orderHelper.getOrderStaus()), "不能进行记账操作!", "确定", null);
                }
                break;
            case R.id.btn_settle_payby_deposit://押金支付
                if (mListner != null) {
                    mListner.collectMoney(PayChanel.PAY_BY_DEPOSIT);
                }
                break;
            case R.id.btn_settle_payby_vipcard://会员卡支付
                if (mListner != null) {
                    mListner.collectMoney(PayChanel.PAY_BY_VIPCARD);
                }
                break;
            case R.id.btn_settle_payby_account://会员余额支付
                if (mListner != null) {
                    mListner.collectMoney(PayChanel.PAY_BY_ACCOUNT);
                }
                break;
            case R.id.btn_settle_payby_alipay://支付宝支付
                if (mListner != null) {
                    mListner.collectMoney(PayChanel.PAY_BY_ALIPAY);
                }
                break;

            case R.id.btn_settle_payby_wechat://微信支付
                if (mListner != null) {
                    mListner.collectMoney(PayChanel.PAY_BY_WECHAT);
                }
                break;

            case R.id.btn_settle_payby_others:
                new NomelNoteDialog(mActivity, "开发中", "功能开发中，请关注产品升级!", "确定", null);
                break;

            case R.id.id_for_member_info:
                OrderHelper orderHelper = OrderHelper.getInstance(mActivity);
                OrderInfoData orderData = orderHelper.getOrderData();

                if (orderData != null) {
                    getAmounts(orderData.getCustomerId());
                }
                break;
        }
    }

    //用户账户金额信息
    public void checkMemberAmount() {
        OrderInfoData orderData = OrderHelper.getInstance(mActivity).getOrderData();
        if (orderData == null) {
            return;
        }
        AmountsModel.DataInfo amountInfo = SelectMemberUtil.getInstance().getDataInfo();
        if (amountInfo != null && orderData.getCustomerId() == amountInfo.getCustomerid()) {
            refreshAmount(amountInfo, amountInfo.getCustomerid());
        } else if (orderData.getCustomerId() > 0) {
            getAmounts(orderData.getCustomerId());
        }
    }


    public void getAmounts(final int customerid) {
        showAnimation();
        new GetCustomeAmontProto(mContext, new GetCustomeAmontProto.ProtoListener() {
            @Override
            public void onCompleted(AmountsModel.DataInfo dataInfo) {
                refreshAmount(dataInfo, dataInfo.getCustomerid());
                clearAnimation();
            }
        }, customerid);
    }

    public void showAnimation() {
        ivProgressbar.setVisibility(View.VISIBLE);

        if (operatingAnim != null) {
            ivProgressbar.startAnimation(operatingAnim);
        }
    }

    public void clearAnimation() {
        ivProgressbar.setVisibility(View.GONE);

        if (operatingAnim != null) {
            ivProgressbar.clearAnimation();
        }
    }
}
