package com.warmsoft.pos.bean;

import java.io.Serializable;

/**
 * Created by brooks on 16/11/7.
 */
public class AccountInfoModel implements Serializable {
    int code;
    String message;
    DataInfo Data;

    public static class DataInfo implements Serializable {
        String CardNo;
        int CardId;
        double CardType;
        double CardBalance;
        double MemberCardGift;
        double AccountBalance;
        double AccountGift;
        double DepositType;
        double DepositBalance;
        double TotalConsumption;
        int Score;
        String InfoType;

        public String getCardNo() {
            return CardNo;
        }

        public void setCardNo(String cardNo) {
            CardNo = cardNo;
        }

        public int getCardId() {
            return CardId;
        }

        public void setCardId(int cardId) {
            CardId = cardId;
        }

        public double getCardType() {
            return CardType;
        }

        public void setCardType(double cardType) {
            CardType = cardType;
        }

        public double getCardBalance() {
            return CardBalance;
        }

        public void setCardBalance(double cardBalance) {
            CardBalance = cardBalance;
        }

        public double getMemberCardGift() {
            return MemberCardGift;
        }

        public void setMemberCardGift(double memberCardGift) {
            MemberCardGift = memberCardGift;
        }

        public double getAccountBalance() {
            return AccountBalance;
        }

        public void setAccountBalance(double accountBalance) {
            AccountBalance = accountBalance;
        }

        public double getAccountGift() {
            return AccountGift;
        }

        public void setAccountGift(double accountGift) {
            AccountGift = accountGift;
        }

        public double getDepositType() {
            return DepositType;
        }

        public void setDepositType(double depositType) {
            DepositType = depositType;
        }

        public double getDepositBalance() {
            return DepositBalance;
        }

        public void setDepositBalance(double depositBalance) {
            DepositBalance = depositBalance;
        }

        public double getTotalConsumption() {
            return TotalConsumption;
        }

        public void setTotalConsumption(double totalConsumption) {
            TotalConsumption = totalConsumption;
        }

        public int getScore() {
            return Score;
        }

        public void setScore(int score) {
            Score = score;
        }

        public String getInfoType() {
            return InfoType;
        }

        public void setInfoType(String infoType) {
            InfoType = infoType;
        }
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataInfo getData() {
        return Data;
    }

    public void setData(DataInfo data) {
        Data = data;
    }
}
