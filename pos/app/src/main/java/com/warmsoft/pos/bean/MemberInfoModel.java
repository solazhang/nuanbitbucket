package com.warmsoft.pos.bean;

import java.io.Serializable;
import java.util.List;

/**
 * Created by brooks on 16/8/30.
 */
public class MemberInfoModel implements Serializable {
    String name;
    String mobile;
    String level;
    int VIPLevel;

    List<PetInfoModel> pets;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public List<PetInfoModel> getPets() {
        return pets;
    }

    public void setPets(List<PetInfoModel> pets) {
        this.pets = pets;
    }

    public int getVIPLevel() {
        return VIPLevel;
    }

    public void setVIPLevel(int VIPLevel) {
        this.VIPLevel = VIPLevel;
    }
}
