package com.warmsoft.pos.service;

import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.Nullable;

import com.warmsoft.pos.util.LogUtil;

import java.util.ArrayList;

/**
 * Created by brooks on 16/8/24.
 */
public class SyncDataService extends Service {

    public final static String RUN_KEY = "runKey";
    public final static int UPLOAD_ORDER_DATAS = 10001;

    public final static int sync_time = 2 * 1000;
    public final static int sync_order_time = 5 * 1000;

    ArrayList<SyncDataModel> uploads = new ArrayList<>();

    /**
     * 创建 Handler 对象，作为进程 传递 postDelayed 之用
     */
    private Handler handler = new Handler();

    /**
     * 为了确认系统服务运行情况
     */
    private int intCounter = 0;
    private int syncCounter = 0;
    private int runValue = 0;
    private Runnable orderTaskRunnable;
    private Runnable addMemberTaskRunnable;

    @Override
    public void onCreate() {
        super.onCreate();

        LogUtil.error("SyncDataService onCreate");
    }

    @Override
    public void onDestroy() {
        recycleRes();
        LogUtil.error("SyncDataService onDestroy");

        super.onDestroy();
    }

    private void recycleRes() {
        if (uploads != null && uploads.size() > 0) {
            uploads.clear();
        }
        handler.removeCallbacks(uploadRunnable);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        LogUtil.error("SyncDataService onStartCommand");

        injectData();
        if (intent != null) {
            runValue = intent.getIntExtra(RUN_KEY, 0);
        }

        if (orderTaskRunnable == null) {
            createTaskRunnable();
            handler.postDelayed(orderTaskRunnable, sync_order_time);
        }

        if (addMemberTaskRunnable == null) {
            createAddMemberRunnable();
            handler.postDelayed(addMemberTaskRunnable, sync_order_time);
        }


        handler.postDelayed(uploadRunnable, 2 * 1000);

        return super.onStartCommand(intent, flags, startId);
    }

    private void injectData() {
        scanNewMembers();
    }

    private void scanNewMembers() {

    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        LogUtil.error("SyncDataService onBind");

        return null;
    }

    /**
     * 成员变量 myTasks为Runnable对象，作为Timer之用
     */
    private Runnable uploadRunnable = new Runnable() {

        @Override
        public void run() {
            //递增counter整数，作为后台服务运行时间识别
            intCounter++;
            if (runValue == UPLOAD_ORDER_DATAS) {
                //UploadDataUtil.getInstance(getApplicationContext()).uploadDatas();//上传营业数据
            } else {
                //UploadDataUtil.getInstance(getApplicationContext()).uploadDatas();//上传营业数据
                SyncDataUtil.getInstance().syncAll(); //同步所有的数据
            }
        }
    };

    private void createTaskRunnable() {
        orderTaskRunnable = new Runnable() {
            @Override
            public void run() {
                UploadDataUtil.getInstance(getApplicationContext()).uploadDatas();//订单数据异步上传
                if (orderTaskRunnable != null) {
                    handler.postDelayed(orderTaskRunnable, sync_order_time);
                }
            }
        };
    }

    private void createAddMemberRunnable() {
        addMemberTaskRunnable = new Runnable() {
            @Override
            public void run() {
                UploadDataUtil.getInstance(getApplicationContext()).uploadMember();//会员上传
                if (addMemberTaskRunnable != null) {
                    handler.postDelayed(addMemberTaskRunnable, sync_time);
                }

                //LogUtil.error("addMemberTaskRunnable is alive ....");
            }
        };
    }
}
