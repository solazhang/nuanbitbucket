package com.warmsoft.pos.bean;

import com.warmsoft.pos.litepal.data.CustomerList;

import java.io.Serializable;
import java.util.List;

/**
 * Created by brooks on 16/1/26.
 */
public class MembersModel implements Serializable {
    int code;
    String message;
    Data Data;

    public static class Data implements Serializable {
        List<CustomerList> CustomerList;
        int Count;

        public List<CustomerList> getCustomerList() {
            return CustomerList;
        }

        public void setCustomerList(List<com.warmsoft.pos.litepal.data.CustomerList> customerList) {
            CustomerList = customerList;
        }

        public int getCount() {
            return Count;
        }

        public void setCount(int count) {
            Count = count;
        }
    }

    public MembersModel.Data getData() {
        return Data;
    }

    public void setData(MembersModel.Data data) {
        Data = data;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
