package com.warmsoft.pos.service;

import java.io.Serializable;

/**
 * Created by brooks on 16/8/24.
 */

public class SyncDataModel implements Serializable {
    int flag;//代表是哪个model层
    String id;

    public int getFlag() {
        return flag;
    }

    public void setFlag(int flag) {
        this.flag = flag;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
