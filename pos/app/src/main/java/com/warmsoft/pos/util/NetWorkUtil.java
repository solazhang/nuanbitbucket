package com.warmsoft.pos.util;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.LineNumberReader;

public class NetWorkUtil {
    public static int WIFI = 1, CMWAP = 2, CMNET = 3, UNIWAP = 4, UNINET = 5, G3WAP = 6, G3NET = 7, CTWAP = 8,
            CTNET = 9, OTHER = 10;

    /**
     * @param context
     * @return 是否有可用网络
     */
    public static boolean isNetWorkAvailable(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo network = cm.getActiveNetworkInfo();
        if (network != null) {
            return network.isAvailable() && network.isConnected();
        }

        return false;
    }

    public static String getMacAddress(Context context) {
        String mac = "";
        try {
            WifiManager wifi = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
            WifiInfo info = wifi.getConnectionInfo();
            mac = info.getMacAddress();
        } catch (Exception e) {
        }

        if (mac == null || "".equals(mac)) {
            mac = getMacAddress();
        }

        return mac;
    }

    public static String getMacAddress() {
        String macSerial = "";
        String str = "";
        try {
            Process pp = Runtime.getRuntime().exec("cat /sys/class/net/wlan0/address ");
            InputStreamReader ir = new InputStreamReader(pp.getInputStream());
            LineNumberReader input = new LineNumberReader(ir);
            for (; null != str; ) {
                str = input.readLine();
                if (str != null) {
                    macSerial = str.trim();// 去空格
                    break;
                }
            }
        } catch (IOException e) {
            // 赋予默认值
            // ex.printStackTrace();
        }

        return macSerial;
    }

    /**
     * 获取当前的网络状态 -1：没有网络 1：WIFI网络 2：cmwap网络 3：cmnet网络 4:uniwap 5:3gwap 6:ctwap
     *
     * @param context
     * @return
     */
    public static int getAPNType(Context context) {
        int netType = -1;
        try {
            ConnectivityManager connMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();

            if (networkInfo == null) {
                return netType;
            }

            int nType = networkInfo.getType();
            if (nType == ConnectivityManager.TYPE_MOBILE) {
                Trace.e("networkInfo.getExtraInfo()", "networkInfo.getExtraInfo() is " + networkInfo.getExtraInfo());
                String extraInfoString = networkInfo.getExtraInfo().toLowerCase();
                if (extraInfoString.equals("cmnet")) {
                    netType = CMNET;
                } else if (extraInfoString.equals("cmwap")) {
                    netType = CMWAP;
                } else if (extraInfoString.equals("uniwap")) {
                    netType = UNIWAP;
                } else if (extraInfoString.equals("uninet")) {
                    netType = UNINET;
                } else if (extraInfoString.equals("3gwap")) {
                    netType = G3WAP;
                } else if (extraInfoString.equals("3gnet")) {
                    netType = G3NET;
                } else if (extraInfoString.equals("ctwap")) {
                    netType = CTWAP;
                } else if (extraInfoString.equals("ctnet")) {
                    netType = CTNET;
                } else {
                    netType = OTHER;
                }
            } else if (nType == ConnectivityManager.TYPE_WIFI) {
                netType = WIFI;
            }
        } catch (Exception e) {
        }

        return netType;
    }
}
