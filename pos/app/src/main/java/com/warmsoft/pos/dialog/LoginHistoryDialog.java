package com.warmsoft.pos.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.warmsoft.pos.R;
import com.warmsoft.pos.adapter.LoginHistoryAdapter;
import com.warmsoft.pos.layout.view.LayoutActivityLogin;
import com.warmsoft.pos.litepal.data.UserLoginData;

import org.litepal.crud.DataSupport;

import java.util.ArrayList;
import java.util.List;

/**
 * 作者: lijinliu on 2016/8/5.
 * 邮箱：jinliu.li@warmsoft.com
 * 描述: 历史登录用户
 */
public class LoginHistoryDialog implements View.OnClickListener {

    private Dialog mDialog;
    private Activity mActivity;
    private LayoutInflater mInflater;

    private ListView listView;
    private LoginHistoryAdapter adapter;
    private List<UserLoginData> datas = new ArrayList<UserLoginData>();

    private Button btnClose;
    protected String loginName = "";

    protected LayoutActivityLogin layoutLogin;

    public LoginHistoryDialog(Activity activity, String loginName, LayoutActivityLogin layoutLogin) {
        this.mActivity = activity;
        this.mInflater = LayoutInflater.from(mActivity);
        this.loginName = loginName;
        this.layoutLogin = layoutLogin;
        createLoadingDialog(mActivity);
    }

    /**
     * 得到自定义的progressDialog
     *
     * @param context
     * @return
     */
    private void createLoadingDialog(Context context) {
        View rootView = mInflater.inflate(R.layout.dialog_login_history, null);
        LinearLayout layout = (LinearLayout) rootView.findViewById(R.id.dialog_view);
        initView(rootView);
        mDialog = new Dialog(context, R.style.fullScreenDialog);
        mDialog.setCancelable(true);
        int MATH_PARENT = LinearLayout.LayoutParams.MATCH_PARENT;
        mDialog.setContentView(layout, new LinearLayout.LayoutParams(MATH_PARENT, MATH_PARENT));// 设置布局
        mDialog.show();
    }

    private void initView(View rootView) {
        btnClose = (Button) rootView.findViewById(R.id.btn_close);
        btnClose.setOnClickListener(this);
        listView = (ListView) rootView.findViewById(R.id.listview_loginuser);
        datas = DataSupport.findAll(UserLoginData.class);
        adapter = new LoginHistoryAdapter(mActivity, datas, loginName);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                if (datas != null && i < datas.size()) {
                    layoutLogin.initLoginUser(datas.get(i));
                    dismiss();
                }
            }
        });
    }

    @Override
    public void onClick(View view) {
        if (view == btnClose) {
            dismiss();
        }
    }


    public void dismiss() {
        if (mDialog != null) {
            mDialog.dismiss();
            mDialog = null;
        }
    }
}
