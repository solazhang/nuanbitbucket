package com.warmsoft.pos.dialog;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.hardware.usb.UsbManager;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.print.demo.IPrinterOpertion;
import com.android.print.demo.UsbOperation;
import com.android.print.demo.utils.FileUtils;
import com.android.print.demo.utils.PrintUtils;
import com.android.print.sdk.PrinterConstants;
import com.android.print.sdk.PrinterInstance;
import com.warmsoft.pos.MainActivity;
import com.warmsoft.pos.R;

/**
 * 作者: lijinliu on 2016/8/5.
 * 邮箱：jinliu.li@warmsoft.com
 * 描述: 用户注销
 */
public class CancellationDialog implements View.OnClickListener {

    private Dialog mDialog;
    private MainActivity mActivity;
    private LayoutInflater mInflater;

    private TextView btnCancle;
    private TextView btnConfirm;

    public CancellationDialog(MainActivity activity) {
        this.mActivity = activity;
        this.mInflater = LayoutInflater.from(mActivity);
        createLoadingDialog(mActivity);
    }

    /**
     * 得到自定义的progressDialog
     *
     * @param context
     * @return
     */
    private void createLoadingDialog(Context context) {
        View rootView = mInflater.inflate(R.layout.dialog_exit_warmpos, null);
        LinearLayout layout = (LinearLayout) rootView.findViewById(R.id.dialog_view);
        initView(rootView);
        mDialog = new Dialog(context, R.style.fullScreenDialog);
        mDialog.setCancelable(true);
        int MATH_PARENT = LinearLayout.LayoutParams.MATCH_PARENT;
        mDialog.setContentView(layout, new LinearLayout.LayoutParams(MATH_PARENT, MATH_PARENT));// 设置布局
        mDialog.show();
    }

    private void initView(View rootView) {
        btnConfirm = (TextView) rootView.findViewById(R.id.btn_confirm);
        btnCancle = (TextView) rootView.findViewById(R.id.btn_cancle);
        btnConfirm.setOnClickListener(this);
        btnCancle.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        if (view == btnCancle) {
            dismiss();
        } else if (view == btnConfirm) {
            mDialog.dismiss();
            mActivity.exitWarmPos();
        }
    }


    public void dismiss() {
        if (mDialog != null) {
            mDialog.dismiss();
            mDialog = null;
        }
    }
}
