package com.warmsoft.pos.adapter.entiy;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.warmsoft.pos.R;
import com.warmsoft.pos.litepal.data.PayAmountData;
import com.warmsoft.pos.util.Util;

import java.util.List;

/**
 * 作者: lijinliu on 2016/8/9.
 * 邮箱：jinliu.li@warmsoft.com
 * 描述: 支付信息
 */
public class PayInfoAdapter extends BaseAdapter {
    protected Context mContext;
    protected List<PayAmountData> datas;
    protected int mSelected = 0;

    protected int drawableSure;
    protected int drawableUnSure;

    public PayInfoAdapter(Context context, List<PayAmountData> datas) {
        mContext = context;
        drawableSure = R.mipmap.warmpos_sure;
        drawableUnSure = R.mipmap.warmsoft_order_point;
        this.datas = datas;
    }

    public void setData(List<PayAmountData> datas) {
        this.datas = datas;
        this.notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        if (datas == null) {
            return 0;
        } else {
            return datas.size();
        }
    }

    @Override
    public Object getItem(int position) {
        if (datas == null) {
            return null;
        } else {
            return datas.get(position);
        }
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @SuppressLint("ViewHolder")
    @Override
    public View getView(int position, View view, ViewGroup parent) {
        ViewHolder holde;
        if (view == null) {
            view = LayoutInflater.from(mContext).inflate(R.layout.item_order_payinfo, null);
            holde = new ViewHolder(view);
        } else {
            holde = (ViewHolder) view.getTag();
            if (holde == null) {
                view = LayoutInflater.from(mContext).inflate(R.layout.item_order_payinfo, null);
                holde = new ViewHolder(view);
            }
        }
        if (holde != null) {
            holde.bindData((PayAmountData) getItem(position));
        }

        return view;
    }

    class ViewHolder {
        TextView tvPayChanelName;
        TextView tvPayAmount;

        ViewHolder(View view) {
            tvPayChanelName = (TextView) view.findViewById(R.id.tv_pay_chanel_name);
            tvPayAmount = (TextView) view.findViewById(R.id.tv_pay_amount);
            view.setTag(this);
        }

        void bindData(final PayAmountData data) {
            if (data == null) {
                return;
            } else {
                tvPayChanelName.setText(data.getPayChanelName() + "");
                tvPayAmount.setText(Util.subFloat(data.getPayAmount()) + "");
            }
        }
    }
}
