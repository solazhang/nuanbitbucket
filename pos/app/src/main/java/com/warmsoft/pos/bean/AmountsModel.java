package com.warmsoft.pos.bean;

import java.io.Serializable;

/**
 * Created by brooks on 16/9/26.
 */
public class AmountsModel implements Serializable {
    int code;
    String message;
    DataInfo Data;

    public static class DataInfo implements Serializable {
        int customerid;
        double Account;
        double AccountGift;
        double MemberCard;
        double MemberCardGift;
        double PrePay;
        double PreCards;
        int Score;

        public int getCustomerid() {
            return customerid;
        }

        public void setCustomerid(int customerid) {
            this.customerid = customerid;
        }

        public double getAccount() {
            return Account;
        }

        public void setAccount(double account) {
            Account = account;
        }

        public double getMemberCard() {
            return MemberCard;
        }

        public void setMemberCard(double memberCard) {
            MemberCard = memberCard;
        }

        public double getPrePay() {
            return PrePay;
        }

        public void setPrePay(double prePay) {
            PrePay = prePay;
        }

        public double getAccountGift() {
            return AccountGift;
        }

        public void setAccountGift(double accountGift) {
            AccountGift = accountGift;
        }

        public double getMemberCardGift() {
            return MemberCardGift;
        }

        public void setMemberCardGift(double memberCardGift) {
            MemberCardGift = memberCardGift;
        }

        public double getPreCards() {
            return PreCards;
        }

        public void setPreCards(double preCards) {
            PreCards = preCards;
        }

        public int getScore() {
            return Score;
        }

        public void setScore(int score) {
            Score = score;
        }
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataInfo getData() {
        return Data;
    }

    public void setData(DataInfo data) {
        Data = data;
    }


}
