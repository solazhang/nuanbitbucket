package com.warmsoft.pos.support.listview.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.TextView;

import com.warmsoft.pos.R;
import com.warmsoft.pos.layout.view.LayoutFragmentPos;
import com.warmsoft.pos.litepal.data.CatgoryItemInfo;
import com.warmsoft.pos.litepal.data.GoodItemInfo;
import com.warmsoft.pos.support.listview.Model;
import com.warmsoft.pos.support.listview.adapter.GridViewAdapter;
import com.warmsoft.pos.support.listview.bean.Type;

import java.util.ArrayList;
import java.util.List;

public class ProTypeFragment extends Fragment {

    private ItemClickListener mItemClickListener;
    private TextClickListener mTextClickListener;

    private ArrayList<Type> list;
    private GridView gridView;
    private GridViewAdapter adapter;
    private Type type;
    private String typename;
    private int icon;

    int index;
    private int tag = 0;
    private TextView textRight;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_pro_type, null);
        gridView = (GridView) view.findViewById(R.id.listView);
        textRight = (TextView) view.findViewById(R.id.topright);
        index = getArguments().getInt("index");
        tag = getArguments().getInt("tag");
        typename = LayoutFragmentPos.list[index];
        icon = Model.iconList[index];

        ((TextView) view.findViewById(R.id.toptype)).setText(typename);
        GetTypeList(LayoutFragmentPos.goodsList);
        adapter = new GridViewAdapter(getActivity(), list);
        gridView.setAdapter(adapter);

        gridView.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
                if (mItemClickListener != null && arg2 < list.size()) {
                    mItemClickListener.productClick(index, arg2);

                    list.get(arg2).setSelect(list.get(arg2).getSelect() + 1);

                    updateGoodInfo();
                }
            }
        });

        textRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mTextClickListener != null){
                    mTextClickListener.viewClick();
                }

            }
        });
        initEvents();
        return view;
    }

    private void updateGoodInfo() {
        ArrayList<Type> types = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            types.add(list.get(i));
        }

        adapter.update(types);
        adapter.notifyDataSetChanged();
    }

    private void GetTypeList(List<CatgoryItemInfo> dataInfoList) {
        if (dataInfoList.size() <= 0) {
            return;
        }

        list = new ArrayList<>();

        List<GoodItemInfo> info = dataInfoList.get(index).getInfo();
        for (int i = 0; i < info.size(); i++) {
            type = new Type(i, info.get(i).getDrugsName(), icon, info.get(i).getChoose());
            list.add(type);
        }
    }

    public void setItemClickListener(ItemClickListener listener) {
        mItemClickListener = listener;
    }

    public void setTextClickListener(TextClickListener listener) {
        mTextClickListener = listener;
    }

    private void initEvents() {
    }


    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    public interface ItemClickListener {
        void productClick(int index, int position);
    }

    public interface TextClickListener{
        void viewClick();
    }
}
