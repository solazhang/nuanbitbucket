package com.warmsoft.pos.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.warmsoft.pos.R;
import com.warmsoft.pos.litepal.data.UserLoginData;

import java.util.List;

/**
 * 作者: lijinliu on 2016/8/9.
 * 邮箱：jinliu.li@warmsoft.com
 * 描述: 开单时弹出窗中会员列表
 */
public class LoginHistoryAdapter extends BaseAdapter {
    protected Context mContext;
    protected List<UserLoginData> datas;
    protected String loginName = "";

    public LoginHistoryAdapter(Context context, List<UserLoginData> datas,String loginName) {
        mContext = context;
        this.datas = datas;
        this.loginName = loginName;
    }

    public void setData(List<UserLoginData> datas) {
        this.datas = datas;
        this.notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        if (datas == null) {
            return 0;
        } else {
            return datas.size();
        }
    }

    @Override
    public Object getItem(int position) {
        if (datas == null) {
            return null;
        } else {
            return datas.get(position);
        }
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @SuppressLint("ViewHolder")
    @Override
    public View getView(int position, View view, ViewGroup parent) {
        ViewHolder holde;
        if (view == null) {
            view = LayoutInflater.from(mContext).inflate(R.layout.item_login_history, null);
            holde = new ViewHolder(view);
        } else {
            holde = (ViewHolder) view.getTag();
            if (holde == null) {
                view = LayoutInflater.from(mContext).inflate(R.layout.item_login_history, null);
                holde = new ViewHolder(view);
            }
        }
        if (holde != null) {
            UserLoginData item = (UserLoginData) getItem(position);
            holde.bindData(item);
        }

        return view;
    }

    class ViewHolder {
        TextView tvName;
        ImageView ivIsNowLogin;

        ViewHolder(View view) {
            tvName = (TextView) view.findViewById(R.id.tv_name);
            ivIsNowLogin = (ImageView) view.findViewById(R.id.iv_isnow_loging);
            view.setTag(this);
        }

        void bindData(final UserLoginData data) {
            if (data == null) {
                return;
            } else {
                tvName.setText(data.getName() + "");
                ivIsNowLogin.setVisibility(View.GONE);
                if (data.getLoginName().equals(loginName)) {
                    ivIsNowLogin.setVisibility(View.VISIBLE);
                }
            }
        }
    }
}
