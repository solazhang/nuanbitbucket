package com.warmsoft.pos.util;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.Iterator;
import java.util.Map;

public class PrefUtil {
    public static final String oath_token_key = "oath_token_key";
    public static final String oath_token_value = "oath_token_value";

    public static final String oath_user_info = "oath_user_info";
    public static final String oath_user_info_value = "oath_user_info_value";

    public static final String oath_anonycustom_info = "oath_anonycustom_info";
    public static final String oath_anonygustom_info_value = "oath_anonycustom_info_value";

    public static final String goods_key = "goods_key";
    public static final String goods_key_value = "goods_key_value";

    public static final String customer_key = "customer_key";
    public static final String customer_key_ts_value = "customer_key_ts_value";
    public static final String customer_pet_ts_key_value = "customer_pet_ts_key_value";

    public static void savePreferenceByItem(Context ctx, String spName, String itemKey, String itemValue) {
        SharedPreferences sp = ctx.getSharedPreferences(spName, Context.MODE_PRIVATE);
        // 获取编辑的对象
        SharedPreferences.Editor editor = sp.edit();
        // 添加数据
        editor.putString(itemKey, itemValue);
        // 提交
        editor.commit();
    }

    public static void savePreferenceByMap(Context ctx, String pName, Map<String, String> userMap) {
        SharedPreferences sp = ctx.getSharedPreferences(pName, Context.MODE_PRIVATE);
        // 获取编辑的对象
        SharedPreferences.Editor editor = sp.edit();
        // 添加数据

        Iterator iter = userMap.keySet().iterator();
        while (iter.hasNext()) {
            String key = (String) iter.next();
            String val = (String) userMap.get(key);

            editor.putString(key, val);
        }
        // 提交
        editor.commit();
    }

    public static String getSharedPreference(Context ctx, String spName, String spKey) {
        SharedPreferences sp = ctx.getSharedPreferences(spName, 0);
        String spValue = sp.getString(spKey, "");
        return spValue;
    }

    public static String getSharedPreference(Context ctx, String spName, String spKey, String def) {
        SharedPreferences sp = ctx.getSharedPreferences(spName, 0);
        String spValue = sp.getString(spKey, def);
        return spValue;
    }
}
