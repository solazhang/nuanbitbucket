package com.warmsoft.pos.util;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.provider.Settings.Secure;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.telephony.TelephonyManager;

import java.lang.reflect.Method;
import java.util.Locale;

/**
 * 作者: lijinliu on 2016/8/5.
 * 邮箱：jinliu.li@warmsoft.com
 * 描述: 手机基础信息
 */
public class PhoneInfo {
    private static final String logTag = "PhoneInfo:";

    int CAMERA_PERMISSION = 8888;
    int MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE=0;

    private int mSdkInt;
    private String mSdkRelease;

    private String mLanguage;
    private String mImei;

    private String mResolution;

    private String mModel;
    private String mBuildnumber;
    private String mAndroidId;

    private static PhoneInfo mPhoneInfo;

    public static PhoneInfo getInstance(Context context) {
        if (mPhoneInfo == null) {
            mPhoneInfo = new PhoneInfo(context);
        }
        return mPhoneInfo;
    }

    private PhoneInfo(Context context) {
        TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);

        try {
            mSdkInt = android.os.Build.VERSION.SDK_INT;
            mSdkRelease = android.os.Build.VERSION.RELEASE;
            mLanguage = Locale.getDefault().getLanguage() + "_" + Locale.getDefault().getCountry();
        } catch (Exception e) {
            Trace.e(logTag, "get sdkRelease info error", e);
        }

        try {

            if (ContextCompat.checkSelfPermission(context, Manifest.permission.READ_PHONE_STATE)
                    != PackageManager.PERMISSION_GRANTED) {
//                ActivityCompat.requestPermissions((Activity) context,
//                        new String[]{Manifest.permission.CAMERA}, CAMERA_PERMISSION);
            } else {
                mImei = tm.getDeviceId();
            }

            if (Util.isNullStr(mImei)) {
                mImei = "123443211234432";
            }
        } catch (Exception e) {
            Trace.e(logTag, "get Imei info error", e);
        }

        setDeviceVersionInfo();

        try {
            mResolution = getScreenResolution(context);
        } catch (Exception e) {
            Trace.e(logTag, "get Resolution info error", e);
        }

        try {
            mAndroidId = Secure.getString(context.getContentResolver(), Secure.ANDROID_ID);
            if (Util.isNullStr(mAndroidId)) {
                mAndroidId = "0000dddd666e549c";
            }
        } catch (Exception e) {
            Trace.e(logTag, "get AndroidId info error", e);
        }
    }

    private void setDeviceVersionInfo() {
        try {
            Object object = new Object();
            Method get = Class.forName("android.os.SystemProperties").getMethod("get", String.class);
            mModel = (String) get.invoke(object, "ro.product.model");
            mBuildnumber = (String) get.invoke(object, "ro.build.display.id");
        } catch (Exception e) {
            Trace.e(logTag, "get DeviceVersion Info error", e);
        }
    }

    public static String getScreenResolution(Context context) {
        int screenWidth = context.getResources().getDisplayMetrics().widthPixels;
        int screenHeight = context.getResources().getDisplayMetrics().heightPixels;

        if (screenWidth > screenHeight) {
            int tmp = screenWidth;
            screenWidth = screenHeight;
            screenHeight = tmp;
        }
        return String.valueOf(screenWidth) + "x" + String.valueOf(screenHeight);
    }

    public int getmSdkInt() {
        return mSdkInt;
    }

    public String getmSdkRelease() {
        return mSdkRelease;
    }

    public String getmLanguage() {
        return mLanguage;
    }

    public String getmImei() {
        return mImei;
    }

    public String getmResolution() {
        return mResolution;
    }

    public String getmModel() {
        return mModel;
    }

    public String getmBuildnumber() {
        return mBuildnumber;
    }

    public String getmAndroidId() {
        return mAndroidId;
    }

    public static PhoneInfo getmPhoneInfo() {
        return mPhoneInfo;
    }

    public String getPhoneInfo() {
        StringBuffer sb = new StringBuffer();
        sb.append(mSdkInt);
        sb.append(",");
        sb.append(mSdkRelease);
        sb.append(",");
        sb.append(mLanguage);
        sb.append(",");
        sb.append(mImei);
        sb.append(",");
        sb.append(mResolution);
        sb.append(",");
        sb.append(mModel);
        sb.append(",");
        sb.append(mBuildnumber);
        sb.append(",");
        sb.append(mAndroidId);

        return sb.toString();
    }
}
