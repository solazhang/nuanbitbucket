package com.warmsoft.pos.bean;

import com.warmsoft.pos.litepal.data.CatgoryItemInfo;

import org.litepal.crud.DataSupport;

import java.io.Serializable;
import java.util.List;

/**
 * Created by brooks on 16/1/26.
 */
public class GoodDataModel extends DataSupport implements Serializable {
    int code;
    String message;

    Data Data;

    public static class Data implements Serializable {
        List<CatgoryItemInfo> dataInfo;

        public List<CatgoryItemInfo> getDataInfo() {
            return dataInfo;
        }

        public void setDataInfo(List<CatgoryItemInfo> dataInfo) {
            this.dataInfo = dataInfo;
        }
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public GoodDataModel.Data getData() {
        return Data;
    }

    public void setData(GoodDataModel.Data data) {
        Data = data;
    }
}
