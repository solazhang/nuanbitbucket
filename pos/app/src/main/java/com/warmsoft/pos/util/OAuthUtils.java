package com.warmsoft.pos.util;

import android.content.Context;
import android.text.TextUtils;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.warmsoft.pos.bean.AuthModel;

/**
 * Created by brooks on 16/6/14.
 */
public class OAuthUtils {

    private static OAuthUtils instance = null;

    public static OAuthUtils getInstance() {
        if (instance == null) {
            synchronized (OAuthUtils.class) {
                instance = new OAuthUtils();
            }
        }

        return instance;
    }

    public static String token = "";

    public String getToken(Context mContext) {
        if (!TextUtils.isEmpty(token)) {
            return token;
        }

        token = PrefUtil.getSharedPreference(mContext, PrefUtil.oath_token_key, PrefUtil.oath_token_value);
        if (!TextUtils.isEmpty(token)) {
            return token;
        }

        return token;
    }

    public void setToken(Context mContext, String t) {
        token = t;
        PrefUtil.savePreferenceByItem(mContext, PrefUtil.oath_token_key, PrefUtil.oath_token_value, t);
    }

    public void cleanToken(Context mContext) {
        setToken(mContext, "");
        PrefUtil.savePreferenceByItem(mContext, PrefUtil.oath_token_key, PrefUtil.oath_token_value, "");
    }

    public static AuthModel.DataInfo.UserInfo userInfo = null;
    public static AuthModel.DataInfo.AnonymousCustomerInfo anonyCustomInfo = null;

    public AuthModel.DataInfo.UserInfo getUserinfo(Context mContext) {
        if (userInfo != null) {
            return userInfo;
        }

        String json = PrefUtil.getSharedPreference(mContext, PrefUtil.oath_user_info, PrefUtil.oath_user_info_value);
        if (!TextUtils.isEmpty(json)) {
            Gson gson = new Gson();
            userInfo = gson.fromJson(json, new TypeToken<AuthModel.DataInfo.UserInfo>() {
            }.getType());
            return userInfo;
        }

        return userInfo;
    }

    public void setUserinfo(Context mContext, AuthModel.DataInfo.UserInfo info) {
        OAuthUtils.userInfo = info;
        PrefUtil.savePreferenceByItem(mContext, PrefUtil.oath_user_info, PrefUtil.oath_user_info_value, new Gson().toJson(info));
    }

    public AuthModel.DataInfo.AnonymousCustomerInfo getAnonyCustomInfo(Context mContext) {
        if (anonyCustomInfo != null) {
            return anonyCustomInfo;
        }

        String json = PrefUtil.getSharedPreference(mContext, PrefUtil.oath_anonycustom_info, PrefUtil.oath_anonygustom_info_value);
        if (!TextUtils.isEmpty(json)) {
            Gson gson = new Gson();
            anonyCustomInfo = gson.fromJson(json, new TypeToken<AuthModel.DataInfo.AnonymousCustomerInfo>() {
            }.getType());
            return anonyCustomInfo;
        }

        return anonyCustomInfo;
    }

    public void setAnonyCustomInfo(Context mContext, AuthModel.DataInfo.AnonymousCustomerInfo info) {
        OAuthUtils.anonyCustomInfo = info;
        PrefUtil.savePreferenceByItem(mContext, PrefUtil.oath_anonycustom_info, PrefUtil.oath_anonygustom_info_value, new Gson().toJson(info));
    }

    public void cleanUserinfo(Context mContext) {
    }

    public void cleanAll(Context mContext) {
        cleanToken(mContext);
        cleanUserinfo(mContext);
    }
}
