package com.warmsoft.pos.net;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.widget.Toast;

import com.warmsoft.pos.service.SyncDataService;

/**
 * Created by brooks on 16/8/23.
 */
public class NetMonitor extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo wifiNetInfo = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

        if (wifiNetInfo.isConnected()) { //wifi连接
            Toast.makeText(context, "Wifi is OK! ", Toast.LENGTH_SHORT).show();

            //启动服务
            Intent service = new Intent(context, SyncDataService.class);
            context.startService(service);
        } else {
            Toast.makeText(context, "Wifi is disconnected ", Toast.LENGTH_SHORT).show();

            //关闭服务
            Intent service = new Intent(context, SyncDataService.class);
            context.stopService(service);
        }
    }
}