package com.warmsoft.pos.fragment.main;

import android.content.ContentValues;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.warmsoft.base.BaseFragment;
import com.warmsoft.pos.R;
import com.warmsoft.pos.bean.AuthModel;
import com.warmsoft.pos.dialog.NewOrderPetSelectDialog;
import com.warmsoft.pos.dialog.NomelNoteDialog;
import com.warmsoft.pos.enumutil.OrderStatusEnum;
import com.warmsoft.pos.layout.view.LayoutFragmentPos;
import com.warmsoft.pos.layout.view.LayoutOrderGood;
import com.warmsoft.pos.layout.view.LayoutPosKeyBoard;
import com.warmsoft.pos.layout.view.LayoutPosOrder;
import com.warmsoft.pos.layout.view.LayoutSales;
import com.warmsoft.pos.layout.view.LayoutSettleMain;
import com.warmsoft.pos.listener.FmPosLitener;
import com.warmsoft.pos.litepal.data.CustomerList;
import com.warmsoft.pos.litepal.data.LitepalHelper;
import com.warmsoft.pos.litepal.data.OrderGoodsData;
import com.warmsoft.pos.litepal.data.OrderInfoData;
import com.warmsoft.pos.litepal.data.PayAmountData;
import com.warmsoft.pos.litepal.data.PetData;
import com.warmsoft.pos.print.PrintUtil;
import com.warmsoft.pos.print.SunMiPrintUtils;
import com.warmsoft.pos.util.OAuthUtils;
import com.warmsoft.pos.util.OrderHelper;

import org.greenrobot.eventbus.EventBus;
import org.litepal.crud.DataSupport;

import java.util.List;

public class FragmentPos extends BaseFragment implements FmPosLitener {
    private OrderHelper mOrderHelp;

    public LayoutFragmentPos mLayoutMainPos;
    public LayoutPosOrder mLayoutOrder;
    public LayoutPosKeyBoard mLayoutKeyBoard;
    public LayoutSales mLayoutSales;
    public LayoutOrderGood mLayoutOrderGood;
    public LayoutSettleMain mLayoutSettleMain;
    public static int isNewCustomId = 0;

    public static FragmentPos newInstance() {
        FragmentPos fragment = new FragmentPos();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (mLayoutView == null) {
            mLayoutView = inflater.inflate(R.layout.fragment_main_pos, container, false);
            initViews();
            initPrint();
        } else {
            ViewGroup viewGroup = (ViewGroup) mLayoutView.getParent();
            if (viewGroup != null) {
                viewGroup.removeView(mLayoutView);
            }
        }
        return mLayoutView;
    }

    private void initViews() {
        mLayoutMainPos = new LayoutFragmentPos(mLayoutView, mActivity, this);
        mLayoutOrder = new LayoutPosOrder(mLayoutView, mActivity, this);
        mLayoutKeyBoard = new LayoutPosKeyBoard(mLayoutView, mActivity, this);
        mLayoutSales = new LayoutSales(mLayoutView, mActivity, this);
        mLayoutOrderGood = new LayoutOrderGood(mLayoutView, mActivity, this);
        mLayoutSettleMain = new LayoutSettleMain(mLayoutView, mActivity, this);
    }

    private void initPrint() {
        //初始化打印机
//        PrintUtil.getInstance().initPrinter(getActivity());
        SunMiPrintUtils.getInstance().initPrinter();
    }

    //付款确认
    @Override
    public void payConfirm(String orderNo) {
        mLayoutOrder.refershList(orderNo);
    }

    //代付款点击
    @Override
    public void settleListClick(OrderInfoData orderData) {
        mLayoutKeyBoard.refershMemberInfo(orderData.getCustomerId());
        mLayoutOrder.setOrder(orderData);
        initData();
    }

    //清空支付
    @Override
    public void clearAllPays() {
        mOrderHelp = OrderHelper.getInstance(mContext);
        if (mOrderHelp.getOrderStaus() == OrderStatusEnum.HandIn) {
            mOrderHelp.clearAllPays();
            mLayoutOrder.setOrder(mOrderHelp.getOrderData());
            initData();
        } else {
            new NomelNoteDialog(mActivity, "清空支付", "定单已结算或退单!", "", null);
        }
    }

    //删除某笔支付
    @Override
    public void clearOnePays(PayAmountData data) {
        mOrderHelp = OrderHelper.getInstance(mContext);
        if (mOrderHelp.getOrderStaus() == OrderStatusEnum.HandIn) {
            mOrderHelp = OrderHelper.getInstance(mContext);
            mOrderHelp.clearOnePays(data);
            mLayoutOrder.setOrder(mOrderHelp.getOrderData());
            initData();
        } else {
            new NomelNoteDialog(mActivity, "清空支付", "定单已结算或退单!", "", null);
        }
    }

    /**
     * 定单商品列表点击
     *
     * @param goodInfo
     */
    public void orderGoodClick(OrderGoodsData goodInfo) {
        mLayoutMainPos.orderGoodClick();
        mLayoutOrderGood.setData(goodInfo);
    }

    /**
     * 隐藏定单商品列表信息
     */
    public void closeGoodView() {
        mLayoutMainPos.closeGoodView();
        mLayoutOrder.closeSelect();
    }


    //商品数量修改，负减正加
    public void setGoodSize(OrderGoodsData goodInfo, float newSize, boolean isClear) {
        mOrderHelp = OrderHelper.getInstance(mContext);
        mOrderHelp.setGoodSize(goodInfo, newSize);
        mLayoutOrder.refershInfo();
        mLayoutOrder.setOrderStatusImg();
        if (isClear) {
            closeGoodView();
        }
        mLayoutKeyBoard.setOrderSize(mOrderHelp.getOrderSize());
        mLayoutMainPos.updateBadView();
    }


    /**
     * 刷会员信息
     */
    @Override
    public void swingMember() {
        mOrderHelp = OrderHelper.getInstance(mContext);
        if (OrderStatusEnum.Bill == mOrderHelp.getOrderStaus()) {
            new NewOrderPetSelectDialog(mActivity, new NewOrderPetSelectDialog.MemberDialogListener() {
                @Override
                public void confirm(CustomerList customer, PetData pet) {
                    refershMemberInfo(customer, pet);
                }
            });
        } else {
            new NomelNoteDialog(mActivity, "无法更改会员", "已下单或者已结算，无法更换会员", "确定", null);
        }
    }

    public void refershMemberInfo(CustomerList customer, PetData pet) {
        mLayoutOrder.refershMemberInfo(mOrderHelp.addMemberForOrder(customer, pet));
        mLayoutKeyBoard.refershMemberInfo(Integer.parseInt(customer.getCustomerId()));
        mLayoutSettleMain.refershMemberInfo(Integer.parseInt(customer.getCustomerId()));
    }

    //下单成功监听
    public void addOrderSucc() {
        OrderInfoData orderInfoData = OrderHelper.getInstance(mContext).getOrderData();
        if (orderInfoData != null) {
            mLayoutOrder.setNeedPayAmount(orderInfoData.getCountAmount());
            mLayoutKeyBoard.setNeedPayAmount(orderInfoData.getCountAmount());
            mLayoutSettleMain.initData();
        }
    }

    //开单
    @Override
    public void newOrder() {
        mLayoutOrder.newOrder(true, true);
        initData();
    }

    //结算成功
    public void settleOver() {
        mOrderHelp = OrderHelper.getInstance(mContext);
        OrderInfoData nowOrder = OrderHelper.getInstance(mContext).getOrderData();
        List<OrderInfoData> orderList = LitepalHelper.getInstance().getOrderListByCustomId(nowOrder.getCustomerId(), nowOrder.getOrderNo());
        if (orderList != null && orderList.size() > 0) {//结算该会员下一单
            settleListClick(orderList.get(0));
        } else {//无该会员待付款，重新开单
            newOrder();
        }
    }

    //记账
    @Override
    public void keepingAccount() {
        mLayoutOrder.setOrderStatusImg();
    }

    public void initData() {
        mOrderHelp = OrderHelper.getInstance(mContext);
        OrderInfoData orderInfoData = OrderHelper.getInstance(mContext).getOrderData();
        if (orderInfoData == null) {
            orderInfoData = OrderHelper.getInstance(mContext).createNewOrder();
        }
        mLayoutOrder.refershList(orderInfoData.getOrderNo() + "");
        mLayoutOrder.setNeedPayAmount(mOrderHelp.getNeedAmount());
        mLayoutKeyBoard.initData();
        mLayoutSettleMain.initData();
        mLayoutMainPos.updateBadView();
        mLayoutMainPos.showOrderBadgeView();
    }

    //新增加会员
    public void isNewMember() {
        List<CustomerList> customList = DataSupport.where(" isNew=? ", "1").find(CustomerList.class);
        if (customList == null || customList.size() <= 0) {
            if(isNewCustomId>0){
                customList = DataSupport.where(" CustomerID=? ", isNewCustomId+"").find(CustomerList.class);
                isNewCustomId = 0;
            }
        }
        if (customList != null && customList.size() > 0) {
            CustomerList custom = customList.get(customList.size() - 1);
            ContentValues values = new ContentValues();
            values.put("isNew", 0);
            DataSupport.updateAll(CustomerList.class, values, "isNew = ?", "1");

            List<PetData> petDatas = DataSupport.where(" CustomerID=? ", custom.getCustomerId() + "").find(PetData.class);
            if (petDatas != null && petDatas.size() > 0) {
                PetData pet = petDatas.get(petDatas.size() - 1);
                mOrderHelp = OrderHelper.getInstance(mContext);
                if (mOrderHelp.getOrderStaus() == OrderStatusEnum.Bill) {
                    refershMemberInfo(custom, pet);
                } else {
                    newOrder();
                    refershMemberInfo(custom, pet);
                }
            }
        }
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
        resumeHandle.sendEmptyMessageDelayed(0, 1500);
    }

    Handler resumeHandle = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            mOrderHelp = OrderHelper.getInstance(mContext);
            if (mOrderHelp.getOrderData() != null) {
                AuthModel.DataInfo.UserInfo userInfo = OAuthUtils.getInstance().getUserinfo(mContext);
                if (mOrderHelp.getOrderData().getOpId() != userInfo.getId()) {
                    newOrder();
                } else {
                    mLayoutOrder.resumeOrder(mOrderHelp.getOrderData());
                    initData();
                }
            }
            mLayoutMainPos.updateBadView();
        }
    };

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onStop() {
        super.onStop();
    }
}
