package com.warmsoft.pos.util;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.util.TypedValue;
import android.view.Gravity;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.warmsoft.pos.R;

import java.lang.reflect.Field;
import java.math.BigDecimal;

/**
 * 作者: lijinliu on 2016/8/5.
 * 邮箱：jinliu.li@warmsoft.com
 * 描述: 工具类
 */
public class Util {

    /**
     * 空值判断
     *
     * @param str
     * @return
     */
    public static boolean isNullStr(String str) {
        if (str == null || "".equals(str.trim()) || "null".equals(str.trim())) {
            return true;
        }
        return false;
    }

    /**
     * EditText值获取
     *
     * @param et
     * @return
     */
    public static String getViewText(EditText et) {
        if (et != null) {
            return et.getText().toString();
        }
        return null;
    }

    /**
     * TextView值获取
     *
     * @param tv
     * @return
     */
    public static String getViewText(TextView tv) {
        if (tv != null) {
            return tv.getText().toString();
        }
        return null;
    }

    /**
     * DIP转px
     *
     * @param context
     * @param dp
     * @return
     */
    public static int dp2px(Context context, int dp) {
        return Math.round(TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP, dp, context.getResources()
                        .getDisplayMetrics()));
    }

    /**
     * 根据文件名获取图片资源
     *
     * @param pic
     * @return
     */
    public static int getMipmapImage(String pic) {
        if (isNullStr(pic)) {
            return R.mipmap.warmpos_logo;
        }
        Class mipmapClass = R.mipmap.class;
        try {
            Field field = mipmapClass.getDeclaredField(pic);
            return field.getInt(pic);
        } catch (SecurityException e) {
            return R.mipmap.warmpos_logo;
        } catch (NoSuchFieldException e) {
            return R.mipmap.warmpos_logo;
        } catch (IllegalArgumentException e) {
            return R.mipmap.warmpos_logo;
        } catch (IllegalAccessException e) {
            return R.mipmap.warmpos_logo;
        }
    }

    /**
     * 字符全角化，避免由于占位导致的排版混乱问题
     *
     * @param input
     * @return
     */
    public static String ToDBC(String input) {
        if ("null".equals(input) || "0".equals(input) || "0.0".equals(input)
                || "0.00".equals(input)) {
            return "";
        }
        char[] c = input.toCharArray();
        for (int i = 0; i < c.length; i++) {
            if (c[i] == 12288) {
                c[i] = (char) 32;
                continue;
            }
            if (c[i] > 65280 && c[i] < 65375)
                c[i] = (char) (c[i] - 65248);
        }
        return new String(c);
    }

    /**
     * manifest值获取
     *
     * @param name
     * @param context
     * @return
     */
    public static String getMeteData(String name, Context context) {
        ApplicationInfo appInfo;
        try {
            appInfo = context.getPackageManager().getApplicationInfo(context.getPackageName(), PackageManager.GET_META_DATA);
            String value = appInfo.metaData.getString(name);
            return value;
        } catch (NameNotFoundException e) {
            e.printStackTrace();
            Trace.e("Util.getMeteData() NameNotFoundException:");
            Trace.e(e);
        }
        return null;
    }

    public static void showToast(Context context, String msg) {
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
    }

    public static void showToastTop(Context context, String msg) {
        Toast toast = Toast.makeText(context, msg, Toast.LENGTH_LONG);
        toast.setGravity(Gravity.TOP, 0, 100);
        toast.show();
    }

    //验证金额正则
    public static boolean isNumber(String str) {
        java.util.regex.Pattern pattern = java.util.regex.Pattern.compile("^(([1-9]{1}\\d*)|([0]{1}))(\\.(\\d){0,2})?$"); // 判断小数点后一位的数字的正则表达式
        java.util.regex.Matcher match = pattern.matcher(str);
        if (match.matches() == false) {
            return false;
        } else {
            return true;
        }
    }

    //保留两位
    public static float subFloat(float f) {
        BigDecimal b = new BigDecimal(f);
        float f1 = b.setScale(2, BigDecimal.ROUND_HALF_UP).floatValue();
        return f1;
    }
}
