package com.warmsoft.pos.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.warmsoft.pos.R;
import com.warmsoft.pos.litepal.data.CustomerList;

import java.util.List;

/**
 * 作者: lijinliu on 2016/8/9.
 * 邮箱：jinliu.li@warmsoft.com
 * 描述: 开单时弹出窗中会员列表
 */
public class CustomerListAdapter extends BaseAdapter {
    protected Context mContext;
    protected List<CustomerList> datas;

    public CustomerListAdapter(Context context, List<CustomerList> datas) {
        mContext = context;
        this.datas = datas;
    }

    public void setData(List<CustomerList> datas) {
        this.datas = datas;
        this.notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        if (datas == null) {
            return 0;
        } else {
            return datas.size();
        }
    }

    @Override
    public Object getItem(int position) {
        if (datas == null) {
            return null;
        } else {
            return datas.get(position);
        }
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @SuppressLint("ViewHolder")
    @Override
    public View getView(int position, View view, ViewGroup parent) {
        ViewHolder holde;
        if (view == null) {
            view = LayoutInflater.from(mContext).inflate(R.layout.view_item_for_member_list, null);
            holde = new ViewHolder(view);
        } else {
            holde = (ViewHolder) view.getTag();
            if (holde == null) {
                view = LayoutInflater.from(mContext).inflate(R.layout.view_item_for_member_list, null);
                holde = new ViewHolder(view);
            }
        }
        if (holde != null) {
            CustomerList item = (CustomerList) getItem(position);
            holde.bindData(item);
        }

        return view;
    }

    class ViewHolder {
        TextView tvCardId;
        TextView tvMemberName;
        TextView tvMemberMobile;
        TextView tvMemberLevel;
        TextView tvMyInternal;
        LinearLayout llCustomerBackground;

        ViewHolder(View view) {
            tvCardId = (TextView) view.findViewById(R.id.id_for_member_id);
            tvMemberName = (TextView) view.findViewById(R.id.id_for_member_name);
            tvMemberMobile = (TextView) view.findViewById(R.id.id_for_member_mobile);
            tvMemberLevel = (TextView) view.findViewById(R.id.id_for_member_level);
            tvMyInternal = (TextView) view.findViewById(R.id.id_for_member_my_internal);
            llCustomerBackground = (LinearLayout) view.findViewById(R.id.recy_bg_id);
            view.setTag(this);
        }

        void bindData(final CustomerList data) {
            if (data == null) {
                return;
            } else {
                tvCardId.setText(String.valueOf(data.getCardNumber()) + "");
                tvMemberName.setText(String.valueOf(data.getName()) + "");
                tvMemberMobile.setText(String.valueOf(data.getCellPhone()) + "");
                tvMemberLevel.setText(String.valueOf(data.getCardTypeName()) + "");
                tvMyInternal.setText(String.valueOf(data.getScore()) + "");

                if (data.getIsSelect().equalsIgnoreCase("false")) {
                    int normalColor = mContext.getResources().getColor(R.color.transparent);
                    llCustomerBackground.setBackgroundColor(normalColor);
                } else {
                    int selectColor = mContext.getResources().getColor(R.color.dialog_goodsize_edit_color);
                    llCustomerBackground.setBackgroundColor(selectColor);
                }
            }
        }
    }
}
