package com.warmsoft.pos.util;

import android.content.Context;

import com.warmsoft.pos.bean.MemberInfoModel;
import com.warmsoft.pos.bean.PetInfoModel;
import com.warmsoft.pos.litepal.data.CardKind;
import com.warmsoft.pos.litepal.data.CatgoryItemInfo;
import com.warmsoft.pos.litepal.data.CustomerList;
import com.warmsoft.pos.litepal.data.GoodItemInfo;
import com.warmsoft.pos.litepal.data.LitepalHelper;
import com.warmsoft.pos.litepal.data.MemberTaskData;
import com.warmsoft.pos.litepal.data.PetCategory;

import org.litepal.crud.DataSupport;

import java.util.List;

/**
 * 作者: lijinliu on 2016/8/15.
 * 邮箱：jinliu.li@warmsoft.com
 * 描述: 定单操作类
 */
public class MemberUtil {
    private static MemberUtil memberUtil;

    private LitepalHelper mLitepal;
    private Context mContext;
    private CustomerList mCustomer;

    private List<CustomerList> customerLists;
    private List<PetCategory> petCategoryLists;
    private List<CardKind> cardKindLists;

    //商品
    private List<CatgoryItemInfo> catgoryItemInfoList;
    private List<GoodItemInfo> goodItemInfoList;
    String[] list;

    public static MemberUtil getInstance(Context context) {
        if (memberUtil == null) {
            memberUtil = new MemberUtil(context);
        }
        return memberUtil;
    }

    private MemberUtil(Context context) {
        mContext = context;
        mLitepal = LitepalHelper.getInstance();
    }

    public void postAddMembers(int batchNo, MemberInfoModel memberInfoModel, List<PetInfoModel> petInfoModelList) {
        //插入会员上传任务表
        MemberTaskData memberTaskData = new MemberTaskData();
        memberTaskData.setType(TaskConst.TASKTYPE_ADD_MEMBER);//新增订单
        memberTaskData.setBatchNo(batchNo);
        memberTaskData.setStatus(0);//0新任务，9任务在执行，1任务成功，2任务失败
        memberTaskData.setTaskDesc("");
        memberTaskData.setMemberId(batchNo);
        memberTaskData.save();

        LogUtil.error("petInfoModelList data " + DataSupport.findLast(MemberTaskData.class).getId());
    }

    public List<CustomerList> getCustomerLists() {
        return customerLists;
    }

    public void setCustomerLists(List<CustomerList> customerLists) {
        this.customerLists = customerLists;
    }

    public List<PetCategory> getPetCategoryLists() {
        return petCategoryLists;
    }

    public void setPetCategoryLists(List<PetCategory> petCategoryLists) {
        this.petCategoryLists = petCategoryLists;
    }

    public List<CardKind> getCardKindLists() {
        return cardKindLists;
    }

    public void setCardKindLists(List<CardKind> cardKindLists) {
        this.cardKindLists = cardKindLists;
    }

    public List<CatgoryItemInfo> getCatgoryItemInfoList() {
        return catgoryItemInfoList;
    }

    public void setCatgoryItemInfoList(List<CatgoryItemInfo> catgoryItemInfoList) {
        this.catgoryItemInfoList = catgoryItemInfoList;
    }

    public List<GoodItemInfo> getGoodItemInfoList() {
        return goodItemInfoList;
    }

    public void setGoodItemInfoList(List<GoodItemInfo> goodItemInfoList) {
        this.goodItemInfoList = goodItemInfoList;
    }

    public String[] getList() {
        return list;
    }

    public void setList(String[] list) {
        this.list = list;
    }
}
