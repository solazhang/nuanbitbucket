package com.warmsoft.pos.bean;

import java.io.Serializable;

/**
 * Created by brooks on 16/8/30.
 */
public class PetInfoModel implements Serializable {
    String ID;
    String CustomerID;
    String OrgId;
    String PetName;
    String Age;
    String catgory;
    String KindOF;
    String Birthdate;

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getCustomerID() {
        return CustomerID;
    }

    public void setCustomerID(String customerID) {
        CustomerID = customerID;
    }

    public String getOrgId() {
        return OrgId;
    }

    public void setOrgId(String orgId) {
        OrgId = orgId;
    }

    public String getPetName() {
        return PetName;
    }

    public void setPetName(String petName) {
        PetName = petName;
    }

    public String getAge() {
        return Age;
    }

    public void setAge(String age) {
        Age = age;
    }

    public String getCatgory() {
        return catgory;
    }

    public void setCatgory(String catgory) {
        this.catgory = catgory;
    }

    public String getKindOF() {
        return KindOF;
    }

    public void setKindOF(String kindOF) {
        KindOF = kindOF;
    }

    public String getBirthdate() {
        return Birthdate;
    }

    public void setBirthdate(String birthdate) {
        Birthdate = birthdate;
    }
}
