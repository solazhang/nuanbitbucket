package com.warmsoft.pos.bean;

import java.io.Serializable;

/**
 * Created by brooks on 16/1/26.
 */
public class ExceptionModel implements Serializable {
    int code;
    String message;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
