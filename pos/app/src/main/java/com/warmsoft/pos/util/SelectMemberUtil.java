package com.warmsoft.pos.util;

import com.warmsoft.pos.bean.AmountsModel;
import com.warmsoft.pos.litepal.data.CustomerList;
import com.warmsoft.pos.litepal.data.PetData;

import java.util.List;

/**
 * Created by brooks on 16/6/22.
 */
public class SelectMemberUtil {
    private static SelectMemberUtil instance = null;

    public static SelectMemberUtil getInstance() {
        if (instance == null) {
            synchronized (SelectMemberUtil.class) {
                instance = new SelectMemberUtil();
            }
        }

        return instance;
    }

    //获取会员信息
    CustomerList customer = null;

    public CustomerList getCustomer() {
        return customer;
    }

    public void setCustomer(CustomerList customer) {
        this.customer = customer;
    }

    //获取会员宠物列表信息
    List<PetData> petDatas;

    public List<PetData> getPetDatas(CustomerList customer) {
        if (customer == null) {
            return null;
        }

        return petDatas;
    }

    public void setPetDatas(List<PetData> petDatas) {
        this.petDatas = petDatas;
    }

    AmountsModel.DataInfo dataInfo;

    public AmountsModel.DataInfo getDataInfo() {
        return dataInfo;
    }

    public void setDataInfo(AmountsModel.DataInfo dataInfo) {
        this.dataInfo = dataInfo;
    }
}
