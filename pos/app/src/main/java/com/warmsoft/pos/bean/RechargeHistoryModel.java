package com.warmsoft.pos.bean;

import java.io.Serializable;

/**
 * Created by brooks on 16/8/30.
 */
public class RechargeHistoryModel implements Serializable {
    String ReceivedAmount;
    String ChargeDatetime;
    String status;
    String statusText;
    String PayText;
    String Operator;
    String OperatorName;

    public String getReceivedAmount() {
        return ReceivedAmount;
    }

    public void setReceivedAmount(String receivedAmount) {
        ReceivedAmount = receivedAmount;
    }

    public String getChargeDatetime() {
        return ChargeDatetime;
    }

    public void setChargeDatetime(String chargeDatetime) {
        ChargeDatetime = chargeDatetime;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatusText() {
        return statusText;
    }

    public void setStatusText(String statusText) {
        this.statusText = statusText;
    }

    public String getPayText() {
        return PayText;
    }

    public void setPayText(String payText) {
        PayText = payText;
    }

    public String getOperator() {
        return Operator;
    }

    public void setOperator(String operator) {
        Operator = operator;
    }

    public String getOperatorName() {
        return OperatorName;
    }

    public void setOperatorName(String operatorName) {
        OperatorName = operatorName;
    }
}
