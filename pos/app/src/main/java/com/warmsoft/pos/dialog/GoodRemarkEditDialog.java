package com.warmsoft.pos.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.warmsoft.pos.R;
import com.warmsoft.pos.litepal.data.OrderGoodsData;
import com.warmsoft.pos.util.OrderHelper;
import com.warmsoft.pos.util.Util;

/**
 * 作者: lijinliu on 2016/8/5.
 * 邮箱：jinliu.li@warmsoft.com
 * 描述: 商品数量编辑
 */
public class GoodRemarkEditDialog implements View.OnClickListener {

    private Dialog mDialog;
    private Activity mActivity;
    private LayoutInflater mInflater;

    private TextView btnCancle;
    private TextView btnConfirm;
    private EditText editGoodRemark;
    InputMethodManager inputMethodManager;

    private TextView tvInputRemarks;
    private OrderGoodsData medicine;

    public GoodRemarkEditDialog(Activity activity, TextView remarks, OrderGoodsData goodInfo) {
        this.mActivity = activity;
        this.mInflater = LayoutInflater.from(mActivity);
        this.tvInputRemarks = remarks;
        this.medicine = goodInfo;

        inputMethodManager = (InputMethodManager) mActivity.getSystemService(Context.INPUT_METHOD_SERVICE);
        createLoadingDialog(mActivity);
    }

    /**
     * 得到自定义的progressDialog
     *
     * @param context
     * @return
     */
    private void createLoadingDialog(Context context) {
        View rootView = mInflater.inflate(R.layout.dialog_goods_remrk_edit, null);
        LinearLayout layout = (LinearLayout) rootView.findViewById(R.id.dialog_view);
        initView(rootView);
        mDialog = new Dialog(context, R.style.fullScreenDialog);
        mDialog.setCancelable(true);
        int MATH_PARENT = LinearLayout.LayoutParams.MATCH_PARENT;
        mDialog.setContentView(layout, new LinearLayout.LayoutParams(MATH_PARENT, MATH_PARENT));// 设置布局
        mDialog.show();
    }

    private void initView(View rootView) {
        btnConfirm = (TextView) rootView.findViewById(R.id.btn_confirm);
        btnCancle = (TextView) rootView.findViewById(R.id.btn_cancle);
        editGoodRemark = (EditText) rootView.findViewById(R.id.edit_good_remark);
        btnConfirm.setOnClickListener(this);
        btnCancle.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        if (view == btnCancle) {
            dismiss();
        } else if (view == btnConfirm) {
            String input = editGoodRemark.getText().toString();
            if (!Util.isNullStr(input)) {
                medicine.setGoodDesc(input);
                tvInputRemarks.setText(input);
                if (medicine.getID() > 0) {//更新到数据库
                    medicine.update(medicine.getID());
                }
                OrderHelper.getInstance(mActivity).setGoodDesc(medicine.getFeeItemId(), input);
            }
            dismiss();
        }
    }


    public void dismiss() {
        editGoodRemark.requestFocus();
        inputMethodManager.hideSoftInputFromWindow(editGoodRemark.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        if (mDialog != null) {
            mDialog.dismiss();
            mDialog = null;
        }
    }
}
