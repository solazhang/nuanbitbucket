package com.warmsoft.pos.litepal.data;

import org.litepal.crud.DataSupport;

/**
 * Created by brooks on 16/8/15.
 */
public class CardKind extends DataSupport {
    private int Id;
    private int cardId;
    private int Orgid;
    private double ProductRate;
    private double VaccinateRate;
    private double DrugRate;
    private double CosmeticRate;
    private double InpatientRate;
    private double DisposeRate;
    private double ComsumeRate;
    private double XRayRate;
    private double BCRate;
    private double ShowerRate;
    private double ChemRate;
    private double ShizhiRate;
    private double InsectRate;
    private double FosterRate;
    private double PathRate;

    private String Name;

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public int getCardId() {
        return cardId;
    }

    public void setCardId(int cardId) {
        this.cardId = cardId;
    }

    public int getOrgid() {
        return Orgid;
    }

    public void setOrgid(int orgid) {
        Orgid = orgid;
    }

    public double getProductRate() {
        return ProductRate;
    }

    public void setProductRate(double productRate) {
        ProductRate = productRate;
    }

    public double getVaccinateRate() {
        return VaccinateRate;
    }

    public void setVaccinateRate(double vaccinateRate) {
        VaccinateRate = vaccinateRate;
    }

    public double getDrugRate() {
        return DrugRate;
    }

    public void setDrugRate(double drugRate) {
        DrugRate = drugRate;
    }

    public double getCosmeticRate() {
        return CosmeticRate;
    }

    public void setCosmeticRate(double cosmeticRate) {
        CosmeticRate = cosmeticRate;
    }

    public double getInpatientRate() {
        return InpatientRate;
    }

    public void setInpatientRate(double inpatientRate) {
        InpatientRate = inpatientRate;
    }

    public double getDisposeRate() {
        return DisposeRate;
    }

    public void setDisposeRate(double disposeRate) {
        DisposeRate = disposeRate;
    }

    public double getComsumeRate() {
        return ComsumeRate;
    }

    public void setComsumeRate(double comsumeRate) {
        ComsumeRate = comsumeRate;
    }

    public double getXRayRate() {
        return XRayRate;
    }

    public void setXRayRate(double XRayRate) {
        this.XRayRate = XRayRate;
    }

    public double getBCRate() {
        return BCRate;
    }

    public void setBCRate(double BCRate) {
        this.BCRate = BCRate;
    }

    public double getShowerRate() {
        return ShowerRate;
    }

    public void setShowerRate(double showerRate) {
        ShowerRate = showerRate;
    }

    public double getChemRate() {
        return ChemRate;
    }

    public void setChemRate(double chemRate) {
        ChemRate = chemRate;
    }

    public double getShizhiRate() {
        return ShizhiRate;
    }

    public void setShizhiRate(double shizhiRate) {
        ShizhiRate = shizhiRate;
    }

    public double getInsectRate() {
        return InsectRate;
    }

    public void setInsectRate(double insectRate) {
        InsectRate = insectRate;
    }

    public double getFosterRate() {
        return FosterRate;
    }

    public void setFosterRate(double fosterRate) {
        FosterRate = fosterRate;
    }

    public double getPathRate() {
        return PathRate;
    }

    public void setPathRate(double pathRate) {
        PathRate = pathRate;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }
}
