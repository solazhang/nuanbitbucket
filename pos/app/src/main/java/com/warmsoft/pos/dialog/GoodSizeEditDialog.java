package com.warmsoft.pos.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.warmsoft.pos.R;
import com.warmsoft.pos.util.Util;

/**
 * 作者: lijinliu on 2016/8/5.
 * 邮箱：jinliu.li@warmsoft.com
 * 描述: 商品数量编辑
 */
public class GoodSizeEditDialog implements View.OnClickListener {

    private Dialog mDialog;
    private Activity mActivity;
    private LayoutInflater mInflater;

    private TextView btnCancle;
    private TextView btnConfirm;
    private TextView btnClear;
    private TextView tvGoodSize;
    private TextView btn_num_seven;
    private TextView btn_num_eight;
    private TextView btn_num_nine;
    private TextView btn_num_four;
    private TextView btn_num_five;
    private TextView btn_num_six;
    private TextView btn_num_one;
    private TextView btn_num_two;
    private TextView btn_num_three;
    private TextView btn_num_zero;
    private TextView btn_num_pint;
    private TextView btn_num_clear;

    private float initSize = 0;
    private boolean isFirstClick = true;

    private SizeListener mListener;

    public GoodSizeEditDialog(Activity activity, float initSize, SizeListener listener) {
        this.mActivity = activity;
        this.mInflater = LayoutInflater.from(mActivity);
        this.mListener = listener;

        this.initSize = initSize;
        createLoadingDialog(mActivity);
    }

    /**
     * 得到自定义的progressDialog
     *
     * @param context
     * @return
     */
    private void createLoadingDialog(Context context) {
        View rootView = mInflater.inflate(R.layout.dialog_goods_size_edit, null);
        LinearLayout layout = (LinearLayout) rootView.findViewById(R.id.dialog_view);
        initView(rootView);
        mDialog = new Dialog(context, R.style.loadingdialog);
        mDialog.setCancelable(true);
        int MATH_PARENT = LinearLayout.LayoutParams.MATCH_PARENT;
        mDialog.setContentView(layout, new LinearLayout.LayoutParams(MATH_PARENT, MATH_PARENT));// 设置布局
        mDialog.show();
    }

    private void initView(View rootView) {
        btnConfirm = (TextView) rootView.findViewById(R.id.btn_confirm);
        btnCancle = (TextView) rootView.findViewById(R.id.btn_cancle);
        btnClear = (TextView) rootView.findViewById(R.id.btn_clear);
        tvGoodSize = (TextView) rootView.findViewById(R.id.tv_goods_size);
        tvGoodSize.setText(initSize + "");

        btn_num_seven = (TextView) rootView.findViewById(R.id.btn_num_seven);
        btn_num_eight = (TextView) rootView.findViewById(R.id.btn_num_eight);
        btn_num_nine = (TextView) rootView.findViewById(R.id.btn_num_nine);
        btn_num_four = (TextView) rootView.findViewById(R.id.btn_num_four);
        btn_num_five = (TextView) rootView.findViewById(R.id.btn_num_five);
        btn_num_six = (TextView) rootView.findViewById(R.id.btn_num_six);
        btn_num_one = (TextView) rootView.findViewById(R.id.btn_num_one);
        btn_num_two = (TextView) rootView.findViewById(R.id.btn_num_two);
        btn_num_three = (TextView) rootView.findViewById(R.id.btn_num_three);
        btn_num_zero = (TextView) rootView.findViewById(R.id.btn_num_zero);
        btn_num_pint = (TextView) rootView.findViewById(R.id.btn_num_pint);
        btn_num_clear = (TextView) rootView.findViewById(R.id.btn_num_clear);

        btnConfirm.setOnClickListener(this);
        btnCancle.setOnClickListener(this);
        btnClear.setOnClickListener(this);
        btn_num_seven.setOnClickListener(this);
        btn_num_eight.setOnClickListener(this);
        btn_num_nine.setOnClickListener(this);
        btn_num_four.setOnClickListener(this);
        btn_num_five.setOnClickListener(this);
        btn_num_six.setOnClickListener(this);
        btn_num_one.setOnClickListener(this);
        btn_num_two.setOnClickListener(this);
        btn_num_three.setOnClickListener(this);
        btn_num_zero.setOnClickListener(this);
        btn_num_pint.setOnClickListener(this);
        btn_num_clear.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_confirm:
                dismiss();
                if (mListener != null) {
                    String val = tvGoodSize.getText().toString();
                    if (Util.isNullStr(val)) {
                        mListener.numKeyConfim(0);
                    } else {
                        mListener.numKeyConfim(Integer.parseInt(val));
                    }
                }
                break;
            case R.id.btn_cancle:
                dismiss();
                break;
            case R.id.btn_clear:
                isFirstClick = false;
                tvGoodSize.setText("0");
                break;
            case R.id.tv_goods_size:
                break;
            case R.id.btn_num_seven:
                keyClick(7);
                break;
            case R.id.btn_num_eight:
                keyClick(8);
                break;
            case R.id.btn_num_nine:
                keyClick(9);
                break;
            case R.id.btn_num_four:
                keyClick(4);
                break;
            case R.id.btn_num_five:
                keyClick(5);
                break;
            case R.id.btn_num_six:
                keyClick(6);
                break;
            case R.id.btn_num_one:
                keyClick(1);
                break;
            case R.id.btn_num_two:
                keyClick(2);
                break;
            case R.id.btn_num_three:
                keyClick(3);
                break;
            case R.id.btn_num_zero:
                keyClick(0);
                break;
            case R.id.btn_num_pint:
                keyPoint();
                break;
            case R.id.btn_num_clear:
                keyDelete();
                break;
        }
    }

    private void keyClick(int keyValue) {
        if (isFirstClick) {
            isFirstClick = false;
            tvGoodSize.setText(keyValue + "");
        } else {
            String val = tvGoodSize.getText().toString();
            if ("0".equals(val)) {
                tvGoodSize.setText(keyValue + "");
            } else {
                tvGoodSize.setText(val + keyValue);
            }
        }
    }

    private void keyPoint() {

    }

    private void keyDelete() {
        String val = tvGoodSize.getText().toString();
        if (val.length() == 1) {
            tvGoodSize.setText("0");
        } else {
            tvGoodSize.setText(val.substring(0, val.length() - 1));
        }
    }

    public void dismiss() {
        if (mDialog != null) {
            mDialog.dismiss();
            mDialog = null;
        }
    }

    public interface SizeListener {
        void numKeyConfim(int value);
    }
}
