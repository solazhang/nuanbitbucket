package com.warmsoft.pos.service;

import android.content.Context;
import android.support.v4.util.ArrayMap;

import com.google.gson.Gson;
import com.warmsoft.pos.bean.AddOrderForJson;
import com.warmsoft.pos.bean.AddOrderModel;
import com.warmsoft.pos.bean.AuthModel;
import com.warmsoft.pos.bean.DeletePaymentModel;
import com.warmsoft.pos.bean.GetPetOldModel;
import com.warmsoft.pos.bean.MemberReturnModel;
import com.warmsoft.pos.bean.PetInfoModel;
import com.warmsoft.pos.bean.SettlementModel;
import com.warmsoft.pos.enumutil.PayChanel;
import com.warmsoft.pos.fragment.main.FragmentPos;
import com.warmsoft.pos.litepal.data.CardKind;
import com.warmsoft.pos.litepal.data.CustomeCardList;
import com.warmsoft.pos.litepal.data.CustomerList;
import com.warmsoft.pos.litepal.data.LitepalHelper;
import com.warmsoft.pos.litepal.data.MemberTaskData;
import com.warmsoft.pos.litepal.data.OrderInfoData;
import com.warmsoft.pos.litepal.data.OrderTaskData;
import com.warmsoft.pos.litepal.data.PayAmountData;
import com.warmsoft.pos.litepal.data.PetData;
import com.warmsoft.pos.litepal.data.SettleReslutData;
import com.warmsoft.pos.litepal.data.SettlementData;
import com.warmsoft.pos.net.RetrofitManager;
import com.warmsoft.pos.util.LogUtil;
import com.warmsoft.pos.util.NetWorkUtil;
import com.warmsoft.pos.util.OAuthUtils;
import com.warmsoft.pos.util.SelectMemberUtil;
import com.warmsoft.pos.util.TaskConst;
import com.warmsoft.pos.util.TimeUtils;
import com.warmsoft.pos.util.Trace;
import com.warmsoft.pos.util.Util;

import org.greenrobot.eventbus.EventBus;
import org.litepal.crud.DataSupport;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * 作者: lijinliu on 2016/8/25.
 * 邮箱：jinliu.li@warmsoft.com
 * 描述: 营业数据上传
 */
public class UploadDataUtil {

    private static UploadDataUtil helper;
    private Context mContext;
    private LitepalHelper mLitepal;

    public static UploadDataUtil getInstance(Context context) {
        if (helper == null) {
            helper = new UploadDataUtil(context);
        }
        return helper;
    }

    private UploadDataUtil(Context context) {
        mContext = context;
    }

    public synchronized void uploadDatas() {
        if (NetWorkUtil.isNetWorkAvailable(mContext)) {//有网络
            mLitepal = LitepalHelper.getInstance();
            OrderTaskData task = mLitepal.queryNeedUploadDatas();
            startUpload(task);
        }
    }

    private synchronized void startUpload(OrderTaskData task) {
        if (task != null && task.getId() > 0) {
            switch (task.getType()) {
                case TaskConst.TASKTYPE_BILL://下单
                    postBillDatas(task.getId(), task.getOrderNo(), task.getBatchNo());
                    break;
                case TaskConst.TASKTYPE_SETTLE://结算,记账
                    postSettleData(task.getId(), task.getOrderNo());
                    break;
                case TaskConst.TASKTYPE_BACKBILL://下单退单
                    backPayment(task.getId(), task.getOrderNo());
                    break;
                case TaskConst.TASKTYPE_BACKSETTLE://已结算退单
                    backSettle(task.getId(), task.getOrderNo());
                    break;
                case TaskConst.TASKTYPE_KEEPING_ACCOUNTS://记账记录结算
                    keepingAccounts(task.getId(), task.getOrderNo());
                    break;
            }
            //EventBus.getDefault().post(new UploadEndMsg());
        }
    }

    public synchronized void uploadMember() {
        if (NetWorkUtil.isNetWorkAvailable(mContext)) {//有网络
            mLitepal = LitepalHelper.getInstance();
            MemberTaskData task = mLitepal.queryNeedUploadMemberDatas();
            //LogUtil.error("uploadMember task " + task);

            startUploadMembers(task);
        }
    }

    private synchronized void startUploadMembers(MemberTaskData task) {
        if (task != null && task.getId() > 0) {
            LogUtil.error("UploadDataUtil startUploadMembers  task.getId - task.getBatchNo() " + task.getId() + "-" + task.getBatchNo());
            postMembersDatas(task.getId(), task.getBatchNo());
        }
    }

    //接口将id修改为drugid,导致返回的FeeItemId为0,已经和服务器端确认
    private void postBillDatas(final int taskId, final int orderNo, final long batchNo) {
        mLitepal = LitepalHelper.getInstance();
        List<SettlementData> goodList = mLitepal.queryGoodsByBatchNO(batchNo);
        final OrderInfoData orderData = mLitepal.queryOrderInfoByOrderNO(orderNo);

        if (goodList != null && goodList.size() > 0) {
            if (NetWorkUtil.isNetWorkAvailable(mContext)) {//有网络
                Observable<AddOrderModel> observable = RetrofitManager.getInstance().getAPIService().addOrer(createOrderJsonStr(goodList, orderData));
                observable.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Subscriber<AddOrderModel>() {
                            @Override
                            public void onCompleted() {

                            }

                            @Override
                            public void onError(Throwable e) {
                                mLitepal.updateTaskStatus(TaskConst.TASKSTATUS_POSTFAIL, taskId, 0, null);//任务失败
                            }

                            @Override
                            public void onNext(AddOrderModel model) {//离线下单数据提交成功
                                if (model.getCode() == 200 && model.getData() != null) {
                                    List<SettlementData> reponseList = model.getData();
                                    for (SettlementData responseData : reponseList) {
                                        mLitepal.updateBillReponse(batchNo, responseData);
                                    }
                                    mLitepal.updateTaskStatus(TaskConst.TASKSTATUS_POSTSUCC, taskId, 0, null);//任务成功
                                } else {
                                    mLitepal.updateTaskStatus(TaskConst.TASKSTATUS_POSTFAIL, taskId, 0, null);//任务失败
                                }
                            }
                        });
            }
        } else {
            mLitepal.updateTaskStatus(TaskConst.TASKSTATUS_POSTFAIL, taskId, 0, null);//任务失败
        }
    }

    /**
     * 结算
     */
    private void postSettleData(final int taskId, final int orderNo) {
        LogUtil.error("postSettleData taskId-orderNo " + taskId + "-" + orderNo);
        mLitepal = LitepalHelper.getInstance();
        List<SettlementData> goodList = mLitepal.queryGoodsForSettle(orderNo);
        final OrderInfoData orderData = mLitepal.queryOrderInfoByOrderNO(orderNo);
        if (goodList != null && goodList.size() > 0) {
            for (SettlementData data : goodList) {
                data.setID(data.getPaymentId());
                if (Util.isNullStr(data.getCustomerName()) && !Util.isNullStr(orderData.getCustomerName())) {
                    data.setCustomerName(orderData.getCustomerName());
                }
                if (Util.isNullStr(data.getPetName()) && !Util.isNullStr(orderData.getPetName())) {
                    data.setPetName(orderData.getPetName());
                }
                if (Util.isNullStr(data.getCellPhone()) && !Util.isNullStr(orderData.getCellPhone())) {
                    data.setCellPhone(orderData.getCellPhone());
                }
                if (data.getCustomerId() == 0 && orderData.getCustomerId() > 0) {
                    data.setCustomerId(orderData.getCustomerId());
                }
                if (data.getDiscountRate() <= 0) {//折扣率
                    data.setDiscountRate(1);
                }
                data.setActlyPayed(data.getDiscountRate() * data.getTotalAmount());//实收，折扣率*总金额
            }
            Map<String, Object> map = new ArrayMap<>();
            map.put("dicountRate", orderData.getDicountRate());//折扣率
            map.put("param", goodList);
            getPayKind(orderNo + "", map, orderData.getOpId());//支付方式,抹零金额
            map.put("PosNumber", orderData.getOrderNo());//本地单号上传
            map.put("SellerId", orderData.getOpId());//收银员
            String jsonStr = new Gson().toJson(map).replace("\"{", "{").replace("}\"", "}");
            LogUtil.error(jsonStr);
            mLitepal.updateTaskStatus(TaskConst.TASKSTATUS_POSTSUCC, taskId, 0, null);//任务成功
            Observable<SettlementModel> observable = RetrofitManager.getInstance().getAPIService().updatePaymentAndDetalis(jsonStr);
            observable.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Subscriber<SettlementModel>() {
                        @Override
                        public void onCompleted() {
                        }

                        @Override
                        public void onError(Throwable e) {
                            LogUtil.error("Throwable" + e.getMessage());
                            mLitepal.updateTaskStatus(TaskConst.TASKSTATUS_POSTFAIL, taskId, 0, null);//任务失败
                        }

                        @Override
                        public void onNext(SettlementModel model) {
                            if (model.getCode() == 200 && model.getData() != null) {//离线结算提交成在线结算
                                SettleReslutData reslutData = new SettleReslutData();
                                reslutData.setActlyPayed(model.getData().getActlyPayed());
                                reslutData.setDiscountAmount(model.getData().getDiscountAmount());
                                reslutData.setOrderNo(orderNo);
                                reslutData.setOrgId(orderData.getOrgId());
                                reslutData.setPaymentId(model.getData().getId());
                                reslutData.setTotalAmount(model.getData().getTotalAmount());
                                reslutData.save();
                                mLitepal.updateTaskStatus(TaskConst.TASKSTATUS_POSTSUCC, taskId, 0, null);//任务成功
                            } else {
                                mLitepal.updateTaskStatus(TaskConst.TASKSTATUS_POSTFAIL, taskId, 0, null);//任务失败
                            }
                        }
                    });

        } else {
            mLitepal.updateTaskStatus(TaskConst.TASKSTATUS_POSTFAIL, taskId, 0, null);//任务失败
        }
    }

    private void getPayKind(String orderNo, Map map, int operatorId) {
        mLitepal = LitepalHelper.getInstance();
        List<PayAmountData> payList = DataSupport.where("orderNo=? and payChanel=? ", orderNo, PayChanel.PAY_BY_KEEPACCCOUNT + "").order(" id desc ").find(PayAmountData.class);
        if (payList == null || payList.size() == 0) {//记账数据为空
            payList = mLitepal.queryPaysData(orderNo);
        }

        Map<Integer, Float> payKindMap = new HashMap<Integer, Float>();
        if (payList != null && payList.size() > 0) {
            for (PayAmountData payData : payList) {
                if (payData.getPayChanel() == PayChanel.PAY_BY_DISCOUNT) {//抹零
                    map.put("discountAmount", payData.getPayAmount());
                } else {
                    //现金0,银行卡1,银行卡2,微信3,支付宝4,账户余额5,会员卡6,押金7,记账8
                    int serPayChanel = 0;
                    switch (payData.getPayChanel()) {
                        case PayChanel.PAY_BY_CASH://现金";
                            serPayChanel = 0;
                            break;
                        case PayChanel.PAY_BY_BANKCARD://银行卡
                            serPayChanel = 1;
                            break;
                        //case PayChanel.PAY_BY_BANKCARD://储值卡
                        //    serPayChanel = 2;
                        //    break;
                        case PayChanel.PAY_BY_WECHAT://微信
                            serPayChanel = 3;
                            break;
                        case PayChanel.PAY_BY_ALIPAY://支付宝
                            serPayChanel = 4;
                            break;
                        case PayChanel.PAY_BY_ACCOUNT://账户余额
                            serPayChanel = 5;
                            break;
                        case PayChanel.PAY_BY_VIPCARD://会员卡，获取综合卡信息
                            serPayChanel = 6;
                            List<CustomeCardList> dataList = DataSupport.where(" CustomerId=? ", payData.getCustomerId() + "").find(CustomeCardList.class);
                            if (dataList != null && dataList.size() > 0) {
                                dataList.get(0).setCustomerId(payData.getCustomerId());
                                dataList.get(0).setTotalAmount(payData.getPayAmount());
                                dataList.get(0).setActlyAmount(payData.getPayAmount());
                                dataList.get(0).setOperator(operatorId);
                                map.put("MemberCards", dataList);
                            }
                            break;
                        case PayChanel.PAY_BY_DEPOSIT://押金
                            serPayChanel = 7;
                            break;
                        case PayChanel.PAY_BY_KEEPACCCOUNT://记账
                            serPayChanel = 8;
                            break;
                        case PayChanel.PAY_BY_OTHER://其它
                            break;
                    }
                    if (payKindMap.get(serPayChanel) == null || payKindMap.get(serPayChanel) <= 0) {
                        payKindMap.put(serPayChanel, payData.getPayAmount());
                    } else {
                        float sumAmout = payKindMap.get(serPayChanel) + payData.getPayAmount();
                        payKindMap.put(serPayChanel, sumAmout);
                    }
                }
            }
            if (map.get("discountAmount") == null) {
                map.put("discountAmount", 0);
            }
            map.put("payKind", new Gson().toJson(payKindMap).replaceAll("\"", ""));
            LogUtil.error("payKind " + new Gson().toJson(payKindMap).replaceAll("\"", ""));
        } else {
            LogUtil.error("payKind ");
            map.put("payKind", "{0:0.0}");
        }
    }

    /**
     * 未结算退单
     */
    private void backPayment(final int taskId, final int orderNo) {
        mLitepal = LitepalHelper.getInstance();
        List<SettlementData> goodList = mLitepal.queryGoodsForSettle(orderNo);
        List<Integer> payMentDetails = new ArrayList<>();
        if (goodList != null && goodList.size() > 0) {

            for (SettlementData data : goodList) {
                payMentDetails.add(data.getPaymentId());
            }

            Map<String, Object> map = new ArrayMap<>();
            map.put("PaymentDetalis", payMentDetails);
            map.put("OrgId", goodList.get(0).getOrgId());
            String json = new Gson().toJson(map);
            Trace.e(json);
            Observable<DeletePaymentModel> observable = RetrofitManager.getInstance().getAPIService().deletedPaymentDetalisById(json);
            observable.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Subscriber<DeletePaymentModel>() {
                        @Override
                        public void onCompleted() {
                        }

                        @Override
                        public void onError(Throwable e) {
                            mLitepal.updateTaskStatus(TaskConst.TASKSTATUS_POSTFAIL, taskId, 0, null);//任务失败
                        }

                        @Override
                        public void onNext(DeletePaymentModel model) {
                            if (model.getCode() == 200) {//退单成功
                                mLitepal.updateTaskStatus(TaskConst.TASKSTATUS_POSTSUCC, taskId, 0, null);//任务成功
                            } else {
                                mLitepal.updateTaskStatus(TaskConst.TASKSTATUS_POSTFAIL, taskId, 0, null);//任务失败
                            }
                        }
                    });
        } else {
            mLitepal.updateTaskStatus(TaskConst.TASKSTATUS_POSTFAIL, taskId, 0, null);//任务失败
        }
    }

    /**
     * 已结算退单
     */
    private void backSettle(final int taskId, final int orderNo) {
        mLitepal = LitepalHelper.getInstance();
        SettleReslutData reslutData = mLitepal.querySettleReslu(orderNo);
        OrderInfoData orderData = mLitepal.queryOrderInfoByOrderNO(orderNo);
        if (reslutData != null && reslutData.getPaymentId() > 0) {
            Map<String, Object> map = new ArrayMap<>();
            map.put("PaymentId", reslutData.getPaymentId());
            map.put("OrgId", reslutData.getOrgId());
            map.put("CustomerId", orderData.getCustomerId());
            String json = new Gson().toJson(map);
            Trace.e(json);

            Observable<DeletePaymentModel> observable = RetrofitManager.getInstance().getAPIService().deletePaymentById(json.toString());
            observable.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Subscriber<DeletePaymentModel>() {
                        @Override
                        public void onCompleted() {
                        }

                        @Override
                        public void onError(Throwable e) {
                            mLitepal.updateTaskStatus(TaskConst.TASKSTATUS_POSTFAIL, taskId, 0, null);//任务失败
                        }

                        @Override
                        public void onNext(DeletePaymentModel model) {
                            if (model.getCode() == 200) {//退单成功
                                mLitepal.updateTaskStatus(TaskConst.TASKSTATUS_POSTSUCC, taskId, 0, null);//任务成功
                            } else {
                                mLitepal.updateTaskStatus(TaskConst.TASKSTATUS_POSTFAIL, taskId, 0, null);//任务失败
                            }
                        }
                    });
        } else {
            mLitepal.updateTaskStatus(TaskConst.TASKSTATUS_POSTFAIL, taskId, 0, null);//任务失败
        }
    }

    // 记账
    private void keepingAccounts(final int taskId, final int orderNo) {
        mLitepal = LitepalHelper.getInstance();
        SettleReslutData reslutData = mLitepal.querySettleReslu(orderNo);
        OrderInfoData orderData = mLitepal.queryOrderInfoByOrderNO(orderNo);
        if (reslutData != null && reslutData.getPaymentId() > 0) {
            mLitepal = LitepalHelper.getInstance();
            List<PayAmountData> payList = mLitepal.queryPaysData(orderNo + "");
            int serPayChanel = 0;//现金0,银行卡1,银行卡2,微信3,支付宝4,账户余额5,会员卡6,押金7,记账8
            if (payList != null && payList.size() > 0) {
                for (PayAmountData payData : payList) {
                    if (payData.getPayChanel() != PayChanel.PAY_BY_KEEPACCCOUNT) {
                        switch (payData.getPayChanel()) {
                            case PayChanel.PAY_BY_CASH://现金";
                                serPayChanel = 0;
                                break;
                            case PayChanel.PAY_BY_BANKCARD://银行卡
                                serPayChanel = 1;
                                break;
                            case PayChanel.PAY_BY_WECHAT://微信
                                serPayChanel = 3;
                                break;
                            case PayChanel.PAY_BY_ALIPAY://支付宝
                                serPayChanel = 4;
                                break;
                            case PayChanel.PAY_BY_ACCOUNT://账户余额
                                serPayChanel = 5;
                                break;
                            case PayChanel.PAY_BY_VIPCARD://会员卡
                                serPayChanel = 6;
                                break;
                            case PayChanel.PAY_BY_DEPOSIT://押金
                                serPayChanel = 7;
                                break;
                        }
                    }
                }
            }

            Map<String, Object> map = new ArrayMap<>();
            map.put("PaymentId", reslutData.getPaymentId());
            map.put("PaymentMethod", serPayChanel);
            map.put("SellerId", orderData.getOpId());
            String json = new Gson().toJson(map);
            LogUtil.error(json);

            Observable<DeletePaymentModel> observable = RetrofitManager.getInstance().getAPIService().keepAccount(json.toString());
            observable.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Subscriber<DeletePaymentModel>() {
                        @Override
                        public void onCompleted() {
                        }

                        @Override
                        public void onError(Throwable e) {
                            mLitepal.updateTaskStatus(TaskConst.TASKSTATUS_POSTFAIL, taskId, 0, null);//任务失败
                        }

                        @Override
                        public void onNext(DeletePaymentModel model) {
                            if (model.getCode() == 200) {//退单成功
                                mLitepal.updateTaskStatus(TaskConst.TASKSTATUS_POSTSUCC, taskId, 0, null);//任务成功
                            } else {
                                mLitepal.updateTaskStatus(TaskConst.TASKSTATUS_POSTFAIL, taskId, 0, null);//任务失败
                            }
                        }
                    });
        } else {
            mLitepal.updateTaskStatus(TaskConst.TASKSTATUS_POSTFAIL, taskId, 0, null);//任务失败
        }
    }

    private String createOrderJsonStr(List<SettlementData> goodList, OrderInfoData orderData) {
        AddOrderForJson orderInfo = new AddOrderForJson();
        for (int i = 0; i < goodList.size(); i++) {
            AddOrderForJson.Medicine medicine = new AddOrderForJson.Medicine();
            medicine.setOutstorePrice(goodList.get(i).getOutstorePrice());
            medicine.setInstorePrice(goodList.get(i).getInstorePrice());
            medicine.setCount(goodList.get(i).getCount());
            medicine.setDrugsName(goodList.get(i).getFeeItemName());
            medicine.setID(goodList.get(i).getFeeItemId());
            medicine.setOrgId(goodList.get(i).getOrgId());
            medicine.setDrugType(goodList.get(i).getDrugType());
            medicine.setDrugId(goodList.get(i).getFeeItemId());
            orderInfo.getMedicineList().add(medicine);
            orderInfo.getCustomerInfo().setOrgId(medicine.getOrgId());
            orderInfo.getCustomerInfo().setCustomerId(goodList.get(i).getCustomerId());//设置客户信息,默认散客
            orderInfo.getCustomerInfo().setPetId(goodList.get(i).getPetId());//设置宠物信息,默认散客宠物
            orderInfo.getCustomerInfo().setSellerId(orderData.getOpId());//下单员
        }
        String createOrderJsonStr = new Gson().toJson(orderInfo);
        Trace.e("postOffLinePost: ", createOrderJsonStr);
        return createOrderJsonStr;
    }

    private void postMembersDatas(final int taskId, final long batchNo) {
        mLitepal = LitepalHelper.getInstance();
        final List<CustomerList> customerLists = mLitepal.queryMemberInfo(batchNo);
        if (customerLists == null || customerLists.size() <= 0) {
            mLitepal.updateMemberStatus(TaskConst.TASKSTATUS_POSTFAIL, taskId, 0, null);//任务失败
            return;
        }

        List<PetData> petDatas = mLitepal.queryPetDatas(batchNo);

        Map<String, Object> map = new ArrayMap<>();
        AuthModel.DataInfo.UserInfo userInfo = OAuthUtils.getInstance().getUserinfo(mContext);
        map.put("OrgId", userInfo.getOrgId());
        map.put("ID", "0");
        map.put("Name", customerLists.get(0).getName());
        //map.put("VIPLevel", customerLists.get(0).getVIPLevel());
        map.put("CardType", getCardKindId(customerLists.get(0).getCardTypeName()));
        map.put("CellPhone", customerLists.get(0).getCellPhone());
        map.put("ppets", processData(petDatas));

        LogUtil.error("okhttpurl request param " + new Gson().toJson(map));

        Observable<MemberReturnModel> observable = RetrofitManager.getInstance().getAPIService().saveCustomer(new Gson().toJson(map));
        observable.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<MemberReturnModel>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        mLitepal.updateMemberStatus(TaskConst.TASKSTATUS_POSTFAIL, taskId, 0, null);//任务失败
                    }

                    @Override
                    public void onNext(MemberReturnModel model) {
                        if (model.getCode() == 200 && model.getData() != null) {//进行会员单条更新

                            List<CustomerList> customers = DataSupport.where("CustomerId =?", customerLists.get(0).getCustomerId() + "").find(CustomerList.class);
                            SyncDataUtil.getInstance().syncCustomerInfo(false, customers.get(0).getCustomerId());

                            getPetInfoForOrder(batchNo, model.getData().getID() + "", taskId, model.getData().getCardNumber());//更新离线定单数据
                            mLitepal.delPetInfo(batchNo);//确保本地的宠物也被删除

                            FragmentPos.isNewCustomId = model.getData().getID();

                            //重新更新商品显示
                            SelectMemberUtil.getInstance().setCustomer(model.getData()); //用来在商品列表中显示

                            SelectMemberUtil.getInstance().setPetDatas(mLitepal.queryPetDatas(Long.parseLong(customers.get(0).getCustomerId())));

                            EventBus.getDefault().post(new ArrayList<CustomerList>().add(model.getData()));//更新会员信息
                        } else {
                            mLitepal.updateMemberStatus(TaskConst.TASKSTATUS_POSTFAIL, taskId, 0, null);//任务失败
                        }
                    }
                });
    }


    //获取单条宠物信息来更新定单数据，新增会员有离线定单.更新离线定单customeid和petid
    public void getPetInfoForOrder(final long batchNo, String customerId, final int taskId, final String cardNo) {
        mLitepal = LitepalHelper.getInstance();
        List<OrderInfoData> orderList = DataSupport.where(" CustomerID=? ", batchNo + "").find(OrderInfoData.class);
        if (orderList == null || orderList.size() <= 0) {
            mLitepal.updateMemberStatus(TaskConst.TASKSTATUS_POSTSUCC, taskId, 0, null);//任务成功
            return;
        }

        Map<String, Object> map = new HashMap<>();
        map.put("CustomerId", customerId);

        Observable<GetPetOldModel> petObservable = RetrofitManager.getInstance().getAPIService().getPetListByCutomerId(new Gson().toJson(map));
        petObservable.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<GetPetOldModel>() {
                    @Override
                    public void onCompleted() {
                        LogUtil.error("SyncDataUtil syncPetInfo onCompleted");
                    }

                    @Override
                    public void onError(Throwable e) {
                        LogUtil.error("SyncDataUtil syncPetInfo onError");
                        mLitepal.updateMemberStatus(TaskConst.TASKSTATUS_POSTFAIL, taskId, 0, null);//任务失败
                    }

                    @Override
                    public void onNext(GetPetOldModel model) {
                        if (model.getCode() == 200 && model.getData() != null) {
                            List<PetData> list = model.getData();
                            for (int i = 0; i < list.size(); i++) {
                                PetData petData = list.get(i);
                                mLitepal.updateNewMemberOrder(batchNo, petData.getPetName(), petData.getID(), petData.getCustomerID(), cardNo);
                            }
                            mLitepal.updateMemberStatus(TaskConst.TASKSTATUS_POSTSUCC, taskId, 0, null);//任务成功
                        } else {
                            mLitepal.updateMemberStatus(TaskConst.TASKSTATUS_POSTFAIL, taskId, 0, null);//任务失败
                        }
                    }
                });
    }

    private String getCardKindId(String cardTypeName) {
        mLitepal = LitepalHelper.getInstance();
        final List<CardKind> cardKinds = mLitepal.getCardKinds();
        if (cardKinds == null || cardKinds.size() <= 0) {
            return "0";
        }

        for (int i = 0; i < cardKinds.size(); i++) {
            if (cardTypeName.equalsIgnoreCase(cardKinds.get(i).getName())) {
                return String.valueOf(cardKinds.get(i).getCardId());
            }
        }

        return "0";
    }

    private List<PetInfoModel> processData(List<PetData> petDatas) {
        List<PetInfoModel> models = new ArrayList<>();
        for (int i = 0; i < petDatas.size(); i++) {
            PetInfoModel model = new PetInfoModel();
            //model.setID(String.valueOf(petDatas.get(i).getID()));
            //model.setCustomerID(String.valueOf(petDatas.get(i).getCustomerID()));
            model.setCustomerID(String.valueOf(0));
            model.setID(String.valueOf(0));
            model.setKindOF(String.valueOf(petDatas.get(i).getKindOF()));
            model.setOrgId(String.valueOf(petDatas.get(i).getOrgId()));

            model.setPetName(petDatas.get(i).getPetName());
            model.setBirthdate(TimeUtils.getInstance().getAgeByShow(petDatas.get(i).getAge()));
            model.setCatgory(petDatas.get(i).getKindOFText());

            models.add(model);
        }

        return models;
    }
}
