package com.warmsoft.pos.util;

import android.content.Context;
import android.text.TextUtils;
import android.widget.Toast;

import com.warmsoft.pos.R;
import com.warmsoft.pos.bean.ExceptionModel;

/**
 * Created by brooks on 16/6/17.
 */
public class ExceptionUtils {

    private static ExceptionUtils instance = null;

    public static ExceptionUtils getInstance() {
        if (instance == null) {
            synchronized (ExceptionUtils.class) {
                instance = new ExceptionUtils();
            }
        }

        return instance;
    }

    public ExceptionModel getExceptionObj(Context mContext, Throwable e) {
        ExceptionModel exception = new ExceptionModel();
        exception.setCode(8888);
        exception.setMessage(TextUtils.isEmpty(String.valueOf(e.getCause())) ? mContext.getString(R.string.warmlive_exception_exception) : String.valueOf(e.getCause()));

        if (String.valueOf(e.getCause()).contains("java.net.SocketTimeoutException")) {
            exception.setCode(600);
            exception.setMessage(mContext.getString(R.string.warmlive_exception_socket_time_out));
        }

        return exception;
    }

    public void processException(Context mContext, int code, String msg, boolean show) {
        showExceptionToast(mContext, code, msg, show);

//        switch (code) {
//            case 401: //需要重新登录
//            case 402:
//                Intent intent = new sIntent();
//                intent.setClass(mContext, LoginActivity.class);
//                mContext.startActivity(intent);
//
//                RecyclingUtil.getInstance().recyclingAll(mContext);
//                break;
//
//            default:
//                showExceptionToast(mContext, code, msg, show);
//                break;
//        }
    }

    public void showExceptionToast(Context mContext, int code, String msg, boolean show) {
        if (show) {
            Toast.makeText(mContext, "错误码：" + code + "\n错误信息:" + msg, Toast.LENGTH_SHORT).show();
        }
    }

    public void showExceptionToash(Context mContext, ExceptionModel mode) {
        if (mode.getMessage().contains("No address associated with hostname")) {
            Toast.makeText(mContext, "请检查是否联网~~~", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(mContext, "网络开小差~~~", Toast.LENGTH_SHORT).show();
            //Toast.makeText(mContext, "错误码：" + mode.getCode() + "\n错误信息:" + String.valueOf(mode.getMessage()), Toast.LENGTH_SHORT).show();
        }
    }
}
