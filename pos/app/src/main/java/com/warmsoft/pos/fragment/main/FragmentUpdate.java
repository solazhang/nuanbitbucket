package com.warmsoft.pos.fragment.main;

import android.content.ComponentName;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.warmsoft.base.BaseFragment;
import com.warmsoft.pos.R;
import com.warmsoft.pos.SyncDataActivity;
import com.warmsoft.pos.dialog.NomelNoteDialog;
import com.warmsoft.pos.litepal.data.OrderTaskData;
import com.warmsoft.pos.util.NetWorkUtil;
import com.warmsoft.pos.util.TimeUtils;

import org.litepal.crud.DataSupport;

import java.util.Date;
import java.util.List;

public class FragmentUpdate extends BaseFragment implements OnClickListener {
    LinearLayout llUpdateSettingData;
    LinearLayout llUpdateData;

    private TextView tvUpdateSetTime;
    private TextView tvSyncData;

    private TextView tvOrderBatch;

    public static FragmentUpdate newInstance() {
        FragmentUpdate fragment = new FragmentUpdate();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        if (mLayoutView == null) {
            mLayoutView = inflater.inflate(R.layout.fragment_main_update, container, false);
            initViews();
            initEvents();
        } else {
            ViewGroup viewGroup = (ViewGroup) mLayoutView.getParent();
            if (viewGroup != null) {
                viewGroup.removeView(mLayoutView);
            }
        }

        return mLayoutView;
    }

    private void initViews() {
        llUpdateSettingData = (LinearLayout) mLayoutView.findViewById(R.id.id_for_update_setting_data);
        llUpdateSettingData.setOnClickListener(this);

        llUpdateData = (LinearLayout) mLayoutView.findViewById(R.id.layout_order_batch);
        llUpdateData.setOnClickListener(this);

        tvUpdateSetTime = (TextView) mLayoutView.findViewById(R.id.tv_order_from_name_sync_time);
        tvUpdateSetTime.setText(TimeUtils.getInstance().formatYMDHMS(new Date()));

        tvSyncData = (TextView) mLayoutView.findViewById(R.id.tv_order_from_name);
        tvSyncData.setText(TimeUtils.getInstance().formatYMDHMS(new Date()));

        tvOrderBatch = (TextView) mLayoutView.findViewById(R.id.tv_order_batch_op);
    }

    private void initEvents() {
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.id_for_update_setting_data:
                Intent intent = new Intent();
                intent.putExtra("online", "true");
                intent.setClass(getActivity(), SyncDataActivity.class);
                startActivity(intent);
                getActivity().finish();
                break;

            case R.id.layout_order_batch:
                uploadOrderData();
                break;
        }
    }

    private void uploadOrderData() {
        if (NetWorkUtil.isNetWorkAvailable(mContext)) {
            List<OrderTaskData> list = DataSupport.where(" status=? ", "0").find(OrderTaskData.class);
            if (list != null && list.size() > 0) {
                new NomelNoteDialog(mActivity, "数据上传", list.size() + "条营业数据正在上传!", "确定", null);
            } else {
                new NomelNoteDialog(mActivity, "数据上传", "所有营业数据已上传!", "确定", null);
            }
        } else {
            new NomelNoteDialog(mActivity, "无网络", "请打开网络上传营业数据!", "确定", new NomelNoteDialog.DialogListener() {
                @Override
                public void noteDialogConfirm() {
                    Intent intent = null;
                    //判断手机系统的版本  即API大于10 就是3.0或以上版本
                    if (android.os.Build.VERSION.SDK_INT > 10) {
                        intent = new Intent(android.provider.Settings.ACTION_WIRELESS_SETTINGS);
                    } else {
                        intent = new Intent();
                        ComponentName component = new ComponentName("com.android.settings", "com.android.settings.WirelessSettings");
                        intent.setComponent(component);
                        intent.setAction("android.intent.action.VIEW");
                    }
                    mActivity.startActivity(intent);
                }
            });
        }
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }


    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

}
