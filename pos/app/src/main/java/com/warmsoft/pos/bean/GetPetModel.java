package com.warmsoft.pos.bean;

import com.warmsoft.pos.litepal.data.PetData;

import java.io.Serializable;
import java.util.List;

/**
 * 作者: lijinliu on 2016/8/9.
 * 邮箱：jinliu.li@warmsoft.com
 * 描述: 宠物信息
 */
public class GetPetModel implements Serializable {
    int code;
    String message;

    DataInfo Data;

    public static class DataInfo implements Serializable {
        List<PetData> PetList;

        int Count;
        int PageIndex;
        int PageSize;

        public List<PetData> getPetList() {
            return PetList;
        }

        public void setPetList(List<PetData> petList) {
            PetList = petList;
        }

        public int getCount() {
            return Count;
        }

        public void setCount(int count) {
            Count = count;
        }

        public int getPageIndex() {
            return PageIndex;
        }

        public void setPageIndex(int pageIndex) {
            PageIndex = pageIndex;
        }

        public int getPageSize() {
            return PageSize;
        }

        public void setPageSize(int pageSize) {
            PageSize = pageSize;
        }
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataInfo getData() {
        return Data;
    }

    public void setData(DataInfo data) {
        this.Data = data;
    }
}
