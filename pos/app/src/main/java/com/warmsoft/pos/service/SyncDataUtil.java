package com.warmsoft.pos.service;

import android.os.Handler;
import android.os.Message;
import android.support.v4.util.ArrayMap;
import android.text.TextUtils;

import com.google.gson.Gson;
import com.warmsoft.base.MyApplication;
import com.warmsoft.pos.bean.AccountInfoModel;
import com.warmsoft.pos.bean.AccountRechargeHistoryModel;
import com.warmsoft.pos.bean.AccountRechargeModel;
import com.warmsoft.pos.bean.AmountsModel;
import com.warmsoft.pos.bean.AuthModel;
import com.warmsoft.pos.bean.BackCardModel;
import com.warmsoft.pos.bean.DepositRefundModel;
import com.warmsoft.pos.bean.GetPetModel;
import com.warmsoft.pos.bean.GoodDataModel;
import com.warmsoft.pos.bean.MemberRechargeModel;
import com.warmsoft.pos.bean.MembersModel;
import com.warmsoft.pos.bean.PrePledgeRechargeModel;
import com.warmsoft.pos.bean.RemoveRechargeModel;
import com.warmsoft.pos.litepal.data.CatgoryItemInfo;
import com.warmsoft.pos.litepal.data.CustomerList;
import com.warmsoft.pos.litepal.data.GoodItemInfo;
import com.warmsoft.pos.litepal.data.LitepalHelper;
import com.warmsoft.pos.litepal.data.PetData;
import com.warmsoft.pos.net.HttpStatus;
import com.warmsoft.pos.net.RetrofitManager;
import com.warmsoft.pos.protocol.GetOrgCpaymentProto;
import com.warmsoft.pos.protocol.ProtocolListener;
import com.warmsoft.pos.util.List4MapUtil;
import com.warmsoft.pos.util.LogUtil;
import com.warmsoft.pos.util.OAuthUtils;
import com.warmsoft.pos.util.PerformanceUtils;
import com.warmsoft.pos.util.PrefUtil;
import com.warmsoft.pos.util.TaskConst;

import org.greenrobot.eventbus.EventBus;
import org.litepal.crud.DataSupport;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by brooks on 16/8/25.
 */
public class SyncDataUtil {
    private final int SYNC_MEMBER_SUCCESS = 1000;
    private final int SYNC_PETS_SUCCESS = 1001;

    private static SyncDataUtil instance = null;

    public static SyncDataUtil getInstance() {
        if (instance == null) {
            synchronized (SyncDataUtil.class) {
                instance = new SyncDataUtil();
            }
        }

        return instance;
    }

    private LitepalHelper mLitepal;


    public void syncAll() {
        syncPetInfo(true);//同步所有的信息,从宠物开始
    }

    public void syncPetInfo(final boolean flag) {
        LogUtil.error("SyncDataUtil syncPetInfo ...");

        Map<String, Object> map = new ArrayMap<>();
        map.put("CustomerId", null);

        String ts = PrefUtil.getSharedPreference(MyApplication.getContext(), PrefUtil.customer_key, PrefUtil.customer_pet_ts_key_value, "");
        if (!TextUtils.isEmpty(ts)) {
            map.put("Timestamp", ts);
        }

        Observable<GetPetModel> petObservable = RetrofitManager.getInstance().getAPIService().GetPetListModelByCutomerId(new Gson().toJson(map));
        petObservable.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<GetPetModel>() {
                    @Override
                    public void onCompleted() {
                        LogUtil.error("SyncDataUtil syncPetInfo onCompleted");
                    }

                    @Override
                    public void onError(Throwable e) {
                        LogUtil.error("SyncDataUtil syncPetInfo onError");

                        if (flag) {
                            syncGoodInfo(flag);
                        }
                    }

                    @Override
                    public void onNext(GetPetModel model) {
                        long start = PerformanceUtils.getInstance().getReadyTime();

                        new Thread(new SyncPetRunnable(model.getData().getPetList(), flag)).start();

                        PerformanceUtils.getInstance().showConsumeTime(start);

                        if (flag) {
                            syncGoodInfo(flag);
                        }
                    }
                });
    }

    private List<PetData> processNetData(List<PetData> netData) {
        for (int i = 0; i < netData.size(); i++) {
            netData.get(i).setPetId(netData.get(i).getID());
        }
        return netData;
    }

    public void syncSpecialPetInfo(final long batchNo, String customerId, final int taskId, final String cardNo) {
        mLitepal = LitepalHelper.getInstance();
        final List<PetData> petDataList = mLitepal.queryPetDatas(batchNo);
        if (petDataList == null || petDataList.size() <= 0) {
            mLitepal.updateMemberStatus(TaskConst.TASKSTATUS_POSTFAIL, taskId, 0, null);//任务失败
            return;
        }

        LogUtil.error("SyncDataUtil syncPetInfo ...");
        Map<String, Object> map = new HashMap<>();
        map.put("CustomerId", customerId);

        Observable<GetPetModel> petObservable = RetrofitManager.getInstance().getAPIService().GetPetListModelByCutomerId(new Gson().toJson(map));
        petObservable.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<GetPetModel>() {
                    @Override
                    public void onCompleted() {
                        LogUtil.error("SyncDataUtil syncPetInfo onCompleted");
                    }

                    @Override
                    public void onError(Throwable e) {
                        LogUtil.error("SyncDataUtil syncPetInfo onError");
                        mLitepal.updateMemberStatus(TaskConst.TASKSTATUS_POSTFAIL, taskId, 0, null);//任务失败
                    }

                    @Override
                    public void onNext(GetPetModel model) {
                        if (model.getCode() == 200 && model.getData() != null) {
                            List<PetData> petDatas = DataSupport.where("CustomerId =?", petDataList.get(0).getCustomerID() + "").find(PetData.class);
                            List<PetData> list = model.getData().getPetList();
                            for (int i = 0; i < list.size(); i++) {
                                PetData petData = list.get(i);
                                petData.setPetId(petData.getID());
                                petData.update(petDatas.get(i).getID()); //更新宠物会员id
                                mLitepal.updateNewMemberOrder(batchNo, petData.getPetName(), petData.getID(), petData.getCustomerID(), cardNo);
                            }
                            mLitepal.updateMemberStatus(TaskConst.TASKSTATUS_POSTSUCC, taskId, 0, null);//任务成功
                        } else {
                            mLitepal.updateMemberStatus(TaskConst.TASKSTATUS_POSTFAIL, taskId, 0, null);//任务失败
                        }
                    }
                });
    }

    public void syncGoodInfo(final boolean flag) {
        LogUtil.error("SyncDataUtil syncGoodInfo ...");

        Map<String, Object> map = new ArrayMap<>();

        AuthModel.DataInfo.UserInfo userInfo = OAuthUtils.getInstance().getUserinfo(MyApplication.getContext());
        if (userInfo != null) {
            map.put("OrgId", userInfo.getOrgId());

            map.put("DrugType", 1);//获取所有4大类型

            Observable<GoodDataModel> observable = RetrofitManager.getInstance().getAPIService().getGoodData(new Gson().toJson(map));
            observable.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Subscriber<GoodDataModel>() {
                        @Override
                        public void onCompleted() {
                            LogUtil.error("syncData getGoodData onCompleted");
                        }

                        @Override
                        public void onError(Throwable e) {
                            LogUtil.error("syncData getGoodData onError " + e.getMessage());

                            if (flag) {
                                syncCustomerInfo(flag, "");
                            }
                        }

                        @Override
                        public void onNext(GoodDataModel model) {
                            LogUtil.error("syncData getGoodData onNext");

                            if (HttpStatus.status200 == model.getCode()) {
                                DataSupport.deleteAll(GoodItemInfo.class);
                                DataSupport.deleteAll(CatgoryItemInfo.class);

                                List<CatgoryItemInfo> catgoryItemInfos = model.getData().getDataInfo();
                                for (int i = 0; i < catgoryItemInfos.size(); i++) {
                                    catgoryItemInfos.get(i).setType(i);
                                    List<GoodItemInfo> goodItemInfos = catgoryItemInfos.get(i).getInfo();
                                    for (int j = 0; j < goodItemInfos.size(); j++) {
                                        goodItemInfos.get(j).setType(i);
                                        goodItemInfos.get(j).setGoodId(goodItemInfos.get(j).getId());
                                    }
                                    DataSupport.saveAll(goodItemInfos);
                                }
                                DataSupport.saveAll(catgoryItemInfos);
                            }

                            if (flag) {
                                syncCustomerInfo(flag, "");
                            }
                        }
                    });
        }
    }

    /**
     * @param flag
     * @param locCusId 用来删除本地的数据对象
     */
    public void syncCustomerInfo(final boolean flag, final String locCusId) {
        LogUtil.error("SyncDataUtil syncCustomerInfo ...");

        Map<String, Object> map = new ArrayMap<>();
        AuthModel.DataInfo.UserInfo userInfo = OAuthUtils.getInstance().getUserinfo(MyApplication.getContext());
        map.put("OrgId", userInfo.getOrgId());

        String ts = PrefUtil.getSharedPreference(MyApplication.getContext(), PrefUtil.customer_key, PrefUtil.customer_key_ts_value, "");
        if (!TextUtils.isEmpty(ts)) {
            map.put("Timestamp", ts);
        }

        Observable<MembersModel> customObservable = RetrofitManager.getInstance().getAPIService().getCustomerList(new Gson().toJson(map));
        customObservable.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<MembersModel>() {
                    @Override
                    public void onCompleted() {
                        LogUtil.error("syncData getCustomerList onNext");
                    }

                    @Override
                    public void onError(Throwable e) {
                        LogUtil.error("syncData getCustomerList onError");

                        if (flag) {
                            syncUnpayInfo(flag);
                        }

                        EventBus.getDefault().post(new ArrayList<CustomerList>());
                    }

                    @Override
                    public void onNext(MembersModel model) {
                        LogUtil.error("syncData getCustomerList onNext");

                        if (HttpStatus.status200 == model.getCode()) {
                            new Thread(new SyncMemberRunnable(model.getData().getCustomerList(), flag, locCusId)).start();
                        } else {
                            EventBus.getDefault().post(new ArrayList<CustomerList>());
                        }
                    }
                });
    }

    public void syncUnpayInfo(final boolean flag) {
        LogUtil.error("SyncDataUtil syncUnpayInfo ...");
        AuthModel.DataInfo.UserInfo userInfo = OAuthUtils.getInstance().getUserinfo(MyApplication.getContext());
        int orgId = userInfo.getOrgId();
        String startTime = "2016-08-01 10:00:00";
        new GetOrgCpaymentProto(MyApplication.getContext()).queryDataFromService(orgId + "", startTime, new ProtocolListener() {
            @Override
            public void onCompleted() {
            }

            @Override
            public void onNext() {
            }

            @Override
            public void onError() {
            }
        });
    }


    private Handler handler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case SYNC_MEMBER_SUCCESS:
                    boolean f = (boolean) msg.obj;
                    if (f) {
                        syncUnpayInfo(f);
                    }

                    break;
            }

            super.handleMessage(msg);
        }
    };

    class SyncMemberRunnable implements Runnable {
        List<CustomerList> customerLists;
        boolean flag;
        String customerId;

        public SyncMemberRunnable(List<CustomerList> lists, boolean f, String locCustomerId) {
            this.customerLists = lists;
            this.flag = f;
            this.customerId = locCustomerId;
        }

        @Override
        public void run() {
            if (customerLists != null && customerLists.size() > 0) {
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < customerLists.size(); i++) {
                    CustomerList customerList = customerLists.get(i);
                    customerList.setCustomerId(String.valueOf(customerList.getID()));

                    sb.append(customerList.getCustomerId());
                    if (i < customerLists.size() - 1) {
                        sb.append(",");
                    }
                }

                //删除本地的数据
                if (!TextUtils.isEmpty(customerId)) {
                    DataSupport.deleteAll(CustomerList.class, "id > ? and CustomerId in (" + customerId + ")", "0");
                }

                //删除需要的customerlist
                DataSupport.deleteAll(CustomerList.class, "id > ? and CustomerId in (" + sb.toString() + ")", "0");

                //将新的数据存储在数据库中
                DataSupport.saveAll(customerLists);

                // 更新时间戳
                mLitepal = LitepalHelper.getInstance();
                String ts = mLitepal.queryCustomerTimeStamp();
                PrefUtil.savePreferenceByItem(MyApplication.getContext(), PrefUtil.customer_key, PrefUtil.customer_key_ts_value, ts);
            }

            EventBus.getDefault().post(customerLists);//用来刷新界面

            SyncDataUtil.getInstance().syncPetInfo(false); //同步完会员,也需要同步宠物信息

            Message message = new Message();
            message.what = SYNC_MEMBER_SUCCESS;
            message.obj = flag;
            handler.sendMessage(message);
        }
    }

    private List<CustomerList> getDifferentCustomers(List<CustomerList> localCustomers, List<CustomerList> netCustomers) {//获取包含有属性变更的数据
        LogUtil.error("getDifferentCustomers ");
        Map<String, CustomerList> localMaps = List4MapUtil.getInstance().list4Map(localCustomers);
        Map<String, CustomerList> netMaps = List4MapUtil.getInstance().list4Map(netCustomers);

        return List4MapUtil.getInstance().getDifferentMap(localMaps, netMaps);
    }

    private List<PetData> getDifferentPets(List<PetData> localCustomers, List<PetData> netCustomers) {
        LogUtil.error("getDifferentPets ");
        Map<Integer, PetData> localMaps = List4MapUtil.getInstance().list4PetMap(localCustomers);
        Map<Integer, PetData> netMaps = List4MapUtil.getInstance().list4PetMap(netCustomers);

        return List4MapUtil.getInstance().getDifferentPetsMap(localMaps, netMaps);
    }

    class SyncPetRunnable implements Runnable {
        List<PetData> petDatas;
        boolean flag;

        public SyncPetRunnable(List<PetData> lists, boolean f) {
            this.petDatas = lists;
            this.flag = f;
        }

        @Override
        public void run() {
            if (petDatas != null && petDatas.size() > 0) {
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < petDatas.size(); i++) {
                    PetData petData = petDatas.get(i);
                    petData.setPetId(petData.getID());

                    sb.append(petData.getID());
                    if (i < petDatas.size() - 1) {
                        sb.append(",");
                    }
                }

                //删除所有需要更新的petlist
                LogUtil.error("sb " + sb.toString());
                DataSupport.deleteAll(PetData.class, "id>? and petId in (" + sb.toString() + ")", "0");

                //将所有的petlist重新插入到数据库中
                DataSupport.saveAll(petDatas);
            }

            mLitepal = LitepalHelper.getInstance();
            String ts = mLitepal.queryPetTimeStatmp();
            PrefUtil.savePreferenceByItem(MyApplication.getContext(), PrefUtil.customer_key, PrefUtil.customer_pet_ts_key_value, ts);
        }
    }

    public void syncGetAmountHistoires(final Map<String, Object> map) {
        Observable<AccountRechargeHistoryModel> observable = RetrofitManager.getInstance().getAPIService().getAmountHistoires(new Gson().toJson(map));
        observable.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<AccountRechargeHistoryModel>() {
                    @Override
                    public void onCompleted() {
                        LogUtil.error("syncData syncGetCChargeHistories onCompleted");
                    }

                    @Override
                    public void onError(Throwable e) {
                        LogUtil.error("syncData syncGetCChargeHistories onError " + e.getMessage());
                        EventBus.getDefault().post(null);
                    }

                    @Override
                    public void onNext(AccountRechargeHistoryModel model) {
                        LogUtil.error("syncData syncGetCChargeHistories onNext");

                        if (HttpStatus.status200 == model.getCode()) {
                            EventBus.getDefault().post(model);
                        } else {
                            EventBus.getDefault().post(model);
                        }
                    }
                });
    }

    public void syncAccountRecharge(final Map<String, Object> map) {
        Observable<AccountRechargeModel> observable = RetrofitManager.getInstance().getAPIService().accountRecharge(new Gson().toJson(map));
        observable.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<AccountRechargeModel>() {
                    @Override
                    public void onCompleted() {
                        LogUtil.error("syncData syncAccountRecharge onCompleted");
                    }

                    @Override
                    public void onError(Throwable e) {
                        LogUtil.error("syncData syncAccountRecharge onError " + e.getMessage());
                        EventBus.getDefault().post(null);
                    }

                    @Override
                    public void onNext(AccountRechargeModel model) {
                        LogUtil.error("syncData syncAccountRecharge onNext");

                        if (HttpStatus.status200 == model.getCode()) {
                            EventBus.getDefault().post(model.getData());
                        } else {
                            EventBus.getDefault().post(model.getData());
                        }
                    }
                });
    }

    public void syncRetirementAccount(final Map<String, Object> map) {
        Observable<RemoveRechargeModel> observable = RetrofitManager.getInstance().getAPIService().retirementAccount(new Gson().toJson(map));
        observable.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<RemoveRechargeModel>() {
                    @Override
                    public void onCompleted() {
                        LogUtil.error("syncData syncAccountRecharge onCompleted");
                    }

                    @Override
                    public void onError(Throwable e) {
                        LogUtil.error("syncData syncAccountRecharge onError " + e.getMessage());
                        EventBus.getDefault().post(null);
                    }

                    @Override
                    public void onNext(RemoveRechargeModel model) {
                        LogUtil.error("syncData syncAccountRecharge onNext");

                        if (HttpStatus.status200 == model.getCode()) {
                            EventBus.getDefault().post(model.getData());
                        } else {
                            EventBus.getDefault().post(model.getData());
                        }
                    }
                });
    }

    public void syncPrePledgeRecharge(final Map<String, Object> map) {
        Observable<PrePledgeRechargeModel> observable = RetrofitManager.getInstance().getAPIService().depositRecharge(new Gson().toJson(map));
        observable.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<PrePledgeRechargeModel>() {
                    @Override
                    public void onCompleted() {
                        LogUtil.error("syncData syncAccountRecharge onCompleted");
                    }

                    @Override
                    public void onError(Throwable e) {
                        LogUtil.error("syncData syncAccountRecharge onError " + e.getMessage());
                        EventBus.getDefault().post(null);
                    }

                    @Override
                    public void onNext(PrePledgeRechargeModel model) {
                        LogUtil.error("syncData syncAccountRecharge onNext");

                        if (HttpStatus.status200 == model.getCode()) {
                            EventBus.getDefault().post(model.getData());
                        } else {
                            EventBus.getDefault().post(model.getData());
                        }
                    }
                });
    }


    public void syncMemberRecharge(final Map<String, Object> map) {
        Observable<MemberRechargeModel> observable = RetrofitManager.getInstance().getAPIService().cardRecharge(new Gson().toJson(map));
        observable.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<MemberRechargeModel>() {
                    @Override
                    public void onCompleted() {
                        LogUtil.error("syncData syncAccountRecharge onCompleted");
                    }

                    @Override
                    public void onError(Throwable e) {
                        LogUtil.error("syncData syncAccountRecharge onError " + e.getMessage());
                        EventBus.getDefault().post(null);
                    }

                    @Override
                    public void onNext(MemberRechargeModel model) {
                        LogUtil.error("syncData syncAccountRecharge onNext");

                        if (HttpStatus.status200 == model.getCode()) {
                            EventBus.getDefault().post(model.getData());
                        } else {
                            EventBus.getDefault().post(model.getData());
                        }
                    }
                });
    }

    public void syncDepositRefund(final Map<String, Object> map) {
        Observable<DepositRefundModel> observable = RetrofitManager.getInstance().getAPIService().depositRefund(new Gson().toJson(map));
        observable.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<DepositRefundModel>() {
                    @Override
                    public void onCompleted() {
                        LogUtil.error("syncData syncDepositRefund onCompleted");
                    }

                    @Override
                    public void onError(Throwable e) {
                        LogUtil.error("syncData syncDepositRefund onError " + e.getMessage());
                        EventBus.getDefault().post(null);
                    }

                    @Override
                    public void onNext(DepositRefundModel model) {
                        LogUtil.error("syncData syncDepositRefund onNext");

                        if (HttpStatus.status200 == model.getCode()) {
                            EventBus.getDefault().post(model.getData());
                        } else {
                            EventBus.getDefault().post(model.getData());
                        }
                    }
                });
    }

    public void syncGetUserMoneyInFo(final Map<String, Object> map) {
        Observable<AccountInfoModel> observable = RetrofitManager.getInstance().getAPIService().getUserMoneyInFo(new Gson().toJson(map));
        observable.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<AccountInfoModel>() {
                    @Override
                    public void onCompleted() {
                        LogUtil.error("syncData syncAccountRecharge onCompleted");
                    }

                    @Override
                    public void onError(Throwable e) {
                        AccountInfoModel.DataInfo dataInfo = new AccountInfoModel.DataInfo();
                        dataInfo.setInfoType((String) map.get("InfoType"));
                        EventBus.getDefault().post(dataInfo);
                    }

                    @Override
                    public void onNext(AccountInfoModel model) {
                        LogUtil.error("syncData syncAccountRecharge onNext");

                        if (HttpStatus.status200 == model.getCode() && model.getData() != null) {
                            model.getData().setInfoType((String) map.get("InfoType"));
                            EventBus.getDefault().post(model.getData());
                        } else if (model.getData() == null) {
                            AccountInfoModel.DataInfo dataInfo = new AccountInfoModel.DataInfo();
                            dataInfo.setInfoType((String) map.get("InfoType"));
                            EventBus.getDefault().post(dataInfo);
                        } else {
                            EventBus.getDefault().post(model.getData());
                        }
                    }
                });
    }


    public void syncGetUserAllInfo(int customerId) {
        Map<String, Integer> map = new ArrayMap<>();
        map.put("CustomerId", customerId);
        Observable<AmountsModel> observable = RetrofitManager.getInstance().getAPIService().amount(new Gson().toJson(map));
        observable.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<AmountsModel>() {

                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        LogUtil.error("syncData syncGetUserAllInfo onError " + e.getMessage());
                        EventBus.getDefault().post(null);
                    }

                    @Override
                    public void onNext(AmountsModel model) {
                        LogUtil.error("getAmounts onNext ");

                        if (HttpStatus.status200 == model.getCode() && model.getData() != null) {
                            EventBus.getDefault().post(model.getData());
                        } else if (model.getData() == null) {
                            AmountsModel.DataInfo dataInfo = new AmountsModel.DataInfo();
                            EventBus.getDefault().post(dataInfo);
                        }
                    }
                });
    }

    public void syncBackCard(final Map<String, Object> map) {
        Observable<BackCardModel> observable = RetrofitManager.getInstance().getAPIService().backCard(new Gson().toJson(map));
        observable.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<BackCardModel>() {

                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        LogUtil.error("syncData syncGetUserAllInfo onError " + e.getMessage());
                        EventBus.getDefault().post(null);
                    }

                    @Override
                    public void onNext(BackCardModel model) {
                        LogUtil.error("getAmounts onNext ");

                        if (HttpStatus.status200 == model.getCode() && model.getData() != null) {
                            EventBus.getDefault().post(model.getData());
                        } else {
                            EventBus.getDefault().post(model.getData());
                        }
                    }
                });
    }
}
