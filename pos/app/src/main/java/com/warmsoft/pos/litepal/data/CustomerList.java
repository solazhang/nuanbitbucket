package com.warmsoft.pos.litepal.data;

import org.litepal.crud.DataSupport;

import java.io.Serializable;

/**
 * Created by brooks on 16/8/15.
 */
public class CustomerList extends DataSupport implements Serializable {
    private int ID;
    private String CustomerId;
    private int OrgId;
    private int VIPLevel;
    private int Score;
    private int Gender;
    private double RestOfAmount;

    private String Name;
    private String CellPhone;
    private String CardTypeName;
    private String EMail;
    private String Address;
    private String RegistrationDate;
    private String LastInterview;
    private String GenderText;
    private String Birthdate;
    private String DataFrom;
    private String IsSelect = "false";
    private String CardNumber = "";
    private int isNew;//是否是新会员
    private String TimeStamp;
    private String OldCustomerId;

    public String getTimeStamp() {
        return TimeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        TimeStamp = timeStamp;
    }

    public String getIsSelect() {
        return IsSelect;
    }

    public void setIsSelect(String isSelect) {
        IsSelect = isSelect;
    }

    public String getBirthdate() {
        return Birthdate;
    }

    public void setBirthdate(String birthdate) {
        Birthdate = birthdate;
    }

    public String getCustomerId() {
        return CustomerId;
    }

    public void setCustomerId(String customerId) {
        CustomerId = customerId;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public int getOrgId() {
        return OrgId;
    }

    public void setOrgId(int orgId) {
        OrgId = orgId;
    }

    public int getVIPLevel() {
        return VIPLevel;
    }

    public void setVIPLevel(int VIPLevel) {
        this.VIPLevel = VIPLevel;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getCellPhone() {
        return CellPhone;
    }

    public void setCellPhone(String cellPhone) {
        CellPhone = cellPhone;
    }

    public int getScore() {
        return Score;
    }

    public void setScore(int score) {
        Score = score;
    }

    public String getCardTypeName() {
        return CardTypeName;
    }

    public void setCardTypeName(String cardTypeName) {
        CardTypeName = cardTypeName;
    }

    public int getGender() {
        return Gender;
    }

    public void setGender(int gender) {
        Gender = gender;
    }

    public String getEMail() {
        return EMail;
    }

    public void setEMail(String EMail) {
        this.EMail = EMail;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }

    public String getRegistrationDate() {
        return RegistrationDate;
    }

    public void setRegistrationDate(String registrationDate) {
        RegistrationDate = registrationDate;
    }

    public String getLastInterview() {
        return LastInterview;
    }

    public void setLastInterview(String lastInterview) {
        LastInterview = lastInterview;
    }

    public double getRestOfAmount() {
        return RestOfAmount;
    }

    public void setRestOfAmount(double restOfAmount) {
        RestOfAmount = restOfAmount;
    }

    public String getGenderText() {
        return GenderText;
    }

    public void setGenderText(String genderText) {
        GenderText = genderText;
    }

    public String getDataFrom() {
        return DataFrom;
    }

    public void setDataFrom(String dataFrom) {
        DataFrom = dataFrom;
    }

    public String getCardNumber() {
        return CardNumber;
    }

    public void setCardNumber(String cardNumber) {
        CardNumber = cardNumber;
    }

    public int getIsNew() {
        return isNew;
    }

    public void setIsNew(int isNew) {
        this.isNew = isNew;
    }

    public String getOldCustomerId() {
        return OldCustomerId;
    }

    public void setOldCustomerId(String oldCustomerId) {
        OldCustomerId = oldCustomerId;
    }

    //重写equals方法只要age相等，我们就认为对象两个相等
    @Override
    public boolean equals(Object obj) {
        if (obj instanceof CustomerList) {
            CustomerList customer = (CustomerList) obj;
//            return CustomerId ==  customer.getCustomerId();
            return (CustomerId.equalsIgnoreCase(customer.getCustomerId()));
        } else {
            return super.equals(obj);
        }
    }

    @Override
    public int hashCode() {
        if (CustomerId == null) {
            CustomerId = "";
        }
        return CustomerId.hashCode();
    }
}
