package com.warmsoft.pos.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.warmsoft.pos.R;
import com.warmsoft.pos.litepal.data.OrderInfoData;
import com.warmsoft.pos.util.TimeUtils;
import com.warmsoft.pos.util.Util;

import java.util.List;

/**
 * 作者: lijinliu on 2016/8/9.
 * 邮箱：jinliu.li@warmsoft.com
 * 描述: 待付款
 */
public class NeedPayAdapter extends BaseAdapter {
    protected Context mContext;
    protected List<OrderInfoData> datas;

   private int mFlag = -1;
    public NeedPayAdapter(Context context ,int flag) {
        mContext = context;
        mFlag = flag;
    }

    public void setData(List<OrderInfoData> datas) {
        this.datas = datas;
        this.notifyDataSetChanged();
    }

    public OrderInfoData getSelected(int position) {
        OrderInfoData om = null;
        if (datas != null && position < datas.size()) {
            for (int i = 0; i < datas.size(); i++) {
                if (position == i) {
                    om = datas.get(i);
                    om.isSelected = true;
                } else {
                    datas.get(i).isSelected = false;
                }
            }
            this.notifyDataSetChanged();
        }
        return om;
    }

    @Override
    public int getCount() {
        if (datas == null) {
            return 0;
        } else {
            return datas.size();
        }
    }

    @Override
    public Object getItem(int position) {
        if (datas == null) {
            return null;
        } else {
            return datas.get(position);
        }
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @SuppressLint("ViewHolder")
    @Override
    public View getView(int position, View view, ViewGroup parent) {
        ViewHolder holde;
        if (view == null) {
            view = LayoutInflater.from(mContext).inflate(R.layout.item_pending_payment, null);
            holde = new ViewHolder(view);
        } else {
            holde = (ViewHolder) view.getTag();
            if (holde == null) {
                view = LayoutInflater.from(mContext).inflate(R.layout.item_pending_payment, null);
                holde = new ViewHolder(view);
            }
        }
        if (holde != null) {
            OrderInfoData item = (OrderInfoData) getItem(position);
            holde.bindData(item, position);
        }

        return view;
    }

    class ViewHolder {
        LinearLayout layoutBg;
        TextView tvOrderNum;
        TextView tvCustomName;
        TextView tvOrderAmount;
        TextView tvCustomCall;
        TextView tvOrderDate;

        ViewHolder(View view) {
            layoutBg = (LinearLayout) view.findViewById(R.id.layout_bg);
            tvOrderNum = (TextView) view.findViewById(R.id.tv_order_no);
            tvCustomName = (TextView) view.findViewById(R.id.tv_custom_name);
            tvOrderAmount = (TextView) view.findViewById(R.id.tv_order_amount);
            tvCustomCall = (TextView) view.findViewById(R.id.tv_custom_telno);
            tvOrderDate = (TextView) view.findViewById(R.id.tv_order_date);
            view.setTag(this);
        }

        void bindData(final OrderInfoData data, final int position) {
            if (data == null) {
                return;
            } else {
                if(position == 0 && mFlag == 1){
                    tvOrderNum.setText("" + data.getNoes());
                    tvOrderDate.setText(""+ data.getTime());
                }else {
                    tvOrderNum.setText("No." + data.getOrderNo());
                    tvOrderDate.setText(TimeUtils.getInstance().formatYMDHM(data.getOrderDate()) + "");
                }

                tvCustomName.setText(Util.isNullStr(data.getCustomerName()) ? "" : data.getCustomerName());
                tvOrderAmount.setText(data.getCountAmount() + "");
                tvCustomCall.setText(Util.isNullStr(data.getCellPhone()) ? "空" : data.getCellPhone());

                if (data.isSelected) {
                    layoutBg.setBackgroundResource(R.color.dialog_goodsize_edit_color);
                } else {
                    layoutBg.setBackgroundResource(R.color.transparent);
                }

                if(mFlag == 1) {
                    tvCustomName.setVisibility(View.GONE);
                    tvOrderAmount.setVisibility(View.GONE);
                    tvCustomCall.setVisibility(View.GONE);
                    tvOrderNum.setTextColor(mContext.getResources().getColor(R.color.black));
                    tvOrderDate.setTextColor(mContext.getResources().getColor(R.color.black));
                }
            }
        }
    }
}
