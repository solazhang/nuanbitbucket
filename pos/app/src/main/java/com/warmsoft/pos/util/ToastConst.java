package com.warmsoft.pos.util;

/**
 * 作者: lijinliu on 2016/8/5.
 * 邮箱：jinliu.li@warmsoft.com
 * 描述: Toas提示
 */

public class ToastConst
{
	public static final String ERROR_USERINFO_NULL = "员工信息获取失败，请重新登录或者联系管理员！";
	public static final String ERROR_USER_ORGINFO_NULL = "员工机构信息获取失败，请重新登录或者联系管理员！";
	public static final String ERROR_USER_ANONYMOUSINFO_NULL = "机构散客信息获取失败，请重新登录或者联系管理员！";
	public static final String ERROR_USER_ANONYMOUS_PETINFO_NULL = "机构散客宠物信息获取失败，请重新登录或者联系管理员！";
	public static final String ERROR_CREATE_ORDERNO_EXPECTION = "定单号创建失败，请稍后再试或者联系管理员！";

	public static final String ERROR_NO_UNSURE_GOODS = "没有未确认商品!";
}
