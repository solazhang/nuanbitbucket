package com.warmsoft.pos;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.warmsoft.base.BasicActivity;
import com.warmsoft.base.MyApplication;
import com.warmsoft.pos.util.Trace;


/**
 * 作者: lijinliu on 2016/8/5.
 * 邮箱：jinliu.li@warmsoft.com
 * 描述: 使用指南
 */

public class HelpActivity extends BasicActivity {

    private Button btnClose;
    protected TextView mTvCompanUrl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((MyApplication)getApplication()).pushActivity(this);
        setContentView(R.layout.activity_help);
        btnClose = (Button) findViewById(R.id.btn_close);
        mTvCompanUrl = (TextView) findViewById(R.id.tv_company_url);
        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                HelpActivity.this.finish();
            }
        });

        mTvCompanUrl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openBrowser();
            }
        });
    }

    private void openBrowser() {
        try {
            Intent intent = new Intent();
            intent.setAction("android.intent.action.VIEW");
            String url = mTvCompanUrl.getText().toString();
            if (!url.startsWith("http://")) {
                url = "http://" + url;
            }
            Uri content_url = Uri.parse(url);
            intent.setData(content_url);
            startActivity(intent);
        } catch (Exception e) {
            Trace.e(e);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ((MyApplication)getApplication()).popActivity(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    public static void startActivity(Context context) {
        try {
            Intent intent = new Intent(context, HelpActivity.class);
            context.startActivity(intent);
        } catch (Exception e) {
        }
    }


}
