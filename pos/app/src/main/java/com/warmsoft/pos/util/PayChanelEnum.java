package com.warmsoft.pos.util;

/**
 * 作者: lijinliu on 2016/8/16.
 * 邮箱：jinliu.li@warmsoft.com
 * 描述: 支付方式说明
 */
public enum PayChanelEnum {
    cash, bankCard, webChat, aliPay;

    public static PayChanelEnum getPayChanel(String payChanel) {
        if ("1".equals(payChanel)) {
            return cash;
        } else if ("2".equals(payChanel)) {
            return bankCard;
        } else if ("3".equals(payChanel)) {
            return webChat;
        } else if ("4".equals(payChanel)) {
            return aliPay;
        } else {
            return cash;
        }
    }

    public static String getPayChanelName(String payChanel) {
        if ("1".equals(payChanel)) {
            return "现金";
        } else if ("2".equals(payChanel)) {
            return "银行卡";
        } else if ("3".equals(payChanel)) {
            return "微信";
        } else if ("4".equals(payChanel)) {
            return "支付宝";
        } else {
            return "现金";
        }
    }
}
