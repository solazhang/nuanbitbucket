package com.warmsoft.pos.litepal.data;

import org.litepal.crud.DataSupport;

/**
 * Created by brooks on 16/8/15.
 */
public class GoodItemInfo extends DataSupport {
    private int Id;
    private int GoodId;
    private String OrgId;
    private String DrugType;
    private String DrugsName;
    private String Brand;
    private String Category;
    private String InstorePrice;
    private String OutstorePrice;
    private String MemberPrice;
    private String BarCode;//条形码
    private int type;
    private float choose;

    public String getDrugsName() {
        return DrugsName;
    }

    public void setDrugsName(String drugsName) {
        DrugsName = drugsName;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
        setGoodId(id);
    }

    public int getGoodId() {
        return GoodId;
    }

    public void setGoodId(int goodId) {
        GoodId = goodId;
    }

    public String getOrgId() {
        return OrgId;
    }

    public void setOrgId(String orgId) {
        OrgId = orgId;
    }

    public String getDrugType() {
        return DrugType;
    }

    public void setDrugType(String drugType) {
        DrugType = drugType;
    }

    public String getBrand() {
        return Brand;
    }

    public void setBrand(String brand) {
        Brand = brand;
    }

    public String getCategory() {
        return Category;
    }

    public void setCategory(String category) {
        Category = category;
    }

    public String getInstorePrice() {
        return InstorePrice;
    }

    public void setInstorePrice(String instorePrice) {
        InstorePrice = instorePrice;
    }

    public String getOutstorePrice() {
        return OutstorePrice;
    }

    public void setOutstorePrice(String outstorePrice) {
        OutstorePrice = outstorePrice;
    }

    public String getMemberPrice() {
        return MemberPrice;
    }

    public void setMemberPrice(String memberPrice) {
        MemberPrice = memberPrice;
    }

    public String getBarCode() {
        return BarCode;
    }

    public void setBarCode(String barCode) {
        BarCode = barCode;
    }

    public float getChoose() {
        return choose;
    }

    public void setChoose(float choose) {
        this.choose = choose;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}
