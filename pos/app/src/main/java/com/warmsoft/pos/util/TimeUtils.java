package com.warmsoft.pos.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by brooks on 16/6/14.
 */
public class TimeUtils {

    private static TimeUtils instance = null;

    public static TimeUtils getInstance() {
        if (instance == null) {
            synchronized (TimeUtils.class) {
                instance = new TimeUtils();
            }
        }

        return instance;
    }

    public String formatYMD(Date date) {
        if (date == null) {
            return "1990/01/01";
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
        return sdf.format(date);
    }

    public String formatYMDHM(Date date) {
        if (date == null) {
            return "1990/01/01 01:01";
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm");
        return sdf.format(date);
    }

    public String formatYMDHMS(Date date) {
        if (date == null) {
            return "1990/01/01 01:01:01";
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        return sdf.format(date);
    }

    public String formatParmasData(Date date) {
        if (date == null) {
            return "1990-01-01 01:01:01";
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(date);
    }

    //前一天
    public Date beforeDate(Date nowDate) {
        Date beforeDate = new Date();
        Calendar calendar = Calendar.getInstance(); //得到日历
        calendar.setTime(nowDate);//把当前时间赋给日历
        calendar.add(Calendar.DAY_OF_MONTH, -1);  //设置为前一天
        beforeDate = calendar.getTime();   //得到前一天的时间
        return beforeDate;
    }

    /**
     * 字符串转换成日期
     *
     * @param str
     * @return date
     */
    public static Date StrToDate(String str) {

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = null;
        try {
            date = format.parse(str);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }


    public String getAgeByShow(int orgMonth) {

        Calendar mycalendar = Calendar.getInstance();//获取现在时间
        int nowyear = mycalendar.get(Calendar.YEAR);//获取年份
        int nowMonth = mycalendar.get(Calendar.MONTH) + 1;//获取年份

        int ret = orgMonth / 12;//13/12 = 1
        int retM = orgMonth % 12;//13%12 = 1

        if (ret <= 0) {//如果在12个月内
            if (nowMonth > orgMonth) {
                return String.valueOf(nowyear) + "-" + String.valueOf(processMonth(nowMonth - orgMonth)) + "-01";
            } else if (nowMonth <= orgMonth) {
                return String.valueOf(nowyear - 1) + "-" + String.valueOf(processMonth(nowMonth + 12 - orgMonth)) + "-01";
            }
        } else {
            if (nowMonth > retM) {
                return String.valueOf(nowyear - ret) + "-" + String.valueOf(processMonth(nowMonth - retM)) + "-01";
            } else if (nowMonth < retM) {
                return String.valueOf(nowyear - ret - 1) + "-" + String.valueOf(processMonth(nowMonth + 12 - retM)) + "-01";
            } else {
                return String.valueOf(nowyear - ret - 1) + "-" + String.valueOf(processMonth(nowMonth - retM + 12)) + "-01";
            }
        }

        return "2017-01-01";
    }

    public String getTimeDifference(String starTime) {
        int years = 0;
        int month = 0;
        try {
            String[] time = starTime.split("-");
            years = Integer.parseInt(time[0]);
            month = Integer.parseInt(time[1]);
        } catch (Exception e) {
            e.printStackTrace();
            years = 0;
            month = 0;
        }

        // 解析时间出错
        if (years == 0 || month == 0) {
            return 0 + "年" + "0" + "月";
        }

        Calendar mycalendar = Calendar.getInstance();//获取现在时间
        int nowyear = Integer.parseInt(String.valueOf(mycalendar.get(Calendar.YEAR)));//获取年份
        int nowMonth = Integer.parseInt(String.valueOf(mycalendar.get(Calendar.MONTH))) + 1;//获取年份

        // 出生日比当前日还大
        if (years > nowyear || (years == nowyear && month > nowMonth)) {
            return 0 + "年" + "0" + "月";
        }

        //出生日比当前日小
        if (month <= nowMonth) {
            if (nowyear - years > 1) {
                return (nowyear - years) + "年" + (nowMonth - month) + "月";
            } else {
                return (nowyear - years) + "年" + (nowMonth - month) + "月";
            }
        }

        if (month > nowMonth) {
            return (nowyear - years - 1) + "年" + (nowMonth + 12 - month) + "月";
        }

        return 0 + "年" + "0" + "月";
    }

    private static String processMonth(int retMonth) {
        if (retMonth < 10) {
            return "0" + retMonth;
        }

        return "" + retMonth;
    }
}
