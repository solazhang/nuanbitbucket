package com.warmsoft.pos.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.warmsoft.pos.R;
import com.warmsoft.pos.litepal.data.OrderInfoData;
import com.warmsoft.pos.util.TimeUtils;

import java.util.List;

/**
 * 作者: lijinliu on 2016/8/9.
 * 邮箱：jinliu.li@warmsoft.com
 * 描述: 会员待付款列表
 */
public class OrderSelecterAdapter extends BaseAdapter {

    protected Context mContext;
    List<OrderInfoData> datas;
    protected int orderSelected = -1;
    protected int colorWhite = 0;
    protected int colorBlue = 0;
    protected OrderInfoData orderData;

    public OrderSelecterAdapter(Context context, List<OrderInfoData> datas) {
        mContext = context;
        this.datas = datas;
        colorWhite = mContext.getResources().getColor(R.color.white);
        colorBlue = mContext.getResources().getColor(R.color.text_order_blue);
    }

    public void setData(List<OrderInfoData> datas) {
        orderSelected = -1;
        orderData = null;
        this.datas = datas;
        this.notifyDataSetChanged();
    }

    public void setSelect(int select) {
        this.orderSelected = select;
        if (datas != null && select < datas.size()) {
            this.orderData = datas.get(select);
        }
        this.notifyDataSetChanged();
    }

    public OrderInfoData getSelected() {
        return this.orderData;
    }

    @Override
    public int getCount() {
        if (datas == null) {
            return 0;
        } else {
            return datas.size();
        }
    }

    @Override
    public Object getItem(int position) {
        if (datas == null) {
            return null;
        } else {
            return datas.get(position);
        }
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @SuppressLint("ViewHolder")
    @Override
    public View getView(int position, View view, ViewGroup parent) {
        ViewHolder holde;
        if (view == null) {
            view = LayoutInflater.from(mContext).inflate(R.layout.item_member_waitpays, null);
            holde = new ViewHolder(view);
        } else {
            holde = (ViewHolder) view.getTag();
            if (holde == null) {
                view = LayoutInflater.from(mContext).inflate(R.layout.item_member_waitpays, null);
                holde = new ViewHolder(view);
            }
        }
        if (holde != null) {
            OrderInfoData item = (OrderInfoData) getItem(position);
            holde.bindData(item, position);
        }

        return view;
    }

    class ViewHolder {
        TextView tvOrderNo;
        TextView tvOrderAmount;
        TextView tvOrderTime;
        LinearLayout layoutBg;

        ViewHolder(View view) {
            tvOrderNo = (TextView) view.findViewById(R.id.tv_order_no);
            tvOrderAmount = (TextView) view.findViewById(R.id.tv_order_amount);
            tvOrderTime = (TextView) view.findViewById(R.id.tv_order_time);
            layoutBg = (LinearLayout) view.findViewById(R.id.layout_bg);
            view.setTag(this);
        }

        void bindData(final OrderInfoData data, final int position) {
            if (data == null) {
                return;
            } else {
                tvOrderNo.setText(data.getOrderNo() + "");
                tvOrderAmount.setText(data.getCountAmount() + "");
                tvOrderTime.setText(TimeUtils.getInstance().formatYMDHM(data.getOrderDate()) + "");
                if (orderSelected == position) {
                    layoutBg.setBackgroundColor(colorBlue);
                } else {
                    layoutBg.setBackgroundColor(colorWhite);
                }
            }
        }
    }
}
