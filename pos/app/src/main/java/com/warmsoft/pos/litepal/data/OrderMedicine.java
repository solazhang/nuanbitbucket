package com.warmsoft.pos.litepal.data;

import org.litepal.crud.DataSupport;

import java.io.Serializable;

/**
 * 作者: lijinliu on 2016/8/17.
 * 邮箱：jinliu.li@warmsoft.com
 * 描述: 定单商品数据
 */
public class OrderMedicine extends DataSupport implements Serializable {

    private int id;
    private int orderNo;//本地定单号
    private int goodsId;//商品ID
    private int OrgId;//机构ID
    private int Count;//数量,
    private float OutstorePrice;//价格
    private String DrugsName;//名称
    private long DrugType;//类型
    private float InstorePrice;//进价
    private float memberPrice;//会员价
    private boolean isSureed = false;//是否已下单
    private boolean isSelected = false;//

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(int orderNo) {
        this.orderNo = orderNo;
    }

    public int getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(int goodsId) {
        this.goodsId = goodsId;
    }

    public int getOrgId() {
        return OrgId;
    }

    public void setOrgId(int orgId) {
        OrgId = orgId;
    }

    public int getCount() {
        return Count;
    }

    public void setCount(int count) {
        Count = count;
    }

    public float getOutstorePrice() {
        return OutstorePrice;
    }

    public void setOutstorePrice(float outstorePrice) {
        OutstorePrice = outstorePrice;
    }

    public String getDrugsName() {
        return DrugsName;
    }

    public void setDrugsName(String drugsName) {
        DrugsName = drugsName;
    }

    public long getDrugType() {
        return DrugType;
    }

    public void setDrugType(long drugType) {
        DrugType = drugType;
    }

    public float getInstorePrice() {
        return InstorePrice;
    }

    public void setInstorePrice(float instorePrice) {
        InstorePrice = instorePrice;
    }

    public float getMemberPrice() {
        return memberPrice;
    }

    public void setMemberPrice(float memberPrice) {
        this.memberPrice = memberPrice;
    }

    public boolean isSureed() {
        return isSureed;
    }

    public void setIsSureed(boolean isSureed) {
        this.isSureed = isSureed;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setIsSelected(boolean isSelected) {
        this.isSelected = isSelected;
    }
}
