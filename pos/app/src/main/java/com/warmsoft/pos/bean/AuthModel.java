package com.warmsoft.pos.bean;

import java.io.Serializable;

/**
 * Created by brooks on 16/1/26.
 */
public class AuthModel implements Serializable {
    int code;
    String message;
    DataInfo Data;

    public static class DataInfo implements Serializable {
        String Token;
        UserInfo UserInfo;
        AnonymousCustomerInfo AnonymousCustomerInfo;

        public static class UserInfo implements Serializable {
            String Name;
            String LoginName;
            String Pwd;
            String RoleId;
            String IDNumber;
            String BirthDate;
            String PhoneNumber;
            String CreatingDate;
            String LastLoginDate;
            String OpenId;

            int Id;
            int OrgId;
            String OrgName;
            int Gender;
            int Status;
            int DefaultCard;

            public String getName() {
                return Name;
            }

            public void setName(String name) {
                Name = name;
            }

            public String getBirthDate() {
                return BirthDate;
            }

            public void setBirthDate(String birthDate) {
                BirthDate = birthDate;
            }

            public String getPhoneNumber() {
                return PhoneNumber;
            }

            public void setPhoneNumber(String phoneNumber) {
                PhoneNumber = phoneNumber;
            }

            public String getCreatingDate() {
                return CreatingDate;
            }

            public void setCreatingDate(String creatingDate) {
                CreatingDate = creatingDate;
            }

            public String getLastLoginDate() {
                return LastLoginDate;
            }

            public void setLastLoginDate(String lastLoginDate) {
                LastLoginDate = lastLoginDate;
            }

            public String getOpenId() {
                return OpenId;
            }

            public void setOpenId(String openId) {
                OpenId = openId;
            }

            public int getId() {
                return Id;
            }

            public void setId(int id) {
                Id = id;
            }

            public int getOrgId() {
                return OrgId;
            }

            public void setOrgId(int orgId) {
                OrgId = orgId;
            }

            public String getOrgName() {
                return OrgName;
            }

            public void setOrgName(String orgName) {
                OrgName = orgName;
            }

            public int getGender() {
                return Gender;
            }

            public void setGender(int gender) {
                Gender = gender;
            }

            public int getStatus() {
                return Status;
            }

            public void setStatus(int status) {
                Status = status;
            }

            public int getDefaultCard() {
                return DefaultCard;
            }

            public void setDefaultCard(int defaultCard) {
                DefaultCard = defaultCard;
            }

            public String getLoginName() {
                return LoginName;
            }

            public void setLoginName(String loginName) {
                LoginName = loginName;
            }

            public String getPwd() {
                return Pwd;
            }

            public void setPwd(String pwd) {
                Pwd = pwd;
            }

            public String getRoleId() {
                return RoleId;
            }

            public void setRoleId(String roleId) {
                RoleId = roleId;
            }

            public String getIDNumber() {
                return IDNumber;
            }

            public void setIDNumber(String IDNumber) {
                this.IDNumber = IDNumber;
            }
        }

        public static class AnonymousCustomerInfo implements Serializable {
            int AnonymousCustomerID;
            int AnonymousPetID;
            String AnonymousCustomerName;
            String AnonymousPetName;

            public int getAnonymousCustomerID() {
                return AnonymousCustomerID;
            }

            public void setAnonymousCustomerID(int anonymousCustomerID) {
                AnonymousCustomerID = anonymousCustomerID;
            }

            public int getAnonymousPetID() {
                return AnonymousPetID;
            }

            public void setAnonymousPetID(int anonymousPetID) {
                AnonymousPetID = anonymousPetID;
            }

            public String getAnonymousCustomerName() {
                return AnonymousCustomerName;
            }

            public void setAnonymousCustomerName(String anonymousCustomerName) {
                AnonymousCustomerName = anonymousCustomerName;
            }

            public String getAnonymousPetName() {
                return AnonymousPetName;
            }

            public void setAnonymousPetName(String anonymousPetName) {
                AnonymousPetName = anonymousPetName;
            }
        }

        public String getToken() {
            return Token;
        }

        public void setToken(String token) {
            Token = token;
        }

        public DataInfo.UserInfo getUserInfo() {
            return UserInfo;
        }

        public void setUserInfo(DataInfo.UserInfo userInfo) {
            UserInfo = userInfo;
        }

        public DataInfo.AnonymousCustomerInfo getAnonymousCustomerIonfo() {
            return AnonymousCustomerInfo;
        }

        public void setAnonymousCustomerIonfo(DataInfo.AnonymousCustomerInfo anonymousCustomerIonfo) {
            AnonymousCustomerInfo = anonymousCustomerIonfo;
        }
    }


    public DataInfo getData() {
        return Data;
    }

    public void setData(DataInfo data) {
        Data = data;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
