package com.warmsoft.pos;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.ViewGroup;
import android.widget.TextView;

import com.warmsoft.base.BasicActivity;
import com.warmsoft.base.MyApplication;
import com.warmsoft.pos.bean.AuthModel;
import com.warmsoft.pos.dialog.CancellationDialog;
import com.warmsoft.pos.dialog.NomelNoteDialog;
import com.warmsoft.pos.fragment.main.FragmentPos;
import com.warmsoft.pos.fragment.main.FragmentUpdate;
import com.warmsoft.pos.fragment.main.FragmentVip;
import com.warmsoft.pos.layout.view.LayoutActivityMain;
import com.warmsoft.pos.service.SyncDataService;
import com.warmsoft.pos.util.OAuthUtils;

/**
 * 作者: lijinliu on 2016/8/5.
 * 邮箱：jinliu.li@warmsoft.com
 * 描述: 收银
 */
public class MainActivity extends BasicActivity implements LayoutActivityMain.LayoutMainListener {
    private Context mContext;
    private LayoutActivityMain mLayout;
    private PagerAdapter mAdapter;
    private FragmentVip fragmentVip;
    private FragmentPos fragmentPos;
    private int keyCode;
    private KeyEvent event;

    private TextView tvUserInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((MyApplication) getApplication()).pushActivity(this);
        setContentView(R.layout.activity_main);

        init();
    }

    private void init() {
        tvUserInfo = (TextView) findViewById(R.id.tv_main_tab_userinfo);
        AuthModel.DataInfo.UserInfo userInfo = OAuthUtils.getInstance().getUserinfo(mContext);
        if (userInfo == null) {
            exitWarmPos();
            return;
        }

        String userName = OAuthUtils.getInstance().getUserinfo(mContext).getName();
        if (!TextUtils.isEmpty(userName)) {
            tvUserInfo.setText(userName);
        }

        mContext = this.getApplicationContext();
        mLayout = new LayoutActivityMain(this, this);
        initPager();

        Intent intent = new Intent(this, SyncDataService.class);
        intent.putExtra(SyncDataService.RUN_KEY, SyncDataService.UPLOAD_ORDER_DATAS);//不在同步数据，只启数据上传任务
        startService(intent);
    }

    /**
     * 在 MainActivity中调用，防止null
     **/
    public void initPager() {
        mAdapter = new PagerAdapter(this.getSupportFragmentManager());
        mLayout.mPager.setAdapter(mAdapter);
    }

    class PagerAdapter extends FragmentPagerAdapter {
        public PagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int positon) {
            switch (positon) {
                case 0:
                    fragmentPos = FragmentPos.newInstance();
                    return fragmentPos;
                case 1:
                    fragmentVip = FragmentVip.newInstance();
                    return fragmentVip;
                case 2:
                    return FragmentUpdate.newInstance();
                default:
                    return FragmentVip.newInstance();
            }
        }

        @Override
        public int getCount() {
            return 3;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            //super.destroyItem(container, position, object);
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            Fragment f = (Fragment) super.instantiateItem(container, position);
            return f;
        }

        @Override
        public int getItemPosition(Object object) {
            // return super.getItemPosition(object);
            return POSITION_NONE;
        }
    }

    @Override
    public void tabClick(int id) {
        switch (id) {
            case R.id.tv_main_tab_userinfo:
                new CancellationDialog(this);
                break;

            case R.id.tv_main_tab_pos:
                mLayout.mPager.setCurrentItem(0, false);
                if (fragmentPos != null) {
                    fragmentPos.isNewMember();//是否是新增会员
                }
                break;

            case R.id.tv_main_tab_vip:
                mLayout.mPager.setCurrentItem(1, false);
//                if (fragmentVip != null) {
//                    fragmentVip.initListView();
//                }
                break;

            case R.id.tv_main_tab_update:
                mLayout.mPager.setCurrentItem(2, false);
                break;
        }
    }

    public void exitWarmPos() {
        LoginActivity.startActivity(this);
        this.finish();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            new NomelNoteDialog(MainActivity.this, "取消", "确定要退出吗？", "确认", new NomelNoteDialog.DialogListener() {
                @Override
                public void noteDialogConfirm() {
                    finish();
                }
            });
            return true;
        } else if (keyCode == KeyEvent.KEYCODE_MENU) {
            return true;
        } else if (keyCode == KeyEvent.KEYCODE_ENTER) {
            return true;
        }

        return super.onKeyDown(keyCode, event);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ((MyApplication) getApplication()).popActivity(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    public static void startActivity(Context context) {
        try {
            Intent intent = new Intent(context, MainActivity.class);
            context.startActivity(intent);
        } catch (Exception e) {
        }
    }

}