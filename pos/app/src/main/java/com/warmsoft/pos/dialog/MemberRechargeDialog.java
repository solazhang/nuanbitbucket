package com.warmsoft.pos.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.warmsoft.pos.R;
import com.warmsoft.pos.util.Util;


public class MemberRechargeDialog implements View.OnClickListener {

    private Dialog mDialog;
    private DialogListener mListenr;
    private Activity mActivity;
    private LayoutInflater mInflater;
    private String titleStr;
    private String msgStr;
    private String confirmStr;

    private TextView tvTtile;
    private TextView tvMsg;
    private TextView btnCancle;
    private TextView btnConfirm;

    public MemberRechargeDialog(Activity activity, String title, String msg, String confirm, DialogListener listener) {
        this.mActivity = activity;
        this.mInflater = LayoutInflater.from(mActivity);
        this.titleStr = title;
        this.msgStr = msg;
        this.confirmStr = confirm;
        this.mListenr = listener;
        createLoadingDialog(mActivity);
    }

    /**
     * 得到自定义的progressDialog
     *
     * @param context
     * @return
     */
    private void createLoadingDialog(Context context) {
        View rootView = mInflater.inflate(R.layout.dialog_member_recharge, null);
        LinearLayout layout = (LinearLayout) rootView.findViewById(R.id.dialog_view);
        initView(rootView);
        mDialog = new Dialog(context, R.style.fullScreenDialog);
        mDialog.setCancelable(true);
        int MATH_PARENT = LinearLayout.LayoutParams.MATCH_PARENT;
        mDialog.setContentView(layout, new LinearLayout.LayoutParams(MATH_PARENT, MATH_PARENT));// 设置布局
        mDialog.show();
    }

    private void initView(View rootView) {
        tvTtile = (TextView) rootView.findViewById(R.id.tv_title);
        tvMsg = (TextView) rootView.findViewById(R.id.tv_msg);
        tvTtile.setText(titleStr);
        tvMsg.setText(msgStr);
        btnConfirm = (TextView) rootView.findViewById(R.id.btn_confirm);
        btnCancle = (TextView) rootView.findViewById(R.id.btn_cancle);
        btnConfirm.setOnClickListener(this);
        btnCancle.setOnClickListener(this);
        if (!Util.isNullStr(confirmStr)) {
            btnConfirm.setText(confirmStr);
        }
    }

    @Override
    public void onClick(View view) {
        if (view == btnCancle) {
            dismiss();
        } else if (view == btnConfirm) {
            mDialog.dismiss();
            if (mListenr != null) {
                mListenr.noteDialogConfirm();
            }
        }
    }


    public void dismiss() {
        if (mDialog != null) {
            mDialog.dismiss();
            mDialog = null;
        }
    }

    public interface DialogListener {
        void noteDialogConfirm();
    }
}
