package com.warmsoft.pos.util;

/**
 * 作者: lijinliu on 2016/8/5.
 * 邮箱：jinliu.li@warmsoft.com
 * 描述: 常量
 */

public class Constant {
    public static final String AMOUNT = "%1$s元";
    public static final String ORDER_PAY_INFOA = "(实收%1$s,找零%2$s)";
    public static final String ORDER_PAY_INFOB = "(实收%1$s,抹零%2$s)";
    public static final String ADV_WORDS = "小暖医生，科技让工作简单";
    public static final String GOODSIZE = "%1$s份";
    public static final String GOODPRICE = "%1$s元/份";
    public static final String ORDER_NEED_COLLECTION_AMOUNT = "还需收款：%1$s元";
}
