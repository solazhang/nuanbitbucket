package com.warmsoft.pos.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.warmsoft.pos.R;
import com.warmsoft.pos.adapter.CustomeSelecterAdapter;
import com.warmsoft.pos.adapter.OrderSelecterAdapter;
import com.warmsoft.pos.adapter.PetSelecterAdapter;
import com.warmsoft.pos.bean.AuthModel;
import com.warmsoft.pos.fragment.main.FragmentPos;
import com.warmsoft.pos.litepal.data.CustomerList;
import com.warmsoft.pos.litepal.data.LitepalHelper;
import com.warmsoft.pos.litepal.data.OrderInfoData;
import com.warmsoft.pos.litepal.data.PetData;
import com.warmsoft.pos.util.OAuthUtils;
import com.warmsoft.pos.util.OrderHelper;
import com.warmsoft.pos.util.Util;

import org.litepal.crud.DataSupport;

import java.util.ArrayList;
import java.util.List;

/**
 * 作者: lijinliu on 2016/8/5.
 * 邮箱：jinliu.li@warmsoft.com
 * 描述: 当前会员待付款
 */
public class MemberWaitPaysDialog implements View.OnClickListener {

    private FragmentPos mFragmentPos;
    private Dialog mDialog;
    private Activity mActivity;
    private OrderHelper mOrderHelper;
    private LayoutInflater mInflater;
    private int mCustomeId;

    private TextView tvBtnCancle;
    private TextView tvBtnCofirm;
    private TextView tvNoRecoredNote;
    private TextView tvWaitpayTitle;

    private ListView listview;
    OrderSelecterAdapter mAdapter;
    List<OrderInfoData> mOrderList;

    private CustomerList customer;//选中的待付款

    public MemberWaitPaysDialog(Activity activity, FragmentPos fragmentPos, int customeId) {
        this.mActivity = activity;
        this.mFragmentPos = fragmentPos;
        this.mInflater = LayoutInflater.from(mActivity);
        this.mOrderHelper = OrderHelper.getInstance(mActivity);
        this.mCustomeId = customeId;
        createLoadingDialog(mActivity);
    }

    /**
     * 得到自定义的progressDialog
     *
     * @param context
     * @return
     */
    private void createLoadingDialog(Context context) {
        View rootView = mInflater.inflate(R.layout.dialog_waitpay_select, null);
        LinearLayout layout = (LinearLayout) rootView.findViewById(R.id.dialog_view);
        initView(rootView);
        initEvents();
        mDialog = new Dialog(context, R.style.fullScreenDialog);
        mDialog.setCancelable(false);
        int MATH_PARENT = LinearLayout.LayoutParams.MATCH_PARENT;
        mDialog.setContentView(layout, new LinearLayout.LayoutParams(MATH_PARENT, MATH_PARENT));// 设置布局
        mDialog.show();

        initDate();//已选会员数据
    }

    private void initView(View rootView) {
        tvBtnCancle = (TextView) rootView.findViewById(R.id.tv_btn_cancle);
        tvBtnCofirm = (TextView) rootView.findViewById(R.id.btn_select_confirm);
        tvNoRecoredNote = (TextView) rootView.findViewById(R.id.tv_no_recored);
        tvWaitpayTitle = (TextView) rootView.findViewById(R.id.tv_member_waitpay_title);

        listview = (ListView) rootView.findViewById(R.id.listview_orders);
        mAdapter = new OrderSelecterAdapter(mActivity, mOrderList);
        listview.setAdapter(mAdapter);
    }

    private void initEvents() {
        tvBtnCancle.setOnClickListener(this);
        tvBtnCofirm.setOnClickListener(this);
        listview.setOnItemClickListener(petListClick);
    }

    public void initDate() {
        CustomerList custom = LitepalHelper.getInstance().queryCustomInfo(mCustomeId + "");
        if (custom != null) {
            tvWaitpayTitle.setText(custom.getName() + "的待付款");
        } else {
            tvWaitpayTitle.setText("无相应会员");
        }

        mOrderList = LitepalHelper.getInstance().getOrderListByCustomId(mCustomeId, mOrderHelper.getOrderData().getOrderNo());
        if (mOrderList != null && mOrderList.size() > 0) {
            listview.setVisibility(View.VISIBLE);
            tvNoRecoredNote.setVisibility(View.GONE);

            mAdapter.setData(mOrderList);
            for (int i = 0; i < mOrderList.size(); i++) {
                if (mOrderList.get(i).getOrderNo() == mOrderHelper.getOrderData().getOrderNo()) {
                    mAdapter.setSelect(i);
                    break;
                }
            }

        } else {
            listview.setVisibility(View.GONE);
            tvNoRecoredNote.setVisibility(View.VISIBLE);
        }
    }


    AdapterView.OnItemClickListener petListClick = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
            mAdapter.setSelect(i);
        }
    };


    @Override
    public void onClick(View view) {
        if (view == tvBtnCofirm) {
            if (mAdapter.getSelected() == null) {
                Util.showToastTop(mActivity, "请选择待一笔付款!");
            } else {
                dismiss();
                OrderInfoData selected = mAdapter.getSelected();
                if (selected.getOrderNo() != mOrderHelper.getOrderData().getOrderNo()) {
                    mFragmentPos.settleListClick(selected);
                }
            }
        } else if (view == tvBtnCancle) {
            dismiss();
        }
    }

    public void dismiss() {
        if (mDialog != null) {
            mDialog.dismiss();
            mDialog = null;
        }
    }
}
