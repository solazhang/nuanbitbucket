package com.warmsoft.pos.util;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.TextView;

/**
 * Created by sangxiewu on 16/12/20.
 */
public class DrawableUtil {
    private final static int left = 1;
    private final static int top = 2;
    private final static int right = 3;
    private final static int bottom = 4;

    private static DrawableUtil instance = null;

    public static DrawableUtil getInstance() {
        if (instance == null) {
            synchronized (DrawableUtil.class) {
                instance = new DrawableUtil();
            }
        }

        return instance;
    }

    public void setDrawableOriType(Context mContext, int type, View view, int resId) {
        Drawable drawable = mContext.getResources().getDrawable(resId);

        switch (type) {
            case left:
                if (drawable != null) {
                    drawable.setBounds(0, 0, AppUtils.dip2px(mContext, 20), AppUtils.dip2px(mContext, 20));
                    ((TextView) view).setCompoundDrawables(drawable, null, null, null);
                }
                break;

            case top:
                if (drawable != null) {
                    drawable.setBounds(0, 0, AppUtils.dip2px(mContext, 20), AppUtils.dip2px(mContext, 20));
                    ((TextView) view).setCompoundDrawables(null, drawable, null, null);
                }
                break;

            case right:
                if (drawable != null) {
                    drawable.setBounds(0, 0, AppUtils.dip2px(mContext, 20), AppUtils.dip2px(mContext, 20));
                    ((TextView) view).setCompoundDrawables(null, null, drawable, null);
                }
                break;

            case bottom:
                if (drawable != null) {
                    drawable.setBounds(0, 0, AppUtils.dip2px(mContext, 20), AppUtils.dip2px(mContext, 20));
                    ((TextView) view).setCompoundDrawables(null, null, null, drawable);
                }
                break;
        }
    }


}
