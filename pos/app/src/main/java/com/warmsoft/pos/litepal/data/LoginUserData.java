package com.warmsoft.pos.litepal.data;

import org.litepal.crud.DataSupport;

/**
 * 作者: lijinliu on 2016/8/5.
 * 邮箱：jinliu.li@warmsoft.com
 * 描述: 登录用户信息缓存
 */
public class LoginUserData extends DataSupport {

    private int id;
    private String signName;
    private String signPwd;
    private String token;
    private long expiredTime;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSignName() {
        return signName;
    }

    public void setSignName(String signName) {
        this.signName = signName;
    }

    public String getSignPwd() {
        return signPwd;
    }

    public void setSignPwd(String signPwd) {
        this.signPwd = signPwd;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public long getExpiredTime() {
        return expiredTime;
    }

    public void setExpiredTime(long expiredTime) {
        this.expiredTime = expiredTime;
    }
}
