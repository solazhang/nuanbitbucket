package com.warmsoft.pos.dialog;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.hardware.usb.UsbManager;
import android.net.DhcpInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.print.demo.BluetoothOperation;
import com.android.print.demo.IPrinterOpertion;
import com.android.print.demo.UsbOperation;
import com.android.print.demo.WifiOperation;
import com.android.print.demo.utils.FileUtils;
import com.android.print.demo.utils.PrintUtils;
import com.android.print.sdk.PrinterConstants;
import com.android.print.sdk.PrinterInstance;
import com.android.print.sdk.wifi.WifiAdmin;
import com.warmsoft.pos.R;

/**
 * 作者: lijinliu on 2016/8/5.
 * 邮箱：jinliu.li@warmsoft.com
 * 描述: 数据加载，联网dialog
 */
public class PrintDialog implements View.OnClickListener {

    private Dialog mDialog;
    private Context mContext;
    private LayoutInflater mInflater;

    protected static IPrinterOpertion myOpertion;
    private static boolean isConnected;
    private PrinterInstance mPrinter;
    private boolean is58mm = true;

    private Button btnPrint;
    private Button btnCancle;
    private TextView conn_state;
    private TextView conn_address;
    private TextView conn_name;

    public PrintDialog(Activity activity) {
        this.mContext = activity;
        this.mInflater = LayoutInflater.from(mContext);
        createLoadingDialog(mContext);
        new FileUtils().createFile(activity);//向SD卡上写入打印文件
    }

    /**
     * 得到自定义的progressDialog
     *
     * @param context
     * @return
     */
    private void createLoadingDialog(Context context) {
        View rootView = mInflater.inflate(R.layout.dialog_print_note, null);
        LinearLayout layout = (LinearLayout) rootView.findViewById(R.id.dialog_view);
        initView(rootView);
        mDialog = new Dialog(context, R.style.loadingdialog);
        mDialog.setCancelable(false);
        int MATH_PARENT = LinearLayout.LayoutParams.MATCH_PARENT;
        mDialog.setContentView(layout, new LinearLayout.LayoutParams(MATH_PARENT, MATH_PARENT));// 设置布局
        mDialog.show();
    }

    private void initView(View rootView) {
        btnPrint = (Button) rootView.findViewById(R.id.btn_print_confirm);
        btnCancle = (Button) rootView.findViewById(R.id.btn_print_cancle);
        btnPrint.setOnClickListener(this);
        btnCancle.setOnClickListener(this);

        conn_state = (TextView) rootView.findViewById(R.id.connect_state);
        conn_state.setOnClickListener(this);
        conn_name = (TextView) rootView.findViewById(R.id.connect_name);
        conn_name.setOnClickListener(this);
        conn_address = (TextView) rootView.findViewById(R.id.connect_address);
        conn_address.setOnClickListener(this);
        openConn();
    }

    private void updateButtonState() {
        if (!isConnected) {
            conn_address.setText(R.string.no_conn_address);
            conn_state.setText(R.string.connect);
            conn_name.setText(R.string.no_conn_name);

        } else {
            conn_address.setText(mContext.getResources().getString(R.string.disconnect));
            conn_state.setText(R.string.disconnect);
            conn_name.setText(mContext.getResources().getString(R.string.disconnect));
        }

    }

    @Override
    public void onClick(View view) {
        if (view == btnCancle) {
            dismiss();
        } else if (view == conn_state || view == conn_address || view == conn_name) {
            openConnDialog();
        } else if (view == btnPrint) {
            PrintUtils.printNote(mContext.getResources(), mPrinter, is58mm);
        }
    }

    private void openConnDialog() {
        if (!isConnected) {
            new AlertDialog.Builder(mContext).setTitle(com.android.print.demo.R.string.str_message)
                    .setMessage(com.android.print.demo.R.string.str_connlast)
                    .setPositiveButton(com.android.print.demo.R.string.yesconn, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface arg0, int arg1) {
                            openConn();
                        }
                    })
                    .setNegativeButton(com.android.print.demo.R.string.str_resel, new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            myOpertion = new UsbOperation(mContext, mHandler);
                            myOpertion.chooseDevice();
                        }

                    })
                    .show();
        } else {
            myOpertion.close();
            myOpertion = null;
            mPrinter = null;
        }

    }
    private void openConn(){
        myOpertion = new UsbOperation(mContext, mHandler);
        UsbManager manager = (UsbManager) mContext.getSystemService(Context.USB_SERVICE);
        myOpertion.usbAutoConn(manager);
    }


    //用于接受连接状态消息的 Handler
    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case PrinterConstants.Connect.SUCCESS:
                    isConnected = true;
                    mPrinter = myOpertion.getPrinter();
                    break;
                case PrinterConstants.Connect.FAILED:
                    isConnected = false;
                    Toast.makeText(mContext, com.android.print.demo.R.string.conn_failed,
                            Toast.LENGTH_SHORT).show();
                    break;
                case PrinterConstants.Connect.CLOSED:
                    isConnected = false;
                    Toast.makeText(mContext, com.android.print.demo.R.string.conn_closed, Toast.LENGTH_SHORT).show();
                    break;
                case PrinterConstants.Connect.NODEVICE:
                    isConnected = false;
                    Toast.makeText(mContext, com.android.print.demo.R.string.conn_no, Toast.LENGTH_SHORT).show();
                    break;

                default:
                    break;
            }
            updateButtonState();
        }

    };

    public void dismiss() {
        if (mDialog != null) {
            mDialog.dismiss();
            mDialog = null;
        }
    }
}
