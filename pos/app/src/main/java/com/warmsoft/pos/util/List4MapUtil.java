package com.warmsoft.pos.util;

import com.warmsoft.pos.litepal.data.CustomerList;
import com.warmsoft.pos.litepal.data.PetData;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by brooks on 16/9/13.
 */
public class List4MapUtil {

    private static List4MapUtil instance = null;

    public static List4MapUtil getInstance() {
        if (instance == null) {
            synchronized (List4MapUtil.class) {
                instance = new List4MapUtil();
            }
        }

        return instance;
    }

    /**
     * @param
     * @return Map<Key,List>
     * @description 实现将一个List转化为Map<Key,List>的样式
     */
    public Map<String, CustomerList> list4Map(List<CustomerList> list) {
        Map<String, CustomerList> map = new HashMap<>();

        if ((list != null) && (list.size() != 0)) {
            for (CustomerList dataInfo : list) {
                String customerId = dataInfo.getCustomerId();
                map.put(customerId, dataInfo);
            }
        }

        return map;
    }

    //更新有属性变更的数据
    public List<CustomerList> getDifferentMap(Map<String, CustomerList> localMaps, Map<String, CustomerList> netMaps) {
        List<CustomerList> customerLists = new ArrayList<>();
        for (Map.Entry<String, CustomerList> customer : netMaps.entrySet()) {
            if (localMaps.get(customer.getKey()) == null) { //证明localmaps中无该项
                customerLists.add(customer.getValue());
            } else {
                CustomerList loc = localMaps.get(customer.getKey());
                CustomerList net = customer.getValue();

                LogUtil.error("CustomerList loc " + loc.getTimeStamp());
                LogUtil.error("CustomerList net " + net.getTimeStamp());

                if (loc.getTimeStamp() != null && !loc.getTimeStamp().equalsIgnoreCase(net.getTimeStamp())) {//判断属性有变更
                    //数据库中进行更新
                    net.update(loc.getID());
                }
            }
        }

        return customerLists;
    }

    //获取两个集合的并集
    public List<CustomerList> getUpdateCollect(Map<String, CustomerList> localMaps, Map<String, CustomerList> netMaps) {
        List<CustomerList> customerLists = new ArrayList<>();
        for (Map.Entry<String, CustomerList> customer : netMaps.entrySet()) {//对网络请求的遍历
            if (localMaps.get(customer.getKey()) == null) { //证明localmaps中无该项
                customerLists.add(customer.getValue());
            } else {
                CustomerList loc = localMaps.get(customer.getKey()); //localmaps中有,需要更新
                CustomerList net = customer.getValue();

                LogUtil.error("CustomerList loc " + loc.getTimeStamp());
                LogUtil.error("CustomerList net " + net.getTimeStamp());

                if (loc.getTimeStamp() != null && !loc.getTimeStamp().equalsIgnoreCase(net.getTimeStamp())) {//判断属性有变更
                    customerLists.add(net);
                } else { //属性没有变更,直接接入到系统中
                    customerLists.add(loc);
                }
            }
        }

        for (int i = 0; i < customerLists.size(); i++) {
            LogUtil.error("customerLists i = " + i + ", customerId = " + customerLists.get(i).getCustomerId());
        }


        return customerLists;
    }

    public Map<Integer, PetData> list4PetMap(List<PetData> list) {
        Map<Integer, PetData> map = new HashMap<>();

        if ((list != null) && (list.size() != 0)) {
            for (PetData dataInfo : list) {
                int petId = dataInfo.getPetId();
                map.put(petId, dataInfo);
            }
        }

        return map;
    }

    public List<PetData> getDifferentPetsMap(Map<Integer, PetData> localMaps, Map<Integer, PetData> netMaps) {
        List<PetData> petDatas = new ArrayList<>();
        for (Map.Entry<Integer, PetData> customer : netMaps.entrySet()) {
            if (localMaps.get(customer.getKey()) == null) { //证明localmaps中无该项
                petDatas.add(customer.getValue());
            }
        }

        return petDatas;
    }
}
