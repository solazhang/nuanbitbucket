package com.warmsoft.pos.litepal.data;

import org.litepal.crud.DataSupport;

/**
 * 用户会员卡信息
 */
public class CustomeCardList extends DataSupport {
    private int id;
    private int OrgId;//": 7997,
    private int CustomerId;//": 0,
    private double TotalAmount;//": 0,
    private double ActlyAmount;//": 0,
    private int CardId;//": 28116,
    private int CardNo;//": "66995577",
    private int Operator;//": 0,
    private int CardType;//": 2812,
    private double CardBalance;//": 1626.53,

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getOrgId() {
        return OrgId;
    }

    public void setOrgId(int orgId) {
        OrgId = orgId;
    }

    public int getCustomerId() {
        return CustomerId;
    }

    public void setCustomerId(int customerId) {
        CustomerId = customerId;
    }

    public double getTotalAmount() {
        return TotalAmount;
    }

    public void setTotalAmount(double totalAmount) {
        TotalAmount = totalAmount;
    }

    public double getActlyAmount() {
        return ActlyAmount;
    }

    public void setActlyAmount(double actlyAmount) {
        ActlyAmount = actlyAmount;
    }

    public int getCardId() {
        return CardId;
    }

    public void setCardId(int cardId) {
        CardId = cardId;
    }

    public int getCardNo() {
        return CardNo;
    }

    public void setCardNo(int cardNo) {
        CardNo = cardNo;
    }

    public int getOperator() {
        return Operator;
    }

    public void setOperator(int operator) {
        Operator = operator;
    }

    public int getCardType() {
        return CardType;
    }

    public void setCardType(int cardType) {
        CardType = cardType;
    }

    public double getCardBalance() {
        return CardBalance;
    }

    public void setCardBalance(double cardBalance) {
        CardBalance = cardBalance;
    }
}
