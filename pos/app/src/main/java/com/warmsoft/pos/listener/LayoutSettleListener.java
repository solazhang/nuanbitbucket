package com.warmsoft.pos.listener;

/**
 * 作者: lijinliu on 2016/8/30.
 * 邮箱：jinliu.li@warmsoft.com
 * 描述: LayoutSettlement事件监听
 */
public interface LayoutSettleListener {
    void continueCollect();
    void moling();
    void print();
    void clearAllPays();
    void swingMember();
    void payConfirm(String orderNo);
    void settleOrder();
    void collectMoney(int payChanel);
    void keepingAccount();
}
