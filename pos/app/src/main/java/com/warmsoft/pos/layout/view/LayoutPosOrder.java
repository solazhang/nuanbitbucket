package com.warmsoft.pos.layout.view;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.print.demo.sunmi.AidlUtil;
import com.warmsoft.base.MyApplication;
import com.warmsoft.pos.R;
import com.warmsoft.pos.adapter.OrderAdapter;
import com.warmsoft.pos.dialog.KillOrderDialog;
import com.warmsoft.pos.dialog.MemberWaitPaysDialog;
import com.warmsoft.pos.dialog.NomelNoteDialog;
import com.warmsoft.pos.enumutil.OrderStatusEnum;
import com.warmsoft.pos.enumutil.PayChanel;
import com.warmsoft.pos.fragment.main.FragmentPos;
import com.warmsoft.pos.litepal.data.GoodItemInfo;
import com.warmsoft.pos.litepal.data.LitepalHelper;
import com.warmsoft.pos.litepal.data.OrderGoodsData;
import com.warmsoft.pos.litepal.data.OrderInfoData;
import com.warmsoft.pos.litepal.data.PayAmountData;
import com.warmsoft.pos.print.PrintUtil;
import com.warmsoft.pos.print.SunMiPrintUtils;
import com.warmsoft.pos.util.Constant;
import com.warmsoft.pos.util.OrderHelper;
import com.warmsoft.pos.util.TimeUtils;
import com.warmsoft.pos.util.Util;

import java.util.List;

/**
 * 作者: lijinliu on 2016/8/9.
 * 邮箱：jinliu.li@warmsoft.com
 * 描述: 对应 view_pos_order.xml
 */
public class LayoutPosOrder implements View.OnClickListener, AdapterView.OnItemClickListener {

    private OrderHelper orderHelper;
    private Activity mActivity;
    private FragmentPos fragmentPos;
    private Context mContext;
    public OrderAdapter adapter;

    public TextView tvOrderNumValue;//定单单号
    public TextView tvMemberNum;//会员卡号
    public TextView tvMemberName;//会员名称
    public TextView tvPetName;//宠物名称

    public TextView tvOrderTime;//定单日期
    public TextView tvOrderSize;//定单项
    public TextView tvOrderAmount;//定单金额
    public TextView tvOrderRecevie;//应收
    public TextView tvOrderActual;//实收
    public TextView tvBadgeView;
    protected RelativeLayout mLyaoutWaitPays;
    public ListView listView;

    public Button btnNewOrder;
    public Button btnCancelOrder;
    public Button btnPostOrder;
    public Button btnPrintOrder;

    public ImageView ivOrderStatus;
    private LinearLayout delLayout; //显示删除选项控件
    private LinearLayout orderLayout, backOrDelLayout;
    private Button btnBack;
    private Button btnDel;

    public LayoutPosOrder(View rootView, Activity activity, FragmentPos fragment) {
        this.mActivity = activity;
        this.fragmentPos = fragment;
        this.mContext = mActivity.getApplicationContext();

        tvOrderNumValue = (TextView) rootView.findViewById(R.id.tv_order_num_value);
        tvMemberName = (TextView) rootView.findViewById(R.id.tv_member_name);
        tvMemberNum = (TextView) rootView.findViewById(R.id.tv_member_num);
        tvPetName = (TextView) rootView.findViewById(R.id.tv_order_pet_name);

        tvOrderTime = (TextView) rootView.findViewById(R.id.tv_order_time);
        tvOrderSize = (TextView) rootView.findViewById(R.id.tv_order_list_size);
        tvOrderAmount = (TextView) rootView.findViewById(R.id.tv_order_list_amount);
        tvOrderRecevie = (TextView) rootView.findViewById(R.id.tv_order_recevie_value);
        tvOrderActual = (TextView) rootView.findViewById(R.id.tv_order_actual_value);
        tvBadgeView = (TextView) rootView.findViewById(R.id.id_for_order_badge_view);
        mLyaoutWaitPays = (RelativeLayout) rootView.findViewById(R.id.layout_member_waitpays);
        listView = (ListView) rootView.findViewById(R.id.list_orderlist);

        btnNewOrder = (Button) rootView.findViewById(R.id.btn_new_order);
        btnCancelOrder = (Button) rootView.findViewById(R.id.btn_cancel_order);
        btnPostOrder = (Button) rootView.findViewById(R.id.btn_post_order);
        btnPrintOrder = (Button) rootView.findViewById(R.id.btn_print_order);
        ivOrderStatus = (ImageView) rootView.findViewById(R.id.iv_order_stauts);
        orderLayout = (LinearLayout) rootView.findViewById(R.id.order_id);
        backOrDelLayout = (LinearLayout) rootView.findViewById(R.id.del_id);
        btnBack = (Button) rootView.findViewById(R.id.btn_back_id);
        btnDel = (Button) rootView.findViewById(R.id.btn_delete_id);

        delLayout = (LinearLayout) rootView.findViewById(R.id.layout_order_batch);

//        if (PrintUtil.getInstance().getConnectStatus()) {
        if (SunMiPrintUtils.getInstance().getConnectStatus()) {
            btnPrintOrder.setBackground(mContext.getResources().getDrawable(R.drawable.warmsoft_btn_print_order));
        } else {
            btnPrintOrder.setBackground(mContext.getResources().getDrawable(R.mipmap.warmpos_print_ressdown));
        }
        adapter = new OrderAdapter(mContext, OrderHelper.getInstance(mContext).getOrderGoods(), false);
        listView.setAdapter(adapter);
        initEvents();
    }

    private void initEvents() {
        btnNewOrder.setOnClickListener(this);
        btnCancelOrder.setOnClickListener(this);
        btnPostOrder.setOnClickListener(this);
        btnPrintOrder.setOnClickListener(this);
        listView.setOnItemClickListener(this);
        delLayout.setOnClickListener(this);
        btnBack.setOnClickListener(this);
        btnDel.setOnClickListener(this);
        mLyaoutWaitPays.setOnClickListener(this);
    }


    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        OrderGoodsData om = adapter.getSelected(i);
        if (om != null) {
            fragmentPos.orderGoodClick(om);
        }
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_new_order:
                //new NewOrderPetSelectDialog(mActivity);
                newOrderCheck();
                break;
            case R.id.btn_cancel_order:
                if (refundOrderCheck()) {
                    new KillOrderDialog(mActivity, new KillOrderDialog.DialogListener() {
                        @Override
                        public void keyConfirm() {
                            refundOrder();
                        }
                    });
                }
                break;
            case R.id.btn_post_order:
                postOrder();
                break;
            case R.id.btn_print_order:
//                if (PrintUtil.getInstance().getConnectStatus()) {
                if (SunMiPrintUtils.getInstance().getConnectStatus()) {
//                    PrintUtil.getInstance().printOrder(mActivity);
                    SunMiPrintUtils.getInstance().printOrder(mActivity);
                    btnPrintOrder.setBackground(mContext.getResources().getDrawable(R.drawable.warmsoft_btn_print_order));
                } else {
//                    PrintUtil.getInstance().initPrinter(mActivity);
                    AidlUtil.getInstance().connectPrinterService(MyApplication.getInstance().getApplicationContext());
                    btnPrintOrder.setBackground(mContext.getResources().getDrawable(R.mipmap.warmpos_print_ressdown));
                }
                break;
            case R.id.layout_order_batch:
                showCheckBox(!adapter.getFlag());
                break;
            case R.id.btn_back_id:
                showCheckBox(false);
                break;
            case R.id.btn_delete_id:
                showDialog();
                break;
            case R.id.layout_member_waitpays://会员代付款
                OrderInfoData orderData = OrderHelper.getInstance(mActivity).getOrderData();
                if (orderData != null && orderData.getCustomerId() > 0) {
                    int customeId = orderData.getCustomerId();
                    new MemberWaitPaysDialog(mActivity, fragmentPos, customeId);
                } else {
                    new NomelNoteDialog(mActivity, "无会员", "请选择会员!", "确定", null);
                }
                break;
        }
    }

    private void showCheckBox(boolean show) {
        orderHelper = OrderHelper.getInstance(mContext);
        if (orderHelper.getUnSureGoodSize() > 0) {
            adapter.setFlag(show);
            adapter.notifyDataSetChanged();
            if (show) {
                orderLayout.setVisibility(View.GONE);
                backOrDelLayout.setVisibility(View.VISIBLE);
            } else {
                orderLayout.setVisibility(View.VISIBLE);
                backOrDelLayout.setVisibility(View.GONE);
            }
        } else {
            new NomelNoteDialog(mActivity, "批量处理", "没有未确认商品!", "确认", null);
        }
    }

    private void showDialog() {
        new NomelNoteDialog(mActivity, "删除", "确定要删除吗？", "确认", new NomelNoteDialog.DialogListener() {
            @Override
            public void noteDialogConfirm() {
                List<OrderGoodsData> checkList = adapter.getCheckList();
                showCheckBox(false);
                if (checkList != null && checkList.size() > 0) {
                    for (OrderGoodsData good : checkList) {
                        setGoodSize(good, 0);
                    }
                    fragmentPos.mLayoutMainPos.closesdPlayView();
                }
                adapter.removeCheckList();
            }
        });
    }

    private void setGoodSize(OrderGoodsData medicine, float size) {
        if (medicine.getSureCount() > 0 && size <= medicine.getSureCount()) {//最小size不能小于已下单的size
            size = medicine.getSureCount();
            medicine.setIsSure(1);
        } else if (medicine.getSureCount() > 0 && size > medicine.getSureCount()) {
            medicine.setIsSure(0);
        }
        fragmentPos.setGoodSize(medicine, size, size == 0);
    }

    /**
     * 创建定单
     */
    public void newOrder(boolean createOrderNo, Boolean tag) {
        orderHelper = OrderHelper.getInstance(mContext);
        OrderInfoData orderData = orderHelper.getOrderData();
        if (orderData == null || orderData.getOrderNo() <= 0 || createOrderNo) {
            orderData = orderHelper.createNewOrder();
        } else {
            orderData = orderHelper.clearOrderGoods();
        }
        if (orderData != null) {
            tvOrderNumValue.setText(orderData.getOrderNo() + "");
            adapter.setData(orderHelper.getOrderGoods());
            tvOrderSize.setText("");
            tvOrderAmount.setText("");
            tvOrderTime.setText(TimeUtils.getInstance().formatYMDHM(orderData.getOrderDate()));
            tvOrderRecevie.setText("");
            tvOrderActual.setText("");
            refershMemberInfo(orderData);
        }
        setOrderStatusImg();
        if (tag) {
            fragmentPos.closeGoodView();
        }
        fragmentPos.initData();
    }

    ///订单点击
    public void setOrder(OrderInfoData orderData) {
        orderHelper = OrderHelper.getInstance(mContext);
        orderHelper.setOrder(orderData);

        if (orderData != null) {
            tvOrderNumValue.setText(orderData.getOrderNo() + "");
            adapter.setData(orderHelper.getOrderGoods());
            tvOrderSize.setText(orderHelper.getOrderSize() + "");
            float orderAmount = orderHelper.getOrderData().getCountAmount();
            tvOrderAmount.setText(String.format(Constant.AMOUNT, Util.subFloat(orderAmount) + ""));
            tvOrderTime.setText(TimeUtils.getInstance().formatYMDHM(orderData.getOrderDate()));
            setNeedPayAmount(orderData.getCountAmount());
            refershList(orderData.getOrderNo() + "");
            refershMemberInfo(orderData);
            setOrderStatusImg();
        }
        fragmentPos.mLayoutMainPos.updateBadView();
    }

    //数据更新后刷新定单
    public void resumeOrder(OrderInfoData orderData) {
        orderHelper = OrderHelper.getInstance(mContext);
        if (orderData != null) {
            tvOrderNumValue.setText(orderData.getOrderNo() + "");
            adapter.setData(orderHelper.getOrderGoods());
            tvOrderSize.setText(orderHelper.getOrderSize() + "");
            float orderAmount = orderHelper.getOrderData().getCountAmount();
            tvOrderAmount.setText(String.format(Constant.AMOUNT, Util.subFloat(orderAmount) + ""));
            tvOrderTime.setText(TimeUtils.getInstance().formatYMDHM(orderData.getOrderDate()));
            setNeedPayAmount(orderData.getCountAmount());
            refershList(orderData.getOrderNo() + "");
            refershMemberInfo(orderData);
            setOrderStatusImg();
        }
    }

    public void addGood(GoodItemInfo goodsInfo, Boolean flag) {
        orderHelper = OrderHelper.getInstance(mContext);
        OrderStatusEnum orderStatus = orderHelper.getOrderStaus();
        switch (orderStatus) {
            case Null://未开单
                newOrder(true, flag);
                break;
            case Bill://已开单
                break;
            case HandIn://已下单,追单
                orderHelper = OrderHelper.getInstance(mContext);
                orderHelper.reOpenOrder();//重新打开定单
                break;
            case Settle://定单已结算，重新开单
                newOrder(true, true);
                break;
            case ReOpen://重新打开定单
                break;
            case BackHandIn://已下单数据退单，重新开单
                newOrder(true, true);
                break;
            case BackSettle://已结算数据退单,重新开单
                newOrder(true, true);
                break;
            case KeepingAccounts://记账时重新开单
                newOrder(true, true);
                break;
        }
        OrderInfoData orderInfoData = orderHelper.getOrderData();
        if (orderInfoData == null || orderInfoData.getOrderNo() <= 0) {
            newOrder(true, flag);
        }
        orderAddGoods(goodsInfo);
        setOrderStatusImg();
    }

    //下单
    public void postOrder() {
        orderHelper = OrderHelper.getInstance(mContext);
        OrderStatusEnum orderStatus = orderHelper.getOrderStaus();
        switch (orderStatus) {
            case Null:
                new NomelNoteDialog(mActivity, "未开单", "请先开单再添加商品", "开单", new NomelNoteDialog.DialogListener() {
                    @Override
                    public void noteDialogConfirm() {
                        newOrder(true, true);
                    }
                });
                break;
            case Bill://已开单
                if (orderHelper.getOrderSize() > 0) {//有商品信息
                    adapter.setData(orderHelper.postOrder());
                    if (fragmentPos != null) {
                        fragmentPos.addOrderSucc();//下单成功
                    }
                } else {
                    new NomelNoteDialog(mActivity, "无商品", "请添加商品", "确定", null);
                }
                break;
            case HandIn://该单已下单
                new NomelNoteDialog(mActivity, "已下单", "该定单已经下单", "确定", null);
                break;
            case Settle://定单已结算
                new NomelNoteDialog(mActivity, "已结算", "该定单已经结算", "确定", null);
                break;
            case ReOpen://重新打开定单
                adapter.setData(orderHelper.postOrder());
                if (fragmentPos != null) {
                    fragmentPos.addOrderSucc();//下单成功
                }
                break;
            case BackHandIn://已下单数据退单
                new NomelNoteDialog(mActivity, "已撤单", "该定单已撤单", "确定", null);
                break;
            case BackSettle://已结算数据退单
                new NomelNoteDialog(mActivity, "已撤单", "该定单已撤单", "确定", null);
                break;
        }
    }

    //添加商品
    private void orderAddGoods(GoodItemInfo goodsInfo) {
        orderHelper = OrderHelper.getInstance(mContext);
        orderHelper.addGoods(goodsInfo);
        refershInfo();
    }

    //更改商品数量时间更新金额
    public void refershInfo() {
        orderHelper = OrderHelper.getInstance(mContext);
        float orderAmount = orderHelper.getOrderData().getCountAmount();
        adapter.setData(orderHelper.getOrderGoods());
        tvOrderSize.setText(orderHelper.getOrderSize() + "");
        tvOrderAmount.setText(String.format(Constant.AMOUNT, Util.subFloat(orderAmount) + ""));
    }

    //开单校验
    public void newOrderCheck() {
        orderHelper = OrderHelper.getInstance(mContext);
        OrderStatusEnum orderStatus = orderHelper.getOrderStaus();
        if (orderStatus == OrderStatusEnum.Bill) {//新开单未下单
            if (orderHelper.getOrderGoods().size() > 0) {//提示重新开单
                new NomelNoteDialog(mActivity, "重新开单", "确定撤销当前订单所有商品？", "确定", new NomelNoteDialog.DialogListener() {
                    @Override
                    public void noteDialogConfirm() {
                        newOrder(false, true);
                    }
                });
            } else {
                newOrder(false, true);
            }
        } else {
            newOrder(true, true);
        }
    }

    //定单状态提示图片
    public void setOrderStatusImg() {
        orderHelper = OrderHelper.getInstance(mContext);
        OrderStatusEnum orderStatus = orderHelper.getOrderStaus();
        switch (orderStatus) {
            case Null:
                ivOrderStatus.setVisibility(View.GONE);
                break;
            case Bill://已开单";
                ivOrderStatus.setVisibility(View.GONE);
                break;
            case HandIn://该单已下单";
                ivOrderStatus.setImageResource(R.mipmap.warmpos_order);
                ivOrderStatus.setVisibility(View.VISIBLE);
                break;
            case Settle://定单已结算";
                ivOrderStatus.setImageResource(R.mipmap.warmpos_settlement);
                ivOrderStatus.setVisibility(View.VISIBLE);
                break;
            case ReOpen://重新打开定单";
                ivOrderStatus.setVisibility(View.GONE);
                break;
            case BackHandIn://已下单数据退单
                ivOrderStatus.setImageResource(R.mipmap.warmpos_chargeback_grey);
                ivOrderStatus.setVisibility(View.VISIBLE);
                break;
            case BackSettle://已结算数据退单
                ivOrderStatus.setImageResource(R.mipmap.warmpos_chargeback_grey);
                ivOrderStatus.setVisibility(View.VISIBLE);
                break;
            case KeepingAccounts://记账
                ivOrderStatus.setImageResource(R.mipmap.warmpos_seal_charge);
                ivOrderStatus.setVisibility(View.VISIBLE);
                break;
        }
    }

    //撤单校验
    public boolean refundOrderCheck() {
        orderHelper = OrderHelper.getInstance(mContext);
        OrderStatusEnum orderStatus = orderHelper.getOrderStaus();
        switch (orderStatus) {
            case Null://"未开单";
                new NomelNoteDialog(mActivity, "无定单", "无定单信息!", "确定", null);
                break;
            case Bill://"已开单";
                return true;
            case HandIn://已下单,重新打开定单 追单
                return true;
            case Settle://定单已结算";
                new NomelNoteDialog(mActivity, "已结算", "已结算定单不能撤单!", "确定", null);
                break;
            case ReOpen://重新打开定单";
                return true;
            case BackHandIn://已下单数据退单
                new NomelNoteDialog(mActivity, "已撤单", "该定单已撤单!", "确定", null);
                break;
            case BackSettle://已结算数据退单
                new NomelNoteDialog(mActivity, "已撤单", "该定单已撤单!", "确定", null);
                break;
        }
        return false;
    }

    //撤单
    public void refundOrder() {
        orderHelper = OrderHelper.getInstance(mContext);
        if (orderHelper.getOrderStaus() == OrderStatusEnum.Bill) {//重新开单
            newOrder(false, true);
        } else {
            orderHelper.refundOrder();
            fragmentPos.newOrder();//退单后重新开单
        }
        setOrderStatusImg();
        fragmentPos.mLayoutMainPos.updateBadView();
    }

    //更新会员
    public void refershMemberInfo(OrderInfoData orderData) {
        if (orderData != null) {
            tvMemberNum.setText(orderData.getMemberCardNo() + "");
            tvMemberName.setText(orderData.getCustomerName() + "");
            tvPetName.setText(orderData.getPetName());
            checkMemberWaitPays(orderData.getCustomerId(), orderData.getOrderNo());
        }
    }

    public void checkMemberWaitPays(int customeId, int orderNo) {
        List<OrderInfoData> orderList = LitepalHelper.getInstance().getOrderListByCustomId(customeId, orderNo);
        if (orderList != null && orderList.size() > 0) {
            tvBadgeView.setText(orderList.size() + "");
            tvBadgeView.setVisibility(View.VISIBLE);
            tvBadgeView.setTag(customeId);
        } else {
            tvBadgeView.setText("0");
            tvBadgeView.setVisibility(View.GONE);
            tvBadgeView.setTag(0);
        }
    }

    //下单后更新应收金额
    public void setNeedPayAmount(float amount) {
        String amountStr = String.format(Constant.AMOUNT, Util.subFloat(amount) + "");
        tvOrderRecevie.setText(amountStr);
        setOrderStatusImg();
    }

    //商品详情关闭，退出选中状态
    public void closeSelect() {
        if (adapter != null) {
            adapter.getSelected(-1);
        }
    }

    //已收金额
    public void refershList(String orderNo) {
        List<PayAmountData> datas = LitepalHelper.getInstance().queryPaysData(orderNo);
        float amout = 0.00f;
        if (datas != null && datas.size() > 0) {
            float moling = 0;
            for (PayAmountData payData : datas) {
                if (payData.getPayChanel() == PayChanel.PAY_BY_DISCOUNT) {
                    moling = payData.getPayAmount();
                } else if (payData.getPayChanel() != PayChanel.PAY_BY_KEEPACCCOUNT) {//记账金额不算入实收金额
                    amout += payData.getPayAmount();
                }
            }
            float change = 0.00f;
            if (amout > datas.get(0).getOrderAmount()) {
                change = amout - datas.get(0).getOrderAmount();
            }
            if (moling > 0) {
                tvOrderActual.setText(String.format(Constant.ORDER_PAY_INFOB, Util.subFloat(amout) + "", Util.subFloat(moling) + ""));
            } else {
                tvOrderActual.setText(String.format(Constant.ORDER_PAY_INFOA, Util.subFloat(amout) + "", Util.subFloat(change) + ""));
            }
        } else {
            tvOrderActual.setText("");
        }
    }

}
