package com.warmsoft.pos.bean;

import java.io.Serializable;

/**
 * Created by brooks on 16/11/7.
 */
public class AccountRechargeModel implements Serializable {
    int code;
    String message;
    DataInfo Data;

    public static class DataInfo implements Serializable {
        boolean IsOk;
        String ErrorMessage;
        String SerialNumber;

        public boolean isOk() {
            return IsOk;
        }

        public void setOk(boolean ok) {
            IsOk = ok;
        }

        public String getErrorMessage() {
            return ErrorMessage;
        }

        public void setErrorMessage(String errorMessage) {
            ErrorMessage = errorMessage;
        }

        public String getSerialNumber() {
            return SerialNumber;
        }

        public void setSerialNumber(String serialNumber) {
            SerialNumber = serialNumber;
        }
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataInfo getData() {
        return Data;
    }

    public void setData(DataInfo data) {
        Data = data;
    }
}
