package com.warmsoft.pos.layout.view;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.Editable;
import android.text.Selection;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.warmsoft.pos.R;
import com.warmsoft.pos.bean.AuthModel;
import com.warmsoft.pos.fragment.main.FragmentPos;
import com.warmsoft.pos.litepal.data.CatgoryItemInfo;
import com.warmsoft.pos.litepal.data.GoodItemInfo;
import com.warmsoft.pos.litepal.data.OrderGoodsData;
import com.warmsoft.pos.litepal.data.OrderInfoData;
import com.warmsoft.pos.support.listview.adapter.GVAdapter;
import com.warmsoft.pos.support.listview.fragment.ProTypeFragment;
import com.warmsoft.pos.util.LogUtil;
import com.warmsoft.pos.util.MemberUtil;
import com.warmsoft.pos.util.OAuthUtils;
import com.warmsoft.pos.util.OrderHelper;
import com.warmsoft.pos.util.Util;

import org.litepal.crud.DataSupport;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 作者: lijinliu on 2016/8/31.
 * 邮箱：jinliu.li@warmsoft.com
 * 描述: FragmentPos 布局数据
 */
public class LayoutFragmentPos implements View.OnClickListener, ProTypeFragment.ItemClickListener, ProTypeFragment.TextClickListener, TextWatcher {
    private Activity mActivity;
    private FragmentPos fragmentPos;
    private Context mContext;
    private View mLayoutView;

    private LayoutInflater inflater;

    private LinearLayout llWaitCash;
    private LinearLayout llSell;
    private LinearLayout sdPlayView;
    private LinearLayout llCash;
    private FrameLayout goodLayout;
    private LinearLayout layoutMainKeys;
    ;
    private ScrollView scrollView;
    private Button btnWaitCash;
    private Button btnSell;
    private Button btnCash;
    protected TextView tvOrderBadgeView;

    public static String[] list;
    public static List<CatgoryItemInfo> goodsList = new ArrayList<>();
    private TextView[] tvList;
    private View[] views;

    private ViewPager viewpager;
    private int currentItem = 0;
    private ShopAdapter shopAdapter;
    private EditText etSearch;
    private ImageView ivClean;
    private GridView myGridView;
    private GVAdapter myAdapter;
    private ImageView searchBack;
    int m = 0;
    private int clickViewId = 0;
    List<GoodItemInfo> goodlist = new ArrayList<>();

    private boolean inputByMBW = false;
    private final int msgSearch = 1;
    private final int msgMBW = 2;//条形码扫入
    private final int searchViewId = 123321;


    public LayoutFragmentPos(View rootView, Activity activity, FragmentPos fragment) {
        this.mActivity = activity;
        this.mContext = mActivity.getApplicationContext();
        this.fragmentPos = fragment;
        this.mLayoutView = rootView;
        this.inflater = LayoutInflater.from(mContext);
        initViews();
        initGoodInfo();
        showOrderBadgeView();
    }

    private void initGoodInfo() {
        goodsList.clear();
        goodsList.addAll(MemberUtil.getInstance(mContext).getCatgoryItemInfoList());

        list = MemberUtil.getInstance(mContext).getList();

        shopAdapter = new ShopAdapter(fragmentPos.getFragmentManager());
        inflater = LayoutInflater.from(mContext);
        showToolsView();
    }

    private void initViews() {
        viewpager = (ViewPager) mLayoutView.findViewById(R.id.goods_pager);
        viewpager.setAdapter(shopAdapter);
        viewpager.setOnPageChangeListener(onPageChangeListener);

        llWaitCash = (LinearLayout) mLayoutView.findViewById(R.id.id_for_wait_cash_ll);
        llSell = (LinearLayout) mLayoutView.findViewById(R.id.id_for_sell_ll);
        sdPlayView = (LinearLayout) mLayoutView.findViewById(R.id.sdplay_id);
        llCash = (LinearLayout) mLayoutView.findViewById(R.id.id_for_cash_ll);

        goodLayout = (FrameLayout) mLayoutView.findViewById(R.id.id_for_good_click);
        layoutMainKeys = (LinearLayout) mLayoutView.findViewById(R.id.layout_main_key);

        btnWaitCash = (Button) mLayoutView.findViewById(R.id.id_for_wait_cash);
        btnWaitCash.setOnClickListener(this);
        tvOrderBadgeView = (TextView) mLayoutView.findViewById(R.id.id_order_badge_view);
        btnSell = (Button) mLayoutView.findViewById(R.id.id_for_sell);
        btnSell.setOnClickListener(this);
        btnSell.setSelected(true);
        myGridView = (GridView) mLayoutView.findViewById(R.id.gridView);
        btnCash = (Button) mLayoutView.findViewById(R.id.id_for_cash);
        btnCash.setOnClickListener(this);
        ivClean = (ImageView) mLayoutView.findViewById(R.id.id_for_cleansp);
        etSearch = (EditText) mLayoutView.findViewById(R.id.id_for_search_inputsp);
        searchBack = (ImageView) mLayoutView.findViewById(R.id.searchback);
        searchBack.setVisibility(View.VISIBLE);
        searchBack.setOnClickListener(this);
        etSearch.addTextChangedListener(this);
        ivClean.setOnClickListener(this);

        etSearch.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    etSearch.setHint(null);
                } else {
                    etSearch.setHint("请输入搜索内容或者点击后扫描商品条形码...");
                }
            }
        });

        etSearch.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int keyCode, KeyEvent keyEvent) {
                if (keyCode == KeyEvent.KEYCODE_ENTER) {
                    etSearch.requestFocus();
                    mHandler.removeMessages(msgSearch);
                    mHandler.removeMessages(msgMBW);
                    mHandler.sendEmptyMessageDelayed(msgMBW, 240);
                    return true;
                }
                return false;
            }
        });

        scrollView = (ScrollView) mLayoutView.findViewById(R.id.tools_scrlllview);

        myGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {

                goodlist.get(arg2).setChoose(goodlist.get(arg2).getChoose() + 1);
                myAdapter = new GVAdapter(mContext, goodlist);
                myGridView.setAdapter(myAdapter);
                myAdapter.notifyDataSetChanged();
                fragmentPos.mLayoutOrder.addGood(goodlist.get(arg2), false);
                updateBadView();
            }
        });
    }

    private void updateGoodInfo() {
        myAdapter.update(goodlist);
        myAdapter.notifyDataSetChanged();
    }


    //批量删除时关闭搜索页面
    public void closesdPlayView() {
        if (sdPlayView.getVisibility() == View.VISIBLE) {
            etSearch.setText("");
            sdPlayView.setVisibility(View.GONE);
            llSell.setVisibility(View.VISIBLE);
            layoutMainKeys.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onClick(View view) {
        clickViewId = view.getId();
        setViewVisible(clickViewId);
        switch (view.getId()) {
            case R.id.id_for_cleansp:
                clickViewId = searchViewId;
                etSearch.setText("");
                break;
            case R.id.searchback:
                clickViewId = R.id.id_for_sell;//返回销货
                btnSell.setSelected(true);
                etSearch.setText("");
                InputMethodManager imm = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
                etSearch.requestFocus();
                imm.hideSoftInputFromWindow(etSearch.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
                sdPlayView.setVisibility(View.INVISIBLE);
                llSell.setVisibility(View.VISIBLE);
                layoutMainKeys.setVisibility(View.VISIBLE);
                break;

        }
    }

    private void setViewVisible(int id) {
        btnWaitCash.setSelected(false);
        btnSell.setSelected(false);
        btnCash.setSelected(false);

        switch (id) {
            case R.id.id_for_wait_cash:
                llWaitCash.setVisibility(View.VISIBLE);
                llSell.setVisibility(View.GONE);
                llCash.setVisibility(View.GONE);
                goodLayout.setVisibility(View.GONE);
                btnWaitCash.setSelected(true);
                break;

            case R.id.id_for_sell:
                llWaitCash.setVisibility(View.GONE);
                llSell.setVisibility(View.VISIBLE);
                llCash.setVisibility(View.GONE);
                goodLayout.setVisibility(View.GONE);
                btnSell.setSelected(true);
                break;

            case R.id.id_for_cash:
                llWaitCash.setVisibility(View.GONE);
                llSell.setVisibility(View.GONE);
                llCash.setVisibility(View.VISIBLE);
                goodLayout.setVisibility(View.GONE);
                btnCash.setSelected(true);
                fragmentPos.mLayoutSales.initListData();
                break;
        }
        fragmentPos.initData();
    }

    //定单商品列表点击
    public void orderGoodClick() {
        llWaitCash.setVisibility(View.GONE);
        llSell.setVisibility(View.GONE);
        llCash.setVisibility(View.GONE);
        layoutMainKeys.setVisibility(View.GONE);
        etSearch.setText("");
        sdPlayView.setVisibility(View.GONE);
        goodLayout.setVisibility(View.VISIBLE);
    }

    // 隐藏定单商品列表信息
    public void closeGoodView() {
        layoutMainKeys.setVisibility(View.VISIBLE);
        if (clickViewId == 0) {
            clickViewId = R.id.id_for_sell;//初始为销货界面
        }
        btnWaitCash.setSelected(false);
        btnSell.setSelected(false);
        btnCash.setSelected(false);

        switch (clickViewId) {
            case R.id.id_for_wait_cash:
                llWaitCash.setVisibility(View.VISIBLE);
                llSell.setVisibility(View.GONE);
                llCash.setVisibility(View.GONE);
                goodLayout.setVisibility(View.GONE);
                btnWaitCash.setSelected(true);
                break;

            case R.id.id_for_sell:
                llWaitCash.setVisibility(View.GONE);
                llSell.setVisibility(View.VISIBLE);
                llCash.setVisibility(View.GONE);
                goodLayout.setVisibility(View.GONE);
                btnSell.setSelected(true);
                break;
            case R.id.id_for_cash:
                llWaitCash.setVisibility(View.GONE);
                llSell.setVisibility(View.GONE);
                llCash.setVisibility(View.VISIBLE);
                goodLayout.setVisibility(View.GONE);
                btnCash.setSelected(true);
                break;
            case searchViewId:
                clickViewId = searchViewId;
                goodLayout.setVisibility(View.GONE);
                sdPlayView.setVisibility(View.VISIBLE);
                llSell.setVisibility(View.GONE);
                layoutMainKeys.setVisibility(View.GONE);
                etSearch.clearFocus();
                break;
        }
    }

    // 商品添加监听
    @Override
    public void productClick(int index, int position) {
        if (index < goodsList.size()) {
            List<GoodItemInfo> infolist = goodsList.get(index).getInfo();
            if (position < infolist.size()) {
                GoodItemInfo goodsInfo = infolist.get(position);
                fragmentPos.mLayoutOrder.addGood(goodsInfo, true);
                fragmentPos.mLayoutKeyBoard.setOrderSize(OrderHelper.getInstance(mContext).getOrderSize());

                //更新商品选择数量
                float curCount = goodsInfo.getChoose() + 1;
                goodsList.get(index).getInfo().get(position).setChoose(curCount);
                //更新目录数量
                float catCount = goodsList.get(index).getChoose() + 1;
                goodsList.get(index).setChoose(catCount);

                TextView tvCatCount = ((TextView) views[index].findViewById(R.id.id_for_select_count));
                if (catCount > 0) {
                    tvCatCount.setVisibility(View.VISIBLE);
                    tvCatCount.setText(catCount > 99 ? "99+" : String.valueOf(catCount));
                } else {
                    tvCatCount.setVisibility(View.GONE);
                }
                LogUtil.error("catCount: " + goodsList.get(index).getInfo().get(position).getChoose());
                LogUtil.error("curCount: " + goodsList.get(index).getChoose());
            }
        }

    }


    /**
     * OnPageChangeListener<br/>
     * 监听ViewPager选项卡变化事的事件
     */
    private ViewPager.OnPageChangeListener onPageChangeListener = new ViewPager.OnPageChangeListener() {

        @Override
        public void onPageSelected(int arg0) {
            if (viewpager.getCurrentItem() != arg0)
                viewpager.setCurrentItem(arg0);

            if (currentItem != arg0) {
                changeTextColor(arg0, false);
                changeTextLocation(arg0);
            }

            currentItem = arg0;
        }

        @Override
        public void onPageScrollStateChanged(int arg0) {
        }

        @Override
        public void onPageScrolled(int arg0, float arg1, int arg2) {
        }
    };

    @Override
    public void viewClick() {
        clickViewId = searchViewId;
        sdPlayView.setVisibility(View.VISIBLE);
        llSell.setVisibility(View.INVISIBLE);
        layoutMainKeys.setVisibility(View.GONE);
        etSearch.clearFocus();
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        //isByMbw();
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        //isByMbw();
    }

    @Override
    public void afterTextChanged(Editable s) {
        mHandler.removeMessages(msgSearch);
        mHandler.sendEmptyMessageDelayed(msgSearch, 200);
    }

    private synchronized void isByMbw() {
        if (inputByMBW) {
            inputByMBW = false;
            etSearch.setText("");
        }
    }

    private int searchWordLenght = 0;

    Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case msgSearch:
                    if (inputByMBW) {
                        inputByMBW = false;
                        return;
                    }
                    String newText = etSearch.getText().toString().trim();
                    if (!Util.isNullStr(newText)) {
                        String searchSql = " id>? and ( DrugsName like '%" + newText + "%' or BarCode like '%" + newText + "%') ";
                        goodlist = DataSupport.where(searchSql, "0").find(GoodItemInfo.class);
                    } else {
                        goodlist = null;
                    }
                    searchResult(false);
                    this.sendEmptyMessageDelayed(3, 240);//获取焦点
                    break;
                case msgMBW:
                    inputByMBW = true;
                    String barCode = etSearch.getText().toString().trim();
                    if (searchWordLenght < barCode.length()) {
                        barCode = barCode.substring(searchWordLenght);
                    }
                    etSearch.setText(barCode);
                    String searchSql = " id>? and BarCode like '%" + barCode + "%' ";
                    goodlist = DataSupport.where(searchSql, "0").find(GoodItemInfo.class);
                    searchResult(true);
                    this.sendEmptyMessageDelayed(3, 240);//获取焦点
                    break;
                case 3:
                    Editable etext = etSearch.getText();
                    searchWordLenght = etext.length();
                    Selection.setSelection(etext, searchWordLenght);
                    etSearch.requestFocus();
                    break;
            }
        }
    };

    private void searchResult(boolean addGood) {
        if (goodlist != null && goodlist.size() > 0) {
            if (addGood && goodlist.size() == 1) {
                fragmentPos.mLayoutOrder.addGood(goodlist.get(0), false);
            }
            Map<Integer, Float> map = new HashMap<Integer, Float>();//goodID,goodSize
            List<OrderGoodsData> orderGoodList = OrderHelper.getInstance(mContext).getOrderGoods();
            for (OrderGoodsData goodsData : orderGoodList) {
                map.put(goodsData.getFeeItemId(), goodsData.getCount());
            }
            for (int j = 0; goodlist.size() > j; j++) {
                int goodId = goodlist.get(j).getGoodId();
                float goodSize = map.get(goodId) == null ? 0 : map.get(goodId);
                goodlist.get(j).setChoose(goodSize);
            }
            updateBadView();
        }
        myAdapter = new GVAdapter(mContext, goodlist);
        myGridView.setAdapter(myAdapter);
        myAdapter.notifyDataSetChanged();
    }

    /**
     * ViewPager 加载选项卡
     *
     * @author Administrator
     */
    private class ShopAdapter extends FragmentPagerAdapter {

        public ShopAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int index) {
            ProTypeFragment fragment = new ProTypeFragment();
            fragment.setItemClickListener(LayoutFragmentPos.this);
            fragment.setTextClickListener(LayoutFragmentPos.this);
            Bundle bundle = new Bundle();
            bundle.putInt("index", index);
            fragment.setArguments(bundle);
            return fragment;
        }

        @Override
        public int getCount() {
            if (list == null) {
                return 0;
            }

            return list.length;
        }
    }

    /**
     * 动态生成显示items中的textview
     */
    private void showToolsView() {
        LinearLayout toolsLayout = (LinearLayout) mLayoutView.findViewById(R.id.tools);
        tvList = new TextView[list.length];
        views = new View[list.length];

        for (int i = 0; i < list.length; i++) {
            View view = inflater.inflate(R.layout.item_list_layout, null);
            view.setId(i);
            view.setOnClickListener(toolsItemListener);
            TextView textView = (TextView) view.findViewById(R.id.text);
            textView.setText(list[i]);
            toolsLayout.addView(view);
            tvList[i] = textView;
            views[i] = view;
        }
        changeTextColor(0, false);
    }

    private View.OnClickListener toolsItemListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            viewpager.setCurrentItem(v.getId());
        }
    };

    /**
     * 改变textView的颜色
     *
     * @param id
     */


    private void changeTextColor(int id, Boolean pFlag) {
        for (int i = 0; i < tvList.length; i++) {
            if (i != id) {
                tvList[i].setBackgroundColor(0x00000000);
                tvList[i].setTextColor(mContext.getResources().getColor(R.color.white));

            }
            if (pFlag) {
                TextView tvCatCount = ((TextView) views[i].findViewById(R.id.id_for_select_count));
                tvCatCount.setVisibility(View.GONE);
            }

        }
        tvList[id].setBackgroundColor(mContext.getResources().getColor(R.color.alpha_20_white));
        tvList[id].setTextColor(mContext.getResources().getColor(R.color.white));
    }


    /**
     * 改变栏目位置
     *
     * @param clickPosition
     */
    private void changeTextLocation(int clickPosition) {
        int x = (views[clickPosition].getTop());
        scrollView.smoothScrollTo(0, x);
    }


    public void updateBadView() {
        Map<Integer, Float> map = new HashMap<Integer, Float>();//goodID,goodSize
        List<OrderGoodsData> orderGoodList = OrderHelper.getInstance(mContext).getOrderGoods();
        for (OrderGoodsData goodsData : orderGoodList) {
            map.put(goodsData.getFeeItemId(), goodsData.getCount());
        }

        for (int j = 0; goodsList.size() > j; j++) {
            List<GoodItemInfo> info = goodsList.get(j).getInfo();
            float catCount = 0;
            for (int i = 0; i < info.size(); i++) {
                GoodItemInfo goodItem = info.get(i);
                int goodId = goodItem.getGoodId();
                float goodSize = map.get(goodId) == null ? 0 : map.get(goodId);

                if (goodSize > 0) {
                    goodItem.setChoose(goodSize);
                    catCount = catCount + goodSize;
                } else {
                    goodItem.setChoose(0);
                }

            }
            TextView tvCatCount = ((TextView) views[j].findViewById(R.id.id_for_select_count));
            if (catCount > 0) {
                tvCatCount.setVisibility(View.VISIBLE);
                tvCatCount.setText(catCount > 99 ? "99+" : String.valueOf(catCount));
            } else {
                tvCatCount.setVisibility(View.GONE);
            }
            goodsList.get(j).setChoose(catCount);
        }
        changeTextColor(0, false);
        shopAdapter = new ShopAdapter(fragmentPos.getFragmentManager());
        viewpager.setAdapter(shopAdapter);
        shopAdapter.notifyDataSetChanged();
    }

    public void showOrderBadgeView() {
        AuthModel.DataInfo.UserInfo userInfo = OAuthUtils.getInstance().getUserinfo(mContext);
        int size = DataSupport.where(" status in ('1','5','6') and OrgId=? ", userInfo.getOrgId() + "").count(OrderInfoData.class);
        if (size > 0) {
            tvOrderBadgeView.setVisibility(View.VISIBLE);
            if (size > 99) {
                tvOrderBadgeView.setText("99+");
            } else {
                tvOrderBadgeView.setText(size + "");
            }
        } else {
            tvOrderBadgeView.setVisibility(View.GONE);
            tvOrderBadgeView.setText("0");
        }
    }
}