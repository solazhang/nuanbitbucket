package com.warmsoft.pos.bean;

import java.io.Serializable;

/**
 * Created by brooks on 16/11/7.
 */
public class MemberRechargeInfoModel implements Serializable {
    int code;
    String message;
    DataInfo Data;

    public static class DataInfo implements Serializable {
        String CardNo;
        int CardType;
        int CardBalance;
        int AccountBalance;
        int DepositType;
        int DepositBalance;
        int TotalConsumption;
        int Score;
        String InfoType;

        public String getCardNo() {
            return CardNo;
        }

        public void setCardNo(String cardNo) {
            CardNo = cardNo;
        }

        public int getCardType() {
            return CardType;
        }

        public void setCardType(int cardType) {
            CardType = cardType;
        }

        public int getCardBalance() {
            return CardBalance;
        }

        public void setCardBalance(int cardBalance) {
            CardBalance = cardBalance;
        }

        public int getAccountBalance() {
            return AccountBalance;
        }

        public void setAccountBalance(int accountBalance) {
            AccountBalance = accountBalance;
        }

        public int getDepositType() {
            return DepositType;
        }

        public void setDepositType(int depositType) {
            DepositType = depositType;
        }

        public String getInfoType() {
            return InfoType;
        }

        public void setInfoType(String infoType) {
            InfoType = infoType;
        }

        public int getDepositBalance() {
            return DepositBalance;
        }

        public void setDepositBalance(int depositBalance) {
            DepositBalance = depositBalance;
        }

        public int getTotalConsumption() {
            return TotalConsumption;
        }

        public void setTotalConsumption(int totalConsumption) {
            TotalConsumption = totalConsumption;
        }

        public int getScore() {
            return Score;
        }

        public void setScore(int score) {
            Score = score;
        }
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataInfo getData() {
        return Data;
    }

    public void setData(DataInfo data) {
        Data = data;
    }
}
