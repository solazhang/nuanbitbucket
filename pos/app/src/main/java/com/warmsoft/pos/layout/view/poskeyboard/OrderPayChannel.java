package com.warmsoft.pos.layout.view.poskeyboard;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.warmsoft.pos.R;
import com.warmsoft.pos.litepal.data.PayAmountData;
import com.warmsoft.pos.util.Constant;
import com.warmsoft.pos.util.Util;

import org.litepal.crud.DataSupport;

import java.util.List;

/**
 * 作者: lijinliu on 2016/8/10.
 * 邮箱：jinliu.li@warmsoft.com
 * 描述: view_pos_keyboder.xml 中部分布局:支付方式:卡支付/现金支付
 */
public class OrderPayChannel implements View.OnClickListener {

    private Activity mActivity;
    private Context mContext;
    private PayListener payListener;

    public LinearLayout layoutMain;
    public TextView tvChanelTitle;
    public TextView tvNeedPayAmout;
    public TextView tvCollectAmount;
    public LinearLayout layoutChange;
    public TextView tvChangeAmount;
    public TextView btnPayCancle;
    public TextView btnPayConfirm;

    public OrderPayChannel(View rootView, Activity activity, PayListener listener) {
        this.mActivity = activity;
        this.payListener = listener;
        this.mContext = mActivity.getApplicationContext();


        layoutMain = (LinearLayout) rootView.findViewById(R.id.layout_order_pay_chanel);
        tvChanelTitle = (TextView) rootView.findViewById(R.id.tv_pay_chanel_title);
        tvNeedPayAmout = (TextView) rootView.findViewById(R.id.tv_pay_chanel_needpay_amount);
        tvCollectAmount = (TextView) rootView.findViewById(R.id.tv_pay_chanel_collect_amount);
        layoutChange = (LinearLayout) rootView.findViewById(R.id.layout_pay_chanel_change);
        tvChangeAmount = (TextView) rootView.findViewById(R.id.tv_pay_chanel_change_amount);
        btnPayCancle = (TextView) rootView.findViewById(R.id.tv_btn_pay_cancle);
        btnPayConfirm = (TextView) rootView.findViewById(R.id.tv_btn_pay_confirm);

        initEvents();
    }

    private void initEvents() {
        btnPayCancle.setOnClickListener(this);
        btnPayConfirm.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tv_btn_pay_cancle:
                if (payListener != null) {
                    payListener.cancelPay();
                }
                break;
            case R.id.tv_btn_pay_confirm:
                if (payListener != null) {
                    payListener.payConfirm();
                }
                break;
        }
    }

    public void payByCash(float needPayAmount) {
        layoutMain.setVisibility(View.VISIBLE);
        layoutChange.setVisibility(View.VISIBLE);
        tvChanelTitle.setText("现金");
        tvCollectAmount.setText( Util.subFloat(needPayAmount)+"");
        tvNeedPayAmout.setText( Util.subFloat(needPayAmount)+"");
        tvChangeAmount.setText("0");
    }

    public void payByCard(float needPayAmount) {
        layoutMain.setVisibility(View.VISIBLE);
        //layoutChange.setVisibility(View.GONE);
        tvChanelTitle.setText("银行卡");
        tvCollectAmount.setText(Util.subFloat(needPayAmount)+"");
        tvNeedPayAmout.setText(Util.subFloat(needPayAmount)+"");
        tvChangeAmount.setText("0");
    }


    public interface PayListener {
        void cancelPay();
        void payConfirm();
    }
}
