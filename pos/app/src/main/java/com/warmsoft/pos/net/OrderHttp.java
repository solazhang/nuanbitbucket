package com.warmsoft.pos.net;

import android.content.Context;

import com.google.gson.Gson;
import com.warmsoft.pos.bean.AddOrderForJson;
import com.warmsoft.pos.bean.AddOrderModel;
import com.warmsoft.pos.bean.SettlementModel;
import com.warmsoft.pos.enumutil.OrderStatusEnum;
import com.warmsoft.pos.litepal.data.LitepalHelper;
import com.warmsoft.pos.litepal.data.OrderInfoData;
import com.warmsoft.pos.litepal.data.SettlementData;
import com.warmsoft.pos.util.NetWorkUtil;
import com.warmsoft.pos.util.Trace;
import com.warmsoft.pos.util.Util;

import org.litepal.crud.DataSupport;

import java.util.ArrayList;
import java.util.List;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * 作者: lijinliu on 2016/8/25.
 * 邮箱：jinliu.li@warmsoft.com
 * 描述: 定单接口操作
 */
public class OrderHttp {

    private static OrderHttp orderHttp;
    private Context mContext;
    private LitepalHelper mLitepal;
    private List<SettlementData> mGoodList;

    public static OrderHttp getInstance(Context context) {
        if (orderHttp == null) {
            orderHttp = new OrderHttp(context);
        }
        return orderHttp;
    }

    private OrderHttp(Context context) {
        mContext = context;
        mLitepal = LitepalHelper.getInstance();
    }

}
