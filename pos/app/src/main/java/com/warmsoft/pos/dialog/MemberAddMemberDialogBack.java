package com.warmsoft.pos.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;

import com.warmsoft.pos.R;
import com.warmsoft.pos.litepal.data.CardKind;
import com.warmsoft.pos.util.LogUtil;
import com.warmsoft.pos.util.MemberUtil;

import org.angmarch.views.NiceSpinner;

import java.util.ArrayList;
import java.util.List;


public class MemberAddMemberDialogBack implements View.OnClickListener {

    private Dialog mDialog;
    private DialogListener mListenr;
    private Activity mActivity;
    private LayoutInflater mInflater;
    private String titleStr;
    private String msgStr;
    private String confirmStr;

    private NiceSpinner mDialogMemberCard;

    //    private TextView tvTtile;
//    private TextView tvMsg;
//    private TextView btnCancle;
//    private TextView btnConfirm;
    ArrayList<CardKind> mCardKinds;

    public MemberAddMemberDialogBack(Activity activity, String title, String msg, String confirm, DialogListener listener, ArrayList<CardKind> memberCards) {
        LogUtil.error("initDialogMemberAdd mCardKinds " + memberCards.size());

        this.mActivity = activity;
        this.mInflater = LayoutInflater.from(mActivity);
        this.titleStr = title;
        this.msgStr = msg;
        this.confirmStr = confirm;
        this.mListenr = listener;
        this.mCardKinds = memberCards;

        createLoadingDialog(mActivity);
    }

    /**
     * 得到自定义的progressDialog
     *
     * @param context
     * @return
     */
    private void createLoadingDialog(Context context) {
        View rootView = mInflater.inflate(R.layout.dialog_member_add, null);
        LinearLayout layout = (LinearLayout) rootView.findViewById(R.id.dialog_view);
        initView(rootView, context);
        mDialog = new Dialog(context, R.style.fullScreenDialog);
        mDialog.setCancelable(true);
        int MATH_PARENT = LinearLayout.LayoutParams.MATCH_PARENT;
        mDialog.setContentView(layout, new LinearLayout.LayoutParams(MATH_PARENT, MATH_PARENT));// 设置布局
        mDialog.show();
    }

    private void initView(View rootView, Context mContext) {
        mDialogMemberCard = (NiceSpinner) rootView.findViewById(R.id.id_for_member_card_add);
        initDialogMemberAdd(mContext);
//        tvTtile = (TextView) rootView.findViewById(R.id.tv_title);
//        tvMsg = (TextView) rootView.findViewById(R.id.tv_msg);
//        tvTtile.setText(titleStr);
//        tvMsg.setText(msgStr);
//        btnConfirm = (TextView) rootView.findViewById(R.id.btn_confirm);
//        btnCancle = (TextView) rootView.findViewById(R.id.btn_cancle);
//        btnConfirm.setOnClickListener(this);
//        btnCancle.setOnClickListener(this);
//        if (!Util.isNullStr(confirmStr)) {
//            btnConfirm.setText(confirmStr);
//        }
    }

    @Override
    public void onClick(View view) {
//        if (view == btnCancle) {
//            dismiss();
//        } else if (view == btnConfirm) {
//            mDialog.dismiss();
//            if (mListenr != null) {
//                mListenr.noteDialogConfirm();
//            }
//        }
    }


    public void dismiss() {
        if (mDialog != null) {
            mDialog.dismiss();
            mDialog = null;
        }
    }

    public interface DialogListener {
        void noteDialogConfirm();
    }

    public void initDialogMemberAdd(Context mContext) {
        LogUtil.error("initDialogMemberAdd mCardKinds " + mCardKinds.size());

        ArrayList<CardKind> cardKinds = (ArrayList<CardKind>) MemberUtil.getInstance(mContext).getCardKindLists();

        if (cardKinds.size() > 0) {
//            memberInfoModel.setVIPLevel(memberCards.get(0).getId());
//            memberInfoModel.setLevel(memberCards.get(0).getName());

            mDialogMemberCard.setSelectedIndex(0);
            mDialogMemberCard.attachDataSource(parseSpinnerDisplay(cardKinds));
            mDialogMemberCard.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
//                    memberInfoModel.setVIPLevel(memberCards.get(i).getId());
//                    memberInfoModel.setLevel(memberCards.get(i).getName());
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });
        }
    }


    private List<String> parseSpinnerDisplay(List<CardKind> list) {

        List<String> l = new ArrayList<>();
        for (CardKind s : list) {
            l.add(s.getName());
        }

        return l;
    }
}
