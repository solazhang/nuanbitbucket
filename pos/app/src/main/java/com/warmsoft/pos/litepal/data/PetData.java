package com.warmsoft.pos.litepal.data;

import org.litepal.crud.DataSupport;

import java.io.Serializable;

/**
 * 作者: lijinliu on 2016/8/17.
 * 邮箱：jinliu.li@warmsoft.com
 * 描述: 宠物信息
 */
public class PetData extends DataSupport implements Serializable {

    private int ID;
    private int petId;//": 1615959,
    private int CustomerID;//": 1599629,
    private int OrgId;//": 7980,
    private String PetName;//": "WWWW",
    private int Gender;//": 1104,
    private String GenderText;//": "母",
    private String Birthdate;//": "2016-06-16T11:54:00.773",
    private int KindOF;//品种ID": 1075,
    private String KindOFText;//品种说明": null,
    private long Variety;//宠物类型
    private String VarietyText;//宠物名称
    private String TimeStamp;//更新时间戳
    private int Age;//": 0,

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public int getPetId() {
        return petId;
    }

    public void setPetId(int petId) {
        this.petId = petId;
    }

    public int getCustomerID() {
        return CustomerID;
    }

    public void setCustomerID(int customerID) {
        CustomerID = customerID;
    }

    public int getOrgId() {
        return OrgId;
    }

    public void setOrgId(int orgId) {
        OrgId = orgId;
    }

    public String getPetName() {
        return PetName;
    }

    public void setPetName(String petName) {
        PetName = petName;
    }

    public int getGender() {
        return Gender;
    }

    public void setGender(int gender) {
        Gender = gender;
    }

    public String getGenderText() {
        return GenderText;
    }

    public void setGenderText(String genderText) {
        GenderText = genderText;
    }

    public String getBirthdate() {
        return Birthdate;
    }

    public void setBirthdate(String birthdate) {
        Birthdate = birthdate;
    }

    public long getVariety() {
        return Variety;
    }

    public void setVariety(long variety) {
        Variety = variety;
    }

    public String getVarietyText() {
        return VarietyText;
    }

    public void setVarietyText(String varietyText) {
        VarietyText = varietyText;
    }

    public int getAge() {
        return Age;
    }

    public void setAge(int age) {
        Age = age;
    }

    public int getKindOF() {
        return KindOF;
    }

    public void setKindOF(int kindOF) {
        KindOF = kindOF;
    }

    public String getKindOFText() {
        return KindOFText;
    }

    public void setKindOFText(String kindOFText) {
        KindOFText = kindOFText;
    }

    public String getTimeStamp() {
        return TimeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        TimeStamp = timeStamp;
    }
}
