package com.warmsoft.pos.util;

/**
 * 作者: lijinliu on 2016/8/5.
 * 邮箱：jinliu.li@warmsoft.com
 * 描述: Toas提示
 */

public class TaskConst {
    public static final int TASKTYPE_BILL = 1;//下单
    public static final int TASKTYPE_SETTLE = 2;//结算
    public static final int TASKTYPE_BACKBILL = 3;//下单数据退单
    public static final int TASKTYPE_BACKSETTLE = 4;//结算退单
    public static final int TASKTYPE_ADD_MEMBER = 5;//增加会员
    public static final int TASKTYPE_KEEPING_ACCOUNTS = 6;//记账

    public static final int TASKSTATUS_REDAY = 0;//任务可执行
    public static final int TASKSTATUS_POSTING = 9;//任务执行中
    public static final int TASKSTATUS_POSTSUCC = 1;//任务执行成功
    public static final int TASKSTATUS_POSTFAIL = 2;//任务执行失败



}
