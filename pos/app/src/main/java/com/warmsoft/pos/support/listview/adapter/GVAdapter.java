package com.warmsoft.pos.support.listview.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.warmsoft.pos.R;
import com.warmsoft.pos.litepal.data.GoodItemInfo;
import com.warmsoft.pos.support.listview.bean.Type;
import com.warmsoft.pos.view.SquareLayout;

import java.util.ArrayList;
import java.util.List;

public class GVAdapter extends BaseAdapter {
    private List<GoodItemInfo> list;
    private GoodItemInfo type;
    private Context context;
    Holder view;

    public GVAdapter(Context context, List<GoodItemInfo> list) {
        this.list = list;
        this.context = context;
    }


    public void setDatas(List<GoodItemInfo> list){
        this.list = list;
        this.notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        if (list != null && list.size() > 0)
            return list.size();
        else
            return 0;
    }

    public void update(List<GoodItemInfo> l) {
        if (list != null && list.size() > 0) {
            list.clear();
            list.addAll(l);
        }

        notifyDataSetChanged();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = View.inflate(context, R.layout.item_for_square, null);
            view = new Holder(convertView);
            convertView.setTag(view);
        } else {
            view = (Holder) convertView.getTag();
        }
        if (list != null && list.size() > 0) {
            type = list.get(position);

            view.name.setText(type.getDrugsName());
            view.name.setTextColor(context.getResources().getColor(R.color.text_deep_back));
            if (type.getChoose() > 0) { //为0,隐藏
                view.badge.setVisibility(View.VISIBLE);
                view.badge.setText(type.getChoose() > 99 ? "99+" : String.valueOf(type.getChoose()));
            } else {
                view.badge.setVisibility(View.INVISIBLE);
            }
        }

        return convertView;
    }

    private class Holder {
        private ImageView icon;
        private TextView name;
        private SquareLayout square;
        TextView badge;

        public Holder(View view) {
            name = (TextView) view.findViewById(R.id.typename);
            badge = (TextView) view.findViewById(R.id.id_for_badge_view);
        }
    }

}
