package com.warmsoft.pos.bean;

import com.warmsoft.pos.litepal.data.CustomeCardList;

import java.io.Serializable;
import java.util.List;

/**
 * 会员卡信息
 */
public class CardListModel implements Serializable {
    int code;
    String message;
    List<CustomeCardList> Data;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<CustomeCardList> getData() {
        return Data;
    }

    public void setData(List<CustomeCardList> data) {
        Data = data;
    }
}
