package com.warmsoft.pos.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.warmsoft.pos.R;
import com.warmsoft.pos.adapter.entiy.OrderPaysAdapter;
import com.warmsoft.pos.listener.FmPosLitener;
import com.warmsoft.pos.litepal.data.OrderInfoData;
import com.warmsoft.pos.litepal.data.PayAmountData;
import com.warmsoft.pos.util.OrderHelper;

import org.litepal.crud.DataSupport;

import java.util.ArrayList;
import java.util.List;

/**
 * 作者: lijinliu on 2016/8/5.
 * 邮箱：jinliu.li@warmsoft.com
 * 描述: 已付款删除
 */
public class ClearPayedsDialog implements View.OnClickListener {

    private Dialog mDialog;
    private Activity mActivity;
    private LayoutInflater mInflater;
    private FmPosLitener mListener;

    private TextView tvNopaysRecored;
    private ListView listView;
    private OrderPaysAdapter adapter;
    private List<PayAmountData> datas = new ArrayList<PayAmountData>();

    private TextView btnClose;

    public ClearPayedsDialog(Activity activity, FmPosLitener listener) {
        this.mActivity = activity;
        this.mInflater = LayoutInflater.from(mActivity);
        this.mListener = listener;
        createLoadingDialog(mActivity);
    }

    /**
     * 得到自定义的progressDialog
     *
     * @param context
     * @return
     */
    private void createLoadingDialog(Context context) {
        View rootView = mInflater.inflate(R.layout.dialog_clear_payeds, null);
        LinearLayout layout = (LinearLayout) rootView.findViewById(R.id.dialog_view);
        initView(rootView);
        mDialog = new Dialog(context, R.style.fullScreenDialog);
        mDialog.setCancelable(true);
        int MATH_PARENT = LinearLayout.LayoutParams.MATCH_PARENT;
        mDialog.setContentView(layout, new LinearLayout.LayoutParams(MATH_PARENT, MATH_PARENT));// 设置布局
        mDialog.show();
    }

    private void initView(View rootView) {
        btnClose = (TextView) rootView.findViewById(R.id.btn_close);
        tvNopaysRecored = (TextView) rootView.findViewById(R.id.tv_nopays_recored);
        listView = (ListView) rootView.findViewById(R.id.listview_payeds);

        btnClose.setOnClickListener(this);
        OrderInfoData orderData = OrderHelper.getInstance(mActivity).getOrderData();
        if (orderData != null) {
            datas = DataSupport.where(" orderNo=? ", orderData.getOrderNo() + "").find(PayAmountData.class);
        }
        if (datas == null || datas.size() == 0) {
            listView.setVisibility(View.GONE);
            tvNopaysRecored.setVisibility(View.VISIBLE);
        } else {
            adapter = new OrderPaysAdapter(mActivity, datas, mListener);
            listView.setAdapter(adapter);
        }
    }

    @Override
    public void onClick(View view) {
        if (view == btnClose) {
            dismiss();
            if (mListener != null && datas != null && datas.size() > 0) {
                mListener.clearAllPays();
            }
        }
    }


    public void dismiss() {
        if (mDialog != null) {
            mDialog.dismiss();
            mDialog = null;
        }
    }
}
