package com.warmsoft.pos.layout.view;

import android.app.Activity;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.warmsoft.pos.HelpActivity;
import com.warmsoft.pos.R;
import com.warmsoft.pos.dialog.LoginHistoryDialog;
import com.warmsoft.pos.litepal.data.UserLoginData;
import com.warmsoft.pos.util.SimpleDeEnCode;
import com.warmsoft.pos.util.Trace;
import com.warmsoft.pos.util.Util;

import org.litepal.crud.DataSupport;

import java.util.List;

/**
 * 作者: lijinliu on 2016/8/5.
 * 邮箱：jinliu.li@warmsoft.com
 * 描述: 对应 activity_login.xml
 */
public class LayoutActivityLogin implements View.OnClickListener {

    private LoginViewListener mListener;
    private Activity mActivity;
    private boolean isSavePwd = false;
    public EditText etName;
    public EditText etPwd;
    public TextView btnSignIn;
    public TextView tvUpdateCheck;
    public ImageView ivBtnMore;
    public ImageView ivSavepwdStatus;
    public ImageView ivUpdateIcon;
    public LinearLayout layoutBtnSavepwd;
    public LinearLayout layoutBtnUpdate;
    public LinearLayout layoutBtnHelp;

    public LayoutActivityLogin(Activity activity, LoginViewListener listener) {
        mListener = listener;
        mActivity = activity;
        etName = (EditText) activity.findViewById(R.id.et_edit_name);
        etPwd = (EditText) activity.findViewById(R.id.et_edit_passwd);
        btnSignIn = (TextView) activity.findViewById(R.id.btn_sing_in);

        tvUpdateCheck = (TextView) activity.findViewById(R.id.btn_upgrade_check);
        ivUpdateIcon = (ImageView) activity.findViewById(R.id.id_for_update_icon);

        ivBtnMore = (ImageView) activity.findViewById(R.id.iv_btn_more);
        ivBtnMore.setVisibility(View.GONE);//暂时隐藏
        layoutBtnSavepwd = (LinearLayout) activity.findViewById(R.id.layout_btn_savepwd);
        ivSavepwdStatus = (ImageView) activity.findViewById(R.id.iv_savepwd_status);
        layoutBtnUpdate = (LinearLayout) activity.findViewById(R.id.layout_btn_update);
        layoutBtnHelp = (LinearLayout) activity.findViewById(R.id.layout_btn_help);
//        etName.setText("Athuang");
//        etPwd.setText("136162lp");
//        //etName.setText("曹丽丽");
//        etName.setText("jax11");
//        etPwd.setText("1234567");
//        etName.setText("机灵鬼陈卫萍");
//        etPwd.setText("135246");
//        etName.setText("李雪锋医生");
//        etPwd.setText("1234567");

        lastLoginUser();

        initEvents();
    }

    private void lastLoginUser() {
        try {
            List<UserLoginData> ulist = DataSupport.order("lastLoginDate desc").limit(1).find(UserLoginData.class);
            if (ulist != null && ulist.size() > 0) {
                initLoginUser(ulist.get(0));
            }
        } catch (Exception e) {
            Trace.e(e);
        }
    }

    public void initLoginUser(UserLoginData userLoginData) {
        if (userLoginData != null) {
            if ("0".equals(userLoginData.getStatus())) {
                isSavePwd = false;
                ivSavepwdStatus.setImageResource(R.mipmap.warmpos_pwd_unsaved);
                etName.setText(userLoginData.getLoginName());
                etPwd.setText("");
            } else {
                isSavePwd = true;
                ivSavepwdStatus.setImageResource(R.mipmap.warmpos_circle_selected);
                etName.setText(userLoginData.getLoginName());
                etPwd.setText(SimpleDeEnCode.decode(userLoginData.getLoginPwd()));
            }
        }
    }

    private void initEvents() {
        btnSignIn.setOnClickListener(this);
        ivBtnMore.setOnClickListener(this);
        layoutBtnSavepwd.setOnClickListener(this);
        layoutBtnUpdate.setOnClickListener(this);
        layoutBtnHelp.setOnClickListener(this);
    }

    public void refreshUpdate(boolean flag) {
        if (flag) {
            ivUpdateIcon.setImageResource(R.mipmap.warmpos_upgradered);
            tvUpdateCheck.setTextColor((mActivity).getResources().getColor(R.color.red));
        } else {
            ivUpdateIcon.setImageResource(R.mipmap.warmpos_upgrade);
            tvUpdateCheck.setTextColor((mActivity).getResources().getColor(R.color.white));
        }
    }

    /**
     * 是否保存密码
     *
     * @return
     */
    public boolean isSavePwd() {
        return isSavePwd;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_sing_in:
                if (mListener != null) {
                    mListener.login();
                }
                break;
            case R.id.layout_btn_savepwd:
                if (isSavePwd) {
                    isSavePwd = false;
                    ivSavepwdStatus.setImageResource(R.mipmap.warmpos_pwd_unsaved);
                } else {
                    isSavePwd = true;
                    ivSavepwdStatus.setImageResource(R.mipmap.warmpos_circle_selected);
                }
                break;

            case R.id.iv_btn_more:
                new LoginHistoryDialog(mActivity, Util.getViewText(etName), this);
                break;

            case R.id.layout_btn_update:
                if (mListener != null) {
                    mListener.checkUpgdate();
                }
                break;

            case R.id.layout_btn_help:
                HelpActivity.startActivity(mActivity);
                break;
        }
    }

    public interface LoginViewListener {
        public void login();

        public void checkUpgdate();
    }
}
