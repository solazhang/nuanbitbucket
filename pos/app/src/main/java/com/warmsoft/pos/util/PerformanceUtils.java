package com.warmsoft.pos.util;

/**
 * Created by brooks on 16/6/14.
 */
public class PerformanceUtils {

    private static PerformanceUtils instance = null;

    public static PerformanceUtils getInstance() {
        if (instance == null) {
            synchronized (PerformanceUtils.class) {
                instance = new PerformanceUtils();
            }
        }

        return instance;
    }

    public long getReadyTime() {
        return System.nanoTime();
    }

    public void showConsumeTime(long startTime) {
        long consumingTime = System.nanoTime() - startTime;
        LogUtil.error("Ok PerformanceUtils " + consumingTime / 1000 / 1000 + "ms");
    }

    public void showConsumeTime(long startTime, String msg) {
        long consumingTime = System.nanoTime() - startTime;
        LogUtil.error("Ok PerformanceUtils " + msg + " consume " + consumingTime / 1000 / 1000 + "ms");
    }
}
