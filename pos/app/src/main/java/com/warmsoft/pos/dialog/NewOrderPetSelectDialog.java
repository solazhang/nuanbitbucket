package com.warmsoft.pos.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.warmsoft.pos.R;
import com.warmsoft.pos.adapter.CustomeSelecterAdapter;
import com.warmsoft.pos.adapter.PetSelecterAdapter;
import com.warmsoft.pos.bean.AuthModel;
import com.warmsoft.pos.litepal.data.CustomerList;
import com.warmsoft.pos.litepal.data.OrderInfoData;
import com.warmsoft.pos.litepal.data.PetData;
import com.warmsoft.pos.util.OAuthUtils;
import com.warmsoft.pos.util.OrderHelper;
import com.warmsoft.pos.util.Util;
import com.warmsoft.pos.view.PullUpListView;

import org.litepal.crud.DataSupport;

import java.util.ArrayList;
import java.util.List;

/**
 * 作者: lijinliu on 2016/8/5.
 * 邮箱：jinliu.li@warmsoft.com
 * 描述: 开单弹出会员宠物选择窗
 */
public class NewOrderPetSelectDialog implements View.OnClickListener, TextWatcher, AdapterView.OnItemClickListener {

    private MemberDialogListener mListener;
    private Dialog mDialog;
    private Context mContext;
    private Activity mActivity;
    private LayoutInflater mInflater;

    private TextView tvBtnCancle;
    private TextView tvBtnCofirm;
    private EditText actvSearchWord;
    private LinearLayout layoutSearchList;
    private boolean isItemClick = false;
    private PullUpListView listviewMember;
    CustomeSelecterAdapter customeAdapter;
    List<CustomerList> customeList = new ArrayList<CustomerList>();

    private ListView listviewPet;
    PetSelecterAdapter petAdapter;
    List<PetData> petList = new ArrayList<PetData>();

    InputMethodManager inputMethodManager;

    private CustomerList customer;//选口的会员
    protected int mPage = 0;
    protected int limitSize = 20;

    public NewOrderPetSelectDialog(Activity activity, MemberDialogListener listener) {
        this.mActivity = activity;
        this.mContext = activity;
        this.mListener = listener;
        inputMethodManager = (InputMethodManager) mActivity.getSystemService(Context.INPUT_METHOD_SERVICE);
        this.mInflater = LayoutInflater.from(mContext);
        createLoadingDialog(mContext);
    }

    /**
     * 得到自定义的progressDialog
     *
     * @param context
     * @return
     */
    private void createLoadingDialog(Context context) {
        View rootView = mInflater.inflate(R.layout.dialog_member_pet_select, null);
        LinearLayout layout = (LinearLayout) rootView.findViewById(R.id.dialog_view);
        initView(rootView);
        initEvents();
        mDialog = new Dialog(context, R.style.fullScreenDialog);
        mDialog.setCancelable(false);
        int MATH_PARENT = LinearLayout.LayoutParams.MATCH_PARENT;
        mDialog.setContentView(layout, new LinearLayout.LayoutParams(MATH_PARENT, MATH_PARENT));// 设置布局
        mDialog.show();

        initDate();//已选会员数据
    }

    private void initView(View rootView) {
        tvBtnCancle = (TextView) rootView.findViewById(R.id.tv_btn_cancle);
        tvBtnCofirm = (TextView) rootView.findViewById(R.id.btn_select_confirm);
        actvSearchWord = (EditText) rootView.findViewById(R.id.actv_search_word);
        layoutSearchList = (LinearLayout) rootView.findViewById(R.id.layout_custom_searchlist);
        listviewMember = (PullUpListView) rootView.findViewById(R.id.listview_member);
        customeAdapter = new CustomeSelecterAdapter(mContext, customeList);
        listviewMember.setPageSize(limitSize);
        listviewMember.setAdapter(customeAdapter);

        listviewPet = (ListView) rootView.findViewById(R.id.listview_pet);
        petAdapter = new PetSelecterAdapter(mContext, petList);
        listviewPet.setAdapter(petAdapter);
    }

    private void initEvents() {
        tvBtnCancle.setOnClickListener(this);
        tvBtnCofirm.setOnClickListener(this);
        actvSearchWord.addTextChangedListener(this);
        listviewMember.setOnItemClickListener(this);
        listviewPet.setOnItemClickListener(petListClick);

        listviewMember.setOnLoadListener(new PullUpListView.OnLoadListener() {
            public void onLoad() {
                mPage++;
                loadMembers(actvSearchWord.getText().toString());
            }
        });
    }

    public void initDate() {
        AuthModel.DataInfo.AnonymousCustomerInfo customerIonfo = OAuthUtils.getInstance().getAnonyCustomInfo(mContext);
        OrderInfoData orderData = OrderHelper.getInstance(mContext).getOrderData();
        if (customerIonfo.getAnonymousPetID() != orderData.getPetId()) {
            isItemClick = true;//防止 onTextChanged更新数据
            actvSearchWord.setText(orderData.getMemberCardNo() + "");
            actvSearchWord.setSelection(orderData.getMemberCardNo().length());

            customeList = DataSupport.where(" CustomerId=? ", orderData.getCustomerId() + "").find(CustomerList.class);
            if (customeList != null && customeList.size() > 0) {
                customer = customeList.get(0);
            }

            petList = DataSupport.where(" CustomerID=? ", orderData.getCustomerId() + "").find(PetData.class);
            petAdapter.setData(petList);
            for (int i = 0; i < petList.size(); i++) {
                if (petList.get(i).getPetId() == orderData.getPetId()) {
                    petAdapter.setSelect(i);
                    break;
                }
            }
        }
        actvSearchWord.requestFocus();
        inputMethodManager.toggleSoftInput(0, InputMethodManager.HIDE_NOT_ALWAYS);
    }

    @Override
    public void onClick(View view) {
        if (view == tvBtnCofirm) {
            if (petAdapter.getSelPet() == null) {
                Util.showToastTop(mContext, "请选择宠物!");
            } else {
                dismiss();
                if (mListener != null) {
                    mListener.confirm(customer, petAdapter.getSelPet());
                }
            }
        } else if (view == tvBtnCancle) {
            dismiss();
        }
    }

    @Override
    public void afterTextChanged(Editable arg0) {

    }

    @Override
    public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {

    }

    AdapterView.OnItemClickListener petListClick = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
            petAdapter.setSelect(i);
        }
    };

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
        if (customeList != null && position < customeList.size()) {
            isItemClick = true;
            layoutSearchList.setVisibility(View.GONE);
            customer = customeList.get(position);
            String customerID = customer.getCustomerId() + "";
            actvSearchWord.setText(customer.getCardNumber() + "");
            actvSearchWord.setSelection(customer.getCardNumber().length());
            petList = DataSupport.where(" CustomerID=? ", customerID).find(PetData.class);
            petAdapter.setData(petList);
            initSoftKeyBoard();
        }
    }

    @Override
    public void onTextChanged(CharSequence s, int arg1, int arg2, int arg3) {
        if (isItemClick) {
            isItemClick = false;
            return;
        }
        String newText = s.toString().trim();

        petList = new ArrayList<>();
        petAdapter.setData(petList);

        if (!Util.isNullStr(newText)) {
            customeList = new ArrayList<>();
            mPage = 0;
            loadMembers(newText);
        } else {
            layoutSearchList.setVisibility(View.GONE);
        }
    }

    private void loadMembers(String searchKey) {
        AuthModel.DataInfo.UserInfo userInfo = OAuthUtils.getInstance().getUserinfo(mContext);
        String searchSql = " orgid=? and ( CardNumber like '%" + searchKey + "%'" + " or Name like '%" + searchKey + "%'" + " or CellPhone like'%" + searchKey + "%') ";
        List<CustomerList> load = DataSupport.where(searchSql, userInfo.getOrgId() + "").limit(limitSize).offset(mPage * limitSize).find(CustomerList.class);
        if (load != null && load.size() > 0) {
            customeList.addAll(load);
            listviewMember.setResultSize(load.size());
        }

        if (customeList != null && customeList.size() > 0) {
            layoutSearchList.setVisibility(View.VISIBLE);
            customeAdapter.setData(customeList);
            if (mPage == 0) {
                listviewMember.setSelection(0);
            }
        } else {
            layoutSearchList.setVisibility(View.GONE);
        }
    }

    public void dismiss() {
        initSoftKeyBoard();
        if (mDialog != null) {
            mDialog.dismiss();
            mDialog = null;
        }
    }

    private void initSoftKeyBoard() {
        actvSearchWord.requestFocus();
        inputMethodManager.hideSoftInputFromWindow(actvSearchWord.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
    }

    public interface MemberDialogListener {
        void confirm(CustomerList customer, PetData pet);
    }
}
