package com.warmsoft.pos.layout.view;

import android.app.Activity;
import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.warmsoft.pos.R;
import com.warmsoft.pos.adapter.NeedPayAdapter;
import com.warmsoft.pos.bean.AuthModel;
import com.warmsoft.pos.enumutil.OrderStatusEnum;
import com.warmsoft.pos.listener.FmPosLitener;
import com.warmsoft.pos.litepal.data.LitepalHelper;
import com.warmsoft.pos.litepal.data.OrderInfoData;
import com.warmsoft.pos.protocol.GetOrgCpaymentProto;
import com.warmsoft.pos.protocol.ProtocolListener;
import com.warmsoft.pos.util.OAuthUtils;
import com.warmsoft.pos.util.TimeUtils;
import com.warmsoft.pos.util.Util;
import com.warmsoft.pos.view.PullUpListView;

import org.litepal.crud.DataSupport;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 作者: lijinliu on 2016/8/9.
 * 邮箱：jinliu.li@warmsoft.com
 * 描述: 待付款 对应 view_sales_main.xml
 */
public class LayoutSales implements View.OnClickListener, AdapterView.OnItemClickListener {
    private Activity mActivity;
    private Context mContext;
    private FmPosLitener mPosLitener;

    public Button btnPendingPayment;//待付款
    public Button btnKeepingAccounts;//待付款
    public Button btnHavePayment;//有付款
    protected TextView mTvAmountTitle;
    protected PullUpListView listview;
    protected NeedPayAdapter adapter;
    private RelativeLayout rlSearchLayout;
    private ImageView ivSearch;
    private EditText etSearch;
    private List<OrderInfoData> datas;
    private ImageView ivRefresh, ivClean, ivHide;
    private Animation operatingAnim;

    private int dataChanel = 0;
    private boolean isQueryIng = false;

    protected int mPage = 0;
    protected int limitSize = 20;

    public LayoutSales(View rootView, Activity activity, FmPosLitener posLitener) {
        this.mActivity = activity;
        this.mContext = mActivity.getApplicationContext();
        this.mPosLitener = posLitener;
        btnPendingPayment = (Button) rootView.findViewById(R.id.btn_pending_payment);
        btnKeepingAccounts = (Button) rootView.findViewById(R.id.btn_keeping_accounts);
        btnHavePayment = (Button) rootView.findViewById(R.id.btn_have_payment);
        listview = (PullUpListView) rootView.findViewById(R.id.listview_notpay_order);
        mTvAmountTitle = (TextView) rootView.findViewById(R.id.tv_amount_title);
        adapter = new NeedPayAdapter(mContext, 0);
        listview.setPageSize(limitSize);
        listview.setAdapter(adapter);
        btnPendingPayment.setSelected(true);
        ivRefresh = (ImageView) rootView.findViewById(R.id.id_for_refresh);
        ivRefresh.setOnClickListener(this);
        ivClean = (ImageView) rootView.findViewById(R.id.id_for_clean);
        ivClean.setOnClickListener(this);
        ivSearch = (ImageView) rootView.findViewById(R.id.id_for_search);
        ivSearch.setOnClickListener(this);
        rlSearchLayout = (RelativeLayout) rootView.findViewById(R.id.id_for_search_layout);
        rlSearchLayout.setVisibility(View.GONE);
        etSearch = (EditText) rootView.findViewById(R.id.id_for_search_input);
        etSearch.addTextChangedListener(textWatcher);
        ivHide = (ImageView) rootView.findViewById(R.id.id_for_search_hide);
        ivHide.setVisibility(View.GONE);
        ivHide.setOnClickListener(this);
        etSearch.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    etSearch.setHint(null);
                } else {
                    etSearch.setHint("请输入搜索内容...");
                }
            }
        });
        initEvents();

        operatingAnim = AnimationUtils.loadAnimation(mActivity, R.anim.rotate_animation);
        LinearInterpolator lin = new LinearInterpolator();
        operatingAnim.setInterpolator(lin);
    }

    TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int i, int i1, int i2) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            String newText = s.toString().trim();
            queryPageOne(newText);
        }
    };

    public void queryPageOne(String seqrchKey) {
        mPage = 0;
        datas = new ArrayList<OrderInfoData>();
        searchQurery(dataChanel, seqrchKey);
    }

    public void initListData() {
        btnPendingPayment.setSelected(false);
        btnKeepingAccounts.setSelected(false);
        btnHavePayment.setSelected(false);

        switch (dataChanel) {
            case 0://未付
                btnPendingPayment.setSelected(true);
                break;
            case 1://记账
                btnKeepingAccounts.setSelected(true);
                break;
            case 2://已付
                btnHavePayment.setSelected(true);
                break;
        }
        queryPageOne(null);
    }

    private void initEvents() {
        btnPendingPayment.setSelected(true);
        btnHavePayment.setSelected(false);
        btnKeepingAccounts.setOnClickListener(this);
        btnPendingPayment.setOnClickListener(this);
        btnHavePayment.setOnClickListener(this);
        listview.setOnItemClickListener(this);
        listview.setOnLoadListener(new PullUpListView.OnLoadListener() {
            public void onLoad() {
                mPage++;
                searchQurery(dataChanel, null);
            }
        });
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        OrderInfoData orderData = adapter.getSelected(i);//选中
        if (mPosLitener != null && orderData != null) {
            if (OrderStatusEnum.getOrderEnum(orderData.getStatus()) == OrderStatusEnum.ReOpen) {//重新打开的定单改为已下单状态
                String status = OrderStatusEnum.getEnumValue(OrderStatusEnum.HandIn);
                orderData.setStatus(status);
                LitepalHelper.getInstance().updateOrderStatus(status, orderData.getId(), OAuthUtils.getInstance().getUserinfo(mContext));
            }
            mPosLitener.settleListClick(orderData);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_pending_payment:
                mTvAmountTitle.setText("应付总价");
                this.dataChanel = 0;
                initListData();
                break;
            case R.id.btn_keeping_accounts:
                mTvAmountTitle.setText("记账金额");
                this.dataChanel = 1;
                initListData();
                break;
            case R.id.btn_have_payment:
                mTvAmountTitle.setText("付款总额");
                this.dataChanel = 2;
                initListData();
                break;
            case R.id.id_for_refresh:
                if (operatingAnim != null) {
                    ivRefresh.startAnimation(operatingAnim);
                    queryDataFromService();
                }
                break;
            case R.id.id_for_search:
                hideSearchTitle();
                break;
            case R.id.id_for_clean:
                etSearch.setText("");
                break;
            case R.id.id_for_search_hide:
                showSearchTitle();
                break;

        }
    }

    private void searchQurery(int dataChanel, String newText) {
        this.dataChanel = dataChanel;
        AuthModel.DataInfo.UserInfo userInfo = OAuthUtils.getInstance().getUserinfo(mContext);
        StringBuffer sb = new StringBuffer();

        switch (dataChanel) {
            case 0://未付
                sb.append(" status in ('1','5') and id>? and OrgId=");
                break;
            case 1://记账
                sb.append(" status in ('6') and id>? and OrgId=");
                break;
            case 2://已付
                sb.append(" status in ('2','3','4') and id>? and OrgId=");
                break;
        }
        sb.append(userInfo.getOrgId());
        sb.append(" ");
        if (!Util.isNullStr(newText)) {
            sb.append(" and ( orderNo like '%");
            sb.append(newText);
            sb.append("%' or CustomerName like '%");
            sb.append(newText);
            sb.append("%' or CellPhone like '%");
            sb.append(newText);
            sb.append("%') ");
        }
        List<OrderInfoData> queryDatas = DataSupport.where(sb.toString(), "0").order(" orderNo DESC ").limit(limitSize).offset(mPage * limitSize).find(OrderInfoData.class);
        if (datas == null) {
            datas = new ArrayList<OrderInfoData>();
        }

        if (queryDatas != null && queryDatas.size() > 0) {
            datas.addAll(queryDatas);
            listview.setResultSize(queryDatas.size());
        }
        adapter.setData(datas);
        if (mPage == 0) {
            listview.setSelection(0);
        }
        ivRefresh.clearAnimation();
    }


    private void hideSearchTitle() {
        ivSearch.setVisibility(View.INVISIBLE);
        rlSearchLayout.setVisibility(View.VISIBLE);
        btnPendingPayment.setVisibility(View.INVISIBLE);
        btnKeepingAccounts.setVisibility(View.INVISIBLE);
        btnHavePayment.setVisibility(View.INVISIBLE);
        ivRefresh.setVisibility(View.INVISIBLE);
        ivHide.setVisibility(View.VISIBLE);
    }

    private void showSearchTitle() {
        ivSearch.setVisibility(View.VISIBLE);
        rlSearchLayout.setVisibility(View.GONE);
        btnPendingPayment.setVisibility(View.VISIBLE);
        btnKeepingAccounts.setVisibility(View.VISIBLE);
        btnHavePayment.setVisibility(View.VISIBLE);
        ivRefresh.setVisibility(View.VISIBLE);
        ivHide.setVisibility(View.GONE);

        InputMethodManager imm = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
        ivSearch.requestFocus();
        imm.hideSoftInputFromWindow(ivSearch.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        queryPageOne(null);
    }


    //服务端查询代付款
    public synchronized void queryDataFromService() {
        if (!isQueryIng) {
            isQueryIng = true;
            TimeUtils timeUtil = TimeUtils.getInstance();
            AuthModel.DataInfo.UserInfo userInfo = OAuthUtils.getInstance().getUserinfo(mActivity);
            int orgId = userInfo.getOrgId();
            String startTime = timeUtil.formatParmasData(timeUtil.beforeDate(new Date()));//前一天

            new GetOrgCpaymentProto(mContext).queryDataFromService(orgId + "", startTime, new ProtocolListener() {
                @Override
                public void onCompleted() {
                }

                @Override
                public void onNext() {
                    queryPageOne(null);
                    isQueryIng = false;
                }

                @Override
                public void onError() {
                    queryPageOne(null);
                    isQueryIng = false;
                }
            });
        }
    }
}
