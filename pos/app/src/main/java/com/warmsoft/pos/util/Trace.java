package com.warmsoft.pos.util;

import android.util.Log;

/**
 * 作者: lijinliu on 2016/8/5.
 * 邮箱：jinliu.li@warmsoft.com
 * 描述: 日志输出
 */
public class Trace {

    private static final String logTag = "warmpos";
    private static final boolean DEBUG = true;

    public final static void e(String tag, String msg, Throwable tr) {
        if (DEBUG)
            Log.e(logTag, tag + msg + "\n", tr);
    }

    public final static void e(String tag, String msg) {
        if (DEBUG)
            Log.e(logTag, tag + msg + "\n");
    }

    public final static void e(String msg) {
        if (DEBUG)
            Log.e(logTag, msg + "\n");
    }

    public final static void e(Throwable tr) {
        if (DEBUG)
            Log.e(logTag, logTag, tr);
    }

    public final static void d(String tag, String msg) {
        if (DEBUG)
            Log.d(logTag, tag + msg + "\n");
    }

    public final static void d(String msg) {
        if (DEBUG)
            Log.d(logTag, msg + "\n");
    }

    public final static void i(String tag, String msg) {
        if (DEBUG)
            Log.d(logTag, tag + msg + "\n");
    }

    public final static void i(String msg) {
        if (DEBUG)
            Log.d(logTag, msg + "\n");
    }

}
