package com.warmsoft.pos.adapter.entiy;

import com.warmsoft.pos.litepal.data.GoodItemInfo;

/**
 * 作者: lijinliu on 2016/8/9.
 * 邮箱：jinliu.li@warmsoft.com
 * 描述: 定单列表信息
 */
public class OrderItem {

    private String name;
    private boolean isSureed = false;
    private int size;
    private float  InstorePrice;
    private float  OutstorePrice;
    private float  MemberPrice;
    private float amount;
    private GoodItemInfo goodsInfo;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isSureed() {
        return isSureed;
    }

    public void setIsSureed(boolean isSureed) {
        this.isSureed = isSureed;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public float getInstorePrice() {
        return InstorePrice;
    }

    public void setInstorePrice(float instorePrice) {
        InstorePrice = instorePrice;
    }

    public float getOutstorePrice() {
        return OutstorePrice;
    }

    public void setOutstorePrice(float outstorePrice) {
        OutstorePrice = outstorePrice;
    }

    public float getMemberPrice() {
        return MemberPrice;
    }

    public void setMemberPrice(float memberPrice) {
        MemberPrice = memberPrice;
    }

    public float getAmount() {
        return amount;
    }

    public void setAmount(float amount) {
        this.amount = amount;
    }

    public GoodItemInfo getGoodsInfo() {
        return goodsInfo;
    }

    public void setGoodsInfo(GoodItemInfo goodsInfo) {
        this.goodsInfo = goodsInfo;
    }
}
