package com.warmsoft.pos.fragment.main;

import android.os.Bundle;
import android.support.v4.util.ArrayMap;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.warmsoft.base.BaseFragment;
import com.warmsoft.pos.R;
import com.warmsoft.pos.adapter.CommonAdapter;
import com.warmsoft.pos.adapter.CustomerAccountRechargeListAdapter;
import com.warmsoft.pos.adapter.CustomerListAdapter;
import com.warmsoft.pos.adapter.PetInfoRecyclerViewAdapter;
import com.warmsoft.pos.adapter.RechargeHistoryRecyclerViewAdapter;
import com.warmsoft.pos.adapter.RecyclerViewAdapter;
import com.warmsoft.pos.bean.AccountInfoModel;
import com.warmsoft.pos.bean.AccountRechargeHistoryModel;
import com.warmsoft.pos.bean.AccountRechargeModel;
import com.warmsoft.pos.bean.AmountsModel;
import com.warmsoft.pos.bean.AuthModel;
import com.warmsoft.pos.bean.BackCardModel;
import com.warmsoft.pos.bean.DepositRefundModel;
import com.warmsoft.pos.bean.MemberInfoModel;
import com.warmsoft.pos.bean.MemberRechargeModel;
import com.warmsoft.pos.bean.PetInfoModel;
import com.warmsoft.pos.bean.PrePledgeRechargeModel;
import com.warmsoft.pos.bean.RemoveRechargeModel;
import com.warmsoft.pos.dialog.LoadingDialog;
import com.warmsoft.pos.dialog.MemberAddMemberDialog;
import com.warmsoft.pos.dialog.NomelNoteDialog;
import com.warmsoft.pos.litepal.data.CardKind;
import com.warmsoft.pos.litepal.data.CustomerList;
import com.warmsoft.pos.litepal.data.LitepalHelper;
import com.warmsoft.pos.litepal.data.PetCategory;
import com.warmsoft.pos.litepal.data.PetData;
import com.warmsoft.pos.net.HttpStatus;
import com.warmsoft.pos.service.SyncDataUtil;
import com.warmsoft.pos.util.DrawableUtil;
import com.warmsoft.pos.util.LogUtil;
import com.warmsoft.pos.util.MemberUtil;
import com.warmsoft.pos.util.OAuthUtils;
import com.warmsoft.pos.util.PerformanceUtils;
import com.warmsoft.pos.util.SelectMemberUtil;
import com.warmsoft.pos.util.Util;
import com.warmsoft.pos.view.PullUpListView;

import org.angmarch.views.NiceSpinner;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.litepal.crud.DataSupport;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FragmentVip extends BaseFragment implements OnClickListener, TextWatcher {
    private final int REFRESH_CUSTOMER = 1000;
    protected int mPage = 0;
    protected int limitSize = 20;//通过分页显示相关的数据
    RecyclerView mRecyclerAddPet;
    PullUpListView mPullUpListView;
    CustomerListAdapter customeAdapter;

    RecyclerView mRechargeHistoryRecycler;
    RechargeHistoryRecyclerViewAdapter rechargeHistoryRecyclerViewAdapter;

    //    PullUpListView mPullUpListView;
//    CustomerListAdapter customeAdapter;
//
//    PullUpListView mPullUpListView;
//    CustomerListAdapter customeAdapter;
    PullUpListView mAccountRechargePullUplistView;
    CustomerAccountRechargeListAdapter mCustomerAccountRechargeListAdapter;
    Animation operatingAnim;
    List<CustomerList> customeList = new ArrayList<>();
    List<CustomerList> accountRechargeLists = new ArrayList<>();
    List<CustomerList> prePledgeLists = new ArrayList<>();
    List<CustomerList> memberRechargeLists = new ArrayList<>();
    int recharge = 0;//0 隐藏全部,1显示账户充值,2显示预交押金,3显示会员卡充值
    MemberInfoModel memberInfoModel = new MemberInfoModel();
    List<PetInfoModel> petInfoModelList = new ArrayList<>();
    List<PetData> showPets = new ArrayList<>();
    boolean monitor = false;
    private List<CustomerList> mDatas = new ArrayList<>();
    private CommonAdapter<PetInfoModel> mShowPetsAdapter;
    private RecyclerViewAdapter recyclerViewAdapter;
    private PetInfoRecyclerViewAdapter showPetRecyclerViewAdapter;

    private ImageView ivMemberInfoIcon;
    private TextView tvMemberName;
    private TextView tvMobile;
    private TextView tvId;
    private TextView tvBirthDay;
    private TextView tvCardTime;
    private TextView tvCardLevel;
    private TextView tvOrgin;
    private TextView tvAddress;
    private TextView tvAddNewMember;
    private TextView tvMail;
    //private TextView tvLast;
    private TextView tvPetInfo;
    private TextView tvBalance;

    private ImageView ivHistory;
    private ImageView ivRefresh;
    private ImageView ivMemberRefresh;
    private TextView tvMemberRefresh;
    private ImageView ivSearch;
    private ImageView ivHide;
    private Button btnMemberList;
    private Button btnAddMember;
    private Button btnMemberRecharge;

    private RelativeLayout rlAddPetInfo;
    private LinearLayout llCustomerBackground;
    private TextView tvSubmit;
    private TextView tvCancel;
    private List<CardKind> memberCards = new ArrayList<>();
    private List<PetCategory> petCategoryList = new ArrayList<>();
    private RelativeLayout rlSearchLayout;
    private ImageView ivClean;
    private EditText etSearch;
    private RadioGroup rgMemberRecharge;
    private LinearLayout llCashPledge;
    private LinearLayout llAccountRecharge;
    private LinearLayout llMemberRecharge;

    //账户充值
    private TextView tvAccountRechargeTotalAmount;
    private TextView tvAccountRechargeCashAmount;
    private TextView tvAccountRechargeCurrentPoint;
    private EditText etAccountRechargeCashRecharge;
    private EditText etAccountRechargeSwingCardRecharge;
    private EditText etAccountRechargeBonusAmount;
    private EditText etAccountRechargeBonusPoint;
    private TextView tvAccountRechargeRecharge;
    private TextView tvAccountRechargeReturnRecharge;

    //预交押金
    private TextView tvPledgeType;
    private TextView tvPledgeTotalAmount;
    private EditText etPrePledge;
    private EditText etPreSwingCard;
    private TextView tvPreCommit;
    private TextView tvReturnPledge;

    //会员卡充值
    private TextView tvMemberRechargeCardNumber;
    private TextView tvMemberRechargeCardType;
    private TextView tvMemberRechargeCardBalance;
    private EditText etMemberRechargeCashRecharge;
    private EditText etMemberRechargeSwingCardRecharge;
    private EditText etMemberRechargeBonusAmount;
    private TextView tvMemberRechargeNew;
    private TextView tvMemberRechargeRecharge;
    private TextView tvMemberRechargeReturnPledge;
    private TextView tvMemberRechargeReturnCard;
    private RadioButton rbAccountRecharge;
    private RadioButton rbCashPledge;
    private RadioButton rbMemberRecharge;
    private NiceSpinner memberCard;
    private ImageView ivMemberAdd;

    //会员相关
    private EditText etName;
    private EditText etMobile;

    //账户充值显示
    private LinearLayout llMemberList;
    private LinearLayout llAccountList;
    private LinearLayout llPreRechargeList;
    private LinearLayout llMemberRechargeList;
    private LitepalHelper mLitepal;
    private CustomerList customer;//选中的会员
    private LoadingDialog mDialog;

    private final int TYPE_OF_MEMEBER_ADD = 1;
    private final int TYPE_OF_MEMEBER_SHOW = 2;

    //控制区域
    private RelativeLayout rlLeftControl;
    private LinearLayout llMemControl;
    private RelativeLayout rlMemShow;
    private RelativeLayout rlMemberAdd;
    private RelativeLayout rlMemAddControl;
    private RelativeLayout rlLeftRechargeControl;

    //充值记录
    private LinearLayout llRechargeSwitchControl;
    private TextView tvRechargeEmptyInfo;

    private LinearLayout llRechargeSwitch;
    private LinearLayout llRecordHistorySwitch;

    private TextView tvNewRechargeActionSwitch;
    private TextView tvNewRechageHistorySwitch;
    private TextView tvRechargeTopMiddle;

    //显示会员
    private RecyclerView petInfoRecyclerView;
    private ImageView ivRefreshCurrentAmount;

    //会员列表和充值记录之间切换
    private TextView tvRechargeActionSwitch;
    private TextView tvRechageHistorySwitch;
    private RadioGroup rgMemberRechargeSwitch;

    private RadioButton rbNewAccountRecharge;
    private RadioButton rbNewCashPledge;
    private RadioButton rbNewMemberRecharge;

    //新账户-充值
    private Button tvTopControl;
    private TextView tvTopRechargeHistory;
    private TextView tvTopRechargeControl;

    private ImageView ivNewMemberHeader;
    private TextView tvNewMemberCardId;
    private TextView tvNewMemberName;
    private TextView tvNewMemberMobile;
    private TextView tvNewMemberCardLevel;
    private TextView tvNewMemberAccountAmount;
    private TextView tvNewMemberPledge;
    private TextView tvNewMemberRechargeCard;
    private TextView tvNewMemberCurrentPoints;

    private LinearLayout llNewAccountContent;
    private LinearLayout llNewPrePledgeContent;
    private LinearLayout llNewMemberRechargeContent;

    private LinearLayout llNewRechargeAccount;
    private LinearLayout llNewRemoveRechargeAccount;

    private ProgressBar pbNewAccountRecharge;

    //帐户充值行为
    private LinearLayout llNewRechargeBackCard;
    private TextView tvNewRemoveRechargeCard;

    private TextView tvNewRemoveValidAccount;
    private ImageView tvNewRemoveValidAccountHelp;

    private int accountRechargeType = 0; //0:现金, 1:银行卡, 2微信, 3支付宝
    private int prePledgeRechargeType = 0; //0:现金, 1:银行卡, 2微信, 3支付宝
    private int memberRechargeType = 0; //0:现金, 1:银行卡, 2微信, 3支付宝

    private int accountRemoveRechargeType = 0; //0:现金, 1:银行卡, 2微信, 3支付宝
    private int prePledgeRemoveRechargeType = 0; //0:现金, 1:银行卡, 2微信, 3支付宝
    private int memberRemoveRechargeType = 0; //0:现金, 1:银行卡, 2微信, 3支付宝

    private RadioGroup rgNewAccountRecharge;
    private RadioButton rbNewAccountRechargeCard;
    private RadioButton rbNewAccountRechargeCash;
    private RadioButton rbNewAccountRechargeWx;
    private RadioButton rbNewAccountRechargeAlipay;

    private EditText etNewAccountRechargeAmount;
    private EditText etNewAccountRechargeBonusAmount;
    private EditText etNewAccountRechargeBonusPoints;
    private TextView tvNewAccountRechargeRecharge;
    private LinearLayout llNewAccountRechargeRecharge;
    private ProgressBar pbNewAccountRechargeRecharge;
    private TextView tvNewAccountRechargeRechargeCurrentAmount;

    private RadioGroup rgNewRemoveAccountRecharge;
    private RadioButton rbNewRemoveAccountRechargeCard;
    private RadioButton rbNewRemoveAccountRechargeCash;
    private RadioButton rbNewRemoveAccountRechargeWx;
    private RadioButton rbNewRemoveAccountRechargeAlipay;

    private TextView etNewRemoveAccountRechargeAmount;
    private TextView etNewRemoveAccountRechargeBonusAmount;
    private TextView etNewRemoveAccountRechargeBonusPoints;
    private TextView tvRemoveNewAccountRechargeRechargeAllAmount;
    private TextView tvRemoveNewAccountRechargeRechargeCancel;
    private LinearLayout llRemoveNewAccountRechargeRechargeCommit;
    private ProgressBar pbRemoveNewAccountRechargeRecharge;
    private TextView tvRemoveNewAccountRechargeRechargeCommit;
    private TextView tvRemonveNewAccountRechargeRechargeCurrentAmount;

    //新预交押金-冲押金
    private LinearLayout llNewPrePledgeRechargeAccount;
    private LinearLayout llNewPrePledgeRechargeRemoveAccount;

    private LinearLayout llNewPrePledgeRechargeRecharge;
    private ProgressBar pbNewPrePledgeRechargeRecharge;

    private RadioGroup rgNewPrePledgeRG;
    private RadioButton rbNewPrePledgeCard;
    private RadioButton rbNewPrePledgeCash;
    private RadioButton rbNewPrePledgeWx;
    private RadioButton rbNewPrePledgeAlipay;

    private EditText etNewPrePledgeAmount;
    private TextView tvNewPrePledgeCommit;
    private TextView tvNewPrePledgeCurrentAmount;
    private TextView tvNewPrePledgeRemovePledge;

    //新预交押金-退押金
    private RadioGroup rgNewRemovePrePledgeRG;
    private RadioButton rbNewRemovePrePledgeCard;
    private RadioButton rbNewRemovePrePledgeCash;
    private RadioButton rbNewRemovePrePledgeWx;
    private RadioButton rbNewRemovePrePledgeAlipay;

    private TextView etNewRemovePrePledgeAmount;
    private TextView tvNewRemovePrePledgeCancel;
    private TextView tvNewRemovePrePledgeCommit;
    private LinearLayout tvNewRemovePrePledgeShowInfoll;
    private TextView tvValidRemovePrepledge;

    //新会员卡-充值
    private LinearLayout llNewMemberRechargeAccount;
    private LinearLayout llNewMemberRechargeRemoveAccount;
    private TextView tvNewMemberRechargeEmptyAccount;

    private ProgressBar pbNewMemberRechargeGetCurrentInfo;
    private ProgressBar pbNewMemberRechargeCommit;

    private LinearLayout llNewMemberRecharge;

    private TextView tvCardNumber;

    private RadioGroup rgNewMemberRG;
    private RadioButton rbNewMemberCard;
    private RadioButton rbNewMemberCash;
    private RadioButton rbNewMemberWx;
    private RadioButton rbNewMemberAlipay;

    private EditText etNewMemberAmount;
    private EditText etNewMemberBonusAmount;
    private TextView tvNewMemberCommit;
    private TextView tvNewMemberCurrentAmount;
    private TextView tvNewMemberRemoveBankCard;

    //新会员卡-退款
    private int cardId = -1;
    private TextView tvRemoveCardNumber;

    private RadioGroup rgNewRemoveMemberRG;
    private RadioButton rbNewRemoveMemberCard;
    private RadioButton rbNewRemoveMemberCash;
    private RadioButton rbNewRemoveMemberWx;
    private RadioButton rbNewRemoveMemberAlipay;

    private TextView etNewRemoveMemberAllAmount;
    private TextView etNewRemoveMemberBonusPoints;
    private TextView etNewRemoveMemberBonusAmount;
    private TextView tvNewRemoveMemberAllAmount;
    private TextView tvNewRemoveMemberCancel;
    private ProgressBar pbNewRemoveMemberCommit;
    private LinearLayout llNewRemoveMemberCommit;
    private TextView tvNewRemoveMemberCommit;
    private LinearLayout tvNewRemoveMemberShowInfoll;

    private TextView tvNewRemoveMembeAmount;
    private ImageView ivNewRemoveMemberHelp;

    private LinearLayout llRemoveNewPrePledgeRechargeCommit;
    private ProgressBar pbRemoveNewNewPrePledgeRecharge;
    private TextView tvMemberCount;
    private TextView tvMemberCountLabel;

    //充值历史
    List<AccountRechargeHistoryModel.DataInfo> rechargeHistoryLists = new ArrayList<>();

    //账户充值
    NomelNoteDialog.DialogListener accountRecharge = new NomelNoteDialog.DialogListener() {

        @Override
        public void noteDialogConfirm() {
            if (customer != null) {
                pbNewAccountRechargeRecharge.setVisibility(View.VISIBLE);
                tvNewAccountRechargeRecharge.setText("充值中...");

                String amount = etNewAccountRechargeAmount.getText().toString().trim();
                String bonusAmount = etNewAccountRechargeBonusAmount.getText().toString().trim();
                String bonusPoints = etNewAccountRechargeBonusPoints.getText().toString().trim();
                String totalAmount = tvNewAccountRechargeRechargeCurrentAmount.getText().toString().trim();
                totalAmount = totalAmount.replace("¥", "").trim();

                LogUtil.error("totalAmount " + totalAmount);

//              #以前的接口
//                Map<String, Object> map = new ArrayMap<>();
//                map.put("CustomerID", customer.getCustomerId());
//                map.put("CustomerName", customer.getName());
//                map.put("Operator", OAuthUtils.getInstance().getUserinfo(mContext).getId());
//                map.put("OperatorName", OAuthUtils.getInstance().getUserinfo(mContext).getName());
//                map.put("CardPayed", getCurrentRechargeAmount(1, amount, accountRechargeType));
//                map.put("Caish", getCurrentRechargeAmount(0, amount, accountRechargeType));
//                map.put("ExtendAmount", bonusAmount);
//                map.put("CurrentTotalAmount", totalAmount.replace("¥", "").trim());
//                map.put("AliPay", getCurrentRechargeAmount(3, amount, accountRechargeType));
//                map.put("WeChat", getCurrentRechargeAmount(2, amount, accountRechargeType));
//                map.put("Status", 0);
//                map.put("Score", bonusPoints);

                Map<String, Object> map = new ArrayMap<>();
                map.put("OrgId", customer.getOrgId());
                map.put("CustomerId", customer.getCustomerId());
                map.put("Operator", OAuthUtils.getInstance().getUserinfo(mContext).getId());
                map.put("OperatorName", OAuthUtils.getInstance().getUserinfo(mContext).getName());
                map.put("CardPayed", getCurrentRechargeAmount(1, amount, accountRechargeType));
                map.put("Caish", getCurrentRechargeAmount(0, amount, accountRechargeType));
                map.put("ExtendAmount", bonusAmount);
                map.put("CurrentTotalAmount", totalAmount);
                map.put("AliPay", getCurrentRechargeAmount(3, amount, accountRechargeType));
                map.put("WeChat", getCurrentRechargeAmount(2, amount, accountRechargeType));
                map.put("Status", 0);
                map.put("BarCode", "");
                map.put("Score", bonusPoints);

                SyncDataUtil.getInstance().syncAccountRecharge(map);
            }
        }
    };

    NomelNoteDialog.DialogListener prePledgeRecharge = new NomelNoteDialog.DialogListener() {

        @Override
        public void noteDialogConfirm() {
            if (customer != null) {
                pbNewPrePledgeRechargeRecharge.setVisibility(View.VISIBLE);
                tvNewPrePledgeCommit.setText("预交中...");

                String amount = etNewPrePledgeAmount.getText().toString().trim();

//                Map<String, Object> map = new ArrayMap<>();
//                map.put("CustomerID", customer.getCustomerId());
//                map.put("CustomerName", customer.getName());
//                map.put("Operator", OAuthUtils.getInstance().getUserinfo(mContext).getId());
//                map.put("OperatorName", OAuthUtils.getInstance().getUserinfo(mContext).getName());
//                map.put("CardPayed", getCurrentRechargeAmount(1, amount, prePledgeRechargeType));
//                map.put("Caish", getCurrentRechargeAmount(0, amount, prePledgeRechargeType));
//                map.put("AliPay", getCurrentRechargeAmount(3, amount, prePledgeRechargeType));
//                map.put("WeChat", getCurrentRechargeAmount(2, amount, prePledgeRechargeType));
//                map.put("Status", 7695);

                Map<String, Object> map = new ArrayMap<>();
                map.put("OrgId", customer.getOrgId());
                map.put("CustomerId", customer.getCustomerId());
                map.put("CustomerName", customer.getName());
                map.put("Operator", OAuthUtils.getInstance().getUserinfo(mContext).getId());
                map.put("OperatorName", OAuthUtils.getInstance().getUserinfo(mContext).getName());
                map.put("CardPayed", getCurrentRechargeAmount(1, amount, prePledgeRechargeType));
                map.put("Caish", getCurrentRechargeAmount(0, amount, prePledgeRechargeType));
                map.put("AliPay", getCurrentRechargeAmount(3, amount, prePledgeRechargeType));
                map.put("WeChat", getCurrentRechargeAmount(2, amount, prePledgeRechargeType));
                map.put("Status", 7695);
                map.put("Score", 0);
                map.put("BarCode", "");

                SyncDataUtil.getInstance().syncPrePledgeRecharge(map);
            }
        }
    };

    NomelNoteDialog.DialogListener memberRecharge = new NomelNoteDialog.DialogListener() {

        @Override
        public void noteDialogConfirm() {
            if (customer != null) {
                pbNewMemberRechargeCommit.setVisibility(View.VISIBLE);
                tvNewMemberCommit.setText("充值中...");

                String amount = etNewMemberAmount.getText().toString().trim();
                String bonusAmount = etNewMemberBonusAmount.getText().toString().trim();
                String totalAmount = tvNewMemberCurrentAmount.getText().toString().trim();

//                Map<String, Object> map = new ArrayMap<>();
//                map.put("CustomerID", customer.getCustomerId());
//                map.put("CustomerName", customer.getName());
//                map.put("PaymentType", 1);//账单类型 0:消费 1：充值 2：退款
//                map.put("CardType", 2812);//卡类型：默认2812（综合卡）
//                map.put("PayedByCard", getCurrentRechargeAmount(1, amount, memberRechargeType));
//                map.put("PayedByCashie", getCurrentRechargeAmount(0, amount, memberRechargeType));
//                map.put("ExtendAmount", bonusAmount);
//                map.put("CurrentTotalAmount", totalAmount.replace("¥", ""));
//                map.put("AliPay", getCurrentRechargeAmount(3, amount, memberRechargeType));
//                map.put("WeChat", getCurrentRechargeAmount(2, amount, memberRechargeType));

                Map<String, Object> map = new ArrayMap<>();
                map.put("OrgId", customer.getOrgId());
                map.put("CustomerId", customer.getCustomerId());
                map.put("CustomerName", customer.getName());
                map.put("PaymentType", 1);//账单类型 0:消费 1：充值 2：退款
                map.put("CardType", 2812);//卡类型：默认2812（综合卡）
                map.put("PayedByCard", getCurrentRechargeAmount(1, amount, memberRechargeType));
                map.put("PayedByCashie", getCurrentRechargeAmount(0, amount, memberRechargeType));
                map.put("ExtendAmount", bonusAmount);
                map.put("CurrentTotalAmount", totalAmount.replace("¥", ""));
                map.put("AliPay", getCurrentRechargeAmount(3, amount, memberRechargeType));
                map.put("WeChat", getCurrentRechargeAmount(2, amount, memberRechargeType));
                map.put("BarCode", "");
                map.put("Score", 0);

                SyncDataUtil.getInstance().syncMemberRecharge(map);
            }
        }
    };

    NomelNoteDialog.DialogListener accountRemoveRecharge = new NomelNoteDialog.DialogListener() {

        @Override
        public void noteDialogConfirm() {
            if (customer != null) {
                pbRemoveNewAccountRechargeRecharge.setVisibility(View.VISIBLE);
                tvRemoveNewAccountRechargeRechargeCommit.setText("退款中...");

                String amount = etNewRemoveAccountRechargeAmount.getText().toString().trim();
                String bonusAmount = etNewRemoveAccountRechargeBonusAmount.getText().toString().trim();
                String bonusPoints = etNewRemoveAccountRechargeBonusPoints.getText().toString().trim();

//                Map<String, Object> map = new ArrayMap<>();
//                map.put("CustomerID", customer.getCustomerId());
//                map.put("CustomerName", customer.getName());
//                map.put("Amount", Double.parseDouble(amount));
//                map.put("PresentAmount", Double.parseDouble(bonusAmount));
//                map.put("Score", Integer.parseInt(bonusPoints));

                Map<String, Object> map = new ArrayMap<>();
                map.put("OrgId", customer.getOrgId());
                map.put("CustomerID", customer.getCustomerId());
                map.put("CustomerName", customer.getName());
                map.put("Amount", Double.parseDouble(amount));//总金额
                map.put("Caish", (Double.parseDouble(amount) - Double.parseDouble(bonusAmount)));//需要退的金额 总金额-赠送金额
                map.put("CardPayed", 0);
                map.put("Score", Integer.parseInt(bonusPoints));

                SyncDataUtil.getInstance().syncRetirementAccount(map);
            }
        }
    };

    NomelNoteDialog.DialogListener prePledgeRemoveRecharge = new NomelNoteDialog.DialogListener() {

        @Override
        public void noteDialogConfirm() {
            if (customer != null) {
                pbRemoveNewNewPrePledgeRecharge.setVisibility(View.VISIBLE);
                tvNewRemovePrePledgeCommit.setText("退押金中...");

                String amount = etNewRemovePrePledgeAmount.getText().toString().trim();

//                Map<String, Object> map = new ArrayMap<>();
//                map.put("OrgId", customer.getOrgId());
//                map.put("CustomerId", customer.getCustomerId());
//                map.put("CustomerName", customer.getName());
//                map.put("Amount", amount);
//                map.put("Caish", amount);
//                map.put("CardPayed", 0);
//                map.put("Account", amount);
//                map.put("PreType", 7695);
//                map.put("Score", 0);

                Map<String, Object> map = new ArrayMap<>();
                map.put("OrgId", customer.getOrgId());
                map.put("CustomerId", customer.getCustomerId());
                map.put("CustomerName", customer.getName());
                map.put("Amount", amount);//实际退的金额
                map.put("Caish", amount);
                map.put("CardPayed", 0);
                map.put("Account", 0);
                map.put("PreType", 7695);
                map.put("Score", 0);

                SyncDataUtil.getInstance().syncDepositRefund(map);
            }
        }
    };

    NomelNoteDialog.DialogListener memberRemoveRecharge = new NomelNoteDialog.DialogListener() {

        @Override
        public void noteDialogConfirm() {
            if (customer != null) {
                pbNewRemoveMemberCommit.setVisibility(View.VISIBLE);
                tvNewRemoveMemberCommit.setText("退卡中...");

                String bonusAmount = etNewRemoveMemberBonusAmount.getText().toString().trim();
                String totalAmount = tvNewRemoveMemberAllAmount.getText().toString().trim();

//                Map<String, Object> map = new ArrayMap<>();
//                map.put("CustomerId", customer.getCustomerId());
//                map.put("ActlyAmount", totalAmount);
//                map.put("ExtendPrice", bonusAmount);
//                map.put("PresentAmount", 0);
//                map.put("PaymentType", 3);
//                map.put("CardId", cardId);
//                map.put("CardNo", tvRemoveCardNumber.getText().toString().trim());

                Map<String, Object> map = new ArrayMap<>();
                map.put("OrgId", customer.getOrgId());
                map.put("CustomerId", customer.getCustomerId());
                map.put("Operator", OAuthUtils.getInstance().getUserinfo(mContext).getId());
                map.put("TotalAmount", totalAmount);//实退金额
                map.put("ActlyAmount", Double.parseDouble(totalAmount) - Double.parseDouble(bonusAmount));//实退金额
                map.put("PaymentType", 3);
                map.put("CardId", cardId);
                map.put("CardNo", tvRemoveCardNumber.getText().toString().trim());

                SyncDataUtil.getInstance().syncBackCard(map);
            }
        }
    };

    private String getCurrentRechargeAmount(int type, String amount, int accountRechargeType) {
        return type == accountRechargeType ? amount : "0";
    }

    //会员添加
    private RecyclerView mAddRecyclerView;

    public static FragmentVip newInstance() {
        FragmentVip fragment = new FragmentVip();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (mLayoutView == null) {
            mLayoutView = inflater.inflate(R.layout.fragment_main_vip, container, false);

            initViews();
            initEvents();
        } else {
            ViewGroup viewGroup = (ViewGroup) mLayoutView.getParent();
            if (viewGroup != null) {
                viewGroup.removeView(mLayoutView);
            }
        }

        return mLayoutView;
    }

    private void initViews() {
        ivMemberInfoIcon = (ImageView) mLayoutView.findViewById(R.id.id_for_member_header);
        tvMemberName = (TextView) mLayoutView.findViewById(R.id.id_for_member_name);
        tvBalance = (TextView) mLayoutView.findViewById(R.id.id_for_card_money);
        tvMobile = (TextView) mLayoutView.findViewById(R.id.id_for_member_mobile);
        tvId = (TextView) mLayoutView.findViewById(R.id.id_for_member_id);
        tvBirthDay = (TextView) mLayoutView.findViewById(R.id.id_for_birth_day);
        tvCardTime = (TextView) mLayoutView.findViewById(R.id.id_for_card_time);
        tvCardLevel = (TextView) mLayoutView.findViewById(R.id.id_for_new_member_level);
        tvOrgin = (TextView) mLayoutView.findViewById(R.id.id_for_orgin);
        tvAddress = (TextView) mLayoutView.findViewById(R.id.id_for_address);
        tvMail = (TextView) mLayoutView.findViewById(R.id.id_for_mail);
//        tvLast = (TextView) mLayoutView.findViewById(R.id.id_for_last_interview);
        tvPetInfo = (TextView) mLayoutView.findViewById(R.id.id_for_pet_info);

        ivHistory = (ImageView) mLayoutView.findViewById(R.id.id_for_history_record);
        ivHistory.setOnClickListener(this);

        ivRefresh = (ImageView) mLayoutView.findViewById(R.id.id_for_refresh);
        ivRefresh.setOnClickListener(this);

        ivSearch = (ImageView) mLayoutView.findViewById(R.id.id_for_search);
        ivSearch.setOnClickListener(this);

        ivMemberRefresh = (ImageView) mLayoutView.findViewById(R.id.id_for_member_refresh);
        ivMemberRefresh.setOnClickListener(this);

        tvMemberRefresh = (TextView) mLayoutView.findViewById(R.id.id_for_member_refresh_tv);
        tvMemberRefresh.setOnClickListener(this);

        tvAddNewMember = (TextView) mLayoutView.findViewById(R.id.id_for_memeber_add);
        DrawableUtil.getInstance().setDrawableOriType(mContext, 1, tvAddNewMember, R.mipmap.icon_add);
        tvAddNewMember.setOnClickListener(this);

        operatingAnim = AnimationUtils.loadAnimation(getActivity(), R.anim.rotate_animation);
        LinearInterpolator lin = new LinearInterpolator();
        operatingAnim.setInterpolator(lin);

        rlSearchLayout = (RelativeLayout) mLayoutView.findViewById(R.id.id_for_search_layout);
        rlSearchLayout.setVisibility(View.VISIBLE);

        ivClean = (ImageView) mLayoutView.findViewById(R.id.id_for_clean);
        ivClean.setOnClickListener(this);

        ivHide = (ImageView) mLayoutView.findViewById(R.id.id_for_search_hide);
        ivHide.setVisibility(View.GONE);
        ivHide.setOnClickListener(this);

        etSearch = (EditText) mLayoutView.findViewById(R.id.id_for_search_input);
        etSearch.addTextChangedListener(this);
        etSearch.setOnKeyListener(new View.OnKeyListener() {
            @Override

            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (keyCode == KeyEvent.KEYCODE_ENTER) {
                    return true;
                }
                return false;
            }
        });
        etSearch.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    etSearch.setHint(null);
                } else {
                    etSearch.setHint("会员编号/姓名/手机号");
                }
            }
        });

        llMemControl = (LinearLayout) mLayoutView.findViewById(R.id.id_for_member_control);
        rlMemShow = (RelativeLayout) mLayoutView.findViewById(R.id.id_for_left_rl_member_list_show);
        rlMemberAdd = (RelativeLayout) mLayoutView.findViewById(R.id.id_for_left_rl_init_info);
        rlMemAddControl = (RelativeLayout) mLayoutView.findViewById(R.id.id_for_left_rl_add_member);

        rlMemShow.setVisibility(View.VISIBLE);
        rlMemAddControl.setVisibility(View.INVISIBLE);

        btnMemberList = (Button) mLayoutView.findViewById(R.id.id_for_member_list);
        btnAddMember = (Button) mLayoutView.findViewById(R.id.id_for_add_member);
        btnMemberRecharge = (Button) mLayoutView.findViewById(R.id.id_for_add_recharge);

        btnAddMember.setOnClickListener(this);
        btnMemberList.setOnClickListener(this);
        btnMemberRecharge.setOnClickListener(this);

        rbAccountRecharge = (RadioButton) mLayoutView.findViewById(R.id.id_for_account_recharge_rb);
        rbCashPledge = (RadioButton) mLayoutView.findViewById(R.id.id_for_cash_pledge_rb);
        rbMemberRecharge = (RadioButton) mLayoutView.findViewById(R.id.id_for_member_recharge_rb);

        rbAccountRecharge.setOnClickListener(this);
        rbCashPledge.setOnClickListener(this);
        rbMemberRecharge.setOnClickListener(this);

        tvSubmit = (TextView) mLayoutView.findViewById(R.id.id_for_commit);
        tvCancel = (TextView) mLayoutView.findViewById(R.id.id_for_cancel);

        tvSubmit.setOnClickListener(this);
        tvCancel.setOnClickListener(this);


        rlAddPetInfo = (RelativeLayout) mLayoutView.findViewById(R.id.id_for_add_pet_rl);
        rlAddPetInfo.setOnClickListener(this);

        etName = (EditText) mLayoutView.findViewById(R.id.id_for_name_value);
        etMobile = (EditText) mLayoutView.findViewById(R.id.id_for_mobile_value);

        llMemberList = (LinearLayout) mLayoutView.findViewById(R.id.id_for_member_list_ll);

        llAccountList = (LinearLayout) mLayoutView.findViewById(R.id.id_for_account_recharge_ll);
        llPreRechargeList = (LinearLayout) mLayoutView.findViewById(R.id.id_for_pre_recharge_ll);
        llMemberRechargeList = (LinearLayout) mLayoutView.findViewById(R.id.id_for_member_recharge_ll);

        //显示会员充值
        llCashPledge = (LinearLayout) mLayoutView.findViewById(R.id.id_for_cash_pledge);
        llAccountRecharge = (LinearLayout) mLayoutView.findViewById(R.id.id_for_account_recharge);
        llMemberRecharge = (LinearLayout) mLayoutView.findViewById(R.id.id_for_member_recharge);

        //账户充值
        tvAccountRechargeTotalAmount = (TextView) mLayoutView.findViewById(R.id.id_for_account_recharge_all_amount);
        tvAccountRechargeCashAmount = (TextView) mLayoutView.findViewById(R.id.id_for_account_recharge_cash_amount);
        tvAccountRechargeCurrentPoint = (TextView) mLayoutView.findViewById(R.id.id_for_account_recharge_current_point);
        etAccountRechargeCashRecharge = (EditText) mLayoutView.findViewById(R.id.id_for_account_recharge_cash_recharge);
        etAccountRechargeSwingCardRecharge = (EditText) mLayoutView.findViewById(R.id.id_for_account_recharge_swing_card_recharge);
        etAccountRechargeBonusAmount = (EditText) mLayoutView.findViewById(R.id.id_for_account_recharge_bonus_amount);
        etAccountRechargeBonusPoint = (EditText) mLayoutView.findViewById(R.id.id_for_account_recharge_bonus_point);
        tvAccountRechargeRecharge = (TextView) mLayoutView.findViewById(R.id.id_for_account_recharge_recharge_commit);
        tvAccountRechargeReturnRecharge = (TextView) mLayoutView.findViewById(R.id.id_for_account_recharge_recharge_return);

        tvAccountRechargeRecharge.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                String cashRecharge = etAccountRechargeCashRecharge.getText().toString().trim();
                String swingCardRecharge = etAccountRechargeSwingCardRecharge.getText().toString().trim();
                String bonusAmount = etAccountRechargeBonusAmount.getText().toString().trim();
                String bonusPoints = etAccountRechargeBonusPoint.getText().toString().trim();
                String totalAmount = tvAccountRechargeTotalAmount.getText().toString().trim();

                if (isAllowAccountRecharge(cashRecharge, swingCardRecharge)) {
                    new NomelNoteDialog(mActivity, "确定要充值吗?", "充值金额:" + (Integer.parseInt(cashRecharge) + Integer.parseInt(swingCardRecharge)), "确定", accountRecharge);
                }
            }
        });

        tvAccountRechargeReturnRecharge.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        //预交押金
        tvPledgeType = (TextView) mLayoutView.findViewById(R.id.id_for_cash_pledge_type);
        tvPledgeTotalAmount = (TextView) mLayoutView.findViewById(R.id.id_for_cash_pledge_pledge_amount);
        etPrePledge = (EditText) mLayoutView.findViewById(R.id.id_for_cash_pledge_pre_pledge);
        etPreSwingCard = (EditText) mLayoutView.findViewById(R.id.id_for_cash_pledge_pre_swing_pledge);
        tvPreCommit = (TextView) mLayoutView.findViewById(R.id.id_for_cash_pledge_pre_commit);
        tvReturnPledge = (TextView) mLayoutView.findViewById(R.id.id_for_cash_pledge_return_pledge);

        //会员卡充值
        tvMemberRechargeCardNumber = (TextView) mLayoutView.findViewById(R.id.id_for_member_recharge_card_number);
        tvMemberRechargeCardType = (TextView) mLayoutView.findViewById(R.id.id_for_member_recharge_card_type);
        tvMemberRechargeCardBalance = (TextView) mLayoutView.findViewById(R.id.id_for_member_recharge_card_balance);
        etMemberRechargeCashRecharge = (EditText) mLayoutView.findViewById(R.id.id_for_member_recharge_cash_recharge);
        etMemberRechargeSwingCardRecharge = (EditText) mLayoutView.findViewById(R.id.id_for_member_recharge_swing_card_recharge);
        etMemberRechargeBonusAmount = (EditText) mLayoutView.findViewById(R.id.id_for_member_recharge_bonus_amount);
        tvMemberRechargeNew = (TextView) mLayoutView.findViewById(R.id.id_for_member_add);
        tvMemberRechargeRecharge = (TextView) mLayoutView.findViewById(R.id.id_for_member_recharge_btn);
        tvMemberRechargeReturnPledge = (TextView) mLayoutView.findViewById(R.id.id_for_member_return_pledge);
        tvMemberRechargeReturnCard = (TextView) mLayoutView.findViewById(R.id.id_for_member_return_card);

        rgMemberRecharge = (RadioGroup) mLayoutView.findViewById(R.id.id_for_account_operate_rg);
        rgMemberRecharge.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int checkedId) {
                if (checkedId == R.id.id_for_account_recharge_rb) {
                    LogUtil.error("onCheckedChanged " + checkedId);

                    rbAccountRecharge.setChecked(true);
                    rbCashPledge.setChecked(false);
                    rbMemberRecharge.setChecked(false);

                    processMemberRecharge(1);
                }

                if (checkedId == R.id.id_for_cash_pledge_rb) {
                    LogUtil.error("onCheckedChanged " + checkedId);

                    rbAccountRecharge.setChecked(false);
                    rbCashPledge.setChecked(true);
                    rbMemberRecharge.setChecked(false);

                    processMemberRecharge(2);
                }

                if (checkedId == R.id.id_for_member_recharge_rb) {
                    LogUtil.error("onCheckedChanged " + checkedId);

                    rbAccountRecharge.setChecked(false);
                    rbCashPledge.setChecked(false);
                    rbMemberRecharge.setChecked(true);

                    processMemberRecharge(3);
                }
            }
        });

        //默认选择第一个
//        rbNewAccountRecharge.setChecked(true);
//        rbNewCashPledge.setChecked(false);
//        rbNewMemberRecharge.setChecked(false);
//        processNewMemberRecharge(recharge);

        PetInfoModel model = new PetInfoModel(); //初始化第一条宠物信息
        model.setPetName("");
        model.setAge("");
        model.setID(String.valueOf(petInfoModelList.size()));
        petInfoModelList.add(model);

        mRecyclerAddPet = (RecyclerView) mLayoutView.findViewById(R.id.id_for_list_view);
        mRecyclerAddPet.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerViewAdapter = new RecyclerViewAdapter(mContext, petInfoModelList, petCategoryList);

        if (MemberUtil.getInstance(getActivity()).getPetCategoryLists() != null) {
            petCategoryList.addAll(MemberUtil.getInstance(getActivity()).getPetCategoryLists());
        }

        //会员添加
        mAddRecyclerView = (RecyclerView) mLayoutView.findViewById(R.id.id_for_add_pet_info);
        memberCard = (NiceSpinner) mLayoutView.findViewById(R.id.id_for_member_card);

        //会员显示
        petInfoRecyclerView = (RecyclerView) mLayoutView.findViewById(R.id.id_for_pet_info_of_special_member);
        petInfoRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        initRechargeInfo();

        initListView();
        initCustomerList();
    }

    private void initRechargeInfo() {
        //控制区域
        llRechargeSwitch = (LinearLayout) mLayoutView.findViewById(R.id.id_for_recharge_switch_ll);
        llRecordHistorySwitch = (LinearLayout) mLayoutView.findViewById(R.id.id_for_history_recordg_switch_ll);

        llRechargeSwitchControl = (LinearLayout) mLayoutView.findViewById(R.id.id_for_history_record_switch_control);
        tvRechargeEmptyInfo = (TextView) mLayoutView.findViewById(R.id.id_for_recharge_history_empty);

        rlLeftControl = (RelativeLayout) mLayoutView.findViewById(R.id.id_for_left_control);
        rlLeftRechargeControl = (RelativeLayout) mLayoutView.findViewById(R.id.id_for_recharge_control);

        tvRechargeActionSwitch = (TextView) mLayoutView.findViewById(R.id.id_for_recharge_of_member_show);
        tvRechageHistorySwitch = (TextView) mLayoutView.findViewById(R.id.id_for_recharge_history_of_member_show);

        tvRechargeActionSwitch.setOnClickListener(this);
        tvRechageHistorySwitch.setOnClickListener(this);

        tvNewRechargeActionSwitch = (TextView) mLayoutView.findViewById(R.id.id_for_new_recharge_history);
        tvNewRechageHistorySwitch = (TextView) mLayoutView.findViewById(R.id.id_for_new_recharge);

        tvMemberCount = (TextView) mLayoutView.findViewById(R.id.id_for_member_count);
        tvMemberCountLabel = (TextView) mLayoutView.findViewById(R.id.id_for_member_label);

        tvNewRechargeActionSwitch.setOnClickListener(this);
        tvNewRechageHistorySwitch.setOnClickListener(this);

        tvTopControl = (Button) mLayoutView.findViewById(R.id.id_for_recharge_back_control);
        tvTopControl.setOnClickListener(this);

        DrawableUtil.getInstance().setDrawableOriType(mContext, 1, tvTopControl, R.mipmap.iconback);

        tvRechargeTopMiddle = (TextView) mLayoutView.findViewById(R.id.id_for_top_recharge_middle);

        tvTopRechargeHistory = (TextView) mLayoutView.findViewById(R.id.id_for_new_recharge_history);
        tvTopRechargeHistory.setOnClickListener(this);

        tvTopRechargeControl = (TextView) mLayoutView.findViewById(R.id.id_for_new_recharge);
        tvTopRechargeControl.setOnClickListener(this);

        llNewAccountContent = (LinearLayout) mLayoutView.findViewById(R.id.id_for_new_recharge_of_account);
        llNewPrePledgeContent = (LinearLayout) mLayoutView.findViewById(R.id.id_for_new_recharge_of_pre_pledge);
        llNewMemberRechargeContent = (LinearLayout) mLayoutView.findViewById(R.id.id_for_new_recharge_of_member_recharge);

        rbNewAccountRecharge = (RadioButton) mLayoutView.findViewById(R.id.id_for_new_account_recharge_rb);
        rbNewCashPledge = (RadioButton) mLayoutView.findViewById(R.id.id_for_new_cash_pledge_rb);
        rbNewMemberRecharge = (RadioButton) mLayoutView.findViewById(R.id.id_for_new_member_recharge_rb);

        rgMemberRechargeSwitch = (RadioGroup) mLayoutView.findViewById(R.id.id_for_new_account_operate_rg);
        rgMemberRechargeSwitch.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int checkedId) {
                if (checkedId == R.id.id_for_new_account_recharge_rb) {
                    LogUtil.error("onCheckedChanged " + checkedId);

                    rbNewAccountRecharge.setChecked(true);
                    rbNewCashPledge.setChecked(false);
                    rbNewMemberRecharge.setChecked(false);

                    processNewMemberRecharge(1);
                }

                if (checkedId == R.id.id_for_new_cash_pledge_rb) {
                    LogUtil.error("onCheckedChanged " + checkedId);

                    rbNewAccountRecharge.setChecked(false);
                    rbNewCashPledge.setChecked(true);
                    rbNewMemberRecharge.setChecked(false);

                    processNewMemberRecharge(2);
                }

                if (checkedId == R.id.id_for_new_member_recharge_rb) {
                    LogUtil.error("onCheckedChanged " + checkedId);

                    rbNewAccountRecharge.setChecked(false);
                    rbNewCashPledge.setChecked(false);
                    rbNewMemberRecharge.setChecked(true);

                    processNewMemberRecharge(3);
                }
            }
        });

        //刷新当前余额
        ivRefreshCurrentAmount = (ImageView) mLayoutView.findViewById(R.id.id_for_refresh_account_amount_iv);

        //充值的会员信息显示
        ivNewMemberHeader = (ImageView) mLayoutView.findViewById(R.id.id_for_new_member_recharge_header);
        tvNewMemberCardId = (TextView) mLayoutView.findViewById(R.id.id_for_new_member_recharge_id);
        tvNewMemberName = (TextView) mLayoutView.findViewById(R.id.id_for_new_member_recharge_name);
        tvNewMemberMobile = (TextView) mLayoutView.findViewById(R.id.id_for_new_member_recharge_mobile);
        tvNewMemberCardLevel = (TextView) mLayoutView.findViewById(R.id.id_for_new_member_recharge_level);

        tvNewMemberAccountAmount = (TextView) mLayoutView.findViewById(R.id.id_for_new_recharge_account);
        tvNewMemberPledge = (TextView) mLayoutView.findViewById(R.id.id_for_new_recharge_pledge);
        tvNewMemberRechargeCard = (TextView) mLayoutView.findViewById(R.id.id_for_new_recharge_recharge_card);
        tvNewMemberCurrentPoints = (TextView) mLayoutView.findViewById(R.id.id_for_new_recharge_recharge_points);

        //充值信息,账户充值
        llNewRechargeAccount = (LinearLayout) mLayoutView.findViewById(R.id.id_for_recharge_of_account_recharge_ll);
        llNewRemoveRechargeAccount = (LinearLayout) mLayoutView.findViewById(R.id.id_for_recharge_of_account_remove_recharge_ll);

        llNewRechargeBackCard = (LinearLayout) mLayoutView.findViewById(R.id.id_for_new_remove_recharge_back_card_content_ll);
        tvNewRemoveRechargeCard = (TextView) mLayoutView.findViewById(R.id.id_for_new_remove_recharge_back_card);
        tvNewRemoveValidAccount = (TextView) mLayoutView.findViewById(R.id.id_for_new_account_back_cash_show_info);
        tvNewRemoveValidAccountHelp = (ImageView) mLayoutView.findViewById(R.id.id_for_new_account_back_cash_show_info_help);

        tvNewRemoveRechargeCard.setOnClickListener(this);

        rgNewAccountRecharge = (RadioGroup) mLayoutView.findViewById(R.id.id_for_new_account_recharge_operate_rg);
        rbNewAccountRechargeCash = (RadioButton) mLayoutView.findViewById(R.id.id_for_new_account_recharge_operate_cash_rb);
        rbNewAccountRechargeCard = (RadioButton) mLayoutView.findViewById(R.id.id_for_new_account_recharge_operate_card_rb);
        rbNewAccountRechargeWx = (RadioButton) mLayoutView.findViewById(R.id.id_for_new_account_recharge_operate_wx_rb);
        rbNewAccountRechargeAlipay = (RadioButton) mLayoutView.findViewById(R.id.id_for_new_account_recharge_operate_alipay_rb);

        rgNewAccountRecharge.setOnCheckedChangeListener(rechargeListener);

        //默认方式为现金
        rbNewAccountRechargeCash.setChecked(true);

        etNewAccountRechargeAmount = (EditText) mLayoutView.findViewById(R.id.id_for_new_recharge_of_account_amount);
        etNewAccountRechargeBonusAmount = (EditText) mLayoutView.findViewById(R.id.id_for_new_recharge_of_bonus_amount);
        etNewAccountRechargeBonusPoints = (EditText) mLayoutView.findViewById(R.id.id_for_new_recharge_of_bonus_points);
        tvNewAccountRechargeRecharge = (TextView) mLayoutView.findViewById(R.id.id_for_new_recharge_recharge_account_commit);
        llNewAccountRechargeRecharge = (LinearLayout) mLayoutView.findViewById(R.id.id_for_new_recharge_recharge_account_commit_ll);
        pbNewAccountRechargeRecharge = (ProgressBar) mLayoutView.findViewById(R.id.id_for_new_recharge_recharge_account_progress);

        llNewAccountRechargeRecharge.setOnClickListener(this);//提交充值

        tvNewAccountRechargeRechargeCurrentAmount = (TextView) mLayoutView.findViewById(R.id.id_for_new_recharge_of_account_current_amount);
        pbNewAccountRecharge = (ProgressBar) mLayoutView.findViewById(R.id.id_for_new_recharge_get_current_info_account_progress);

        //充值信息,账户退款
        rgNewRemoveAccountRecharge = (RadioGroup) mLayoutView.findViewById(R.id.id_for_new_remove_back_cash_rg);
        rbNewRemoveAccountRechargeCash = (RadioButton) mLayoutView.findViewById(R.id.id_for_new_remove_back_cash_rb_cash);
        rbNewRemoveAccountRechargeCard = (RadioButton) mLayoutView.findViewById(R.id.id_for_new_remove_back_cash_rb_back_card);
        rbNewRemoveAccountRechargeWx = (RadioButton) mLayoutView.findViewById(R.id.id_for_new_remove_back_cash_rb_wx);
        rbNewRemoveAccountRechargeAlipay = (RadioButton) mLayoutView.findViewById(R.id.id_for_new_remove_back_cash_rb_alipay);

        rgNewRemoveAccountRecharge.setOnCheckedChangeListener(rechargeListener);

        //默认方式为现金
        rbNewRemoveAccountRechargeCash.setChecked(true);

        etNewRemoveAccountRechargeAmount = (TextView) mLayoutView.findViewById(R.id.id_for_new_remove_recharge_back_cash_all_amount);
        etNewRemoveAccountRechargeBonusAmount = (TextView) mLayoutView.findViewById(R.id.id_for_new_remove_recharge_back_cash_bonus_amount);
        etNewRemoveAccountRechargeBonusPoints = (TextView) mLayoutView.findViewById(R.id.id_for_new_remove_recharge_of_bonus_amount);
        tvRemoveNewAccountRechargeRechargeAllAmount = (TextView) mLayoutView.findViewById(R.id.id_for_new_remove_account_back_amount);
        tvRemoveNewAccountRechargeRechargeCancel = (TextView) mLayoutView.findViewById(R.id.id_for_new_remove_account_back_cash_cancel);

        llRemoveNewAccountRechargeRechargeCommit = (LinearLayout) mLayoutView.findViewById(R.id.id_for_new_remove_account_back_cash_commit_ll);
        pbRemoveNewAccountRechargeRecharge = (ProgressBar) mLayoutView.findViewById(R.id.id_for_new_remove_account_back_cash_progress);
        tvRemoveNewAccountRechargeRechargeCommit = (TextView) mLayoutView.findViewById(R.id.id_for_new_remove_account_back_cash_commit);

        llRemoveNewAccountRechargeRechargeCommit.setOnClickListener(this);

        //充值退款取消和退款
        tvRemoveNewAccountRechargeRechargeCancel.setOnClickListener(this);
        tvRemoveNewAccountRechargeRechargeCommit.setOnClickListener(this);

        //预交押金-预交
        llNewPrePledgeRechargeAccount = (LinearLayout) mLayoutView.findViewById(R.id.id_for_new_pre_pledge_recharge_ll);
        llNewPrePledgeRechargeRemoveAccount = (LinearLayout) mLayoutView.findViewById(R.id.id_for_new_remove_pre_pledge_recharge_ll);

        llNewPrePledgeRechargeRecharge = (LinearLayout) mLayoutView.findViewById(R.id.id_for_new_pre_pledge_recharge_account_commit_ll);
        pbNewPrePledgeRechargeRecharge = (ProgressBar) mLayoutView.findViewById(R.id.id_for_new_pre_pledge_recharge_account_progress);

        llNewPrePledgeRechargeRecharge.setOnClickListener(this);

        rgNewPrePledgeRG = (RadioGroup) mLayoutView.findViewById(R.id.id_for_recharge_of_new_pre_pledge_rg);
        rbNewPrePledgeCash = (RadioButton) mLayoutView.findViewById(R.id.id_for_recharge_of_new_pre_pledge_rb_cash);
        rbNewPrePledgeCard = (RadioButton) mLayoutView.findViewById(R.id.id_for_recharge_of_new_pre_pledge_rb_bank_card);
        rbNewPrePledgeWx = (RadioButton) mLayoutView.findViewById(R.id.id_for_recharge_of_new_pre_pledge_rb_bank_wx);
        rbNewPrePledgeAlipay = (RadioButton) mLayoutView.findViewById(R.id.id_for_recharge_of_new_pre_pledge_rb_bank_alipay);

        rgNewPrePledgeRG.setOnCheckedChangeListener(rechargeListener);

        //默认方式为现金
        rbNewPrePledgeCash.setChecked(true);

        etNewPrePledgeAmount = (EditText) mLayoutView.findViewById(R.id.id_for_recharge_of_new_pre_pledge_amount);
        tvNewPrePledgeCommit = (TextView) mLayoutView.findViewById(R.id.id_for_recharge_of_new_pre_pledge_commit);
        tvNewPrePledgeCurrentAmount = (TextView) mLayoutView.findViewById(R.id.id_for_new_remove_recharge_pre_pledge_of_account_current_amount);
        tvNewPrePledgeRemovePledge = (TextView) mLayoutView.findViewById(R.id.id_for_new_pre_pledge_back_card);//退押金

        tvNewPrePledgeRemovePledge.setOnClickListener(this);

        //新预交押金-退押金
        rgNewRemovePrePledgeRG = (RadioGroup) mLayoutView.findViewById(R.id.id_for_new_remove_pre_pledge_back_cash_rg);
        rbNewRemovePrePledgeCash = (RadioButton) mLayoutView.findViewById(R.id.id_for_new_remove_recharge_pre_pledge_back_cash_rb_cash);
        rbNewRemovePrePledgeCard = (RadioButton) mLayoutView.findViewById(R.id.id_for_new_remove_recharge_pre_pledge_back_cash_rb_back_card);
        rbNewRemovePrePledgeWx = (RadioButton) mLayoutView.findViewById(R.id.id_for_new_remove_recharge_pre_pledge_back_cash_rb_wx);
        rbNewRemovePrePledgeAlipay = (RadioButton) mLayoutView.findViewById(R.id.id_for_new_remove_recharge_pre_pledge_back_cash_rb_alipay);

        rgNewRemovePrePledgeRG.setOnCheckedChangeListener(rechargeListener);

        //默认方式为现金
        rbNewRemovePrePledgeCash.setChecked(true);

        etNewRemovePrePledgeAmount = (TextView) mLayoutView.findViewById(R.id.id_for_new_remove_recharge_pre_pledge_back_cash_bonus_amount);
        tvNewRemovePrePledgeCancel = (TextView) mLayoutView.findViewById(R.id.id_for_new_remove_recharge_pre_pledge_back_cash_cancel);
        tvNewRemovePrePledgeCommit = (TextView) mLayoutView.findViewById(R.id.id_for_new_remove_recharge_pre_pledge_back_cash_commit);
        tvNewRemovePrePledgeShowInfoll = (LinearLayout) mLayoutView.findViewById(R.id.id_for_new_remove_recharge_pre_pledge_back_cash_show_info_ll);

        tvValidRemovePrepledge = (TextView) mLayoutView.findViewById(R.id.id_for_new_remove_recharge_pre_pledge_back_cash_show_info);

        tvNewRemovePrePledgeCancel.setOnClickListener(this);
        tvNewRemovePrePledgeCommit.setOnClickListener(this);

        pbRemoveNewNewPrePledgeRecharge = (ProgressBar) mLayoutView.findViewById(R.id.id_for_new_remove_recharge_pre_pledge_progress);
        llRemoveNewPrePledgeRechargeCommit = (LinearLayout) mLayoutView.findViewById(R.id.id_for_new_remove_pre_pledge_commit_ll);
        llRemoveNewPrePledgeRechargeCommit.setOnClickListener(this);

        //新会员卡-充值
        llNewMemberRechargeAccount = (LinearLayout) mLayoutView.findViewById(R.id.id_for_new_member_recharge_recharge_ll);
        llNewMemberRechargeRemoveAccount = (LinearLayout) mLayoutView.findViewById(R.id.id_for_new_remove_member_recharge_recharge_ll);
        tvNewMemberRechargeEmptyAccount = (TextView) mLayoutView.findViewById(R.id.id_for_new_member_recharge_empty_info);

        tvCardNumber = (TextView) mLayoutView.findViewById(R.id.id_for_new_member_recharge_card_number);
        pbNewMemberRechargeGetCurrentInfo = (ProgressBar) mLayoutView.findViewById(R.id.id_for_new_member_recharge_get_current_info_account_progress);

        rgNewMemberRG = (RadioGroup) mLayoutView.findViewById(R.id.id_for_new_member_recharge_account_operate_rg);
        rbNewMemberCash = (RadioButton) mLayoutView.findViewById(R.id.id_for_new_member_recharge_recharge_type_cash);
        rbNewMemberCard = (RadioButton) mLayoutView.findViewById(R.id.id_for_new_member_recharge_recharge_type_card);
        rbNewMemberWx = (RadioButton) mLayoutView.findViewById(R.id.id_for_new_member_recharge_recharge_type_wx);
        rbNewMemberAlipay = (RadioButton) mLayoutView.findViewById(R.id.id_for_new_member_recharge_recharge_type_alipay);

        rgNewPrePledgeRG.setOnCheckedChangeListener(rechargeListener);

        //默认方式为现金
        rbNewMemberCash.setChecked(true);

        etNewMemberAmount = (EditText) mLayoutView.findViewById(R.id.id_for_new_member_recharge_of_account_amount);
        etNewMemberBonusAmount = (EditText) mLayoutView.findViewById(R.id.id_for_new_member_recharge_of_bonus_amount);
        tvNewMemberCommit = (TextView) mLayoutView.findViewById(R.id.id_for_new_member_recharge_recharge_commit);

        llNewMemberRecharge = (LinearLayout) mLayoutView.findViewById(R.id.id_for_new_member_recharge_account_commit_ll);
        llNewMemberRecharge.setOnClickListener(this);

        pbNewMemberRechargeCommit = (ProgressBar) mLayoutView.findViewById(R.id.id_for_new_member_recharge_account_progress);

        tvNewMemberCurrentAmount = (TextView) mLayoutView.findViewById(R.id.id_for_new_remove_member_recharge_recharge_of_account_current_amount);
        tvNewMemberRemoveBankCard = (TextView) mLayoutView.findViewById(R.id.id_for_new_remove_member_recharge_recharge_back_card);//退会员充值卡

        tvNewMemberRemoveBankCard.setOnClickListener(this);

        //新会员卡-退款
        tvRemoveCardNumber = (TextView) mLayoutView.findViewById(R.id.id_for_new_remove_member_recharge_card_number);

        rgNewRemoveMemberRG = (RadioGroup) mLayoutView.findViewById(R.id.id_for_new_remove_member_recharge_back_cash_rg);
        rbNewRemoveMemberCash = (RadioButton) mLayoutView.findViewById(R.id.id_for_new_remove_member_recharge_back_cash_rb_cash);
        rbNewRemoveMemberCard = (RadioButton) mLayoutView.findViewById(R.id.id_for_new_remove_member_recharge_back_cash_rb_back_card);
        rbNewRemoveMemberWx = (RadioButton) mLayoutView.findViewById(R.id.id_for_new_remove_member_recharge_back_cash_rb_wx);
        rbNewRemoveMemberAlipay = (RadioButton) mLayoutView.findViewById(R.id.id_for_new_remove_member_recharge_back_cash_rb_alipay);

        rgNewRemoveMemberRG.setOnCheckedChangeListener(rechargeListener);

        //默认方式为现金
        rbNewRemoveMemberCash.setChecked(true);

        etNewRemoveMemberAllAmount = (TextView) mLayoutView.findViewById(R.id.id_for_new_remove_member_recharge_recharge_back_cash_all_amount);
        etNewRemoveMemberBonusPoints = (TextView) mLayoutView.findViewById(R.id.id_for_new_remove_member_recharge_of_remove_bonus_points);
        etNewRemoveMemberBonusAmount = (TextView) mLayoutView.findViewById(R.id.id_for_new_remove_member_recharge_recharge_back_cash_bonus_amount);
        tvNewRemoveMemberCancel = (TextView) mLayoutView.findViewById(R.id.id_for_new_remove_member_recharge_back_cash_cancel);

        tvNewRemoveMemberCommit = (TextView) mLayoutView.findViewById(R.id.id_for_new_remove_member_recharge_back_cash_commit);
        pbNewRemoveMemberCommit = (ProgressBar) mLayoutView.findViewById(R.id.id_for_new_remove_member_recharge_back_cash_progress);
        llNewRemoveMemberCommit = (LinearLayout) mLayoutView.findViewById(R.id.id_for_new_remove_member_recharge_back_cash_commit_ll);

        tvNewRemoveMemberAllAmount = (TextView) mLayoutView.findViewById(R.id.id_for_new_remove_member_recharge_all_amount);
        tvNewRemoveMemberShowInfoll = (LinearLayout) mLayoutView.findViewById(R.id.id_for_new_remove_member_recharge_show_info_ll);

        tvNewRemoveMembeAmount = (TextView) mLayoutView.findViewById(R.id.id_for_new_member_back_cash_show_info);
        ivNewRemoveMemberHelp = (ImageView) mLayoutView.findViewById(R.id.id_for_new_member_back_cash_show_info_help);
        ivNewRemoveMemberHelp.setOnClickListener(this);

        tvNewRemoveMemberCancel.setOnClickListener(this);
        tvNewRemoveMemberCommit.setOnClickListener(this);
        llNewRemoveMemberCommit.setOnClickListener(this);

        //充值的历史记录
        mRechargeHistoryRecycler = (RecyclerView) mLayoutView.findViewById(R.id.id_for_history_record_switch_rv);
        mRechargeHistoryRecycler.setLayoutManager(new LinearLayoutManager(getActivity()));

        rechargeHistoryRecyclerViewAdapter = new RechargeHistoryRecyclerViewAdapter(mContext, rechargeHistoryLists);
        mRechargeHistoryRecycler.setAdapter(rechargeHistoryRecyclerViewAdapter);
    }

    private void initCustomerList() {
        AuthModel.DataInfo.UserInfo userInfo = OAuthUtils.getInstance().getUserinfo(mContext);
        String searchSql = " orgid=? ";
        List<CustomerList> load = DataSupport.where(searchSql, userInfo.getOrgId() + "").limit(limitSize).offset(mPage * limitSize).order("id desc").find(CustomerList.class);
        if (load != null && load.size() >= 0) {
            customeList.addAll(load);
            mPullUpListView.setResultSize(load.size());
        }

        int count = DataSupport.where().count(CustomerList.class);
        if (count > 0) {
            tvMemberCount.setVisibility(View.VISIBLE);
            tvMemberCountLabel.setVisibility(View.VISIBLE);

            tvMemberCount.setText(String.valueOf(count));
        } else {
            tvMemberCount.setVisibility(View.GONE);
            tvMemberCountLabel.setVisibility(View.GONE);
        }
    }

    private void loadCustomerList(String searchKey) {
        AuthModel.DataInfo.UserInfo userInfo = OAuthUtils.getInstance().getUserinfo(mContext);

        String searchSql = " orgid=? ";
        if (!TextUtils.isEmpty(searchKey)) {
            searchSql = " orgid=? and ( CardNumber like '%" + searchKey + "%'" + " or Name like '%" + searchKey + "%'" + " or CellPhone like'%" + searchKey + "%') ";
        } else {
            searchSql = " orgid=? ";
        }

        List<CustomerList> load = DataSupport.where(searchSql, userInfo.getOrgId() + "").limit(limitSize).offset(mPage * limitSize).order("id desc").find(CustomerList.class);
        if (load != null && load.size() >= 0) {
            customeList.addAll(load);
            mPullUpListView.setResultSize(load.size());
        }

        if (customeList != null && customeList.size() > 0) {
            customeAdapter.setData(customeList);

            if (mPage == 0) {

            }
        } else {
            if (load.size() <= 0) {
                customeList.clear();
                customeAdapter.setData(customeList);
            }
        }
    }

    public void initListView() {
        if (customeList != null && customeList.size() > 0) {
            customer = customeList.get(0);
            processCustomerList(0);
            refreshMemberInfo(customer);//选中还需要修改背景颜色
        }

        //会员列表
        mPullUpListView = (PullUpListView) mLayoutView.findViewById(R.id.id_for_pull_list_view);
        customeAdapter = new CustomerListAdapter(mContext, customeList);

        mPullUpListView.setPageSize(limitSize);
        mPullUpListView.setAdapter(customeAdapter);

        mPullUpListView.setOnLoadListener(new PullUpListView.OnLoadListener() {
            public void onLoad() {
                mPage++;
                loadCustomerList(null);
            }
        });

        mPullUpListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                LogUtil.error("onItemClick position " + position);
                if (customeList != null && position < customeList.size()) {
                    switchType(TYPE_OF_MEMEBER_SHOW);//切换显示会员

                    customer = customeList.get(position);
                    refreshMemberInfo(customer);//选中还需要修改背景颜色

                    processCustomerList(position);
                    customeAdapter.setData(customeList);
                }
            }
        });

        ivMemberAdd = (ImageView) mLayoutView.findViewById(R.id.id_for_member_alert_add_icon);
        ivMemberAdd.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                LogUtil.error("id_for_member_add_icon ");
                new MemberAddMemberDialog(getActivity(), memberAddInterface);
            }
        });

        mAccountRechargePullUplistView = (PullUpListView) mLayoutView.findViewById(R.id.id_for_pull_list_view_account_recharge_history);
    }

    private void processCustomerList(int position) {
        for (int i = 0; i < customeList.size(); i++) {
            if (i != position) {
                customeList.get(i).setIsSelect("false");
            } else {
                customeList.get(i).setIsSelect("true");
            }
        }
    }

    private String getPetKindOf(String trim) {
        LogUtil.error("getPetKindOf ------------- " + trim);
        for (int i = 0; i < petCategoryList.size(); i++) {
            if (trim.equalsIgnoreCase(String.valueOf(petCategoryList.get(i).getId()))) {
                return String.valueOf(petCategoryList.get(i).getId());
            }
        }
        return String.valueOf(0);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(List<CustomerList> customerLists) {//在内存中进行更新
        if (operatingAnim != null) {
            ivMemberRefresh.clearAnimation();
        }

        if (customerLists == null || customerLists.size() <= 0) { //移除一直转动的问题
            return;
        }

        mPage = 0;
        customeList.clear();
        initCustomerList();

        for (int i = 0; i < customeList.size(); i++) {
            LogUtil.error("customeList getName " + customeList.get(i).getName());
            LogUtil.error("customeList getCardNumber " + customeList.get(i).getCardNumber());
        }

        customeAdapter.setData(customeList);

        if (customerLists != null && customerLists.size() > 0) {
            refreshMemberInfo(customerLists.get(0));
        }

        int count = DataSupport.where().count(CustomerList.class);
        if (count > 0) {
            tvMemberCount.setVisibility(View.VISIBLE);
            tvMemberCountLabel.setVisibility(View.VISIBLE);

            tvMemberCount.setText(String.valueOf(count));
        } else {
            tvMemberCount.setVisibility(View.GONE);
            tvMemberCountLabel.setVisibility(View.GONE);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(AccountRechargeModel.DataInfo dataInfo) {

        CustomerList customerList = SelectMemberUtil.getInstance().getCustomer();
        if (customerList != null && customerList.getCustomerId() != null) {
            showAnimation();

            Map<String, Object> map = new HashMap<>();
            map.put("CustomId", customerList.getCustomerId());
            map.put("InfoType", "1");

            SyncDataUtil.getInstance().syncGetUserMoneyInFo(map);
        }

        LogUtil.error("onMessageEvent getSerialNumber " + dataInfo.getSerialNumber());
        pbNewAccountRechargeRecharge.setVisibility(View.GONE);
        tvNewAccountRechargeRecharge.setText("充值");

        etNewAccountRechargeAmount.setText("");
        etNewAccountRechargeBonusAmount.setText("");
        etNewAccountRechargeBonusPoints.setText("");

        Toast.makeText(mContext, dataInfo.getErrorMessage(), Toast.LENGTH_SHORT);

        if (dataInfo != null) {
            Toast.makeText(mContext, dataInfo.getErrorMessage(), Toast.LENGTH_SHORT);
        } else {
            Toast.makeText(mContext, "充值失败~~", Toast.LENGTH_SHORT);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(PrePledgeRechargeModel.DataInfo dataInfo) {
        if (dataInfo == null || dataInfo.isOk() == false) {
            Toast.makeText(mContext, dataInfo == null ? "预交押金失败~~~" : dataInfo.getErrorMessage(), Toast.LENGTH_SHORT).show();
            return;
        }

        CustomerList customerList = SelectMemberUtil.getInstance().getCustomer();
        if (customerList != null && customerList.getCustomerId() != null) {
            showAnimation();

            Map<String, Object> map = new HashMap<>();
            map.put("CustomId", customerList.getCustomerId());
            map.put("InfoType", "3");

            SyncDataUtil.getInstance().syncGetUserMoneyInFo(map);
        }

        LogUtil.error("onMessageEvent getSerialNumber " + dataInfo.getSerialNumber());
        pbNewPrePledgeRechargeRecharge.setVisibility(View.GONE);
        tvNewPrePledgeCommit.setText("预交");

        etNewPrePledgeAmount.setText("");

        Toast.makeText(mContext, dataInfo.getErrorMessage(), Toast.LENGTH_SHORT);

        if (dataInfo != null) {
            Toast.makeText(mContext, dataInfo.getErrorMessage(), Toast.LENGTH_SHORT);
        } else {
            Toast.makeText(mContext, "预交押金失败~~", Toast.LENGTH_SHORT);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(MemberRechargeModel.DataInfo dataInfo) {
        CustomerList customerList = SelectMemberUtil.getInstance().getCustomer();
        if (customerList != null && customerList.getCustomerId() != null) {
            showAnimation();

            Map<String, Object> map = new HashMap<>();
            map.put("CustomId", customerList.getCustomerId());
            map.put("InfoType", "2");

            SyncDataUtil.getInstance().syncGetUserMoneyInFo(map);
        }

        pbNewMemberRechargeCommit.setVisibility(View.GONE);
        tvNewMemberCommit.setText("充值");

        etNewMemberAmount.setText("");
        etNewMemberBonusAmount.setText("");

        Toast.makeText(mContext, dataInfo.getErrorMessage(), Toast.LENGTH_SHORT);

        if (dataInfo != null) {
            Toast.makeText(mContext, dataInfo.getErrorMessage(), Toast.LENGTH_SHORT);
        } else {
            Toast.makeText(mContext, "充值失败~~", Toast.LENGTH_SHORT);
        }
    }

    //通过syncGetUserMoneyInFo接口单独获取
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(AccountInfoModel.DataInfo dataInfo) {
        LogUtil.error("onMessageEvent getInfoType " + dataInfo.getInfoType());

        switch (Integer.parseInt(dataInfo.getInfoType())) {
            case 1: //账户充值
                tvAccountRechargeTotalAmount.setText(String.valueOf(dataInfo.getTotalConsumption()));
                tvAccountRechargeCashAmount.setText(String.valueOf(dataInfo.getAccountBalance()));
                tvAccountRechargeCurrentPoint.setText(String.valueOf(dataInfo.getScore()));

                //修改左侧数据
                tvNewMemberAccountAmount.setText("¥ " + String.valueOf(dataInfo.getAccountBalance()));
                tvNewMemberCurrentPoints.setText(String.valueOf(dataInfo.getScore()));

                //设置可以退款的金额
                tvNewRemoveValidAccount.setText("可退: ¥" + String.valueOf(dataInfo.getAccountBalance() - dataInfo.getAccountGift()));
                tvRemoveNewAccountRechargeRechargeAllAmount.setText(String.valueOf(dataInfo.getAccountBalance() - dataInfo.getAccountGift()));
                etNewRemoveAccountRechargeAmount.setText(String.valueOf(dataInfo.getAccountBalance()));
                etNewRemoveAccountRechargeBonusAmount.setText(String.valueOf(dataInfo.getAccountGift()));
                etNewRemoveAccountRechargeBonusPoints.setText(String.valueOf(dataInfo.getScore()));

                // 右侧的当前余额
                tvNewAccountRechargeRechargeCurrentAmount.setText("¥ " + String.valueOf(dataInfo.getAccountBalance()));

                clearAnimation();
                break;

            case 3://预交押金
                resetPrePledgeRecharge();

                //修改左侧数据
                tvNewMemberPledge.setText("¥ " + String.valueOf(dataInfo.getDepositBalance()));
                tvNewMemberCurrentPoints.setText(String.valueOf(dataInfo.getScore()));

                //设置可以退款的金额
                tvValidRemovePrepledge.setText("可退: ¥" + String.valueOf(dataInfo.getDepositBalance()));
                etNewRemovePrePledgeAmount.setText("" + String.valueOf(dataInfo.getDepositBalance()));

                // 右侧的当前余额
                tvNewPrePledgeCurrentAmount.setText("¥ " + String.valueOf(dataInfo.getDepositBalance()));

                clearAnimation();
                break;

            case 2://会员卡充值
                pbNewMemberRechargeGetCurrentInfo.setVisibility(View.GONE);//隐藏加载

                if (dataInfo != null && !TextUtils.isEmpty(dataInfo.getCardNo())) {
                    llNewMemberRechargeAccount.setVisibility(View.VISIBLE);
                    llNewMemberRechargeRemoveAccount.setVisibility(View.GONE);
                    tvNewMemberRechargeEmptyAccount.setVisibility(View.GONE);

                    tvCardNumber.setText(String.valueOf(dataInfo.getCardNo()));
                    tvRemoveCardNumber.setText(String.valueOf(dataInfo.getCardNo()));
                    cardId = dataInfo.getCardId();

                    String validAmount = String.format("可退 ¥%s", dataInfo.getCardBalance() - dataInfo.getMemberCardGift());
                    tvNewRemoveMembeAmount.setText(validAmount);

                    resetMemberRecharge();

                    //修改左侧数据
                    tvNewMemberRechargeCard.setText("¥ " + String.valueOf(dataInfo.getCardBalance()));
                    tvNewMemberCurrentPoints.setText(String.valueOf(dataInfo.getScore()));

                    //设置可以会员退款的金额
                    tvNewRemoveValidAccount.setText("可退: ¥" + String.valueOf(dataInfo.getCardBalance() - dataInfo.getMemberCardGift()));
                    tvNewRemoveMemberAllAmount.setText(String.valueOf(dataInfo.getCardBalance() - dataInfo.getMemberCardGift()));
                    etNewRemoveMemberAllAmount.setText(String.valueOf(dataInfo.getCardBalance()));
                    etNewRemoveMemberBonusAmount.setText(String.valueOf(dataInfo.getMemberCardGift()));
                    etNewRemoveMemberBonusPoints.setText(String.valueOf(dataInfo.getScore()));

                    tvNewMemberRemoveBankCard.setText("退款");
                    tvNewMemberRemoveBankCard.setOnClickListener(this);

                    // 右侧的当前余额
                    tvNewMemberCurrentAmount.setText("¥ " + String.valueOf(dataInfo.getCardBalance()));
                } else {
                    llNewMemberRechargeAccount.setVisibility(View.GONE);
                    llNewMemberRechargeRemoveAccount.setVisibility(View.GONE);
                    tvNewMemberRechargeEmptyAccount.setVisibility(View.VISIBLE);

                    tvNewMemberRemoveBankCard.setText("已退款");
                    tvNewMemberRemoveBankCard.setOnClickListener(null);

                    tvNewMemberRechargeCard.setText("¥ 0.0");
                    tvNewMemberCurrentAmount.setText("¥ 0.0");
                }

                clearAnimation();
                break;
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(RemoveRechargeModel.DataInfo dataInfo) {
        if (dataInfo != null) {
            tvRemoveNewAccountRechargeRechargeAllAmount.setText("");
            etNewRemoveAccountRechargeAmount.setText("");
            etNewRemoveAccountRechargeBonusAmount.setText("");
            etNewRemoveAccountRechargeBonusPoints.setText("");

            pbRemoveNewAccountRechargeRecharge.setVisibility(View.GONE);
            tvRemoveNewAccountRechargeRechargeCommit.setText("退款");

            tvNewRemoveValidAccount.setText("可退: ¥0");

            CustomerList customerList = SelectMemberUtil.getInstance().getCustomer();
            if (customerList != null) {
                showAnimation();

                Map<String, Object> map = new HashMap<>();
                map.put("CustomId", customer.getCustomerId());
                map.put("InfoType", "1");

                SyncDataUtil.getInstance().syncGetUserMoneyInFo(map);
            }

            Toast.makeText(mContext, dataInfo.getErrorMessage() + "", Toast.LENGTH_SHORT).show();
            operateAccountRecharge(0);//充值成功后,显示充值界面
        } else {

        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(DepositRefundModel.DataInfo dataInfo) {
        if (dataInfo != null) {
            etNewRemovePrePledgeAmount.setText("");

            pbRemoveNewNewPrePledgeRecharge.setVisibility(View.GONE);
            tvNewRemovePrePledgeCommit.setText("退押金");

            tvValidRemovePrepledge.setText("可退: ¥0");

            CustomerList customerList = SelectMemberUtil.getInstance().getCustomer();
            if (customerList != null) {
                showAnimation();

                Map<String, Object> map = new HashMap<>();
                map.put("CustomId", customer.getCustomerId());
                map.put("InfoType", "3");

                SyncDataUtil.getInstance().syncGetUserMoneyInFo(map);
            }

            Toast.makeText(mContext, dataInfo.getErrorMessage() + "", Toast.LENGTH_SHORT).show();

            operatePrePledgeRecharge(0);
        } else {
            Toast.makeText(mContext, "退押金失败", Toast.LENGTH_SHORT).show();
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(BackCardModel.DataInfo dataInfo) {
        if (dataInfo != null) {
            etNewRemoveMemberAllAmount.setText("");
            etNewRemoveMemberBonusPoints.setText("");
            etNewRemoveMemberBonusAmount.setText("");
            tvNewRemoveMemberAllAmount.setText("");

            pbNewRemoveMemberCommit.setVisibility(View.GONE);
            tvNewRemoveMemberCommit.setText("退款");

            tvNewRemoveMembeAmount.setText("可退: ¥0");

            CustomerList customerList = SelectMemberUtil.getInstance().getCustomer();
            if (customerList != null) {
                showAnimation();

                Map<String, Object> map = new HashMap<>();
                map.put("CustomId", customer.getCustomerId());
                map.put("InfoType", "2");

                SyncDataUtil.getInstance().syncGetUserMoneyInFo(map);
            }

            Toast.makeText(mContext, dataInfo.getErrorMessage() + "", Toast.LENGTH_SHORT).show();
            operateMemberRecharge(0);//充值成功后,显示充值界面
        } else {
            Toast.makeText(mContext, "退款失败", Toast.LENGTH_SHORT).show();
        }
    }

    //获取全部的账户信息
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(AmountsModel.DataInfo dataInfo) {
        if (dataInfo != null) {
            LogUtil.error("getAmounts-onCompleted " + dataInfo.getAccount());
            refreshCurrentAmountInfo(dataInfo);
        } else {

        }

        clearAnimation();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(AccountRechargeHistoryModel accountRechargeHistoryModel) {
        if (accountRechargeHistoryModel == null) {
            return;
        }

        if (HttpStatus.status200 == accountRechargeHistoryModel.getCode() && accountRechargeHistoryModel.getData().size() > 0) {
            //如果有数值,需要显示
            llRechargeSwitchControl.setVisibility(View.VISIBLE);
            tvRechargeEmptyInfo.setVisibility(View.GONE);

            rechargeHistoryRecyclerViewAdapter.setData(accountRechargeHistoryModel.getData());
        } else {
            //显示暂无充值记录
            llRechargeSwitchControl.setVisibility(View.GONE);
            tvRechargeEmptyInfo.setVisibility(View.VISIBLE);
        }
    }

    private void refreshMemberInfo(CustomerList customerList) {
        if (customerList != null) {
            SelectMemberUtil.getInstance().setCustomer(customerList);

            if (customerList.getGender() == 1070) {
                ivMemberInfoIcon.setImageDrawable(mContext.getResources().getDrawable(R.mipmap.icon_man));
            } else if (customerList.getGender() == 1069) {
                ivMemberInfoIcon.setImageDrawable(mContext.getResources().getDrawable(R.mipmap.icon_woman));
            } else {
                ivMemberInfoIcon.setImageDrawable(mContext.getResources().getDrawable(R.mipmap.icon_no));
            }

            tvMemberName.setText(customerList.getName());
            tvBalance.setText(String.valueOf(customerList.getRestOfAmount()));
            tvMobile.setText(customerList.getCellPhone());
            tvId.setText(String.valueOf(customerList.getCardNumber()));
            tvBirthDay.setText(customerList.getBirthdate());
            tvCardTime.setText(customerList.getRegistrationDate());
            tvCardLevel.setText(customerList.getCardTypeName());
            tvOrgin.setText(customerList.getDataFrom() == null ? "" : customerList.getDataFrom());
            tvAddress.setText(customerList.getAddress());
            tvMail.setText(customerList.getEMail());

            LogUtil.error("refreshMemberInfo " + getActivity().getString(R.string.warm_live_last_consumer));
            LogUtil.error("refreshMemberInfo " + customerList.getLastInterview());

            List<PetData> pets = DataSupport.where("CustomerID=?", String.valueOf(customerList.getCustomerId())).find(PetData.class);
            showPetRecyclerViewAdapter = new PetInfoRecyclerViewAdapter(mContext, pets);
            petInfoRecyclerView.setAdapter(showPetRecyclerViewAdapter);
        }
    }

    private void refreshRechargeMemberInfo(CustomerList customerList) {
        if (customerList != null) {
            if (customerList.getGender() == 1070) {
                ivNewMemberHeader.setImageDrawable(mContext.getResources().getDrawable(R.mipmap.icon_man));
            } else if (customerList.getGender() == 1069) {
                ivNewMemberHeader.setImageDrawable(mContext.getResources().getDrawable(R.mipmap.icon_woman));
            } else {
                ivNewMemberHeader.setImageDrawable(mContext.getResources().getDrawable(R.mipmap.icon_no));
            }

            tvNewMemberName.setText(customerList.getName());
            tvNewMemberMobile.setText(customerList.getCellPhone());
            tvNewMemberCardId.setText(String.valueOf(customerList.getCardNumber()));
            tvNewMemberCardLevel.setText(String.valueOf(customerList.getCardTypeName()));

            tvNewMemberAccountAmount.setText("¥ 0.0");
            tvNewMemberPledge.setText("¥ 0.0");
            tvNewMemberRechargeCard.setText("¥ 0.0");

            tvNewMemberCurrentPoints.setText("0");

//            getAmounts(Integer.parseInt(customerList.getCustomerId()));
            showAnimation();
            SyncDataUtil.getInstance().syncGetUserAllInfo(Integer.parseInt(customerList.getCustomerId()));
        }
    }

    private void initEvents() {

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.id_for_refresh://刷新
            case R.id.id_for_member_refresh_tv:
            case R.id.id_for_member_refresh:
                if (operatingAnim != null) {
                    ivMemberRefresh.startAnimation(operatingAnim);
                }

                SyncDataUtil.getInstance().syncCustomerInfo(false, "");
                break;

            case R.id.id_for_memeber_add:
                new MemberAddMemberDialog(getActivity(), memberAddInterface);
                break;

            case R.id.id_for_search:
                hideSearchTitle();
                break;

            case R.id.id_for_clean:
                etSearch.setText("");
                break;

            case R.id.id_for_search_hide:
                showSearchTitle();
                break;

            case R.id.id_for_add_member://增加会员,隐藏之前的相关界面
                showAddMembers();
                break;

            case R.id.id_for_member_list://相当于重新刷新
//                if (operatingAnim != null) {
//                    ivRefresh.startAnimation(operatingAnim);
//                }

                ivSearch.setVisibility(View.VISIBLE);

                //SyncDataUtil.getInstance().syncCustomerInfo(false, "");
                processMemberRecharge(0);

                showMemberListInfo(1);//显示会员列表
                break;

            case R.id.id_for_add_recharge:
                recharge = 1;
                processMemberRecharge(recharge);

                ivSearch.setVisibility(View.GONE);
                showMemberListInfo(2);
                break;

            case R.id.id_for_commit://提交会员
                showMemberList();
                break;

            case R.id.id_for_cancel:
                llMemControl.setVisibility(View.VISIBLE);
                rlMemShow.setVisibility(View.VISIBLE);//右侧控制显示
                rlMemAddControl.setVisibility(View.INVISIBLE);

                resetAddPetInfo();
                break;

            case R.id.id_for_add_pet_rl:
                if (!checkPetInfo()) {
                    Toast.makeText(mContext, "您还有未填写的宠物信息~~~", Toast.LENGTH_SHORT).show();
                    return;
                }

                PetInfoModel model = new PetInfoModel();
                model.setAge("");
                model.setPetName("");

                model.setID(String.valueOf(petCategoryList.size()));
                petInfoModelList.add(model);

                recyclerViewAdapter.setData(petInfoModelList);
                break;

            //账户充值-退款
            case R.id.id_for_account_recharge_recharge_return:

                break;

            case R.id.id_for_recharge_of_member_show:
                operateShowOrHideRecharge(1);
                operateShowOrHideRechargeAndHistory(1);
                break;

            case R.id.id_for_recharge_history_of_member_show:
                operateShowOrHideRecharge(1);
                operateShowOrHideRechargeAndHistory(0);
                break;

            case R.id.id_for_recharge_back_control:
                LogUtil.error("id_for_recharge_back_control ...");
                operateShowOrHideRecharge(0);
                break;

            case R.id.id_for_new_recharge_history://显示历史记录
                operateShowOrHideRechargeAndHistory(0);
                break;

            case R.id.id_for_new_recharge://显示充值
                operateShowOrHideRechargeAndHistory(1);
                break;

            case R.id.id_for_new_remove_recharge_back_card://账号充值退卡
                operateAccountRecharge(1);
                break;

            case R.id.id_for_new_remove_account_back_cash_cancel://帐户充值退款取消
                operateAccountRecharge(0);
                break;

            case R.id.id_for_new_remove_account_back_cash_commit_ll://帐户充值-退款
            case R.id.id_for_new_remove_account_back_cash_commit:
                popupAccountRemoveRechargeDialog(tvRemoveNewAccountRechargeRechargeAllAmount.getText().toString().trim());
                break;

            case R.id.id_for_new_remove_member_recharge_back_cash_commit://会员卡充值-退卡
            case R.id.id_for_new_remove_member_recharge_back_cash_commit_ll:
                popupMemberRemoveRechargeDialog(tvNewRemoveMemberAllAmount.getText().toString().trim());
                break;

            case R.id.id_for_new_remove_recharge_pre_pledge_back_cash_cancel://预交押金-取消
                operatePrePledgeRecharge(0);
                break;

            case R.id.id_for_new_pre_pledge_back_card:////预交押金-退押金
                operatePrePledgeRecharge(1);
                break;

            case R.id.id_for_new_remove_pre_pledge_commit_ll://预交押金-退押金
            case R.id.id_for_new_remove_recharge_pre_pledge_back_cash_commit://预交押金-提交退押金
                if (isPermissionRemovePrePledgeRecharge()) {
                    popupPrePledgeRemoveRechargeDialog(etNewRemovePrePledgeAmount.getText().toString().trim());
                }
                break;

            case R.id.id_for_new_remove_member_recharge_recharge_back_card://会员卡充值-退会员卡
                operateMemberRecharge(1);
                break;

            case R.id.id_for_new_remove_member_recharge_back_cash_cancel://会员卡充值-取消
                operateMemberRecharge(0);
                break;


            case R.id.id_for_new_recharge_recharge_account_commit_ll://账户充值,进行充值
                if (isPermissionAccountRecharge()) {
                    popupAccountRechargeDialog(etNewAccountRechargeAmount.getText().toString().trim());
                }
                break;

            case R.id.id_for_new_pre_pledge_recharge_account_commit_ll://预交押金-预交
                if (isPermissionPrePledgeRecharge()) {
                    popupPrePledgeRechargeDialog(etNewPrePledgeAmount.getText().toString().trim());
                }
                break;

            case R.id.id_for_new_member_back_cash_show_info_help://会员卡充值-帮助图标

                break;

            case R.id.id_for_new_member_recharge_account_commit_ll://会员卡充值-充值
                if (isPermissionMemberCardRecharge()) {
                    popupMemberRechargeDialog(etNewMemberAmount.getText().toString().trim());
                }
                break;
        }
    }

    private void popupAccountRechargeDialog(String amount) {
        new NomelNoteDialog(mActivity, "确定要充值吗?", "充值金额: " + amount, "确定", accountRecharge);
    }

    private void popupPrePledgeRechargeDialog(String amount) {
        new NomelNoteDialog(mActivity, "确定要预交押金吗?", "押金金额: " + amount, "确定", prePledgeRecharge);
    }

    private void popupMemberRechargeDialog(String amount) {
        new NomelNoteDialog(mActivity, "确定要充值吗?", "会员卡金额: " + amount, "确定", memberRecharge);
    }

    private void popupAccountRemoveRechargeDialog(String amount) {
        new NomelNoteDialog(mActivity, "温馨提示", "账户退款: ¥ " + amount, "确定", accountRemoveRecharge);
    }

    private void popupPrePledgeRemoveRechargeDialog(String amount) {
        new NomelNoteDialog(mActivity, "温馨提示", "押金退款: ¥ " + amount, "确定", prePledgeRemoveRecharge);
    }

    private void popupMemberRemoveRechargeDialog(String amount) {
        new NomelNoteDialog(mActivity, "温馨提示", "会员卡退款: ¥ " + amount, "确定", memberRemoveRecharge);
    }

    //账户充值相关
    private void operateAccountRecharge(int type) {
        switch (type) {
            case 0://显示账号充值
                llNewRechargeAccount.setVisibility(View.VISIBLE);
                llNewRemoveRechargeAccount.setVisibility(View.GONE);
                llNewRechargeBackCard.setVisibility(View.GONE);
                tvNewRemoveRechargeCard.setVisibility(View.VISIBLE);
                break;

            case 1://显示账号退款
                llNewRechargeAccount.setVisibility(View.GONE);
                llNewRemoveRechargeAccount.setVisibility(View.VISIBLE);
                llNewRechargeBackCard.setVisibility(View.VISIBLE);
                tvNewRemoveRechargeCard.setVisibility(View.GONE);
                break;
        }
    }

    //预交押金相关
    private void operatePrePledgeRecharge(int type) {
        switch (type) {
            case 0://显示预交押金
                llNewPrePledgeRechargeAccount.setVisibility(View.VISIBLE);
                llNewPrePledgeRechargeRemoveAccount.setVisibility(View.GONE);
                tvNewRemovePrePledgeShowInfoll.setVisibility(View.GONE);
                tvNewPrePledgeRemovePledge.setVisibility(View.VISIBLE);
                break;

            case 1://显示退预交押金
                llNewPrePledgeRechargeAccount.setVisibility(View.GONE);
                llNewPrePledgeRechargeRemoveAccount.setVisibility(View.VISIBLE);
                tvNewRemovePrePledgeShowInfoll.setVisibility(View.VISIBLE);
                tvNewPrePledgeRemovePledge.setVisibility(View.GONE);
                break;
        }
    }

    //会员卡充值相关
    private void operateMemberRecharge(int type) {
        switch (type) {
            case 0://显示会员卡充值
                llNewMemberRechargeAccount.setVisibility(View.VISIBLE);
                llNewMemberRechargeRemoveAccount.setVisibility(View.GONE);
                tvNewRemoveMemberShowInfoll.setVisibility(View.GONE);
                tvNewMemberRemoveBankCard.setVisibility(View.VISIBLE);
                break;

            case 1://显示会员卡退款
                llNewMemberRechargeAccount.setVisibility(View.GONE);
                llNewMemberRechargeRemoveAccount.setVisibility(View.VISIBLE);
                tvNewRemoveMemberShowInfoll.setVisibility(View.VISIBLE);
                tvNewMemberRemoveBankCard.setVisibility(View.GONE);
                break;
        }
    }

    private boolean isAllowAccountRecharge(String cashRecharge, String swingCardRecharge) {
        if (TextUtils.isEmpty(cashRecharge) && TextUtils.isEmpty(swingCardRecharge) ||
                (Integer.parseInt(cashRecharge) <= 0) && (Integer.parseInt(swingCardRecharge) <= 0)) {
            new NomelNoteDialog(mActivity, "出错啦!", "请输入充值金额~~~!", "确定", null);
            return false;
        }

        return true;
    }

    private void showMemberListInfo(int flag) {
        switch (flag) {
            case 1:
                llMemberList.setVisibility(View.VISIBLE);
                llAccountList.setVisibility(View.GONE);
                break;

            case 2://默认
                llMemberList.setVisibility(View.GONE);
                llAccountList.setVisibility(View.VISIBLE);
                break;

            case 3:
                llMemberList.setVisibility(View.VISIBLE);
                llAccountList.setVisibility(View.GONE);
                break;

            case 4:
                llMemberList.setVisibility(View.VISIBLE);
                llAccountList.setVisibility(View.GONE);
                break;

            default:
                llMemberList.setVisibility(View.VISIBLE);
                llAccountList.setVisibility(View.GONE);
                break;
        }
    }

    private boolean checkPetInfo() {
        boolean flag = true;

        if (petInfoModelList.size() <= 0) {
            return flag;
        }

        for (int i = 0; i < petInfoModelList.size(); i++) {
            PetInfoModel model = petInfoModelList.get(i);
            LogUtil.error("checkPetInfo " + model.getPetName() + "- " + model.getAge() + "-" + model.getCatgory());

            if (TextUtils.isEmpty(model.getPetName())) {
                flag = false;
                break;
            }
        }

        return flag;
    }

    private void showAddMembers() {
        monitor = false;

        llMemControl.setVisibility(View.INVISIBLE);//右侧控制不显示
        rlMemShow.setVisibility(View.INVISIBLE);//右侧控制显示
        rlMemAddControl.setVisibility(View.VISIBLE);

        processMemberRecharge(0);//全部隐藏

        //初始化会员列表信息
        memberCards.clear();
        memberCards.addAll(MemberUtil.getInstance(getActivity()).getCardKindLists());

        initSpinner();
        initPetInfo();

        LogUtil.error("memberCards " + memberCards.size());
    }

    private void initPetInfo() {
        mRecyclerAddPet = (RecyclerView) mLayoutView.findViewById(R.id.id_for_list_view);
        mRecyclerAddPet.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRecyclerAddPet.setAdapter(recyclerViewAdapter);
    }

    private void initSpinner() {
        if (memberCards.size() > 0) {
            memberInfoModel.setVIPLevel(memberCards.get(0).getId());
            memberInfoModel.setLevel(memberCards.get(0).getName());

            memberCard.setSelectedIndex(0);
            memberCard.attachDataSource(parseSpinnerDisplay(memberCards));
            memberCard.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    memberInfoModel.setVIPLevel(memberCards.get(i).getId());
                    memberInfoModel.setLevel(memberCards.get(i).getName());
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });
        }
    }

    //提交信息
    private void showMemberList() {
        //提交增加会员信息
        if (TextUtils.isEmpty(etName.getText().toString().trim())) {
            Toast.makeText(mContext, "姓名不能为空~~~", Toast.LENGTH_SHORT).show();
            return;
        }

        if (TextUtils.isEmpty(etMobile.getText().toString().trim())) {
            Toast.makeText(mContext, "手机号码不能为空~~~", Toast.LENGTH_SHORT).show();
            return;
        }

        mLitepal = LitepalHelper.getInstance();
        CustomerList customerList = mLitepal.queryCustomInfoByMobile(etMobile.getText().toString().trim());
        if (customerList != null) {
            Toast.makeText(mContext, "手机号码已存在~~~", Toast.LENGTH_SHORT).show();
            return;
        }

        memberInfoModel.setName(etName.getText().toString().trim());
        memberInfoModel.setMobile(etMobile.getText().toString().trim());

        int bachNo = (int) new Date().getTime();

        //会员插入表
        CustomerList customer = new CustomerList();
        customer.setName(memberInfoModel.getName());
        customer.setCardTypeName(memberInfoModel.getLevel());
        customer.setCellPhone(memberInfoModel.getMobile());
        customer.setVIPLevel(memberInfoModel.getVIPLevel());
        //customer.setID(bachNo);
        customer.setCustomerId(String.valueOf(bachNo));
        customer.setCardNumber(String.valueOf(bachNo));
        customer.setOrgId(OAuthUtils.getInstance().getUserinfo(mContext).getOrgId());
        customer.setIsNew(1);//新增

        customeList.add(0, customer);
        cleanOtherSelectStatus(customeList);

        customeAdapter.setData(customeList);

        customer.save(); //保存
        SelectMemberUtil.getInstance().setCustomer(customer); //用来在商品列表中显示

        List<PetData> datas = new ArrayList<>();
        for (int i = 0; i < petInfoModelList.size(); i++) {
            PetData petData = new PetData();
//            petData.setID(id);
            petData.setCustomerID(bachNo);
            petData.setKindOF(Integer.parseInt(petInfoModelList.get(i).getKindOF() == null ? "" : petInfoModelList.get(i).getKindOF()));
            petData.setAge(Integer.parseInt(petInfoModelList.get(i).getAge()));
            petData.setPetName(petInfoModelList.get(i).getPetName());
            petData.setOrgId(OAuthUtils.getInstance().getUserinfo(mContext).getOrgId());

            petData.save();
            datas.add(petData);

            LogUtil.error("petInfoModelList getPetName " + DataSupport.findLast(PetData.class).getPetName());
        }

        SelectMemberUtil.getInstance().setPetDatas(datas);

        //updatePetInfo(memberInfoModel, petInfoModelList);
        //插入本地数据库
        MemberUtil.getInstance(mContext).postAddMembers(bachNo, memberInfoModel, petInfoModelList);

        resetAddPetInfo();

        llMemControl.setVisibility(View.VISIBLE);
        rlMemShow.setVisibility(View.VISIBLE);//右侧控制显示
        rlMemAddControl.setVisibility(View.INVISIBLE);

        etName.setText("");
        etMobile.setText("");

        //刷新显示会员
        refreshMemberInfo(SelectMemberUtil.getInstance().getCustomer());
    }

    private void cleanOtherSelectStatus(List<CustomerList> customeList) {
        for (int i = 0; i < customeList.size(); i++) {
            if (i > 0) {
                customeList.get(i).setIsSelect("false");
            } else {
                customeList.get(i).setIsSelect("true");
            }
        }
    }

    private void resetAddPetInfo() {
        petInfoModelList = new ArrayList<>();
        memberInfoModel = new MemberInfoModel();

        PetInfoModel model = new PetInfoModel();
        model.setPetName("");
        model.setAge("");
        model.setID(String.valueOf(petInfoModelList.size()));
        memberInfoModel.setName("");
        memberInfoModel.setMobile("");

        petInfoModelList.add(model);

        monitor = true;
        recyclerViewAdapter.setData(petInfoModelList);
    }

    private void hideSearchTitle() {
        rlSearchLayout.setVisibility(View.VISIBLE);
        ivRefresh.setVisibility(View.GONE);
        ivSearch.setVisibility(View.GONE);
        btnMemberList.setVisibility(View.GONE);
        btnAddMember.setVisibility(View.GONE);
        ivHide.setVisibility(View.VISIBLE);

        etSearch.setText(""); //隐藏以后,重置
        processMemberRecharge(0);
    }

    private void showSearchTitle() {
        rlSearchLayout.setVisibility(View.GONE);
        ivRefresh.setVisibility(View.VISIBLE);
        ivSearch.setVisibility(View.VISIBLE);
        btnMemberList.setVisibility(View.VISIBLE);
        btnAddMember.setVisibility(View.VISIBLE);
        ivHide.setVisibility(View.GONE);
        processMemberRecharge(0);
    }

    @Override
    public void onStart() {
        EventBus.getDefault().register(this);

        super.onStart();
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);

        super.onStop();
    }

    @Override
    public void onResume() {
        LogUtil.error("onResume ");

        super.onResume();
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence s, int i, int i1, int i2) {
        String newText = s.toString().trim();
        LogUtil.error("onTextChanged newText " + newText);

        if (!Util.isNullStr(newText)) {
            long start = PerformanceUtils.getInstance().getReadyTime();
            customeList = new ArrayList<>();
            mPage = 0;
            loadCustomerList(newText);
            PerformanceUtils.getInstance().showConsumeTime(start, "onTextChanged");
        } else {
            loadCustomerList(null);
        }
    }

    private List<CustomerList> processSearchData(List<CustomerList> list, String newText) {
        List<CustomerList> searchList = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            CustomerList customer = list.get(i);

            if ((customer.getName() != null && customer.getName().contains(newText) ||
                    (customer.getCellPhone() != null && customer.getCellPhone().contains(newText)))) {
                searchList.add(customer);
            }
        }

        return searchList;
    }

    @Override
    public void afterTextChanged(Editable editable) {

    }

    private List<String> parseSpinnerDisplay(List<CardKind> list) {

        List<String> l = new ArrayList<>();
        for (CardKind s : list) {
            l.add(s.getName());
        }

        return l;
    }

    private void processMemberRecharge(int flag) {
        switch (flag) {
            case 0://隐藏全部
                rgMemberRecharge.setVisibility(View.GONE);
                llCashPledge.setVisibility(View.GONE);
                llAccountRecharge.setVisibility(View.GONE);
                llMemberRecharge.setVisibility(View.GONE);

                llAccountList.setVisibility(View.GONE);
                llPreRechargeList.setVisibility(View.GONE);
                llMemberRechargeList.setVisibility(View.GONE);
                LogUtil.error("processMemberRecharge " + flag);
                break;

            case 1://显示账户充值
                rgMemberRecharge.setVisibility(View.VISIBLE);
                llCashPledge.setVisibility(View.GONE);
                llAccountRecharge.setVisibility(View.VISIBLE);
                llMemberRecharge.setVisibility(View.GONE);

                llAccountList.setVisibility(View.VISIBLE);
                llPreRechargeList.setVisibility(View.GONE);
                llMemberRechargeList.setVisibility(View.GONE);

                //获取可退的金额
                pbNewAccountRecharge.setVisibility(View.VISIBLE);

                //加载和账户相关的数据
                if (customer != null) {
                    Map<String, Object> map = new HashMap<>();
                    map.put("CustomId", customer.getCustomerId());
                    map.put("InfoType", "1");
                    SyncDataUtil.getInstance().syncGetUserMoneyInFo(map);
                }

                LogUtil.error("processMemberRecharge " + flag);
                break;

            case 2://显示预交押金
                rgMemberRecharge.setVisibility(View.VISIBLE);
                llCashPledge.setVisibility(View.VISIBLE);
                llAccountRecharge.setVisibility(View.GONE);
                llMemberRecharge.setVisibility(View.GONE);

                llAccountList.setVisibility(View.GONE);
                llPreRechargeList.setVisibility(View.VISIBLE);
                llMemberRechargeList.setVisibility(View.GONE);

                LogUtil.error("processMemberRecharge " + flag);

                //加载和预交押金相关的数据
                if (customer != null) {
                    Map<String, Object> map = new HashMap<>();
                    map.put("CustomId", customer.getCustomerId());
                    map.put("InfoType", "3");
                    SyncDataUtil.getInstance().syncGetUserMoneyInFo(map);
                }

                break;

            case 3://显示会员卡充值
                rgMemberRecharge.setVisibility(View.VISIBLE);
                llCashPledge.setVisibility(View.GONE);
                llAccountRecharge.setVisibility(View.GONE);
                llMemberRecharge.setVisibility(View.VISIBLE);

                llAccountList.setVisibility(View.GONE);
                llPreRechargeList.setVisibility(View.GONE);
                llMemberRechargeList.setVisibility(View.VISIBLE);

                //加载和会员卡充值相关的数据
                if (customer != null) {
                    Map<String, Object> map = new HashMap<>();
                    map.put("CustomId", customer.getCustomerId());
                    map.put("InfoType", "2");
                    SyncDataUtil.getInstance().syncGetUserMoneyInFo(map);
                }

                LogUtil.error("processMemberRecharge " + flag);
                break;
        }
    }

    private void processNewMemberRecharge(int flag) {
        switch (flag) {
            case 1://显示账户充值
                llNewAccountContent.setVisibility(View.VISIBLE);
                llNewPrePledgeContent.setVisibility(View.GONE);
                llNewMemberRechargeContent.setVisibility(View.GONE);

                //加载和账户相关的数据
                if (customer != null) {
                    Map<String, Object> map = new HashMap<>();
                    map.put("CustomId", customer.getCustomerId());
                    map.put("InfoType", "1");
                    SyncDataUtil.getInstance().syncGetUserMoneyInFo(map);
                }

                LogUtil.error("processMemberRecharge " + flag);
                break;

            case 2://显示预交押金
                llNewAccountContent.setVisibility(View.GONE);
                llNewPrePledgeContent.setVisibility(View.VISIBLE);
                llNewMemberRechargeContent.setVisibility(View.GONE);

                //加载和账户相关的数据
                if (customer != null) {
                    pbNewMemberRechargeGetCurrentInfo.setVisibility(View.VISIBLE);

                    Map<String, Object> map = new HashMap<>();
                    map.put("CustomId", customer.getCustomerId());
                    map.put("InfoType", "3");
                    SyncDataUtil.getInstance().syncGetUserMoneyInFo(map);
                }

                LogUtil.error("processMemberRecharge " + flag);
                break;

            case 3://显示会员卡充值
                llNewAccountContent.setVisibility(View.GONE);
                llNewPrePledgeContent.setVisibility(View.GONE);
                llNewMemberRechargeContent.setVisibility(View.VISIBLE);

                //加载和账户相关的数据
                if (customer != null) {
                    pbNewMemberRechargeGetCurrentInfo.setVisibility(View.VISIBLE);

                    Map<String, Object> map = new HashMap<>();
                    map.put("CustomId", customer.getCustomerId());
                    map.put("InfoType", "2");
                    SyncDataUtil.getInstance().syncGetUserMoneyInFo(map);
                }

                LogUtil.error("processMemberRecharge " + flag);
                break;
        }
    }

    private void resetAccountRecharge() {
        etAccountRechargeCashRecharge.setText("");
        etAccountRechargeSwingCardRecharge.setText("");
        etAccountRechargeBonusAmount.setText("");
        etAccountRechargeBonusPoint.setText("");
        tvAccountRechargeTotalAmount.setText("");
    }

    private void resetPrePledgeRecharge() {
        etAccountRechargeCashRecharge.setText("");
        etAccountRechargeSwingCardRecharge.setText("");
        etAccountRechargeBonusAmount.setText("");
        etAccountRechargeBonusPoint.setText("");
        tvAccountRechargeTotalAmount.setText("");
    }

    private void resetMemberRecharge() {
        etAccountRechargeCashRecharge.setText("");
        etAccountRechargeSwingCardRecharge.setText("");
        etAccountRechargeBonusAmount.setText("");
        etAccountRechargeBonusPoint.setText("");
        tvAccountRechargeTotalAmount.setText("");
    }

    private void dissmiss() {
        if (mDialog != null) {
            mDialog.dismiss();
        }
    }

    MemberAddMemberDialog.MemberAddInterface memberAddInterface = new MemberAddMemberDialog.MemberAddInterface() {

        @Override
        public void onAdd(CustomerList customerList) {
            LogUtil.error("memberAddInterface is back!!! ");
            if (customeAdapter != null && customeList != null) {
                customeList.add(0, customerList);
                customeAdapter.setData(customeList);
            }
        }
    };

    private void switchType(int type) {
        switch (type) {
            case TYPE_OF_MEMEBER_ADD://添加会员模式
                rlMemShow.setVisibility(View.GONE);
                rlMemberAdd.setVisibility(View.VISIBLE);
                break;

            case TYPE_OF_MEMEBER_SHOW://展示会员模式
                rlMemShow.setVisibility(View.VISIBLE);
                rlMemberAdd.setVisibility(View.GONE);
                break;
        }
    }

    //type 0:展示会员列表 1:展示充值
    public void operateShowOrHideRecharge(int type) {
        switch (type) {
            case 0:
                rlLeftControl.setVisibility(View.VISIBLE);
                llMemControl.setVisibility(View.VISIBLE);
                rlLeftRechargeControl.setVisibility(View.GONE);

//                //初始化充值情况
//                rbNewAccountRecharge.setChecked(true);//先按账户充值按钮
                operateAccountRecharge(0);//相当于账户充值-取消按钮

                break;

            case 1:
                rlLeftControl.setVisibility(View.GONE);
                llMemControl.setVisibility(View.GONE);
                rlLeftRechargeControl.setVisibility(View.VISIBLE);

                rbNewAccountRecharge.setChecked(false);
                rbNewCashPledge.setChecked(false);
                rbNewMemberRecharge.setChecked(false);

                //显示会员相关的信息
                refreshRechargeMemberInfo(SelectMemberUtil.getInstance().getCustomer());
                break;
        }
    }

    //type 0:充值历史记录 1:展示充值
    public void operateShowOrHideRechargeAndHistory(int type) {
        switch (type) {
            case 0:
                llRechargeSwitch.setVisibility(View.GONE);
                llRecordHistorySwitch.setVisibility(View.VISIBLE);

                tvNewRechargeActionSwitch.setVisibility(View.GONE);
                tvNewRechageHistorySwitch.setVisibility(View.VISIBLE);

                tvRechargeTopMiddle.setText("充值记录");

                //获取充值记录数据
                if (customer != null) {
                    Map<String, Object> map = new ArrayMap<>();
                    map.put("OrgId", customer.getOrgId());
                    map.put("CustomerId", customer.getCustomerId());

                    SyncDataUtil.getInstance().syncGetAmountHistoires(map);
                } else {
                    llRechargeSwitchControl.setVisibility(View.GONE);
                    tvRechargeEmptyInfo.setVisibility(View.VISIBLE);
                }

                break;

            case 1:
                llRechargeSwitch.setVisibility(View.VISIBLE);
                llRecordHistorySwitch.setVisibility(View.GONE);

                tvNewRechargeActionSwitch.setVisibility(View.VISIBLE);
                tvNewRechageHistorySwitch.setVisibility(View.GONE);

                tvRechargeTopMiddle.setText("充值");

                //初始化账户充值,预交押金和会员卡充值的情况
                rbNewAccountRecharge.setChecked(true);
                rbNewCashPledge.setChecked(false);
                rbNewMemberRecharge.setChecked(false);
                break;
        }
    }

    private void refreshCurrentAmountInfo(AmountsModel.DataInfo dataInfo) {
        LogUtil.error("refreshCurrentAmountInfo " + dataInfo.getMemberCard());

        if (dataInfo != null) {
            //刷新左侧数据
            tvNewMemberAccountAmount.setText("¥ " + String.valueOf(dataInfo.getAccount()));
            tvNewMemberPledge.setText("¥ " + String.valueOf(dataInfo.getPrePay()));
            tvNewMemberRechargeCard.setText("¥ " + String.valueOf(dataInfo.getMemberCard()));

            //刷新右侧,账户、押金和会员卡余额
            tvNewAccountRechargeRechargeCurrentAmount.setText("¥ " + String.valueOf(dataInfo.getAccount()));
            tvNewPrePledgeCurrentAmount.setText("¥ " + String.valueOf(dataInfo.getPrePay()));
            tvNewMemberCurrentAmount.setText("¥" + String.valueOf(dataInfo.getMemberCard()));

            tvNewMemberCurrentPoints.setText(String.valueOf(dataInfo.getScore()));
        }
    }

    public void showAnimation() {
        ivRefreshCurrentAmount.setVisibility(View.VISIBLE);

        if (operatingAnim != null) {
            ivRefreshCurrentAmount.startAnimation(operatingAnim);
        }
    }

    public void clearAnimation() {
        ivRefreshCurrentAmount.setVisibility(View.GONE);

        if (operatingAnim != null) {
            ivRefreshCurrentAmount.clearAnimation();
        }
    }

    public boolean isPermissionAccountRecharge() {
        if (TextUtils.isEmpty(etNewAccountRechargeAmount.getText().toString().trim())) {
            Toast.makeText(mContext, "充值金额不能为空~~~", Toast.LENGTH_SHORT).show();
            return false;
        }

        if (TextUtils.isEmpty(etNewAccountRechargeBonusAmount.getText().toString().trim())) {
            Toast.makeText(mContext, "赠送金额不能为空~~~", Toast.LENGTH_SHORT).show();
            return false;
        }

        if (TextUtils.isEmpty(etNewAccountRechargeBonusPoints.getText().toString().trim())) {
            Toast.makeText(mContext, "赠送积分不能为空~~~", Toast.LENGTH_SHORT).show();
            return false;
        }

        return true;
    }

    public boolean isPermissionPrePledgeRecharge() {
        if (TextUtils.isEmpty(etNewPrePledgeAmount.getText().toString().trim())) {
            Toast.makeText(mContext, "预交金额不能为空~~~", Toast.LENGTH_SHORT).show();
            return false;
        }

        return true;
    }

    public boolean isPermissionMemberCardRecharge() {
        if (TextUtils.isEmpty(etNewMemberAmount.getText().toString().trim())) {
            Toast.makeText(mContext, "充值金额不能为空~~~", Toast.LENGTH_SHORT).show();
            return false;
        }

        if (TextUtils.isEmpty(etNewMemberBonusAmount.getText().toString().trim())) {
            Toast.makeText(mContext, "赠送金额不能为空~~~", Toast.LENGTH_SHORT).show();
            return false;
        }

        return true;
    }

    public boolean isPermissionRemovePrePledgeRecharge() {
        if (TextUtils.isEmpty(etNewRemovePrePledgeAmount.getText().toString().trim())) {
            Toast.makeText(mContext, "总退金额不能为空~~~", Toast.LENGTH_SHORT).show();
            return false;
        }

        if (Double.parseDouble(etNewRemovePrePledgeAmount.getText().toString().trim()) <= 0) {
            Toast.makeText(mContext, "总退金额必须大于零~~~", Toast.LENGTH_SHORT).show();
            return false;
        }

        return true;
    }

    RadioGroup.OnCheckedChangeListener rechargeListener = new RadioGroup.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup radioGroup, int checkedId) {
            // 账户充值--充值
            if (checkedId == R.id.id_for_new_account_recharge_operate_cash_rb) {
                accountRechargeType = 0;
            }

            if (checkedId == R.id.id_for_new_account_recharge_operate_card_rb) {
                accountRechargeType = 1;
            }

            if (checkedId == R.id.id_for_new_account_recharge_operate_wx_rb) {
                accountRechargeType = 2;
            }

            if (checkedId == R.id.id_for_new_account_recharge_operate_alipay_rb) {
                accountRechargeType = 3;
            }

            // 账户充值--退款
            if (checkedId == R.id.id_for_new_remove_back_cash_rb_cash) {
                accountRemoveRechargeType = 0;
            }

            if (checkedId == R.id.id_for_new_remove_back_cash_rb_back_card) {
                accountRemoveRechargeType = 1;
            }

            if (checkedId == R.id.id_for_new_remove_back_cash_rb_wx) {
                accountRemoveRechargeType = 2;
            }

            if (checkedId == R.id.id_for_new_remove_back_cash_rb_alipay) {
                accountRemoveRechargeType = 3;
            }

            // 预交押金--充值
            if (checkedId == R.id.id_for_recharge_of_new_pre_pledge_rb_cash) {
                prePledgeRechargeType = 0;
            }

            if (checkedId == R.id.id_for_recharge_of_new_pre_pledge_rb_bank_card) {
                prePledgeRechargeType = 1;
            }

            if (checkedId == R.id.id_for_recharge_of_new_pre_pledge_rb_bank_wx) {
                prePledgeRechargeType = 2;
            }

            if (checkedId == R.id.id_for_recharge_of_new_pre_pledge_rb_bank_alipay) {
                prePledgeRechargeType = 3;
            }

            // 预交押金--退押金
            if (checkedId == R.id.id_for_new_remove_back_cash_rb_cash) {
                accountRemoveRechargeType = 0;
            }

            if (checkedId == R.id.id_for_new_remove_back_cash_rb_back_card) {
                accountRemoveRechargeType = 1;
            }

            if (checkedId == R.id.id_for_new_remove_back_cash_rb_wx) {
                accountRemoveRechargeType = 2;
            }

            if (checkedId == R.id.id_for_new_remove_back_cash_rb_alipay) {
                accountRemoveRechargeType = 3;
            }

            // 会员卡充值--充值
            if (checkedId == R.id.id_for_new_member_recharge_recharge_type_cash) {
                memberRechargeType = 0;
            }

            if (checkedId == R.id.id_for_new_member_recharge_recharge_type_card) {
                memberRechargeType = 1;
            }

            if (checkedId == R.id.id_for_new_member_recharge_recharge_type_wx) {
                memberRechargeType = 2;
            }

            if (checkedId == R.id.id_for_new_member_recharge_recharge_type_alipay) {
                memberRechargeType = 3;
            }

            // 会员卡充值--退卡
            if (checkedId == R.id.id_for_new_remove_member_recharge_back_cash_rb_cash) {
                accountRemoveRechargeType = 0;
            }

            if (checkedId == R.id.id_for_new_remove_member_recharge_back_cash_rb_back_card) {
                accountRemoveRechargeType = 1;
            }

            if (checkedId == R.id.id_for_new_remove_member_recharge_back_cash_rb_wx) {
                accountRemoveRechargeType = 2;
            }

            if (checkedId == R.id.id_for_new_remove_member_recharge_back_cash_rb_alipay) {
                accountRemoveRechargeType = 3;
            }
        }
    };

    private void syncUserInfo(String type) {
        //加载和账户相关的数据
        if (customer != null) {
            Map<String, Object> map = new HashMap<>();
            map.put("CustomId", customer.getCustomerId());
            map.put("InfoType", type);
            SyncDataUtil.getInstance().syncGetUserMoneyInFo(map);
        }
    }

}
