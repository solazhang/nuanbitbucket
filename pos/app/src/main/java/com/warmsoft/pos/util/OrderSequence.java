package com.warmsoft.pos.util;

import android.content.ContentValues;

import com.warmsoft.pos.litepal.data.OrderNoData;

import org.litepal.crud.DataSupport;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 作者: lijinliu on 2016/8/15.
 * 邮箱：jinliu.li@warmsoft.com
 * 描述: 每天定单号增长获取
 */
public class OrderSequence {
    public static int getOrderNo() {
        OrderNoData data = DataSupport.findLast(OrderNoData.class);
        String nowDateStr = dataFormat(new Date());
        int orderNo = 1;
        if (data != null && data.getOrderNo() > 0 && nowDateStr.equals(dataFormat(data.getOrderDate()))) {
            orderNo = data.getOrderNo() + 1;
            ContentValues values = new ContentValues();
            values.put("orderNo", orderNo);
            DataSupport.update(OrderNoData.class, values, data.getId());
        } else {
            if (data == null) {
                data = new OrderNoData();
                data.setOrderDate(new Date());
                data.setOrderNo(orderNo);
                data.save();
            } else {
                ContentValues values = new ContentValues();
                values.put("orderNo", orderNo);
                DataSupport.update(OrderNoData.class, values, data.getId());
            }
        }
        return orderNo;
    }

    public static int getMowOrderNo() {
        OrderNoData data = DataSupport.findLast(OrderNoData.class);
        if (data != null && data.getOrderNo() > 0) {
            return data.getOrderNo();
        }
        return 1;
    }

    public static String dataFormat(Date date) {
        try {
            String dateStr = new SimpleDateFormat("yyyy-MM-dd").format(date);
            return dateStr;
        } catch (Exception e) {

        }
        return null;
    }
}
