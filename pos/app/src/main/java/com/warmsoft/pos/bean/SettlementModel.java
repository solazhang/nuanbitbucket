package com.warmsoft.pos.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 作者: lijinliu on 2016/8/9.
 * 邮箱：jinliu.li@warmsoft.com
 * 描述: 结算返回
 */
public class SettlementModel implements Serializable {
    int code;
    String message;
    SettlementReslut Data;

    /**
     * 结算结果
     */
    public static class SettlementReslut implements Serializable {
        public int Id;//": 11211370,
        public float totalAmount;//": 244,
        public float actlyPayed;//": 0,
        public float discountAmount;//": 244

        public int getId() {
            return Id;
        }

        public void setId(int id) {
            Id = id;
        }

        public float getTotalAmount() {
            return totalAmount;
        }

        public void setTotalAmount(float totalAmount) {
            this.totalAmount = totalAmount;
        }

        public float getActlyPayed() {
            return actlyPayed;
        }

        public void setActlyPayed(float actlyPayed) {
            this.actlyPayed = actlyPayed;
        }

        public float getDiscountAmount() {
            return discountAmount;
        }

        public void setDiscountAmount(float discountAmount) {
            this.discountAmount = discountAmount;
        }
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public SettlementReslut getData() {
        return Data;
    }

    public void setData(SettlementReslut data) {
        Data = data;
    }
}
