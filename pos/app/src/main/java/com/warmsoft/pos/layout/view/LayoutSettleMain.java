package com.warmsoft.pos.layout.view;

import android.app.Activity;
import android.content.Context;
import android.view.View;

import com.warmsoft.pos.bean.AmountsModel;
import com.warmsoft.pos.dialog.ClearPayedsDialog;
import com.warmsoft.pos.dialog.NomelNoteDialog;
import com.warmsoft.pos.enumutil.OrderStatusEnum;
import com.warmsoft.pos.enumutil.PayChanel;
import com.warmsoft.pos.layout.view.settlement.SettlementKeyBoard;
import com.warmsoft.pos.layout.view.settlement.SettlementPayChanel;
import com.warmsoft.pos.layout.view.settlement.SettlementPrint;
import com.warmsoft.pos.listener.FmPosLitener;
import com.warmsoft.pos.listener.LayoutSettleListener;
import com.warmsoft.pos.litepal.data.OrderInfoData;
import com.warmsoft.pos.util.OrderHelper;
import com.warmsoft.pos.util.SelectMemberUtil;

/**
 * 作者: lijinliu on 2016/8/9.
 * 邮箱：jinliu.li@warmsoft.com
 * 描述: 结算模块对应 view_settlement_main.xml
 */
public class LayoutSettleMain implements LayoutSettleListener {

    private Activity mActivity;
    private Context mContext;
    private OrderHelper orderHelper;
    private FmPosLitener mListener;

    private SettlementPrint mPrintModel;//打印模块
    private SettlementPayChanel mPayChanelModel;//支付模块
    private SettlementKeyBoard mKeyBoard;//键盘

    public LayoutSettleMain(View rootView, Activity activity, FmPosLitener fmListener) {
        this.mActivity = activity;
        this.mContext = mActivity.getApplicationContext();
        this.mListener = fmListener;

        mPrintModel = new SettlementPrint(rootView, activity, this);
        mPayChanelModel = new SettlementPayChanel(rootView, activity, this);
        mKeyBoard = new SettlementKeyBoard(rootView, activity, this);
    }


    public void initData() {
        mPayChanelModel.initData();
        mKeyBoard.initData();
    }

    public void refershMemberInfo(int customeid) {
        mPayChanelModel.refershMemberInfo(customeid);
        mPayChanelModel.getAmounts(customeid);
    }

    //继续收款
    @Override
    public void continueCollect() {
        collectMoney(PayChanel.PAY_BY_CASH);
    }

    //抹零操作
    @Override
    public void moling() {
        orderHelper = OrderHelper.getInstance(mActivity);
        if (orderHelper.getOrderStaus() == OrderStatusEnum.HandIn || orderHelper.getOrderStaus() == OrderStatusEnum.KeepingAccounts) {
            final float molingAmount = OrderHelper.getInstance(mContext).getNeedColletAmountForText();
            new NomelNoteDialog(mActivity, "确认是否抹零？", "当前去零" + molingAmount + "元", "确定", new NomelNoteDialog.DialogListener() {
                @Override
                public void noteDialogConfirm() {
                    OrderHelper.getInstance(mContext).saveCollectAmount(molingAmount, PayChanel.PAY_BY_DISCOUNT);
                    //initData();//刷新界面数据
                    orderHelper.settleOrder(mActivity);//结算
                    settleOrder();//结算成功并重新开单
                }
            });
        } else {
            new NomelNoteDialog(mActivity, "无法抹零", "当前定单状态:" + OrderStatusEnum.getEnumDesc(orderHelper.getOrderStaus()), "确定", null);
        }
    }

    //打印
    @Override
    public void print() {

    }

    //清空支付
    @Override
    public void clearAllPays() {
        new ClearPayedsDialog(mActivity, mListener);
    }


    //刷会员卡
    @Override
    public void swingMember() {
        if (mListener != null) {
            mListener.swingMember();
        }
    }

    //收钱,已下单和已记账才响应
    @Override
    public void collectMoney(int payChanel) {
        orderHelper = OrderHelper.getInstance(mActivity);

        if (orderHelper.getOrderStaus() == OrderStatusEnum.KeepingAccounts && !(payChanel == PayChanel.PAY_BY_CASH || payChanel == PayChanel.PAY_BY_BANKCARD))//记账只支持现金和银行卡支付
        {
            new NomelNoteDialog(mActivity, "记账定单", "请使用现金或者银卡收款!", "确定", null);
            return;
        }

        OrderInfoData orderData = orderHelper.getOrderData();
        AmountsModel.DataInfo amountInfo = SelectMemberUtil.getInstance().getDataInfo();
        if (orderHelper.getOrderStaus() == OrderStatusEnum.HandIn || orderHelper.getOrderStaus() == OrderStatusEnum.KeepingAccounts)//已下单
        {
            if (checkAccount(orderData, amountInfo, payChanel)) {
                mKeyBoard.collectMoney(payChanel);
            }
        } else if (orderHelper.getUnSureGoodSize() > 0) {
            new NomelNoteDialog(mActivity, "不能收款", "定单中有未确认商品!", "确定", null);
        }
    }

    public boolean checkAccount(OrderInfoData orderData, AmountsModel.DataInfo amountInfo, int payChanel) {
        if (orderData != null && amountInfo != null && orderData.getCustomerId() == amountInfo.getCustomerid()) {
            if (payChanel == PayChanel.PAY_BY_ACCOUNT) {
                if (amountInfo.getAccount() - orderData.getAccountAmount() > 0) {
                    return true;
                } else {
                    new NomelNoteDialog(mActivity, "余额不足", "账户余额不足，请使\n用电脑客户端充值!", "确定", null);
                    return false;
                }
            }
            if (payChanel == PayChanel.PAY_BY_VIPCARD) {
                if (amountInfo.getMemberCard() - orderData.getMemberCardAmount() > 0) {
                    return true;
                } else {
                    new NomelNoteDialog(mActivity, "余额不足", "会员卡余额不足，请\n使用电脑客户端充值!", "确定", null);
                    return false;
                }
            }
            if (payChanel == PayChanel.PAY_BY_DEPOSIT) {
                if (amountInfo.getPrePay() - orderData.getPrePayAmount() > 0) {
                    return true;
                } else {
                    new NomelNoteDialog(mActivity, "余额不足", "押金余额不足!", "确定", null);
                    return false;
                }
            }
            return true;
        } else {
            new NomelNoteDialog(mActivity, "会员错误", "信息错误，请点\n\n击会员头像更新!", "确定", null);
            return false;
        }
    }

    @Override
    public void keepingAccount() {
        if (mListener != null) {//更新客单收款信息
            mListener.keepingAccount();
        }
    }

    //确认收钱
    @Override
    public void payConfirm(String orderNo) {
        mPayChanelModel.refershList(orderNo);
        if (mListener != null) {//更新客单收款信息
            mListener.payConfirm(orderNo);
        }
    }


    // 结算
    @Override
    public void settleOrder() {
        if (mListener != null) {//结算成功
            mListener.settleOver();
        }
    }

}
