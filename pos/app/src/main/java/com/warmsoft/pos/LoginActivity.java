package com.warmsoft.pos;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Notification;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.util.ArrayMap;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.gson.Gson;
import com.warmsoft.base.BasicActivity;
import com.warmsoft.base.MyApplication;
import com.warmsoft.pos.bean.AuthModel;
import com.warmsoft.pos.bean.ExceptionModel;
import com.warmsoft.pos.dialog.LoadingDialog;
import com.warmsoft.pos.dialog.NomelNoteDialog;
import com.warmsoft.pos.layout.view.LayoutActivityLogin;
import com.warmsoft.pos.litepal.data.UserLoginData;
import com.warmsoft.pos.net.HttpStatus;
import com.warmsoft.pos.net.RetrofitManager;
import com.warmsoft.pos.print.SunMiPrintUtils;
import com.warmsoft.pos.update.UpgradeModel;
import com.warmsoft.pos.util.AppUtils;
import com.warmsoft.pos.util.ExceptionUtils;
import com.warmsoft.pos.util.LogUtil;
import com.warmsoft.pos.util.NetWorkUtil;
import com.warmsoft.pos.util.OAuthUtils;
import com.warmsoft.pos.util.RecyclingUtil;
import com.warmsoft.pos.util.SimpleDeEnCode;
import com.warmsoft.pos.util.Trace;
import com.warmsoft.pos.util.Util;

import org.litepal.crud.DataSupport;

import java.util.Date;
import java.util.List;
import java.util.Map;

import me.shenfan.updateapp.UpdateService;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;


/**
 * 作者: lijinliu on 2016/8/5.
 * 邮箱：jinliu.li@warmsoft.com
 * 描述: 登录
 */

public class LoginActivity extends BasicActivity implements LayoutActivityLogin.LoginViewListener {

    private LayoutActivityLogin mLoginView;
    private LoadingDialog mDialog;
    private UserLoginData userLoginData = new UserLoginData();

    UpgradeModel upgradeModel = null;
    private int result = 0;
    //配置需要取的权限
    static final String[] PERMISSION = new String[]{
            Manifest.permission.WRITE_EXTERNAL_STORAGE,// 写入权限
            Manifest.permission.READ_EXTERNAL_STORAGE,  //读取权限
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((MyApplication) getApplication()).pushActivity(this);
        setContentView(R.layout.activity_login);

    }

    @Override
    public void login() {
//        SunMiPrintUtils.getInstance().printOrder(this);
        String signName = Util.getViewText(mLoginView.etName);
        String signPwd = Util.getViewText(mLoginView.etPwd);

        if (Util.isNullStr(signName)) {
            Util.showToast(this, "请输入账号!");
            return;
        }
        if (Util.isNullStr(signPwd)) {
            Util.showToast(this, "请输入账号密码!");
            return;
        }

        if (NetWorkUtil.isNetWorkAvailable(this)) {
            mDialog = new LoadingDialog(this);
            mDialog.show("正在登陆...");

            userLoginData.setLoginName(signName);
            userLoginData.setLoginPwd(SimpleDeEnCode.encode(signPwd));
            Map<String, String> map = new ArrayMap<>();
            map.put("LoginName", signName);
            map.put("PassWord", signPwd);

            Observable<AuthModel> observable = RetrofitManager.getInstance().getAPIService().login(new Gson().toJson(map));
            observable.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Subscriber<AuthModel>() {
                        @Override
                        public void onCompleted() {
                        }

                        @Override
                        public void onError(Throwable e) {
                            refreshUI(ExceptionUtils.getInstance().getExceptionObj(LoginActivity.this, e), false);
                        }

                        @Override
                        public void onNext(AuthModel model) {
                            refreshUI(model, false);
                        }
                    });
        } else {//离线登录
            List<UserLoginData> localList = DataSupport.where(" loginName=? and loginPwd=? ", signName, SimpleDeEnCode.encode(signPwd)).find(UserLoginData.class);
            if (localList != null && localList.size() > 0) {
                UserLoginData localDbUser = localList.get(0);
                AuthModel model = new Gson().fromJson(localDbUser.getAuthJson(), AuthModel.class);
                OAuthUtils.getInstance().setToken(this, model.getData().getToken());
                OAuthUtils.getInstance().setUserinfo(this, model.getData().getUserInfo());
                OAuthUtils.getInstance().setAnonyCustomInfo(this, model.getData().getAnonymousCustomerIonfo());

                Intent intent = new Intent();
                intent.putExtra("online", "false");
//                intent.setClass(LoginActivity.this, MainActivity.class);
                intent.setClass(LoginActivity.this, SyncDataActivity.class);
                startActivity(intent);
                LoginActivity.this.finish();
            } else {
                new NomelNoteDialog(this, "离线登录失败", "用户名或密码错误!", "确定", null);
            }
        }
    }

    @Override
    public void checkUpgdate() {
        if (!NetWorkUtil.isNetWorkAvailable(this)) {
            return;
        }

        if (upgradeModel != null) {
            showUpgradeDialog(LoginActivity.this, upgradeModel.getData());
        } else {
            checkUpdate(true);
        }
    }

    private void checkUpdate(final boolean flag) {
        Map<String, String> map = new ArrayMap<>();
        map.put("service", "App_Basic.chkVer");
        map.put("app_version", AppUtils.getVersion(this));
        map.put("os_type", "2");

        Observable<UpgradeModel> observable = RetrofitManager.getInstance().getPhpAPIService().chkVer(map);
        observable.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<UpgradeModel>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        refreshUI(ExceptionUtils.getInstance().getExceptionObj(LoginActivity.this, e), flag);
                    }

                    @Override
                    public void onNext(UpgradeModel model) {
                        refreshUI(model, flag);
                    }
                });
    }

    void hideDialog() {
        if (mDialog != null) {
            mDialog.dismiss();
        }
    }

    public void refreshUI(Object object, boolean flag) {
        if (object instanceof ExceptionModel) {
            ExceptionModel exception = (ExceptionModel) object;
            hideDialog();
            ExceptionUtils.getInstance().showExceptionToash(this, exception);
            if (exception.getMessage().contains("Network is unreachable")) { //启用离线模式
                if (result != 1) {
                    Intent intent = new Intent();
                    intent.putExtra("online", "false");
                    intent.setClass(LoginActivity.this, SyncDataActivity.class);
                    startActivity(intent);
                    LoginActivity.this.finish();
                }

            }
        }

        if (object instanceof AuthModel) { //正常的请求模式
            hideDialog();

            AuthModel model = (AuthModel) object;
            if (HttpStatus.status200 == model.getCode()) {
                userLoginData.setAuthJson(new Gson().toJson(model).toString());
                OAuthUtils.getInstance().setToken(this, model.getData().getToken());
                OAuthUtils.getInstance().setUserinfo(this, model.getData().getUserInfo());
                OAuthUtils.getInstance().setAnonyCustomInfo(this, model.getData().getAnonymousCustomerIonfo());

                saveUserLoginInfo(model.getData().getUserInfo());

                Intent intent = new Intent();
                intent.putExtra("online", "true");
                intent.setClass(LoginActivity.this, SyncDataActivity.class);
                startActivity(intent);

                LoginActivity.this.finish();
            } else {
                ExceptionUtils.getInstance().processException(this, model.getCode(), model.getMessage(), true);
            }
        }

        if (object instanceof UpgradeModel) {
            hideDialog();

            UpgradeModel model = (UpgradeModel) object;

            if (HttpStatus.status200 == model.getCode() && model.getData() != null) {
                if (model.getData() != null && model.getData().getType() == 202) {                //强制升级
                    showUpgradeDialog(LoginActivity.this, model.getData());
                } else if (flag) {
                    showUpgradeDialog(LoginActivity.this, model.getData());
                }

                mLoginView.refreshUpdate(true);
            } else {
                Toast.makeText(LoginActivity.this, "已经是最新版本了~~", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void saveUserLoginInfo(AuthModel.DataInfo.UserInfo userInfo) {
        try {
            if (userInfo != null && !Util.isNullStr(userInfo.getName())) {
                DataSupport.deleteAll(UserLoginData.class);
                userLoginData.setSerId(userInfo.getId());
                userLoginData.setName(userInfo.getName());
                userLoginData.setStatus(mLoginView.isSavePwd() ? "1" : "0");
                userLoginData.setLastLoginDate(new Date());
                //List<UserLoginData> ulist = DataSupport.where("serId =?", userInfo.getId() + "").find(UserLoginData.class);
                //if (ulist != null && ulist.size() > 0) {
                //    userLoginData.update(ulist.get(0).getId());
                //} else {
                //    userLoginData.save();
                //}
                userLoginData.save();
            }
        } catch (Exception e) {
            Trace.e(e);
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            finish();
            return true;
        } else if (keyCode == KeyEvent.KEYCODE_MENU) {
            return true;
        }

        return super.onKeyDown(keyCode, event);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ((MyApplication) getApplication()).popActivity(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onResume() {
        super.onResume();
        checkPermission(PERMISSION);
    }

    @Override
    protected void nextEvent() {
        super.nextEvent();
        mLoginView = new LayoutActivityLogin(this, this);
        if (getIntent().getStringExtra("flag") != null) {
            result = Integer.parseInt(getIntent().getStringExtra("flag"));
        }

        if (NetWorkUtil.isNetWorkAvailable(this)) {
            checkUpdate(false);
        }
    }

    public static void startActivity(Context context) {
        try {
            Intent intent = new Intent(context, LoginActivity.class);
            context.startActivity(intent);
        } catch (Exception e) {
        }
    }

    private void showUpgradeDialog(final Context mContext, final UpgradeModel.DataInfo dataInfo) {
        String negative = dataInfo.getType() == 202 ? mContext.getString(R.string.update_app_upgrade_exit) :
                mContext.getString(R.string.update_app_upgrade_next);

        LayoutInflater inflater = getLayoutInflater();
        LinearLayout layout = (LinearLayout) inflater.inflate(R.layout.dialog_exit_warmpos, null);

        AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
        AlertDialog dialog = builder.setTitle("升级提示")
                .setMessage(dataInfo.getFeature())
                .setCancelable(false)
                .setPositiveButton("去升级", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (dialog != null) {
                            dialog.dismiss();
                        }

                        updateFlag(dataInfo);

                        Toast.makeText(mContext, "可以在通知栏中查看下载进度...", Toast.LENGTH_SHORT).show();
                    }
                })
                .setNegativeButton(negative, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (dialog != null) {
                            dialog.dismiss();
                        }

                        if (dataInfo.getType() == 202) {
                            RecyclingUtil.getInstance().recyclingAll(mContext);
                            MyApplication.getInstance().exit();
                        }
                    }
                }).create();

        dialog.show();

        WindowManager.LayoutParams params = dialog.getWindow().getAttributes();
        params.width = AppUtils.dip2px(LoginActivity.this, 360);
        dialog.getWindow().setAttributes(params);

    }

    public void updateFlag(final UpgradeModel.DataInfo dataInfo) {
        UpdateService.Builder.create(dataInfo.getDownload())
                .setStoreDir("update/flag")
                .setDownloadSuccessNotificationFlag(Notification.DEFAULT_ALL)
                .setDownloadErrorNotificationFlag(Notification.DEFAULT_ALL)
                .build(this);
    }
}
