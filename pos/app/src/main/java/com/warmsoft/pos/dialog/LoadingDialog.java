package com.warmsoft.pos.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.warmsoft.pos.R;
import com.warmsoft.pos.util.Util;

/**
 * 作者: lijinliu on 2016/8/5.
 * 邮箱：jinliu.li@warmsoft.com
 * 描述: 数据加载，联网dialog
 */
public class LoadingDialog {

    private Dialog mDialog;
    private Context mContext;
    private LayoutInflater mInflater;
    private TextView mTvMsg;

    public LoadingDialog(Activity activity) {
        this.mContext = activity;
        this.mInflater = LayoutInflater.from(mContext);
        createLoadingDialog(mContext);
    }

    /**
     * 得到自定义的progressDialog
     *
     * @param context
     * @return
     */
    private void createLoadingDialog(Context context) {
        View rootView = mInflater.inflate(R.layout.dialog_base_loading, null);
        LinearLayout layout = (LinearLayout) rootView.findViewById(R.id.dialog_view);
        mTvMsg = (TextView) rootView.findViewById(R.id.tipTextView);
        /**
         * //图片动画
         ImageView spaceshipImage = (ImageView) v.findViewById(R.id.img);
         Animation hyperspaceJumpAnimation = AnimationUtils.loadAnimation( context, R.anim.load_animation);
         spaceshipImage.startAnimation(hyperspaceJumpAnimation);
         **/
        mDialog = new Dialog(context, R.style.loadingdialog);
        mDialog.setCancelable(false);// 不可以用“返回键”取消
        int MATH_PARENT = LinearLayout.LayoutParams.MATCH_PARENT;
        mDialog.setContentView(layout, new LinearLayout.LayoutParams(MATH_PARENT, MATH_PARENT));// 设置布局
    }

    public void show(String msg) {
        if (mTvMsg != null) {
            if (Util.isNullStr(msg)) {
                mTvMsg.setVisibility(View.GONE);
            } else {
                mTvMsg.setVisibility(View.VISIBLE);
                mTvMsg.setText(msg);
            }
        }
        if (mDialog != null) {
            mDialog.show();
        }
    }

    public void dismiss() {
        if (mDialog != null) {
            mDialog.dismiss();
        }
    }
}
