package com.warmsoft.pos.protocol;

import android.content.Context;
import android.support.v4.util.ArrayMap;

import com.google.gson.Gson;
import com.warmsoft.pos.bean.AmountsModel;
import com.warmsoft.pos.bean.CardListModel;
import com.warmsoft.pos.litepal.data.CustomeCardList;
import com.warmsoft.pos.net.RetrofitManager;
import com.warmsoft.pos.util.LogUtil;
import com.warmsoft.pos.util.SelectMemberUtil;
import com.warmsoft.pos.util.Trace;
import com.warmsoft.pos.util.Util;

import org.litepal.crud.DataSupport;

import java.util.List;
import java.util.Map;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * 作者: lijinliu on 2016/10/11.
 * 邮箱：jinliu.li@warmsoft.com
 * 描述: 获取用户账户金额
 */
public class GetCustomeAmontProto {

    protected Context mContext;
    protected ProtoListener mListener;
    protected int cusomerId;

    public GetCustomeAmontProto(Context context, ProtoListener listener, int cusomerId) {
        this.mContext = context;
        this.mListener = listener;
        this.cusomerId = cusomerId;
        getCustomeAmount();
    }

    public void getCustomeAmount() {
        Map<String, Integer> map = new ArrayMap<>();
        map.put("CustomerId", cusomerId);
        Observable<AmountsModel> observable = RetrofitManager.getInstance().getAPIService().amount(new Gson().toJson(map));
        observable.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<AmountsModel>() {

                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        LogUtil.error("getAmounts onError " + e.getMessage());
                        isError();
                    }

                    @Override
                    public void onNext(AmountsModel obj) {
                        LogUtil.error("getAmounts onNext ");

                        if (200 == obj.getCode()) {
                            //设置内存用的对象
                            obj.getData().setCustomerid(cusomerId);
                            obj.getData().setMemberCard(0);//会员卡金额置成0，重新取综合卡金额
                            SelectMemberUtil.getInstance().setDataInfo(obj.getData());
                            getCardList(obj);
                        } else {
                            isError();
                        }

                    }
                });
    }

    private void getCardList(final AmountsModel amountModel) {
        Map<String, Integer> map = new ArrayMap<>();
        map.put("CustomerId", cusomerId);
        Observable<CardListModel> observable = RetrofitManager.getInstance().getAPIService().membersCardList(new Gson().toJson(map));
        observable.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<CardListModel>() {

                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        LogUtil.error("getCardList onError " + e.getMessage());
                        if (mListener != null) {
                            mListener.onCompleted(amountModel.getData());
                        }
                    }

                    @Override
                    public void onNext(CardListModel obj) {
                        LogUtil.error("getCardList onNext ");
                        if (200 == obj.getCode()) {
                            List<CustomeCardList> dataList = obj.getData();
                            if (dataList != null && dataList.size() > 0) {
                                for (CustomeCardList cardInfo : dataList) {
                                    if (cardInfo.getCardType() == 2812) {//只刷综合卡
                                        amountModel.getData().setMemberCard(cardInfo.getCardBalance());
                                        cardInfo.setCustomerId(cusomerId);
                                        DataSupport.deleteAll(CustomeCardList.class, "CustomerId = ? ", cusomerId + "");//保存前删除
                                        cardInfo.save();//保存
                                        SelectMemberUtil.getInstance().setDataInfo(amountModel.getData());
                                    }
                                }
                            }
                        }
                        if (mListener != null) {
                            mListener.onCompleted(amountModel.getData());
                        }
                    }
                });
    }

    private void isError() {
        AmountsModel.DataInfo dataInfo = new AmountsModel.DataInfo();
        dataInfo.setCustomerid(cusomerId);
        dataInfo.setAccount(0);
        dataInfo.setPrePay(0);
        dataInfo.setMemberCard(0);
        SelectMemberUtil.getInstance().setDataInfo(dataInfo);
        if (mListener != null) {
            mListener.onCompleted(dataInfo);
        }
    }

    public interface ProtoListener {
        void onCompleted(AmountsModel.DataInfo dataInfo);
    }
}
