package com.warmsoft.pos;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.util.ArrayMap;
import android.text.TextUtils;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.warmsoft.base.BasicActivity;
import com.warmsoft.base.MyApplication;
import com.warmsoft.pos.bean.AuthModel;
import com.warmsoft.pos.bean.CardKindModel;
import com.warmsoft.pos.bean.GetPetModel;
import com.warmsoft.pos.bean.GoodDataModel;
import com.warmsoft.pos.bean.MembersModel;
import com.warmsoft.pos.bean.PetsCategoryModel;
import com.warmsoft.pos.bean.ResultModel;
import com.warmsoft.pos.litepal.data.CardKind;
import com.warmsoft.pos.litepal.data.CatgoryItemInfo;
import com.warmsoft.pos.litepal.data.CustomerList;
import com.warmsoft.pos.litepal.data.GoodItemInfo;
import com.warmsoft.pos.litepal.data.LitepalHelper;
import com.warmsoft.pos.litepal.data.PetCategory;
import com.warmsoft.pos.litepal.data.PetData;
import com.warmsoft.pos.net.HttpStatus;
import com.warmsoft.pos.net.RetrofitManager;
import com.warmsoft.pos.protocol.GetOrgCpaymentProto;
import com.warmsoft.pos.protocol.ProtocolListener;
import com.warmsoft.pos.util.List4MapUtil;
import com.warmsoft.pos.util.LogUtil;
import com.warmsoft.pos.util.MemberUtil;
import com.warmsoft.pos.util.OAuthUtils;
import com.warmsoft.pos.util.PerformanceUtils;
import com.warmsoft.pos.util.PrefUtil;
import com.warmsoft.pos.util.Trace;
import com.warmsoft.pos.wxpay.WePaySettingsModel;

import org.litepal.crud.DataSupport;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class SyncDataActivity extends BasicActivity {
    private static final int GO_HOME = 1000;

    private static final long SPLASH_DELAY_MILLIS = 3 * 1000;
    private static final int page_size = 900;
    private int total_count = 0;
    private int total_pet_count = 0;

    private TextView tvUpdateStatus;
    String online = "false";

    private LitepalHelper mLitepal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((MyApplication) getApplication()).pushActivity(this);
        setContentView(R.layout.activity_sync_data);

        initViews();

        online = getIntent().getStringExtra("online");
        if ("false".equalsIgnoreCase(online)) {
            tvUpdateStatus.setText("初始化资源中,请您耐心等待...");
            new Thread(new SyncOfflineRunnable()).start();
        } else {
            syncData(1, page_size);
        }
    }

    private void initRes() {//初始化离线必须资源
        List<CustomerList> localCustomer = DataSupport.where("OrgId =?", OAuthUtils.getInstance().getUserinfo(MyApplication.getContext()).getOrgId() + "").find(CustomerList.class);
        if (localCustomer != null) {
            MemberUtil.getInstance(SyncDataActivity.this).setCustomerLists(new ArrayList<>(localCustomer));//设置显示
        }

//        List<PetData> localpets = DataSupport.where("OrgId =?", OAuthUtils.getInstance().getUserinfo(MyApplication.getContext()).getOrgId() + "").find(PetData.class);
//        MemberUtil.getInstance(SyncDataActivity.this).setPetCategoryLists(new ArrayList<>(localCustomer));//设置显示

        List<PetCategory> petCategories = DataSupport.findAll(PetCategory.class);
        MemberUtil.getInstance(SyncDataActivity.this).setPetCategoryLists(petCategories);

        List<CardKind> cardKinds = DataSupport.findAll(CardKind.class);
        MemberUtil.getInstance(SyncDataActivity.this).setCardKindLists(cardKinds);

        initGoodInfo();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ((MyApplication) getApplication()).popActivity(this);
    }

    private void initViews() {
        tvUpdateStatus = (TextView) findViewById(R.id.id_for_update_status);
        tvUpdateStatus.setText("数据更新中...");
    }

    private Handler mHandler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case GO_HOME:
                    goHome();
                    break;
            }
            super.handleMessage(msg);
        }
    };

    private void goHome() {
        Intent intent = new Intent();
        intent.setClass(this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    private void syncData(final int PageIndex, final int PageSize) {
        if (total_pet_count <= 0) {
            tvUpdateStatus.setText("正在同步宠物信息...");
        } else {
            if (PageIndex * PageSize < total_pet_count) {
                tvUpdateStatus.setText("正在同步宠物信息..." + PageIndex * PageSize + "/" + total_pet_count);
            } else {
                tvUpdateStatus.setText("正在同步宠物信息..." + total_pet_count + "/" + total_pet_count);
            }
        }

        Map<String, Object> map = new ArrayMap<>();
//        map.put("CustomerId", null);
        map.put("CustomerId", OAuthUtils.getInstance().getAnonyCustomInfo(this).getAnonymousCustomerID());
        map.put("PageIndex", String.valueOf(PageIndex));
        map.put("PageSize", String.valueOf(PageSize));

        String ts = PrefUtil.getSharedPreference(MyApplication.getContext(), PrefUtil.customer_key, PrefUtil.customer_pet_ts_key_value, "");
        if (!TextUtils.isEmpty(ts)) {
            map.put("Timestamp", ts);
        }

        Observable<GetPetModel> petObservable = RetrofitManager.getInstance().getAPIService().GetPetListModelByCutomerId(new Gson().toJson(map));
        petObservable.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<GetPetModel>() {
                    @Override
                    public void onCompleted() {
                        LogUtil.error("syncData getPetListByCutomerId onCompleted");
                    }

                    @Override
                    public void onError(Throwable e) {
                        LogUtil.error("syncData getPetListByCutomerId onError", e);

                        getGoodsData();
                    }

                    @Override
                    public void onNext(GetPetModel model) {
                        LogUtil.error("syncData getPetListByCutomerId onNext");

                        if (HttpStatus.status200 == model.getCode()) {
                            total_pet_count = model.getData().getCount();

                            List<PetData> petDataList = model.getData().getPetList();
                            if (petDataList != null && petDataList.size() > 0) {
                                StringBuilder sb = new StringBuilder();
                                for (int i = 0; i < petDataList.size(); i++) {
                                    PetData petData = petDataList.get(i);
                                    petData.setPetId(petData.getID());

                                    sb.append(petData.getID());
                                    if (i < petDataList.size() - 1) {
                                        sb.append(",");
                                    }
                                }

                                //删除所有需要更新的petlist
                                LogUtil.error("sb " + sb.toString());
                                DataSupport.deleteAll(PetData.class, "id>? and petId in (" + sb.toString() + ")", "0");

                                //将所有的petlist重新插入到数据库中
                                DataSupport.saveAll(model.getData().getPetList());

                                syncData(PageIndex + 1, page_size);
                            } else {
                                getGoodsData(); //已经没有数据了
                            }
                        } else {
                            getGoodsData();//分页处理
                        }
                    }
                });
    }

    private void getCustomerInfo(final int PageIndex, final int PageSize) {
        if (total_count <= 0) {
            tvUpdateStatus.setText("正在同步会员信息...");
        } else {
            if (PageIndex * PageSize < total_count) {
                tvUpdateStatus.setText("正在同步会员信息..." + PageIndex * PageSize + "/" + total_count);
            } else {
                tvUpdateStatus.setText("正在同步会员信息..." + total_count + "/" + total_count);
            }

            LogUtil.error("ok getCustomerInfo PageIndex * PageSize  " + PageIndex * PageSize);
            LogUtil.error("ok getCustomerInfo total_count " + total_count);
            LogUtil.error("ok getCustomerInfo (PageIndex * PageSize / page_size) " + (PageIndex * PageSize / total_count));
        }

        Map<String, Object> map = new ArrayMap<>();
        AuthModel.DataInfo.UserInfo userInfo = OAuthUtils.getInstance().getUserinfo(this.getApplicationContext());
        map.put("OrgId", userInfo.getOrgId());
        map.put("PageIndex", String.valueOf(PageIndex));
        map.put("PageSize", String.valueOf(PageSize));

        String ts = PrefUtil.getSharedPreference(MyApplication.getContext(), PrefUtil.customer_key, PrefUtil.customer_key_ts_value, "");
        if (!TextUtils.isEmpty(ts)) {
            map.put("Timestamp", ts);
        }

        Observable<MembersModel> customObservable = RetrofitManager.getInstance().getAPIService().getCustomerList(new Gson().toJson(map));
        customObservable.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<MembersModel>() {
                    @Override
                    public void onCompleted() {
                        LogUtil.error("syncData getCustomerList onNext");
                    }

                    @Override
                    public void onError(Throwable e) {
                        Trace.e(e);
                        LogUtil.error("syncData getCustomerList onError", e);
                        getMemberInfo();
                    }

                    @Override
                    public void onNext(MembersModel model) {
                        LogUtil.error("syncData getCustomerList onNext");

                        if (HttpStatus.status200 == model.getCode()) {
                            List<CustomerList> customerLists = model.getData().getCustomerList();
                            if (customerLists != null && customerLists.size() > 0) {
                                total_count = model.getData().getCount();

                                StringBuilder sb = new StringBuilder();
                                for (int i = 0; i < customerLists.size(); i++) {
                                    CustomerList customerList = customerLists.get(i);
                                    customerList.setCustomerId(String.valueOf(customerList.getID()));

                                    sb.append(customerList.getCustomerId());
                                    if (i < customerLists.size() - 1) {
                                        sb.append(",");
                                    }
                                }

                                //删除需要的customerlist
                                DataSupport.deleteAll(CustomerList.class, "id > ? and CustomerId in (" + sb.toString() + ")", "0");

                                //将新的数据存储在数据库中
                                DataSupport.saveAll(customerLists);

                                getCustomerInfo(PageIndex + 1, page_size);
                            } else {//会员信息已经通过分页的方式下载结束
                                getMemberInfo();//同步待付款信息
                            }
                        } else {
                            getMemberInfo();//同步待付款信息
                        }
                    }
                });
    }

    private void getGoodsData() {
        tvUpdateStatus.setText("正在同步商品信息...");

        mLitepal = LitepalHelper.getInstance();
        String ts = mLitepal.queryPetTimeStatmp();
        PrefUtil.savePreferenceByItem(MyApplication.getContext(), PrefUtil.customer_key, PrefUtil.customer_pet_ts_key_value, ts);

        Map<String, Object> map = new ArrayMap<>();

        AuthModel.DataInfo.UserInfo userInfo = OAuthUtils.getInstance().getUserinfo(this.getApplicationContext());

        if (userInfo == null) {
            Toast.makeText(SyncDataActivity.this, "没有该用户请到cs注册～～", Toast.LENGTH_SHORT).show();
            Intent intent = new Intent();
            intent.setClass(SyncDataActivity.this, LoginActivity.class);
            intent.putExtra("flag", String.valueOf(1));
            startActivity(intent);
            this.finish();
        } else {
            map.put("OrgId", userInfo.getOrgId());
            map.put("DrugType", 1);//获取所有4大类型

            Observable<GoodDataModel> observable = RetrofitManager.getInstance().getAPIService().getGoodData(new Gson().toJson(map));
            observable.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Subscriber<GoodDataModel>() {
                        @Override
                        public void onCompleted() {
                            LogUtil.error("syncData getGoodData onCompleted");
                        }

                        @Override
                        public void onError(Throwable e) {
                            LogUtil.error("syncData getGoodData onError " + e.getMessage());

                            getCustomerInfo(1, page_size);
                        }

                        @Override
                        public void onNext(GoodDataModel model) {
                            LogUtil.error("syncData getGoodData onNext");

                            if (HttpStatus.status200 == model.getCode()) {
                                DataSupport.deleteAll(GoodItemInfo.class);
                                DataSupport.deleteAll(CatgoryItemInfo.class);

                                List<CatgoryItemInfo> catgoryItemInfos = model.getData().getDataInfo();
                                for (int i = 0; i < catgoryItemInfos.size(); i++) {
                                    catgoryItemInfos.get(i).setType(i);
                                    List<GoodItemInfo> goodItemInfos = catgoryItemInfos.get(i).getInfo();
                                    for (int j = 0; j < goodItemInfos.size(); j++) {
                                        goodItemInfos.get(j).setType(i);
                                        goodItemInfos.get(j).setGoodId(goodItemInfos.get(j).getId());
                                    }
                                    DataSupport.saveAll(goodItemInfos);
                                }
                                DataSupport.saveAll(catgoryItemInfos);

                                initGoodInfo();
                            }

                            getCustomerInfo(1, page_size);
                        }
                    });
        }
    }


    public void getUnPaidInfo() {
        tvUpdateStatus.setText("正在同步待付款信息...");

        AuthModel.DataInfo.UserInfo userInfo = OAuthUtils.getInstance().getUserinfo(MyApplication.getContext());
        int orgId = userInfo.getOrgId();
        String startTime = "2016-08-01 10:00:00";
        new GetOrgCpaymentProto(MyApplication.getContext()).queryDataFromService(orgId + "", startTime, new ProtocolListener() {
            @Override
            public void onCompleted() {
                LogUtil.error("syncData getUnPaidInfo onNext");
            }

            @Override
            public void onNext() {
                getWxPaySetting();
            }

            @Override
            public void onError() {
                LogUtil.error("syncData getUnPaidInfo onError");
                getWxPaySetting();
            }
        });

    }

    public void getWxPaySetting() {
        tvUpdateStatus.setText("正在同步支付配置信息...");

        Map<String, String> map = new HashMap<>();

        Observable<WePaySettingsModel> petObservable = RetrofitManager.getInstance().getAPIService().GetWePaySettings(new Gson().toJson(map));
        petObservable.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<WePaySettingsModel>() {
                    @Override
                    public void onCompleted() {
                        LogUtil.error("syncData getWxPaySetting onCompleted");
                    }

                    @Override
                    public void onError(Throwable e) {
                        LogUtil.error("syncData getWxPaySetting onError");
                        mHandler.sendEmptyMessage(GO_HOME);
                    }

                    @Override
                    public void onNext(WePaySettingsModel model) {
                        LogUtil.error("syncData getWxPaySetting onNext");

                        if (model.getCode() == 200) {

                        }
                        mHandler.sendEmptyMessage(GO_HOME);
                    }
                });
    }


    public void getMemberInfo() {
        tvUpdateStatus.setText("正在同步会员卡及打折信息...");

        //时间戳通过查询数据表
        mLitepal = LitepalHelper.getInstance();
        String ts = mLitepal.queryCustomerTimeStamp();
        PrefUtil.savePreferenceByItem(MyApplication.getContext(), PrefUtil.customer_key, PrefUtil.customer_key_ts_value, ts);

        Map<String, String> map = new HashMap<>();

        Observable<CardKindModel> petObservable = RetrofitManager.getInstance().getAPIService().getCardKind(new Gson().toJson(map));
        petObservable.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<CardKindModel>() {
                    @Override
                    public void onCompleted() {
                        LogUtil.error("syncData getMemberInfo onCompleted");
                    }

                    @Override
                    public void onError(Throwable e) {
                        LogUtil.error("syncData CardKindModel onError");
                        getPetsCategory();
                    }

                    @Override
                    public void onNext(CardKindModel model) {
                        LogUtil.error("syncData CardKindModel onNext");

                        if (model.getCode() == 200) {
                            DataSupport.deleteAll(CardKind.class);

                            for (int i = 0; i < model.getData().size(); i++) {
                                model.getData().get(i).setCardId(model.getData().get(i).getId());
                            }

                            long start = PerformanceUtils.getInstance().getReadyTime();
                            MemberUtil.getInstance(SyncDataActivity.this).setCardKindLists(model.getData());

                            DataSupport.saveAll(model.getData());
                            PerformanceUtils.getInstance().showConsumeTime(start);
                        }

                        getPetsCategory();
                    }
                });
    }

    public void getPetsKind() {
        tvUpdateStatus.setText("正在同步宠物品种折信息...");

        Map<String, String> map = new HashMap<>();

        Observable<ResultModel> petObservable = RetrofitManager.getInstance().getAPIService().getPetsKind(new Gson().toJson(map));
        petObservable.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<ResultModel>() {
                    @Override
                    public void onCompleted() {
                        LogUtil.error("syncData getCardKind onCompleted");
                    }

                    @Override
                    public void onError(Throwable e) {
                        LogUtil.error("syncData getCardKind onError");
                        getPetsCategory();
                    }

                    @Override
                    public void onNext(ResultModel model) {
                        LogUtil.error("syncData getCardKind onNext");
//
//                        DataSupport.deleteAll(PetData.class);
//                        for (int i = 0; i < model.getData().size(); i++) {
//                            model.getData().get(i).setPetId(model.getData().get(i).getID());
//                        }
//
//                        long start = PerformanceUtils.getInstance().getReadyTime();
//                        DataSupport.saveAll(model.getData());
//                        PerformanceUtils.getInstance().showConsumeTime(start);
//                        Trace.e("SyncData: ", "Pet Info Sync End, PetSize:" + model.getData().size());
//
                        getPetsCategory();
                    }
                });
    }

    public void getPetsCategory() {
        tvUpdateStatus.setText("正在同步获取宠物种类信息...");

        Map<String, String> map = new HashMap<>();

        Observable<PetsCategoryModel> petObservable = RetrofitManager.getInstance().getAPIService().getPetsCategory(new Gson().toJson(map));
        petObservable.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<PetsCategoryModel>() {
                    @Override
                    public void onCompleted() {
                        LogUtil.error("syncData getPetsCategory onCompleted");
                    }

                    @Override
                    public void onError(Throwable e) {
                        LogUtil.error("syncData getPetsCategory onError");
                        getUnPaidInfo();
                    }

                    @Override
                    public void onNext(PetsCategoryModel model) {
                        LogUtil.error("syncData getPetsCategory onNext");

                        if (model.getCode() == 200) {
                            DataSupport.deleteAll(PetCategory.class);
                            for (int i = 0; i < model.getData().size(); i++) {
                                model.getData().get(i).setCategory_id(String.valueOf(model.getData().get(i).getId()));
                            }

                            long start = PerformanceUtils.getInstance().getReadyTime();

                            MemberUtil.getInstance(SyncDataActivity.this).setPetCategoryLists(model.getData());

                            DataSupport.saveAll(model.getData());
                            PerformanceUtils.getInstance().showConsumeTime(start);
                            Trace.e("SyncData: ", "Pet Info Sync End, PetSize:" + model.getData().size());
                        }

                        getUnPaidInfo();
                    }

                });
    }

    class SyncMemberRunnable implements Runnable {
        List<CustomerList> customerLists;
        boolean flag;

        public SyncMemberRunnable(List<CustomerList> lists, boolean f) {
            this.customerLists = lists;
            this.flag = f;
        }

        @Override
        public void run() {
            List<CustomerList> localCustomer = DataSupport.findAll(CustomerList.class);

            //获取并集用来刷新,获取差集用来存储
            Set<CustomerList> customerSet = new HashSet<>();
            customerSet.addAll(localCustomer);
            customerSet.addAll(customerLists);

            List<CustomerList> addLists = getDifferentCustomers(localCustomer, customerLists);//保存有差异的数据
            if (addLists.size() > 0) {
                DataSupport.saveAll(addLists);
            }
        }
    }

    private List<CustomerList> getDifferentCustomers(List<CustomerList> localCustomers, List<CustomerList> netCustomers) {
        LogUtil.error("getDifferentCustomers ");
        Map<String, CustomerList> localMaps = List4MapUtil.getInstance().list4Map(localCustomers);
        Map<String, CustomerList> netMaps = List4MapUtil.getInstance().list4Map(netCustomers);

        return List4MapUtil.getInstance().getDifferentMap(localMaps, netMaps);
    }

    private List<PetData> getDifferentPets(List<PetData> localCustomers, List<PetData> netCustomers) {
        LogUtil.error("getDifferentPets ");
        Map<Integer, PetData> localMaps = List4MapUtil.getInstance().list4PetMap(localCustomers);
        Map<Integer, PetData> netMaps = List4MapUtil.getInstance().list4PetMap(netCustomers);

        return List4MapUtil.getInstance().getDifferentPetsMap(localMaps, netMaps);
    }

    class SyncPetRunnable implements Runnable {
        List<PetData> petDatas;
        boolean flag;

        public SyncPetRunnable(List<PetData> lists, boolean f) {
            this.petDatas = lists;
            this.flag = f;
        }

        @Override
        public void run() {
            List<PetData> localCustomer = DataSupport.findAll(PetData.class);

            List<PetData> addLists = getDifferentPets(localCustomer, petDatas);//保存有差异的数据
            if (addLists.size() > 0) {
                DataSupport.saveAll(addLists);
            }
        }
    }

    private List<PetData> processNetData(List<PetData> netData) {
        for (int i = 0; i < netData.size(); i++) {
            netData.get(i).setPetId(netData.get(i).getID());
        }
        return netData;
    }

    class SyncOfflineRunnable implements Runnable {

        @Override
        public void run() {
            initRes();

            mHandler.sendEmptyMessage(GO_HOME);
        }
    }

    private void initGoodInfo() {
        String[] list;
        List<CatgoryItemInfo> goodsList = DataSupport.findAll(CatgoryItemInfo.class);

        List<GoodItemInfo> info;
        List<String> catgroy = new ArrayList<>();
        for (int i = 0; i < goodsList.size(); i++) {
            info = DataSupport.where(" type=? ", String.valueOf(goodsList.get(i).getType())).find(GoodItemInfo.class);
            goodsList.get(i).setInfo(info);
            catgroy.add(goodsList.get(i).getCategory());
        }

        list = catgroy.toArray(new String[catgroy.size()]);

        MemberUtil.getInstance(SyncDataActivity.this).setCatgoryItemInfoList(goodsList);
        MemberUtil.getInstance(SyncDataActivity.this).setList(list);
    }
}