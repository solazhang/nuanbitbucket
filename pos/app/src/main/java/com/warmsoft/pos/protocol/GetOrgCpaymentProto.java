package com.warmsoft.pos.protocol;

import android.content.Context;
import android.support.v4.util.ArrayMap;

import com.google.gson.Gson;
import com.warmsoft.pos.bean.CustomerOrderModel;
import com.warmsoft.pos.enumutil.OrderStatusEnum;
import com.warmsoft.pos.litepal.data.CustomerList;
import com.warmsoft.pos.litepal.data.LitepalHelper;
import com.warmsoft.pos.litepal.data.OrderInfoData;
import com.warmsoft.pos.litepal.data.PetData;
import com.warmsoft.pos.litepal.data.SettlementData;
import com.warmsoft.pos.net.HttpStatus;
import com.warmsoft.pos.net.RetrofitManager;
import com.warmsoft.pos.util.LogUtil;
import com.warmsoft.pos.util.OrderHelper;
import com.warmsoft.pos.util.TimeUtils;
import com.warmsoft.pos.util.Trace;

import org.litepal.crud.DataSupport;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * 作者: lijinliu on 2016/9/13.
 * 邮箱：jinliu.li@warmsoft.com
 * 描述: 获取机构待付款
 */
public class GetOrgCpaymentProto {

    protected Context mContext;

    public GetOrgCpaymentProto(Context context) {
        this.mContext = context;
    }

    //服务端查询代付款
    public void queryDataFromService(String orgId, String startDate, final ProtocolListener listener) {
        TimeUtils timeUtil = TimeUtils.getInstance();
        Date nowDate = new Date();
        Map<String, Object> mapNotPay = new ArrayMap<>();
        mapNotPay.put("OrgId", orgId);
        mapNotPay.put("Status", 0);//起始时间
        mapNotPay.put("StartDate", startDate);
        mapNotPay.put("EndDate", timeUtil.formatParmasData(nowDate));

        Observable<CustomerOrderModel> customNotPayOrders = RetrofitManager.getInstance().getAPIService().getCpaymentByOrgId(new Gson().toJson(mapNotPay));
        customNotPayOrders.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<CustomerOrderModel>() {
                    @Override
                    public void onCompleted() {
                        LogUtil.error("GetOrgCpaymentProto queryDataFromService onNext");
                        listener.onCompleted();
                    }

                    @Override
                    public void onError(Throwable e) {
                        LogUtil.error("GetOrgCpaymentProto queryDataFromService onError",e);
                        listener.onError();
                    }

                    @Override
                    public void onNext(CustomerOrderModel model) {
                        LogUtil.error("GetOrgCpaymentProto queryDataFromService onNext");
                        if (HttpStatus.status200 == model.getCode()) {
                            LitepalHelper litepal = LitepalHelper.getInstance();
                            List<SettlementData> dataList = model.getData();
                            List<Integer> petList = new ArrayList<Integer>();

                            for (SettlementData settleData : dataList) {
                                if (!litepal.checkPayment(settleData.getID())) {//数据已存在
                                    settleData.setDiscountRate(1);
                                    settleData.setPaymentId(settleData.getID());
                                    settleData.save();
                                    if (!petList.contains(settleData.getPetId())) {
                                        petList.add(settleData.getPetId());
                                    }
                                }
                            }
                            Trace.e("SyncData: ", "GetOrgCpaymentProto queryDataFromService() Succ, UnPaidISize:" + dataList.size());

                            for (int petid : petList) {
                                OrderInfoData orderDb = litepal.getOrderDataByPetId(petid);
                                List<SettlementData> goodList = litepal.queryWaitPaysByPetId(petid);
                                if (goodList != null && goodList.size() > 0) {
                                    if (orderDb == null) {
                                        orderDb = newOrder(goodList.get(0));
                                    }
                                    OrderInfoData orderUpdate = new OrderInfoData();
                                    orderUpdate.setCountAmount(orderDb.getCountAmount());
                                    orderUpdate.setCountGoods(orderDb.getCountGoods());

                                    for (SettlementData good : goodList) {
                                        SettlementData goodUpdate = new SettlementData();
                                        orderUpdate.setCountAmount(orderUpdate.getCountAmount() + good.getTotalAmount());
                                        orderUpdate.setCountGoods(orderUpdate.getCountGoods() + good.getCount());
                                        goodUpdate.setOrderNo(orderDb.getOrderNo());
                                        goodUpdate.update(good.getID());
                                    }

                                    orderUpdate.update(orderDb.getId());//更新

                                }
                            }
                        }

                        listener.onNext();
                    }
                });
    }

    public OrderInfoData newOrder(SettlementData settleData) {
        List<CustomerList> custome = DataSupport.where(" CustomerId=? ", settleData.getCustomerId() + "").find(CustomerList.class);
        List<PetData> pet = DataSupport.where(" petId=? ", settleData.getPetId() + "").find(PetData.class);

        int orderNum = OrderHelper.getInstance(mContext).createOrderNo();
        OrderInfoData orderData = new OrderInfoData();
        orderData.setOrderNo(orderNum);
        orderData.setPetId(settleData.getPetId());
        if (pet != null && pet.size() > 0) {
            orderData.setPetName(pet.get(0).getPetName());
        } else {
            orderData.setPetName(settleData.getPetName());
        }
        orderData.setCustomerId(settleData.getCustomerId());
        if (custome != null && custome.size() > 0) {
            orderData.setCustomerName(custome.get(0).getName());
            orderData.setCellPhone(custome.get(0).getCellPhone());
            orderData.setMemberCardNo(custome.get(0).getCardNumber());
        } else {
            orderData.setCustomerName(settleData.getCustomerName());
            orderData.setCellPhone(settleData.getCellPhone());
            orderData.setMemberCardNo("0");
        }
        orderData.setStatus(OrderStatusEnum.getEnumValue(OrderStatusEnum.HandIn));//待付
        orderData.setOrgId(settleData.getOrgId());
        orderData.setCountAmount(0);
        orderData.setOrderDate(TimeUtils.getInstance().StrToDate(settleData.getChargeDatetime()));
        orderData.setCountGoods(0);
        orderData.save();
        orderData.setId(DataSupport.max(OrderInfoData.class, "id", int.class));

        return orderData;
    }
}
