package com.warmsoft.pos.litepal.data;

import org.litepal.crud.DataSupport;

import java.util.Date;

/**
 * 作者: lijinliu on 2016/8/5.
 * 邮箱：jinliu.li@warmsoft.com
 * 描述: 付款信息
 */
public class PayAmountData extends DataSupport {

    private int id;
    private String orderNo;
    private int PetId;//宠物ID
    private String PetName;//宠物名称
    private int CustomerId;//客户ID
    private String CustomerName;//客户名称
    private String payChanelName;
    private int payChanel;
    private float orderAmount;//
    private float payAmount;
    private float change;
    private Date payDate;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public int getPetId() {
        return PetId;
    }

    public void setPetId(int petId) {
        PetId = petId;
    }

    public String getPetName() {
        return PetName;
    }

    public void setPetName(String petName) {
        PetName = petName;
    }

    public int getCustomerId() {
        return CustomerId;
    }

    public void setCustomerId(int customerId) {
        CustomerId = customerId;
    }

    public String getCustomerName() {
        return CustomerName;
    }

    public void setCustomerName(String customerName) {
        CustomerName = customerName;
    }

    public String getPayChanelName() {
        return payChanelName;
    }

    public void setPayChanelName(String payChanelName) {
        this.payChanelName = payChanelName;
    }

    public int getPayChanel() {
        return payChanel;
    }

    public void setPayChanel(int payChanel) {
        this.payChanel = payChanel;
    }

    public float getOrderAmount() {
        return orderAmount;
    }

    public void setOrderAmount(float orderAmount) {
        this.orderAmount = orderAmount;
    }

    public float getPayAmount() {
        return payAmount;
    }

    public void setPayAmount(float payAmount) {
        this.payAmount = payAmount;
    }

    public float getChange() {
        return change;
    }

    public void setChange(float change) {
        this.change = change;
    }

    public Date getPayDate() {
        return payDate;
    }

    public void setPayDate(Date payDate) {
        this.payDate = payDate;
    }
}
