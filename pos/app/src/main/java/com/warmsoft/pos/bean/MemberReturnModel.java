package com.warmsoft.pos.bean;

import com.warmsoft.pos.litepal.data.CustomerList;

/**
 * Created by sangxiewu on 16/8/31.
 */
public class MemberReturnModel {
    /**
     * {
     * "code": 200,
     * "message": "message成功",
     * "Data": {
     * "ID": 1873304,
     * "OrgId": 4285,
     * "Name": "mmmm",
     * "Gender": null,
     * "Birthdate": null,
     * "CellPhone": "33",
     * "ContactPhone": null,
     * "EMail": null,
     * "Address": null,
     * "CardNumber": "A00516080021",
     * "CardType": 0,
     * "CardTypeName": null,
     * "Score": null,
     * "ExpiredDate": null,
     * "Remark": null,
     * "Age": null,
     * "VIPLevel": 85,
     * "RegistrationDate": "0001-01-01 00:00:00",
     * "LastInterview": "2016-08-31 16:24:38",
     * "TreatmentStatus": null,
     * "TotalConsume": 0,
     * "yonghuID": null,
     * "amount": null,
     * "RestOfAmount": 0,
     * "FullSpelling": null,
     * "PreAmount": 0,
     * "Telephone2": null,
     * "Telephone3": null,
     * "Status": 0,
     * "CusProvince": null,
     * "CusCity": null,
     * "CusTown": null,
     * "CusStreet": null,
     * "CusStreetAddress": null,
     * "GenderText": null,
     * "DataFrom": null,
     * "DataNum": 0
     * }
     * }
     */

    int code;
    String message;

    CustomerList Data;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public CustomerList getData() {
        return Data;
    }

    public void setData(CustomerList data) {
        Data = data;
    }
}
