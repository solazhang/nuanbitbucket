package com.warmsoft.pos.litepal.data;

import org.litepal.crud.DataSupport;

import java.io.Serializable;

/**
 * 作者: lijinliu on 2016/8/29.
 * 邮箱：jinliu.li@warmsoft.com
 * 描述: 结算数据返回
 */
public class SettleReslutData extends DataSupport {

    private int id;//": 11211370,
    private int orderNo;//本地订单号
    private int paymentId;//退单使用
    private int orgId;
    private float totalAmount;//": 244,
    private float actlyPayed;//": 0,
    private float discountAmount;//": 244

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(int orderNo) {
        this.orderNo = orderNo;
    }

    public int getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(int paymentId) {
        this.paymentId = paymentId;
    }

    public float getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(float totalAmount) {
        this.totalAmount = totalAmount;
    }

    public float getActlyPayed() {
        return actlyPayed;
    }

    public int getOrgId() {
        return orgId;
    }

    public void setOrgId(int orgId) {
        this.orgId = orgId;
    }

    public void setActlyPayed(float actlyPayed) {
        this.actlyPayed = actlyPayed;
    }

    public float getDiscountAmount() {
        return discountAmount;
    }

    public void setDiscountAmount(float discountAmount) {
        this.discountAmount = discountAmount;
    }
}
