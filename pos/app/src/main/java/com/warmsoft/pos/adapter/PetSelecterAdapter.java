package com.warmsoft.pos.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.warmsoft.pos.R;
import com.warmsoft.pos.litepal.data.PetData;

import java.util.List;

/**
 * 作者: lijinliu on 2016/8/9.
 * 邮箱：jinliu.li@warmsoft.com
 * 描述: 开单时宠物选择列表
 */
public class PetSelecterAdapter extends BaseAdapter {

    protected Context mContext;
    protected List<PetData> datas;
    protected int petSelected = -1;
    protected int colorWhite = 0;
    protected int colorBlue = 0;
    protected PetData pet;

    public PetSelecterAdapter(Context context, List<PetData> datas) {
        mContext = context;
        this.datas = datas;
        colorWhite = mContext.getResources().getColor(R.color.white);
        colorBlue = mContext.getResources().getColor(R.color.text_order_blue);
    }

    public void setData(List<PetData> datas) {
        petSelected = -1;
        pet = null;
        this.datas = datas;
        this.notifyDataSetChanged();
    }

    public void setSelect(int select) {
        this.petSelected = select;
        if (datas != null && select < datas.size()) {
            this.pet = datas.get(select);
        }
        this.notifyDataSetChanged();
    }

    public PetData getSelPet() {
        return this.pet;
    }

    @Override
    public int getCount() {
        if (datas == null) {
            return 0;
        } else {
            return datas.size();
        }
    }

    @Override
    public Object getItem(int position) {
        if (datas == null) {
            return null;
        } else {
            return datas.get(position);
        }
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @SuppressLint("ViewHolder")
    @Override
    public View getView(int position, View view, ViewGroup parent) {
        ViewHolder holde;
        if (view == null) {
            view = LayoutInflater.from(mContext).inflate(R.layout.item_member_pet, null);
            holde = new ViewHolder(view);
        } else {
            holde = (ViewHolder) view.getTag();
            if (holde == null) {
                view = LayoutInflater.from(mContext).inflate(R.layout.item_member_pet, null);
                holde = new ViewHolder(view);
            }
        }
        if (holde != null) {
            PetData item = (PetData) getItem(position);
            holde.bindData(item, position);
        }

        return view;
    }

    class ViewHolder {
        TextView tvPetName;
        TextView tvPetKindof;
        TextView tvPetVariety;
        TextView tvPetGender;
        LinearLayout layoutBg;

        ViewHolder(View view) {
            tvPetName = (TextView) view.findViewById(R.id.tv_pet_name);
            tvPetKindof = (TextView) view.findViewById(R.id.tv_pet_kindof);
            tvPetVariety = (TextView) view.findViewById(R.id.tv_pet_variety);
            tvPetGender = (TextView) view.findViewById(R.id.tv_pet_gender);
            layoutBg = (LinearLayout) view.findViewById(R.id.layout_bg);
            view.setTag(this);
        }

        void bindData(final PetData data, final int position) {
            if (data == null) {
                return;
            } else {
                tvPetName.setText(data.getPetName() + "");
                tvPetKindof.setText(data.getKindOFText() + "");
                tvPetVariety.setText(data.getVarietyText() + "");
                tvPetGender.setText(data.getGenderText() + "");

                if (petSelected == position) {
                    layoutBg.setBackgroundColor(colorBlue);
                } else {
                    layoutBg.setBackgroundColor(colorWhite);
                }
            }
        }
    }
}
