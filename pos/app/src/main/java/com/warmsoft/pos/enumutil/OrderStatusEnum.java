package com.warmsoft.pos.enumutil;

/**
 * 作者: lijinliu on 2016/8/23.
 * 邮箱：jinliu.li@warmsoft.com
 * 描述:定单状态说明
 */
public enum OrderStatusEnum {
    Null, Bill, HandIn, Settle,BackHandIn,BackSettle,ReOpen,KeepingAccounts;
    //未开单，开单，下单，结算,下单退单,结算退端,追单(会员信息不可更改),记账

    public static String getEnumValue(OrderStatusEnum statusEnum) {
        String value = "0";
        switch (statusEnum) {
            case Null:
                value = "-1";
                break;
            case Bill:
                value = "0";
                break;
            case HandIn:
                value = "1";
                break;
            case Settle:
                value = "2";
                break;
            case BackHandIn:
                value = "3";
                break;
            case BackSettle:
                value = "4";
                break;
            case ReOpen:
                value = "5";
                break;
            case KeepingAccounts:
                value = "6";
                break;
        }
        return value;
    }

    public static OrderStatusEnum getOrderEnum(String status) {
        OrderStatusEnum orderStatusEnum;
        switch (status) {
            case "-1":
                return OrderStatusEnum.Null;
            case "0":
                return OrderStatusEnum.Bill;
            case "1":
                return OrderStatusEnum.HandIn;
            case "2":
                return OrderStatusEnum.Settle;
            case "3":
                return OrderStatusEnum.BackHandIn;
            case "4":
                return OrderStatusEnum.BackSettle;
            case "5":
                return OrderStatusEnum.ReOpen;
            case "6":
                return OrderStatusEnum.KeepingAccounts;
        }
        return OrderStatusEnum.Null;
    }

    public static String getEnumDesc(OrderStatusEnum statusEnum) {
        String desc = "未开单";
        switch (statusEnum) {
            case Null:
                desc =  "未开单";
                break;
            case Bill:
                desc = "已开单";
                break;
            case HandIn:
                desc = "已下单";
                break;
            case Settle:
                desc = "已结算";
                break;
            case BackHandIn:
                desc = "已退单";
                break;
            case BackSettle:
                desc = "已退单";
                break;
            case ReOpen:
                desc = "追单";
                break;
            case KeepingAccounts:
                desc = "已记账";
                break;
        }
        return desc;
    }
}