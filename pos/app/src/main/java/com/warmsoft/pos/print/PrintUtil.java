package com.warmsoft.pos.print;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.hardware.usb.UsbManager;
import android.os.Handler;
import android.os.Message;
import android.widget.Toast;

import com.android.print.demo.BluetoothOperation;
import com.android.print.demo.IPrinterOpertion;
import com.android.print.demo.UsbOperation;
import com.android.print.demo.sunmi.AidlUtil;
import com.android.print.demo.utils.FileUtils;
import com.android.print.sdk.PrinterConstants;
import com.android.print.sdk.PrinterInstance;
import com.android.print.sdk.Table;
import com.warmsoft.pos.R;
import com.warmsoft.pos.bean.OrderPrintModel;
import com.warmsoft.pos.util.LogUtil;
import com.warmsoft.pos.util.OrderHelper;
import com.warmsoft.pos.util.TimeUtils;

/**
 * Created by brooks on 16/8/16.
 */
public class PrintUtil {

//    private static PrintUtil instance = null;
//
//    public static PrintUtil getInstance() {
//        if (instance == null) {
//            synchronized (PrintUtil.class) {
//                instance = new PrintUtil();
//            }
//        }
//
//        return instance;
//    }
//
//    protected static IPrinterOpertion myOpertion;
//    private static boolean isConnected = false;
//    private PrinterInstance mPrinter;
//
//    private boolean is58mm = true;
//
//
//    public void initPrinter(Activity mContext) {
//        new FileUtils().createFile(mContext);//向SD卡上写入打印文件
//
//        myOpertion = new UsbOperation(mContext, mHandler);
//        UsbManager manager = (UsbManager) mContext.getSystemService(Context.USB_SERVICE);
//        myOpertion.usbAutoConn(manager);
//    }
//
//    public void printOrder(Activity mContext) {
//        if (!isConnected) {
//            initPrinter(mContext);
//            Toast.makeText(mContext, "打印机未连接~", Toast.LENGTH_SHORT).show();
//            return;
//        }
//
//        try {
//            OrderPrintModel model = OrderHelper.getInstance(mContext).getPrintInfo();
//            if (model.getGoodInfoList().size() <= 0) {
//                Toast.makeText(mContext, "空单不能打印~~", Toast.LENGTH_SHORT).show();
//                return;
//            }
//
//            PrintUtil.printNote(mContext.getResources(), mPrinter, is58mm, model);
//        } catch (Exception e) {
////            if (e != null) {
////                LogUtil.error(e.getMessage());
////            }
//        }
//    }
//
//    public boolean getConnectStatus() {
//        return isConnected;
//    }
//
//    //用于接受连接状态消息的 Handler
//    private Handler mHandler = new Handler() {
//        @Override
//        public void handleMessage(Message msg) {
//            switch (msg.what) {
//                case PrinterConstants.Connect.SUCCESS:
//                    isConnected = true;
//                    mPrinter = myOpertion.getPrinter();
//                    break;
//                case PrinterConstants.Connect.FAILED:
//                    isConnected = false;
////                    Toast.makeText(, com.android.print.demo.R.string.conn_failed,
////                            Toast.LENGTH_SHORT).show();
//                    break;
//                case PrinterConstants.Connect.CLOSED:
//                    isConnected = false;
////                    Toast.makeText(mContext, com.android.print.demo.R.string.conn_closed, Toast.LENGTH_SHORT).show();
//                    break;
//                case PrinterConstants.Connect.NODEVICE:
//                    isConnected = false;
////                    Toast.makeText(mContext, com.android.print.demo.R.string.conn_no, Toast.LENGTH_SHORT).show();
//                    break;
//
//                default:
//                    break;
//            }
//        }
//    };
//
//    public static void printNote(Resources resources, PrinterInstance mPrinter,
//                                 boolean is58mm, OrderPrintModel model) {
//        mPrinter.init();
//
//        mPrinter.setFont(0, 0, 0, 0);
//        mPrinter.setPrinter(PrinterConstants.Command.ALIGN, PrinterConstants.Command.ALIGN_LEFT);
//        mPrinter.setPrinter(PrinterConstants.Command.PRINT_AND_WAKE_PAPER_BY_LINE, 2);
//
//        StringBuffer sb = new StringBuffer();
//
//        mPrinter.setPrinter(PrinterConstants.Command.ALIGN, PrinterConstants.Command.ALIGN_CENTER);
//        mPrinter.setCharacterMultiple(0, 1);
//        mPrinter.printText(model.getOrgName() + "\n");
//
//        mPrinter.setPrinter(PrinterConstants.Command.ALIGN, PrinterConstants.Command.ALIGN_LEFT);
//        // 字号使用默认
//        mPrinter.setCharacterMultiple(0, 0);
//        //单号
//        sb.append("单    号:" + model.getOrderNo() + "\n");
//        sb.append("收 银 员:" + model.getOpName() + "\n");
//        sb.append("单据日期:" + TimeUtils.getInstance().formatYMD(model.getOrderDate()) + "\n");
//        sb.append("打印日期:" + TimeUtils.getInstance().formatYMDHMS(model.getPrintDate()) + "\n");
//
//        mPrinter.printText(sb.toString()); // 打印
//
//        printTable(resources, mPrinter, is58mm, model); // 打印表格
//
//        sb = new StringBuffer();
//        if (is58mm) {
//            sb.append("数量:"
//                    + "" + model.getCountGoods() + "\n");
//            sb.append("总计:"
//                    + "" + model.getCountAmount() + "\n");
//
//            sb.append("付款:");
//
//            if (model.getCashAmount() > 0) {
//                sb.append(
//                        "\n " + "现  金:" + model.getCashAmount() + "");
//            }
//
//            if (model.getBankCardAmount() > 0) {
//                sb.append("\n"
//                        + "" + " 银行卡:" + model.getBankCardAmount());
//            }
//
//            if (model.getMemberCardAmount() > 0) {
//                sb.append("\n" + " 会员卡:" + model.getMemberCardAmount());
//            }
//
//            if (model.getPrePayAmount() > 0) {
//                sb.append("\n" + " 押  金:" + model.getPrePayAmount());
//            }
//
//            if (model.getAccountAmount() > 0) {
//                sb.append("\n" + " 账  户:" + model.getAccountAmount());
//            }
//
//            if (model.getWebChatAmount() > 0) {
//                sb.append("\n"
//                        + "" + " 微  信:" + model.getWebChatAmount());
//            }
//
//            if (model.getAliPayAmount() > 0) {
//                sb.append("\n"
//                        + "" + " 支付宝:" + model.getAliPayAmount());
//            }
//
//            if (model.getDiscountAmount() > 0) {
//                sb.append("\n"
//                        + "抹  零:"
//                        + "" + model.getDiscountAmount());
//            }
//
//            sb.append("\n"
//                    + "找  零:"
//                    + "" + model.getChangeAmount());
//
//            sb.append("\n" + "余额:"
//                    + "" + model.getAccountBalance() + "\n");
//        } else {
//            sb.append("数量:"
//                    + "" + model.getCountGoods() + "\n");
//            sb.append("总计:"
//                    + "" + model.getCountAmount() + "\n");
//
//            sb.append("付款:");
//
//            if (model.getCashAmount() > 0) {
//                sb.append("\n " + "现  金:" + model.getCashAmount() + "");
//            }
//
//            if (model.getBankCardAmount() > 0) {
//                sb.append("\n "
//                        + "" + " 银行卡:" + model.getBankCardAmount());
//            }
//
//            if (model.getMemberCardAmount() > 0) {
//                sb.append("\n" + " 会员卡:" + model.getMemberCardAmount());
//            }
//
//            if (model.getPrePayAmount() > 0) {
//                sb.append("\n" + " 押  金:" + model.getPrePayAmount());
//            }
//
//            if (model.getAccountAmount() > 0) {
//                sb.append("\n" + " 账  户:" + model.getAccountAmount());
//            }
//
//            if (model.getWebChatAmount() > 0) {
//                sb.append("\n"
//                        + "" + " 微  信:" + model.getWebChatAmount());
//            }
//
//            if (model.getAliPayAmount() > 0) {
//                sb.append("\n"
//                        + "" + " 支付宝:" + model.getAliPayAmount());
//            }
//
//            if (model.getDiscountAmount() > 0) {
//                sb.append("\n"
//                        + "抹  零:"
//                        + "" + model.getDiscountAmount());
//            }
//
//            sb.append("\n"
//                    + "找  零:"
//                    + "" + model.getChangeAmount());
//
//            sb.append("\n"
//                    + "余额:"
//                    + "" + model.getAccountBalance() + "\n");
//        }
//
//        if (is58mm) {
//            sb.append("-------------------------------\n");
//        } else {
//            sb.append("----------------------------------------------\n");
//        }
//        mPrinter.printText(sb.toString());
//
//        mPrinter.setPrinter(PrinterConstants.Command.ALIGN, PrinterConstants.Command.ALIGN_CENTER);
//        mPrinter.setCharacterMultiple(0, 1);
//        mPrinter.printText(resources.getString(com.android.print.demo.R.string.shop_thanks) + "\n");
//        //mPrinter.printText(resources.getString(com.android.print.demo.R.string.shop_demo) + "\n\n\n");
//
//        mPrinter.setFont(0, 0, 0, 0);
//        mPrinter.setPrinter(PrinterConstants.Command.ALIGN, PrinterConstants.Command.ALIGN_LEFT);
//        mPrinter.setPrinter(PrinterConstants.Command.PRINT_AND_WAKE_PAPER_BY_LINE, 3);
//
//    }
//
//    public static void printTable(Resources resources,
//                                  PrinterInstance mPrinter, boolean is58mm, OrderPrintModel model) {
//        mPrinter.init();
//        String column = resources.getString(R.string.note_title);
//        Table table;
//        if (is58mm) {
//            table = new Table(column, ";", new int[]{12, 6, 7, 7});
//        } else {
//            table = new Table(column, ";", new int[]{18, 10, 10, 12});
//        }
//
//        for (int i = 0; i < model.getGoodInfoList().size(); i++) {
//            OrderPrintModel.GoodInfo info = model.getGoodInfoList().get(i);
//            table.addRow("" + info.getName() + ";" + "" + String.valueOf("" + info.getCount()) + ";" + info.getPrice() + ";" + info.getAmount());
//        }
//
//        mPrinter.printTable(table);
//    }

}