package com.warmsoft.pos.bean;

import java.io.Serializable;
import java.util.List;

/**
 * Created by brooks on 16/11/7.
 */
public class AccountRechargeHistoryModel implements Serializable {
    int code;
    String message;
    List<DataInfo> Data;

    public static class DataInfo implements Serializable {
        String ChargeDatetime;
        String statusText;
        String OperatorName;

        int RowIndex;
        double ReceivedAmount;
        double InAccountAmount;
        double CurrentTotalAmount;
        int status;
        int Operator;

        public String getChargeDatetime() {
            return ChargeDatetime;
        }

        public void setChargeDatetime(String chargeDatetime) {
            ChargeDatetime = chargeDatetime;
        }

        public String getStatusText() {
            return statusText;
        }

        public void setStatusText(String statusText) {
            this.statusText = statusText;
        }

        public String getOperatorName() {
            return OperatorName;
        }

        public void setOperatorName(String operatorName) {
            OperatorName = operatorName;
        }

        public int getRowIndex() {
            return RowIndex;
        }

        public void setRowIndex(int rowIndex) {
            RowIndex = rowIndex;
        }

        public double getReceivedAmount() {
            return ReceivedAmount;
        }

        public void setReceivedAmount(double receivedAmount) {
            ReceivedAmount = receivedAmount;
        }

        public double getInAccountAmount() {
            return InAccountAmount;
        }

        public void setInAccountAmount(double inAccountAmount) {
            InAccountAmount = inAccountAmount;
        }

        public double getCurrentTotalAmount() {
            return CurrentTotalAmount;
        }

        public void setCurrentTotalAmount(double currentTotalAmount) {
            CurrentTotalAmount = currentTotalAmount;
        }

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }

        public int getOperator() {
            return Operator;
        }

        public void setOperator(int operator) {
            Operator = operator;
        }
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataInfo> getData() {
        return Data;
    }

    public void setData(List<DataInfo> data) {
        Data = data;
    }
}
