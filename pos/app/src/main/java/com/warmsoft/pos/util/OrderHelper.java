package com.warmsoft.pos.util;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;

import com.google.gson.Gson;
import com.warmsoft.pos.bean.AmountsModel;
import com.warmsoft.pos.bean.AuthModel;
import com.warmsoft.pos.bean.OrderGoods;
import com.warmsoft.pos.bean.OrderPrintModel;
import com.warmsoft.pos.enumutil.OrderStatusEnum;
import com.warmsoft.pos.enumutil.PayChanel;
import com.warmsoft.pos.litepal.data.CustomerList;
import com.warmsoft.pos.litepal.data.GoodItemInfo;
import com.warmsoft.pos.litepal.data.LitepalHelper;
import com.warmsoft.pos.litepal.data.OrderGoodsData;
import com.warmsoft.pos.litepal.data.OrderInfoData;
import com.warmsoft.pos.litepal.data.OrderNumData;
import com.warmsoft.pos.litepal.data.OrderTaskData;
import com.warmsoft.pos.litepal.data.PayAmountData;
import com.warmsoft.pos.litepal.data.PetData;
import com.warmsoft.pos.print.PrintUtil;
import com.warmsoft.pos.print.SunMiPrintUtils;

import org.litepal.crud.DataSupport;

import java.util.Date;
import java.util.List;

/**
 * 作者: lijinliu on 2016/8/15.
 * 邮箱：jinliu.li@warmsoft.com
 * 描述: 定单操作类
 */
public class OrderHelper {

    private static OrderHelper orderHelper;

    private LitepalHelper mLitepal;
    private Context mContext;
    private OrderInfoData mOrderData;
    private OrderGoods mOrderGoods = new OrderGoods();//定单商品信息(包括所有数据 ，已下单，和未下单数据)

    public static OrderHelper getInstance(Context context) {
        if (orderHelper == null) {
            orderHelper = new OrderHelper(context);
        }
        return orderHelper;
    }

    private OrderHelper(Context context) {
        mContext = context;
        mLitepal = LitepalHelper.getInstance();
    }


    /**
     * 创建新定单
     * 如果用户机构信息，散客信息异常，创建定IDK号异常，则返回空
     *
     * @return
     */
    public OrderInfoData createNewOrder() {
        AuthModel.DataInfo.UserInfo userInfo = OAuthUtils.getInstance().getUserinfo(mContext);
        AuthModel.DataInfo.AnonymousCustomerInfo customerIonfo = OAuthUtils.getInstance().getAnonyCustomInfo(mContext);
        CustomerList customer = mLitepal.queryCustomInfo(customerIonfo.getAnonymousCustomerID() + "");
        if (userInfo == null || Util.isNullStr(userInfo.getName()) || userInfo.getId() <= 0) {
            Util.showToast(mContext, ToastConst.ERROR_USERINFO_NULL);
            return null;
        }

        if (Util.isNullStr(userInfo.getOrgName()) || userInfo.getOrgId() <= 0) {
            Util.showToast(mContext, ToastConst.ERROR_USER_ORGINFO_NULL);
            return null;
        }

        if (customerIonfo == null || Util.isNullStr(customerIonfo.getAnonymousCustomerName()) || customerIonfo.getAnonymousCustomerID() <= 0) {
            Util.showToast(mContext, ToastConst.ERROR_USER_ANONYMOUSINFO_NULL);
            return null;
        }

        if (Util.isNullStr(customerIonfo.getAnonymousPetName()) || customerIonfo.getAnonymousPetID() <= 0) {
            Util.showToast(mContext, ToastConst.ERROR_USER_ANONYMOUS_PETINFO_NULL);
            return null;
        }

        int orderNum = createOrderNo();
        if (orderNum < 0) {
            Util.showToast(mContext, ToastConst.ERROR_CREATE_ORDERNO_EXPECTION);
            return null;
        }
        mOrderData = new OrderInfoData();
        mOrderData.setOrderNo(orderNum);
        mOrderData.setOrderDate(new Date());
        mOrderData.setOpId(userInfo.getId());
        mOrderData.setOpName(userInfo.getName());
        mOrderData.setOrgId(userInfo.getOrgId());
        mOrderData.setOrgName(userInfo.getOrgName());
        mOrderData.setPetId(customerIonfo.getAnonymousPetID());
        mOrderData.setPetName(customerIonfo.getAnonymousPetName());
        mOrderData.setCustomerId(customerIonfo.getAnonymousCustomerID());
        if (customer != null && !Util.isNullStr(customer.getCardNumber())) {
            mOrderData.setMemberCardNo(customer.getCardNumber());
        } else {
            mOrderData.setMemberCardNo("0");
        }
        mOrderData.setCustomerName(customerIonfo.getAnonymousCustomerName());
        mOrderData.setStatus(OrderStatusEnum.getEnumValue(OrderStatusEnum.Bill));//开单状态
        mOrderGoods = new OrderGoods();//清空商品信息

        return mOrderData;
    }

    //清空会员信息和商品信息
    public OrderInfoData clearOrderGoods() {

        AuthModel.DataInfo.UserInfo userInfo = OAuthUtils.getInstance().getUserinfo(mContext);
        AuthModel.DataInfo.AnonymousCustomerInfo customerIonfo = OAuthUtils.getInstance().getAnonyCustomInfo(mContext);
        CustomerList customer = mLitepal.queryCustomInfo(customerIonfo.getAnonymousCustomerID() + "");

        OrderInfoData newOrderData = new OrderInfoData();
        newOrderData.setOrderNo(mOrderData.getOrderNo());
        newOrderData.setOrderDate(new Date());
        newOrderData.setOpId(userInfo.getId());
        newOrderData.setOpName(userInfo.getName());
        newOrderData.setOrgId(userInfo.getOrgId());
        newOrderData.setOrgName(userInfo.getOrgName());
        newOrderData.setPetId(customerIonfo.getAnonymousPetID());
        newOrderData.setPetName(customerIonfo.getAnonymousPetName());
        newOrderData.setCustomerId(customerIonfo.getAnonymousCustomerID());
        if (customer != null && !Util.isNullStr(customer.getCardNumber())) {
            newOrderData.setMemberCardNo(customer.getCardNumber());
        } else {
            newOrderData.setMemberCardNo("0");
        }
        newOrderData.setCustomerName(customerIonfo.getAnonymousCustomerName());
        newOrderData.setStatus(OrderStatusEnum.getEnumValue(OrderStatusEnum.Bill));//开单状态

        mOrderData = newOrderData;
        mOrderGoods = new OrderGoods();//清空商品信息
        return mOrderData;
    }

    /**
     * 待付款点击，设置为当前操作员
     *
     * @param orderData
     */
    public void setOrder(OrderInfoData orderData) {
        AuthModel.DataInfo.UserInfo userInfo = OAuthUtils.getInstance().getUserinfo(mContext);
        mOrderData = orderData;
        mOrderData.setOpId(userInfo.getId());
        mOrderData.setOpName(userInfo.getName());
        mOrderData.setOrgId(userInfo.getOrgId());
        mOrderData.setOrgName(userInfo.getOrgName());
        mOrderGoods = new OrderGoods();//清空商品信息
        mOrderGoods.setAllGoodList(mLitepal.queryGoodsByOrderNO(orderData.getOrderNo()));
    }

    /**
     * 添加会员信息
     * 只有在开单状态下才可以更改定单会员信息
     *
     * @param custom
     * @param pet
     * @return
     */
    public OrderInfoData addMemberForOrder(CustomerList custom, PetData pet) {
        if (mOrderData != null && OrderStatusEnum.getOrderEnum(mOrderData.getStatus()) == OrderStatusEnum.Bill) {
            mOrderData.setPetId(pet.getPetId());
            mOrderData.setPetName(pet.getPetName());
            mOrderData.setCustomerId(Integer.parseInt(custom.getCustomerId()));
            mOrderData.setCustomerName(custom.getName());
            mOrderData.setCellPhone(custom.getCellPhone());
            mOrderData.setMemberCardNo(custom.getCardNumber());
            mOrderGoods.changeMemberInfo(custom, pet);
            SelectMemberUtil.getInstance().setCustomer(custom);//更新会员列表显示当前定单会员
        }
        return mOrderData;
    }

    //添加商品
    public void addGoods(GoodItemInfo goodInfo) {
        if (mOrderGoods == null) {
            mOrderGoods = new OrderGoods();
        }
        mOrderGoods.addGoods(goodInfo, mOrderData);
    }

    //商品数量修改，负减正加
    public void setGoodSize(OrderGoodsData goodInfo, float newSize) {
        if (mOrderGoods != null) {
            mOrderGoods.setGoodSize(mOrderData, goodInfo, newSize);
            if (mOrderGoods.allIsSure()) {//都已确认商品
                if (getOrderStaus() == OrderStatusEnum.ReOpen) {//追单删除
                    mOrderData.setStatus(OrderStatusEnum.getEnumValue(OrderStatusEnum.HandIn));
                }
            } else {
                if (getOrderStaus() == OrderStatusEnum.HandIn) {//追单
                    reOpenOrder();
                }
            }
        }
    }

    /**
     * 下单 返回商品列表
     * 订单中有未确认商口时才能下单
     * 下单会往OrderTaskData中写入一条任务数据,并绑定当前未确认数据
     *
     * @return List<OrderGoodsData>
     */
    public List<OrderGoodsData> postOrder() {
        if (mOrderGoods.getUnSureize() > 0) {
            long batchNo = new Date().getTime();
            if (mOrderGoods.handInOrder(batchNo)) {//商品保存本地成功，写入任务表
                LogUtil.error("postOrder " + "商品保存本地成功，写入任务表");
                mLitepal.setOrderStatus(mOrderData, OrderStatusEnum.HandIn, OAuthUtils.getInstance().getUserinfo(mContext));
                mOrderData.save();//保存定单基础信息

                OrderTaskData orderTask = new OrderTaskData();
                orderTask.setOrderNo(mOrderData.getOrderNo());
                orderTask.setType(TaskConst.TASKTYPE_BILL);//下单
                orderTask.setBatchNo(batchNo);
                orderTask.setStatus(0);//0新任务，9任务在执行，1任务成功，2任务失败
                orderTask.setTaskDesc("HandIn Order");
                orderTask.save();
            }
        } else {
            Util.showToast(mContext, ToastConst.ERROR_NO_UNSURE_GOODS);
        }
        return mOrderGoods.getAllGoodList();
    }

    //找零
    public void setChangeAmoutn(float changeAmoutn) {

        mOrderData.setChangeAmount(changeAmoutn);
    }

    //记账
    public void keepingAccounts() {
        clearAllPays();//清空所有支付
        saveCollectAmount(getNeedAmount(), PayChanel.PAY_BY_KEEPACCCOUNT);//记账金额
        mLitepal.setOrderStatus(mOrderData, OrderStatusEnum.KeepingAccounts, OAuthUtils.getInstance().getUserinfo(mContext));
        OrderTaskData orderTask = new OrderTaskData();
        orderTask.setOrderNo(mOrderData.getOrderNo());
        orderTask.setType(TaskConst.TASKTYPE_SETTLE);//记账走结算接口
        orderTask.setStatus(0);//0新任务，9任务在执行，1任务成功，2任务失败
        orderTask.setTaskDesc("Keeping Accounts");
        orderTask.save();
    }

    /**
     * 收款完成
     */
    public void orderPayed() {

        mOrderGoods.orderPayed(mOrderData.getOrderNo());
    }

    //清空支付
    public void clearAllPays() {
        mOrderData.setCashAmount(0);
        mOrderData.setBankCardAAmount(0);
        mOrderData.setBankCardBAmount(0);
        mOrderData.setWebChatAmount(0);
        mOrderData.setAliPayAmount(0);
        mOrderData.setMemberCardAmount(0);
        mOrderData.setAccountAmount(0);
        mOrderData.setMemberCardAmount(0);
        mOrderData.setPrePayAmount(0);
        mOrderData.setKeepingAccountAmout(0);
        mOrderData.setDiscountAmount(0);
        mLitepal.updateOrderAmount(mOrderData);
        mLitepal.delAllPays(mOrderData.getOrderNo());
    }

    //删除某笔支付
    public void clearOnePays(PayAmountData data) {
        switch (data.getPayChanel()) {
            case PayChanel.PAY_BY_CASH://现金
                mOrderData.setCashAmount(mOrderData.getCashAmount() - data.getPayAmount());
                break;
            case PayChanel.PAY_BY_BANKCARD://银行卡1
                mOrderData.setBankCardAAmount(mOrderData.getBankCardAAmount() - data.getPayAmount());
                break;
            //case PayChanel.PAY_BY_STOREDCARD://银行卡2
            //    mOrderData.setBankCardBAmount(mOrderData.getBankCardBAmount() - data.getPayAmount());
            //    break;
            case PayChanel.PAY_BY_WECHAT://微信
                mOrderData.setWebChatAmount(mOrderData.getWebChatAmount() - data.getPayAmount());
                break;
            case PayChanel.PAY_BY_ALIPAY://支付宝
                mOrderData.setAliPayAmount(mOrderData.getAliPayAmount() - data.getPayAmount());
                break;
            case PayChanel.PAY_BY_ACCOUNT://账户余额
                mOrderData.setAccountAmount(mOrderData.getAccountAmount() - data.getPayAmount());
                break;
            case PayChanel.PAY_BY_VIPCARD://会员卡
                mOrderData.setMemberCardAmount(mOrderData.getMemberCardAmount() - data.getPayAmount());
                break;
            case PayChanel.PAY_BY_DEPOSIT://押金
                mOrderData.setPrePayAmount(mOrderData.getPrePayAmount() - data.getPayAmount());
                break;
            case PayChanel.PAY_BY_KEEPACCCOUNT://记账
                mOrderData.setKeepingAccountAmout(mOrderData.getKeepingAccountAmout() - data.getPayAmount());
                break;
            case PayChanel.PAY_BY_DISCOUNT://"抹零";
                mOrderData.setDiscountAmount(mOrderData.getDiscountAmount() - data.getPayAmount());
                break;
        }
        mLitepal.updateOrderAmount(mOrderData);
        data.delete();
    }

    /**
     * 结算
     * 批次号为0.结算任务根据 orderNo关联数据结算
     */
    public boolean settleOrder(Activity mActivity) {
        try {
            OrderTaskData orderTask = new OrderTaskData();
            orderTask.setOrderNo(mOrderData.getOrderNo());
            if (getOrderStaus()==OrderStatusEnum.KeepingAccounts) {
                orderTask.setType(TaskConst.TASKTYPE_KEEPING_ACCOUNTS);//记账
                orderTask.setTaskDesc("Keeping Accounts Settlement ");
            }else {
                orderTask.setType(TaskConst.TASKTYPE_SETTLE);//结算
                orderTask.setTaskDesc("Settlement Order");
            }
            orderTask.setBatchNo(0);//批次号为0.结算任务根据 orderNo关联数据结算
            orderTask.setStatus(0);//0新任务，9任务在执行，1任务成功，2任务失败
            orderTask.save();
            mLitepal.setOrderStatus(mOrderData, OrderStatusEnum.Settle, OAuthUtils.getInstance().getUserinfo(mContext));
        } catch (Exception e) {
            Trace.e(e);
            return false;
        } finally {
            //打印小票
//            if (PrintUtil.getInstance().getConnectStatus()) {
//                PrintUtil.getInstance().printOrder(mActivity);
//            } else {
//                PrintUtil.getInstance().initPrinter(mActivity);
//            }
            SunMiPrintUtils.getInstance().initPrinter();
            SunMiPrintUtils.getInstance().printOrder(mActivity);

            return true;
        }
    }

    /**
     * 重新打开已下单，未结算定单
     */
    public void reOpenOrder() {
        mLitepal.setOrderStatus(mOrderData, OrderStatusEnum.ReOpen, OAuthUtils.getInstance().getUserinfo(mContext));
    }

    /**
     * 退单
     * 退单任务根据 orderNo关联数据结算
     */
    public void refundOrder() {
        OrderTaskData orderTask = new OrderTaskData();
        if (OrderStatusEnum.getOrderEnum(mOrderData.getStatus()) == OrderStatusEnum.HandIn) {
            mLitepal.setOrderStatus(mOrderData, OrderStatusEnum.BackHandIn, OAuthUtils.getInstance().getUserinfo(mContext));
            orderTask.setType(TaskConst.TASKTYPE_BACKBILL);//下单退单
        } else if (OrderStatusEnum.getOrderEnum(mOrderData.getStatus()) == OrderStatusEnum.Settle) {
            mLitepal.setOrderStatus(mOrderData, OrderStatusEnum.BackSettle, OAuthUtils.getInstance().getUserinfo(mContext));
            orderTask.setType(TaskConst.TASKTYPE_BACKSETTLE);//结算退单
        } else if (OrderStatusEnum.getOrderEnum(mOrderData.getStatus()) == OrderStatusEnum.ReOpen) {
            mLitepal.setOrderStatus(mOrderData, OrderStatusEnum.BackHandIn, OAuthUtils.getInstance().getUserinfo(mContext));
            orderTask.setType(TaskConst.TASKTYPE_BACKBILL);//结算退单
        }
        orderTask.setOrderNo(mOrderData.getOrderNo());
        orderTask.setBatchNo(0);//批次号为0.退单任务根据 orderNo关联数据结算
        orderTask.setStatus(0);//0新任务，9任务在执行，1任务成功，2任务失败
        orderTask.setTaskDesc("Refund Order");
        orderTask.save();
    }

    /**
     * @return
     */
    public OrderInfoData getOrderData() {

        return mOrderData;
    }

    /**
     * 定单项
     *
     * @return
     */
    public int getOrderSize() {
        if (mOrderGoods.getAllGoodList() != null) {
            return mOrderGoods.getAllGoodList().size();
        } else {
            return 0;
        }
    }

    //未确认商品数据
    public int getUnSureGoodSize() {
        if (mOrderGoods.getUnsureGoodList() != null) {
            return mOrderGoods.getUnsureGoodList().size();
        } else {
            return 0;
        }
    }

    //应收款
    public float getNeedAmount() {
        return mOrderGoods.getNeedAmount();
    }

    //已收款
    public float getPayedAmout() {
        List<PayAmountData> datas = mLitepal.queryPaysData(mOrderData.getOrderNo() + "");
        float amout = 0;
        if (datas != null && datas.size() > 0) {
            for (PayAmountData payData : datas) {
                if (payData.getPayChanel() != PayChanel.PAY_BY_KEEPACCCOUNT) {//记账金额不算入已收款
                    amout += payData.getPayAmount();
                }
            }
        }
        return amout;
    }

    //定单已选择商品
    public List<OrderGoodsData> getOrderGoods() {
        return mOrderGoods.getAllGoodList();
    }

    // 还需要收款
    public float getNeedColletAmount() {
        if (mOrderData == null) {
            return 0;
        }
        float amount = getNeedAmount() - getPayedAmout();
        return amount;
    }


    /**
     * 还需要收款(点击银行卡或现金支付时)
     * 只有返回大于0数字
     *
     * @return
     */
    public float getNeedColletAmountForText() {
        OrderStatusEnum orderStatus = getOrderStaus();
        if (orderStatus == OrderStatusEnum.HandIn||orderStatus == OrderStatusEnum.KeepingAccounts) {
            float colletAmount = getNeedColletAmount();
            if (colletAmount > 0) {
                return colletAmount;
            } else {
                return 0.00f;
            }
        } else {
            return 0.00f;
        }
    }

    //收款
    public void saveCollectAmount(float amoutn, int payChanel) {
        savePayData(amoutn, payChanel);//流水记录保存
        switch (payChanel) {
            case PayChanel.PAY_BY_CASH://现金
                mOrderData.setCashAmount(mOrderData.getCashAmount() + amoutn);
                break;
            case PayChanel.PAY_BY_BANKCARD://银行卡1
                mOrderData.setBankCardAAmount(mOrderData.getBankCardAAmount() + amoutn);
                break;
            //case PayChanel.PAY_BY_STOREDCARD://银行卡2
            //    mOrderData.setStoredCardAmount(mOrderData.getStoredCardAmount() + amoutn);
            //    break;
            case PayChanel.PAY_BY_WECHAT://"微信";
                mOrderData.setWebChatAmount(mOrderData.getWebChatAmount() + amoutn);
                break;
            case PayChanel.PAY_BY_ALIPAY:// "支付宝";
                mOrderData.setAliPayAmount(mOrderData.getAliPayAmount() + amoutn);
                break;
            case PayChanel.PAY_BY_ACCOUNT://账户余额
                mOrderData.setAccountAmount(mOrderData.getAccountAmount() + amoutn);
                break;
            case PayChanel.PAY_BY_VIPCARD://会员卡
                mOrderData.setMemberCardAmount(mOrderData.getMemberCardAmount() + amoutn);
                break;
            case PayChanel.PAY_BY_DEPOSIT://押金
                mOrderData.setPrePayAmount(mOrderData.getPrePayAmount() + amoutn);
                break;
            case PayChanel.PAY_BY_KEEPACCCOUNT://记账
                mOrderData.setKeepingAccountAmout(mOrderData.getKeepingAccountAmout() + amoutn);
                break;
            case PayChanel.PAY_BY_DISCOUNT://"抹零";
                mOrderData.setDiscountAmount(mOrderData.getDiscountAmount() + amoutn);
                break;
        }
        mLitepal.updateOrderAmount(mOrderData);
    }


    //订单打印信息
    public OrderPrintModel getPrintInfo() {
        OrderPrintModel printInfo = new OrderPrintModel();
        printInfo.setOrgName(mOrderData.getOrgName());
        printInfo.setOrderNo(mOrderData.getOrderNo());
        printInfo.setOrderDate(mOrderData.getOrderDate());
        printInfo.setPrintDate(new Date());
        printInfo.setOpName(mOrderData.getOpName());
        printInfo.setCountGoods(mOrderData.getCountGoods());
        printInfo.setCountAmount(mOrderData.getCountAmount());
        printInfo.setCashAmount(mOrderData.getCashAmount());
        printInfo.setBankCardAmount(mOrderData.getBankCardAAmount());
        printInfo.setWebChatAmount(mOrderData.getWebChatAmount());
        printInfo.setAliPayAmount(mOrderData.getAliPayAmount());
        printInfo.setAccountAmount(mOrderData.getAccountAmount());
        printInfo.setPrePayAmount(mOrderData.getPrePayAmount());
        printInfo.setMemberCardAmount(mOrderData.getMemberCardAmount());
        printInfo.setChangeAmount(mOrderData.getChangeAmount());
        printInfo.setDiscountAmount(mOrderData.getDiscountAmount());
        printInfo.setAdvWords(Constant.ADV_WORDS);
        AmountsModel.DataInfo dataInfo = SelectMemberUtil.getInstance().getDataInfo();

        if (dataInfo.getCustomerid() == mOrderData.getCustomerId()) {//余额
            printInfo.setAccountBalance((float) dataInfo.getAccount() - mOrderData.getAccountAmount());
            printInfo.setPrePayBalance((float) dataInfo.getPrePay() - mOrderData.getPrePayAmount());
            printInfo.setMemberCardBalance((float) dataInfo.getMemberCard() - mOrderData.getMemberCardAmount());
        } else {
            printInfo.setAccountBalance(0);
            printInfo.setPrePayBalance(0);
            printInfo.setMemberCardBalance(0);
        }

        for (OrderGoodsData goodData : mOrderGoods.getAllGoodList()) {
            OrderPrintModel.GoodInfo goodInfo = new OrderPrintModel.GoodInfo();
            goodInfo.setName(goodData.getFeeItemName());
            goodInfo.setCount(goodData.getCount());
            goodInfo.setPrice(goodData.getAmount());
            goodInfo.setAmount(goodData.getAmount() * goodData.getCount());
            printInfo.getGoodInfoList().add(goodInfo);
        }
        Trace.e(new Gson().toJson(printInfo));
        return printInfo;
    }

    /**
     * 创建订单编号
     * 自增长 Sequence
     *
     * @return
     */
    public int createOrderNo() {
        int orderNo = 1;
        try {
            OrderNumData data = DataSupport.findLast(OrderNumData.class);
            if (data != null) {
                orderNo = data.getOrderNo() + 1;
                ContentValues values = new ContentValues();
                values.put("orderNo", orderNo);
                DataSupport.update(OrderNumData.class, values, data.getId());
            } else {
                data = new OrderNumData();
                data.setOrderDate(new Date());
                data.setOrderNo(orderNo);
                data.save();
            }
        } catch (Exception e) {
            orderNo = -1;
        }
        return orderNo;
    }

    /**
     * 当前定单状态
     *
     * @return
     */
    public OrderStatusEnum getOrderStaus() {
        if (mOrderData == null) {
            return OrderStatusEnum.Null;
        } else {
            return OrderStatusEnum.getOrderEnum(mOrderData.getStatus());
        }
    }

    //支付记录保存
    private void savePayData(float amoutn, int payChanel) {
        int orderNo = mOrderData.getOrderNo();
        PayAmountData payData = new PayAmountData();
        payData.setOrderAmount(orderHelper.getOrderData().getCountAmount());
        payData.setChange(orderHelper.getOrderData().getChangeAmount());
        payData.setOrderNo(orderNo + "");
        payData.setPayAmount(amoutn);
        payData.setPayChanel(payChanel);
        payData.setPayChanelName(PayChanel.getPayChanelName(payChanel));
        OrderInfoData orderData = orderHelper.getOrderData();
        payData.setPayDate(new Date());
        payData.setCustomerId(orderData.getCustomerId());
        payData.setCustomerName(orderData.getCustomerName());
        payData.setPetName(orderData.getPetName());
        payData.setPetId(orderData.getPetId());
        payData.save();
    }

    // 商品备注记录
    public void setGoodDesc(int goodId, String goodDesc) {
        mOrderGoods.setGoodDesc(goodId, goodDesc);
    }
}
