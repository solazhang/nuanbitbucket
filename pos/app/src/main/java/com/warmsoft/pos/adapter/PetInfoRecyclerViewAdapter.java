package com.warmsoft.pos.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.warmsoft.pos.R;
import com.warmsoft.pos.litepal.data.PetData;
import com.warmsoft.pos.util.TimeUtils;

import java.util.ArrayList;
import java.util.List;


public class PetInfoRecyclerViewAdapter extends RecyclerView.Adapter<PetInfoRecyclerViewAdapter.RecyclerViewHolder> {

    private Context mContext;
    List<PetData> petDatas = new ArrayList<>();

    public PetInfoRecyclerViewAdapter(Context context, List<PetData> pets) {
        this.mContext = context;
        this.petDatas = pets;
    }

    public void setData(List<PetData> pets) {
        this.petDatas = pets;
        notifyDataSetChanged();
    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerViewHolder holder = new RecyclerViewHolder(LayoutInflater.from(
                mContext).inflate(R.layout.item_for_show_pet_info, parent,
                false));
        return holder;
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, final int position) {
        if (petDatas.size() > 0) {
            holder.etName.setText(petDatas.get(position).getPetName());
            mContext.getResources().getString(R.string.warm_pos_pet_info_category);

            String result = String.format("[%s, %s]", petDatas.get(position).getKindOFText(), petDatas.get(position).getVarietyText());
            holder.etType.setText(result);

            String age = TimeUtils.getInstance().getTimeDifference(petDatas.get(position).getBirthdate());
            holder.etAge.setText(age);
        }
    }

    @Override
    public int getItemCount() {
        return petDatas.size();
    }

    class RecyclerViewHolder extends RecyclerView.ViewHolder {
        public TextView etName;
        public TextView etType;
        public TextView etAge;

        public RecyclerViewHolder(View itemView) {
            super(itemView);
            etName = (TextView) itemView.findViewById(R.id.id_for_pet_name);
            etType = (TextView) itemView.findViewById(R.id.id_for_pet_type);
            etAge = (TextView) itemView.findViewById(R.id.id_for_pet_age);
        }
    }
}
