package com.warmsoft.pos.layout.view;

import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.TextView;

import com.warmsoft.pos.R;
import com.warmsoft.pos.view.CustomViewPager;

/**
 * 作者: lijinliu on 2016/8/5.
 * 邮箱：jinliu.li@warmsoft.com
 * 描述: 对应 activity_login.xml
 */
public class LayoutActivityMain implements View.OnClickListener {

    private LayoutMainListener mListener;

    public CustomViewPager mPager;
    public TextView tabUser;
    public TextView tabPos;
    public TextView tabVip;
    public TextView tabUpdate;

    protected TextView tabSelected;

    public LayoutActivityMain(Activity activity, LayoutMainListener listener) {
        int drawableWh = (int)activity.getResources().getDimension(R.dimen.main_tabhost_drawable_wh);//分辨率适应
        mListener = listener;
        mPager = (CustomViewPager) activity.findViewById(R.id.pager);
        Drawable drawaUser = activity.getResources().getDrawable(R.mipmap.warmpos_user);
        drawaUser.setBounds(0, 0, drawableWh, drawableWh);
        Drawable drawaCollect = activity.getResources().getDrawable(R.mipmap.warmpos_collect);
        drawaCollect.setBounds(0, 0, drawableWh, drawableWh);
        Drawable drawaMember = activity.getResources().getDrawable(R.mipmap.warmpos_member);
        drawaMember.setBounds(0, 0, drawableWh, drawableWh);
        Drawable drawaUpdate = activity.getResources().getDrawable(R.mipmap.warmpos_update);
        drawaUpdate.setBounds(0, 0, drawableWh, drawableWh);
        tabUser = (TextView) activity.findViewById(R.id.tv_main_tab_userinfo);
        tabUser.setCompoundDrawables(null, drawaUser, null, null);
        tabPos = (TextView) activity.findViewById(R.id.tv_main_tab_pos);
        tabPos.setCompoundDrawables(null, drawaCollect, null, null);
        tabVip = (TextView) activity.findViewById(R.id.tv_main_tab_vip);
        tabVip.setCompoundDrawables(null, drawaMember, null, null);
        tabUpdate = (TextView) activity.findViewById(R.id.tv_main_tab_update);
        tabUpdate.setCompoundDrawables(null, drawaUpdate, null, null);
        tabSelected = tabPos;
        tabPos.setSelected(true);

        initEvents();
    }

    private void initEvents() {
        tabUser.setOnClickListener(this);
        tabPos.setOnClickListener(this);
        tabVip.setOnClickListener(this);
        tabUpdate.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tv_main_tab_userinfo:
                if (mListener != null) {
                    mListener.tabClick(R.id.tv_main_tab_userinfo);
                }
                break;
            case R.id.tv_main_tab_pos:
                tabClick(tabPos);
                if (mListener != null) {
                    mListener.tabClick(R.id.tv_main_tab_pos);
                }
                break;
            case R.id.tv_main_tab_vip:
                tabClick(tabVip);
                if (mListener != null) {
                    mListener.tabClick(R.id.tv_main_tab_vip);
                }
                break;
            case R.id.tv_main_tab_update:
                tabClick(tabUpdate);
                if (mListener != null) {
                    mListener.tabClick(R.id.tv_main_tab_update);
                }
                break;
        }
    }

    private void tabClick(TextView clickView) {
        if (tabSelected.equals(clickView)) {
            return;
        } else {
            tabUser.setSelected(false);
            tabPos.setSelected(false);
            tabVip.setSelected(false);
            tabUpdate.setSelected(false);
            clickView.setSelected(true);
            tabSelected = clickView;
        }
    }

    public interface LayoutMainListener {
        public void tabClick(int id);
    }
}
