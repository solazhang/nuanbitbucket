package com.warmsoft.pos.litepal.data;

import android.content.ContentValues;
import android.content.Context;

import com.warmsoft.pos.bean.AuthModel;
import com.warmsoft.pos.enumutil.OrderStatusEnum;
import com.warmsoft.pos.util.LogUtil;
import com.warmsoft.pos.util.TaskConst;
import com.warmsoft.pos.util.Trace;

import org.litepal.crud.DataSupport;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 作者: lijinliu on 2016/8/25.
 * 邮箱：jinliu.li@warmsoft.com
 * 描述: 数据库类
 */
public class LitepalHelper {

    private static LitepalHelper helper;
    private Context mContext;
    private OrderInfoData orderData;
    private List<SettlementData> goodList = new ArrayList<SettlementData>();

    public static LitepalHelper getInstance() {
        if (helper == null) {
            helper = new LitepalHelper();
        }
        return helper;
    }

    private LitepalHelper() {
    }

    //获取定单
    public OrderInfoData queryOrderInfoByOrderNO(int orderNo) {
        List<OrderInfoData> orderList = DataSupport.where(" orderNo=? ", orderNo + "").find(OrderInfoData.class);
        if (orderList != null && orderList.size() > 0) {
            return orderList.get(0);
        }
        return null;
    }

    //更新定单状态
    public boolean updateOrderStatus(String status, int orderId, AuthModel.DataInfo.UserInfo userInfo) {
        OrderInfoData orderData = new OrderInfoData();
        orderData.setOpId(userInfo.getId());
        orderData.setOpName(userInfo.getName());
        orderData.setOrgId(userInfo.getOrgId());
        orderData.setOrgName(userInfo.getOrgName());
        orderData.setStatus(status);
        orderData.update(orderId);
        return false;
    }

    //更新定单金额
    public boolean updateOrderAmount(OrderInfoData orderData) {
        if (orderData != null && orderData.getId() > 0) {
            OrderInfoData updateData = new OrderInfoData();
            updateData.setCountAmount(orderData.getCountAmount());//总金额
            updateData.setCashAmount(orderData.getCashAmount());//现金收取金额
            updateData.setBankCardAAmount(orderData.getBankCardAAmount());//银行卡1
            updateData.setBankCardBAmount(orderData.getBankCardBAmount());//银行卡2
            updateData.setWebChatAmount(orderData.getWebChatAmount());//微信
            updateData.setAliPayAmount(orderData.getAliPayAmount());//支付宝
            updateData.setAccountAmount(orderData.getAccountAmount());//账户余额
            updateData.setMemberCardAmount(orderData.getMemberCardAmount());//会员卡
            updateData.setPrePayAmount(orderData.getPrePayAmount());//押金
            updateData.setKeepingAccountAmout(orderData.getKeepingAccountAmout());//挂账
            updateData.setChangeAmount(orderData.getChangeAmount());//找零
            updateData.setDiscountAmount(orderData.getDiscountAmount());//抹零金额
            updateData.update(orderData.getId());
        }
        return false;
    }

    /**
     * 定单更新并保存定单状态
     *
     * @param statusEnum
     */
    public void setOrderStatus(OrderInfoData orderData, OrderStatusEnum statusEnum, AuthModel.DataInfo.UserInfo userInfo) {
        if (orderData.getId() > 0) {//直接更新
            orderData.setStatus(OrderStatusEnum.getEnumValue(statusEnum));
            updateOrderStatus(OrderStatusEnum.getEnumValue(statusEnum), orderData.getId(), userInfo);
        } else {//查库更新
            OrderInfoData dbmOrderData = LitepalHelper.getInstance().queryOrderInfoByOrderNO(orderData.getOrderNo());
            if (dbmOrderData != null) {
                updateOrderStatus(OrderStatusEnum.getEnumValue(statusEnum), orderData.getId(), userInfo);
                orderData.setStatus(OrderStatusEnum.getEnumValue(statusEnum));
                orderData.setId(dbmOrderData.getId());
            } else {
                orderData.setStatus(OrderStatusEnum.getEnumValue(statusEnum));
                orderData.save();//保存
            }
        }
    }


    //根据定单号查相关商品数据
    public List<SettlementData> queryGoodsByOrderNO(int orderNo) {
        List<SettlementData> goodlist = DataSupport.where(" orderNo=? ", orderNo + "").find(SettlementData.class);
        if (goodlist != null && goodlist.size() > 0) {
            return goodlist;
        }
        return null;
    }

    //根据支付ID号确定是否已经存在数据库中
    public boolean checkPayment(int paymentId) {
        List<SettlementData> paylist = DataSupport.where(" PaymentId=? and PaymentId>0 ", paymentId + "").find(SettlementData.class);//是否已存在数据库
        if (paylist != null && paylist.size() > 0) {
            return true;
        }
        return false;
    }

    //根据宠物ID获取代付款订单信息
    public OrderInfoData getOrderDataByPetId(int petId) {
        List<OrderInfoData> orderList = DataSupport.where(" PetId=? and status='1' ", petId + "").find(OrderInfoData.class);
        if (orderList != null && orderList.size() > 0) {
            return orderList.get(0);
        }
        return null;
    }

    //根据宠物ID获取付款明细数据
    public List<SettlementData> queryWaitPaysByPetId(int petId) {
        List<SettlementData> goodlist = DataSupport.where(" orderNo=0 and PetId=? ", petId + "").find(SettlementData.class);
        if (goodlist != null && goodlist.size() > 0) {
            return goodlist;
        }
        return null;
    }

    //根据客户ID获取客户待付款列表
    public List<OrderInfoData> getOrderListByCustomId(int customId, int nowOrderNo) {
        List<OrderInfoData> orderList = DataSupport.where(" CustomerId=? and status in ('1','5','6') and orderNo<>" + nowOrderNo + " ", customId + "").find(OrderInfoData.class);
        if (orderList != null && orderList.size() > 0) {
            return orderList;
        }
        return null;
    }

    //根据定单号查相关商品待付款数据（下单成功的。paymentid>0的数据）
    public List<SettlementData> queryGoodsForSettle(int orderNo) {
        List<SettlementData> goodlist = DataSupport.where(" orderNo=? and PaymentId > 0 ", orderNo + "").find(SettlementData.class);
        if (goodlist != null && goodlist.size() > 0) {
            return goodlist;
        }
        return null;
    }

    //更新定任务表数据
    public boolean updateTaskStatus(int status, int taskid, int opTimes, Date opDate) {
        OrderTaskData task = new OrderTaskData();
        task.setStatus(status);
        if (opTimes > 0) {
            task.setOpTimes(opTimes);
        }
        if (opDate != null) {
            task.setTaskTime(opDate);
        }
        task.update(taskid);
        return false;
    }

    //需要提交营业数据
    public synchronized OrderTaskData queryNeedUploadDatas() {
        List<MemberTaskData> memberTaskDataList = DataSupport.where(" opTimes<? and status in ('0','2','9') ", "3").limit(1).find(MemberTaskData.class);
        if (memberTaskDataList != null && memberTaskDataList.size() > 0) {//先上传会员数据
            return null;
        }

        List<OrderTaskData> orderList = DataSupport.where(" status = ? ", "9").find(OrderTaskData.class);//正在执行
        if (orderList != null && orderList.size() > 0) {
            for (OrderTaskData task : orderList) {//判断任务执行超过3分钟,则算超时
                long taskTimes = new Date().getTime() - task.getTaskTime().getTime();
                if (taskTimes > 1000 * 180) {
                    updateTaskStatus(TaskConst.TASKSTATUS_POSTFAIL, task.getId(), task.getOpTimes(), new Date());//任务执行超时
                }
            }
            return null;//有任务在执行反回空
        } else {
            orderList = DataSupport.where(" opTimes<? and status in ('0','2') ", "3").order(" id ").limit(1).find(OrderTaskData.class);//失败的重试三次
            if (orderList != null && orderList.size() > 0) {
                for (OrderTaskData task : orderList) {
                    updateTaskStatus(TaskConst.TASKSTATUS_POSTING, task.getId(), task.getOpTimes() + 1, new Date());//操作中,防止重复取数
                }
                return orderList.get(0);
            }
        }
        return null;
    }

    //更新上传会员信息
    public boolean updateMemberStatus(int status, int taskid, int opTimes, Date opDate) {
        MemberTaskData task = new MemberTaskData();
        task.setStatus(status);
        if (opTimes > 0) {
            task.setOpTimes(opTimes);
        }
        if (opDate != null) {
            task.setTaskTime(opDate);
        }
        task.update(taskid);
        return false;
    }

    //查询需要上传的数据
    public synchronized MemberTaskData queryNeedUploadMemberDatas() {
        List<MemberTaskData> memberTaskDataList = DataSupport.where(" status = ? ", "9").find(MemberTaskData.class);//正在执行
        if (memberTaskDataList != null && memberTaskDataList.size() > 0) {
            for (MemberTaskData task : memberTaskDataList) {//判断任务执行超过3分钟,则算超时
                long taskTimes = new Date().getTime() - task.getTaskTime().getTime();
                if (taskTimes > 1000 * 180) {
                    updateMemberStatus(TaskConst.TASKSTATUS_POSTFAIL, task.getId(), task.getOpTimes(), new Date());//任务执行超时
                }
            }
            return null;//有任务在执行反回空
        } else {
            memberTaskDataList = DataSupport.where(" opTimes<? and status in ('0','2') ", "3").order(" id ").limit(1).find(MemberTaskData.class);//失败的重试三次
            if (memberTaskDataList != null && memberTaskDataList.size() > 0) {
                for (MemberTaskData task : memberTaskDataList) {
                    updateMemberStatus(TaskConst.TASKSTATUS_POSTING, task.getId(), task.getOpTimes() + 1, new Date());//操作中,防止重复取数
                }
                return memberTaskDataList.get(0);
            }
        }
        return null;
    }

    //根据批号,查询会员信息
    public List<CustomerList> queryMemberInfo(long batchNo) {
        List<CustomerList> customerLists = DataSupport.where(" CustomerId=? ", batchNo + "").find(CustomerList.class);
        if (customerLists != null && customerLists.size() > 0) {
            return customerLists;
        }
        return null;
    }

    //根据批号,查询宠物相关信息
    public List<PetData> queryPetDatas(long batchNo) {
        List<PetData> petDatas = DataSupport.where(" CustomerID=? ", batchNo + "").find(PetData.class);
        if (petDatas != null && petDatas.size() > 0) {
            return petDatas;
        }
        return null;
    }

    //根据批号,批量删除
    public void delPetInfo(long batchNo) {
        DataSupport.deleteAll(PetData.class, " CustomerID=? " , batchNo + "");
    }

    //根据批次号获取下单数据
    public List<SettlementData> queryGoodsByBatchNO(long batchNo) {
        List<SettlementData> goodlist = DataSupport.where(" batchNo=? ", batchNo + "").find(SettlementData.class);
        if (goodlist != null && goodlist.size() > 0) {
            return goodlist;
        }
        return null;
    }


    //下单数据更新本地库
    public boolean updateBillReponse(long batchNo, SettlementData response) {
        List<SettlementData> goodlist = DataSupport.where(" batchNo=? and FeeItemId=? ", batchNo + "", response.getFeeItemId() + "").find(SettlementData.class);
        if (goodlist != null && goodlist.size() > 0) {
            SettlementData update = new SettlementData();
            update.setPaymentId(response.getID());
            update.setChargeDatetime(response.getChargeDatetime());//结算时间
            update.update(goodlist.get(0).getID());//更新
            return true;
        }
        return false;
    }

    //更新订单所有实付金额
    public boolean updateActlyPayed(int orderNo) {
        List<SettlementData> goodlist = queryGoodsByOrderNO(orderNo);
        if (goodlist != null && goodlist.size() > 0) {
            for (SettlementData good : goodlist) {
                good.setActlyPayed(good.getTotalAmount());
                good.update(good.getID());
            }
            return true;
        }
        return false;
    }

    //根据定单号查相关商品数据
    public SettleReslutData querySettleReslu(int orderNo) {
        List<SettleReslutData> reslutDatas = DataSupport.where(" orderNo=? ", orderNo + "").find(SettleReslutData.class);
        if (reslutDatas != null && reslutDatas.size() > 0) {
            return reslutDatas.get(0);
        }
        return null;
    }

    //付款记录查询
    public List<PayAmountData> queryPaysData(String orderNo) {
        return DataSupport.where("orderNo=?", orderNo).order(" id desc ").find(PayAmountData.class);
    }

    //客户信息查询
    public CustomerList queryCustomInfo(String customerid) {
        List<CustomerList> customerLists = DataSupport.where(" CustomerId=?", customerid).find(CustomerList.class);
        if (customerLists != null && customerLists.size() > 0) {
            return customerLists.get(0);
        }
        return null;
    }

    //客户信息查询
    public CustomerList queryCustomInfoByMobile(String cellphone) {
        List<CustomerList> customerLists = DataSupport.where(" cellphone=?", cellphone).find(CustomerList.class);
        if (customerLists != null && customerLists.size() > 0) {
            return customerLists.get(0);
        }
        return null;
    }

    //删除定单所有付款记录
    public void delAllPays(int orderNo) {
        DataSupport.deleteAll(PayAmountData.class, " orderNo = ? ", orderNo + "");
    }

    public List<CardKind> getCardKinds() {
        return DataSupport.findAll(CardKind.class);
    }

    //新增会员下单数据更新
    public void updateNewMemberOrder(long batchNo, String petName, int newPetId, int newCustomerId, String newCardNo) {
        List<OrderInfoData> orderList = DataSupport.where(" CustomerID=? and PetName=? ", batchNo + "", petName).find(OrderInfoData.class);
        if (orderList != null && orderList.size() > 0) {
            for (OrderInfoData orderData : orderList) {
                OrderInfoData orderUpdate = new OrderInfoData();
                orderUpdate.setMemberCardNo(newCardNo);
                orderUpdate.setPetId(newPetId);
                orderUpdate.setCustomerId(newCustomerId);
                orderUpdate.update(orderData.getId());

                ContentValues values = new ContentValues();
                values.put("PetId", newPetId);
                values.put("CustomerId", newCustomerId);
                DataSupport.updateAll(SettlementData.class, values, " orderNo=? ", orderData.getOrderNo() + "");
            }
        }
    }

    //查询最大的时间戳
    public String queryCustomerTimeStamp() {
        return DataSupport.max(CustomerList.class, "timestamp", String.class);
    }

    //查询最大的时间戳
    public String queryPetTimeStatmp() {
        return DataSupport.max(PetData.class, "timestamp", String.class);
    }
}
