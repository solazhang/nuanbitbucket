package com.warmsoft.pos.litepal.data;

import org.litepal.crud.DataSupport;

import java.io.Serializable;

/**
 * 作者: lijinliu on 2016/8/23.
 * 邮箱：jinliu.li@warmsoft.com
 * 描述: 定单商品：只保存本地使用。服务器交互使用 SettlementData
 */
public class OrderGoodsData extends DataSupport implements Serializable {

    private int ID;//":9753275,
    private int orderNo;//本地定单号
    private float Amount;//金额
    private float Count;//数量
    private float sureCount;//未确认，未下单数量
    private int PetId;//宠物ID
    private String PetName;//宠物名称
    private int CustomerId;//客户ID
    private String CustomerName;//客户名称
    private int FeeItemId;//项目ID/商品ID
    private String FeeItemName;//项目名称,商品名称
    private long DrugType;//产品类型
    private String FeeItemEnglishName;//项目英文名称
    private int OrgId;//机构
    private int SellerId;//销售人员ID
    private float Cost;//金额
    private String ChargeDatetime;//日期
    private String DataChannel;//渠道
    private float DiscountRate;//打折,
    private String CellPhone;//电话
    private String goodDesc;//商品备注

    private float DiscountAmount;//": 0,折扣总金额 (摸零金额)
    private String PayedDate;//": null,结算时间
    private float ActlyPayed;//": 0,实付金额
    private float TotalAmount;//": null,账单金额

    private int isSure;//,(客户端使用： 0未确认,1已确认)
    private float OutstorePrice;//价格
    private float InstorePrice;//进价
    private float memberPrice;//会员价

    public boolean isSelected = false;//选中

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public int getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(int orderNo) {
        this.orderNo = orderNo;
    }


    public float getAmount() {
        return Amount;
    }

    public void setAmount(float amount) {
        Amount = amount;
    }

    public float getCount() {
        return Count;
    }

    public void setCount(float count) {
        Count = count;
    }

    public float getSureCount() {
        return sureCount;
    }

    public void setSureCount(float sureCount) {
        this.sureCount = sureCount;
    }

    public int getPetId() {
        return PetId;
    }

    public void setPetId(int petId) {
        PetId = petId;
    }

    public String getPetName() {
        return PetName;
    }

    public void setPetName(String petName) {
        PetName = petName;
    }

    public int getCustomerId() {
        return CustomerId;
    }

    public void setCustomerId(int customerId) {
        CustomerId = customerId;
    }

    public String getCustomerName() {
        return CustomerName;
    }

    public void setCustomerName(String customerName) {
        CustomerName = customerName;
    }

    public int getFeeItemId() {
        return FeeItemId;
    }

    public void setFeeItemId(int feeItemId) {
        FeeItemId = feeItemId;
    }

    public String getFeeItemName() {
        return FeeItemName;
    }

    public void setFeeItemName(String feeItemName) {
        FeeItemName = feeItemName;
    }

    public long getDrugType() {
        return DrugType;
    }

    public void setDrugType(long drugType) {
        DrugType = drugType;
    }

    public String getFeeItemEnglishName() {
        return FeeItemEnglishName;
    }

    public void setFeeItemEnglishName(String feeItemEnglishName) {
        FeeItemEnglishName = feeItemEnglishName;
    }

    public int getOrgId() {
        return OrgId;
    }

    public void setOrgId(int orgId) {
        OrgId = orgId;
    }

    public int getSellerId() {
        return SellerId;
    }

    public void setSellerId(int sellerId) {
        SellerId = sellerId;
    }

    public float getCost() {
        return Cost;
    }

    public void setCost(float cost) {
        Cost = cost;
    }

    public String getChargeDatetime() {
        return ChargeDatetime;
    }

    public void setChargeDatetime(String chargeDatetime) {
        ChargeDatetime = chargeDatetime;
    }

    public String getDataChannel() {
        return DataChannel;
    }

    public void setDataChannel(String dataChannel) {
        DataChannel = dataChannel;
    }

    public float getDiscountRate() {
        return DiscountRate;
    }

    public void setDiscountRate(float discountRate) {
        DiscountRate = discountRate;
    }

    public String getCellPhone() {
        return CellPhone;
    }

    public void setCellPhone(String cellPhone) {
        CellPhone = cellPhone;
    }

    public float getDiscountAmount() {
        return DiscountAmount;
    }

    public void setDiscountAmount(float discountAmount) {
        DiscountAmount = discountAmount;
    }

    public String getPayedDate() {
        return PayedDate;
    }

    public void setPayedDate(String payedDate) {
        PayedDate = payedDate;
    }

    public float getActlyPayed() {
        return ActlyPayed;
    }

    public void setActlyPayed(float actlyPayed) {
        ActlyPayed = actlyPayed;
    }

    public float getTotalAmount() {
        return TotalAmount;
    }

    public void setTotalAmount(float totalAmount) {
        TotalAmount = totalAmount;
    }

    public int getIsSure() {
        return isSure;
    }

    public void setIsSure(int isSure) {
        this.isSure = isSure;
    }

    public float getOutstorePrice() {
        return OutstorePrice;
    }

    public void setOutstorePrice(float outstorePrice) {
        OutstorePrice = outstorePrice;
    }

    public float getInstorePrice() {
        return InstorePrice;
    }

    public void setInstorePrice(float instorePrice) {
        InstorePrice = instorePrice;
    }

    public float getMemberPrice() {
        return memberPrice;
    }

    public void setMemberPrice(float memberPrice) {
        this.memberPrice = memberPrice;
    }

    public String getGoodDesc() {
        return goodDesc;
    }

    public void setGoodDesc(String goodDesc) {
        this.goodDesc = goodDesc;
    }
}