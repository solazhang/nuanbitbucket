package com.warmsoft.pos.support.listview.adapter;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.warmsoft.pos.R;
import com.warmsoft.pos.support.listview.bean.Type;
import com.warmsoft.pos.view.SquareLayout;

import java.util.ArrayList;

public class GridViewAdapter extends BaseAdapter {
    private ArrayList<Type> list;
    private Type type;
    private Context context;
    Holder view;

    public GridViewAdapter(Context context, ArrayList<Type> list) {
        this.list = list;
        this.context = context;
    }

    @Override
    public int getCount() {
        if (list != null && list.size() > 0)
            return list.size();
        else
            return 0;
    }

    public void update(ArrayList<Type> l) {
        if (list != null && list.size() > 0) {
            list.clear();
            list.addAll(l);
        }

        notifyDataSetChanged();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = View.inflate(context, R.layout.item_for_square, null);
            view = new Holder(convertView);
            convertView.setTag(view);
        } else {
            view = (Holder) convertView.getTag();
        }
        if (list != null && list.size() > 0) {
            type = list.get(position);

            view.name.setText(type.getTypename());

            if (type.getSelect() > 0) { //为0,隐藏
                view.badge.setVisibility(View.VISIBLE);
                view.badge.setText(type.getSelect() > 99 ? "99+" : String.valueOf(type.getSelect()));
            } else {
                view.badge.setVisibility(View.INVISIBLE);
            }
        }

        return convertView;
    }

    private class Holder {
        private ImageView icon;
        private TextView name;
        private SquareLayout square;
        TextView badge;

        public Holder(View view) {
            name = (TextView) view.findViewById(R.id.typename);
            badge = (TextView) view.findViewById(R.id.id_for_badge_view);
        }
    }

}
