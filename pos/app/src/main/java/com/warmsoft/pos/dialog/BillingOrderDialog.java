package com.warmsoft.pos.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.warmsoft.pos.R;

/**
 * 作者: lijinliu on 2016/8/5.
 * 邮箱：jinliu.li@warmsoft.com
 * 描述: 记账
 */
public class BillingOrderDialog implements View.OnClickListener {

    private Dialog mDialog;
    private DialogListener mListner;
    private Activity mActivity;
    private LayoutInflater mInflater;

    private TextView btnCancle;
    private TextView btnConfirm;
    private TextView mTvBillingInfo;
    private String mInfo = "此订单应付总价%1$s元\n是否记账？";
    private float orderAmout = 0;

    public BillingOrderDialog(Activity activity,float amount, DialogListener listener) {
        this.mActivity = activity;
        this.mListner = listener;
        this.orderAmout = amount;
        this.mInflater = LayoutInflater.from(mActivity);
        createLoadingDialog(mActivity);
    }

    /**
     * 得到自定义的progressDialog
     *
     * @param context
     * @return
     */
    private void createLoadingDialog(Context context) {
        View rootView = mInflater.inflate(R.layout.dialog_order_billing, null);
        LinearLayout layout = (LinearLayout) rootView.findViewById(R.id.dialog_view);
        initView(rootView);
        mDialog = new Dialog(context, R.style.loadingdialog);
        mDialog.setCancelable(true);
        int MATH_PARENT = LinearLayout.LayoutParams.MATCH_PARENT;
        mDialog.setContentView(layout, new LinearLayout.LayoutParams(MATH_PARENT, MATH_PARENT));// 设置布局
        mDialog.show();
    }

    private void initView(View rootView) {
        btnConfirm = (TextView) rootView.findViewById(R.id.btn_confirm);
        btnCancle = (TextView) rootView.findViewById(R.id.btn_cancle);
        btnConfirm.setOnClickListener(this);
        btnCancle.setOnClickListener(this);
        mTvBillingInfo = (TextView) rootView.findViewById(R.id.tv_billing_info);
        mTvBillingInfo.setText(String.format(mInfo,orderAmout));
}

    @Override
    public void onClick(View view) {
        if (view == btnCancle) {
            dismiss();
        } else if (view == btnConfirm) {
            mDialog.dismiss();
            if (mListner != null) {
                mListner.keyConfirm();
            }
        }
    }


    public void dismiss() {
        if (mDialog != null) {
            mDialog.dismiss();
            mDialog = null;
        }
    }

    public interface DialogListener {
        void keyConfirm();
    }
}
