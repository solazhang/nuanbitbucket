package com.warmsoft.pos.litepal.data;

import org.litepal.crud.DataSupport;

import java.util.Date;

/**
 * 作者: lijinliu on 2016/8/26.
 * 邮箱：jinliu.li@warmsoft.com
 * 描述: 定单任务表
 */
public class MemberTaskData extends DataSupport {
    private int id;
    private int memberId;//会员编号
    private long batchNo;//批次号，同一个订单，多次下单的为不同批次，默认为系统时间戳
    private int opTimes;//任务执行次数,最多执行三次
    private int type;//任务类型 1下单任务，2结算任务, 3退单任务
    private int status;//0新任务，9任务在执行，1任务成功，2任务失败
    private String taskDesc;//任务描述: 1下单，2结算
    private Date taskTime;//任务执行时间

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getMemberId() {
        return memberId;
    }

    public void setMemberId(int memberId) {
        this.memberId = memberId;
    }

    public long getBatchNo() {
        return batchNo;
    }

    public void setBatchNo(long batchNo) {
        this.batchNo = batchNo;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getTaskDesc() {
        return taskDesc;
    }

    public void setTaskDesc(String taskDesc) {
        this.taskDesc = taskDesc;
    }

    public int getOpTimes() {
        return opTimes;
    }

    public void setOpTimes(int opTimes) {
        this.opTimes = opTimes;
    }

    public Date getTaskTime() {
        return taskTime;
    }

    public void setTaskTime(Date taskTime) {
        this.taskTime = taskTime;
    }
}
