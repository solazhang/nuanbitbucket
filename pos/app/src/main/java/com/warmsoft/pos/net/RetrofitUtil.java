package com.warmsoft.pos.net;


import com.google.gson.Gson;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.RequestBody;

import java.util.Map;

/**
 * Created by brooks on 16/1/27.
 */
public class RetrofitUtil {
    private static RetrofitUtil instance = null;

    public static RetrofitUtil getInstance() {
        if (instance == null) {
            synchronized (RetrofitUtil.class) {
                instance = new RetrofitUtil();
            }
        }

        return instance;
    }

    public RequestBody getRequestBody(Map<String, Object> map) {
        String json = new Gson().toJson(map);
        return RequestBody.create(MediaType.parse("application/json;charset/utf-8"), json);
    }
}
