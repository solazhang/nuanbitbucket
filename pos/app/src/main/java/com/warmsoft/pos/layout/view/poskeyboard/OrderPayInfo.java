package com.warmsoft.pos.layout.view.poskeyboard;

import android.app.Activity;
import android.content.Context;
import android.support.v4.util.ArrayMap;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.warmsoft.pos.R;
import com.warmsoft.pos.adapter.entiy.PayInfoAdapter;
import com.warmsoft.pos.bean.AmountsModel;
import com.warmsoft.pos.bean.AuthModel;
import com.warmsoft.pos.enumutil.PayChanel;
import com.warmsoft.pos.litepal.data.CustomerList;
import com.warmsoft.pos.litepal.data.LitepalHelper;
import com.warmsoft.pos.litepal.data.OrderInfoData;
import com.warmsoft.pos.litepal.data.PayAmountData;
import com.warmsoft.pos.net.RetrofitManager;
import com.warmsoft.pos.protocol.GetCustomeAmontProto;
import com.warmsoft.pos.util.Constant;
import com.warmsoft.pos.util.LogUtil;
import com.warmsoft.pos.util.OAuthUtils;
import com.warmsoft.pos.util.OrderHelper;
import com.warmsoft.pos.util.SelectMemberUtil;
import com.warmsoft.pos.util.Util;

import org.litepal.crud.DataSupport;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * 作者: lijinliu on 2016/8/10.
 * 邮箱：jinliu.li@warmsoft.com
 * 描述: view_pos_keyboder.xml 中部分布局:支付信息
 */
public class OrderPayInfo {
    private OrderHelper orderHelper;

    protected List<PayAmountData> datas = new ArrayList<PayAmountData>();
    private Activity mActivity;
    private Context mContext;

    public LinearLayout layoutMain;
    public TextView tvOrderSize;
    public TextView tvOrderAmount;
    public TextView tvNeedPay;
    public TextView tvMemberNum;
    public TextView tvMemberJifeng;
    public TextView tvMemberYouhui;
    public TextView tvMemberAmount;
    public TextView tvMemberDeposit;
    public TextView tvMemberAmountcard;
    public LinearLayout LayoutSpendAmount;
    public TextView tvSpentAmount;
    public ListView listView;
    private PayInfoAdapter adapter;
    public TextView payInfo;
    public ImageView ivProgressbar;
    public FrameLayout mRefreshMember;

    Animation operatingAnim;

    public OrderPayInfo(View rootView, Activity activity) {
        this.mActivity = activity;
        this.mContext = mActivity.getApplicationContext();
        layoutMain = (LinearLayout) rootView.findViewById(R.id.layout_order_operat);
        tvOrderSize = (TextView) rootView.findViewById(R.id.tv_order_size);
        tvOrderAmount = (TextView) rootView.findViewById(R.id.tv_order_amount);
        tvNeedPay = (TextView) rootView.findViewById(R.id.tv_need_pay);
        LayoutSpendAmount = (LinearLayout) rootView.findViewById(R.id.layout_spent_amount);
        payInfo = (TextView) rootView.findViewById(R.id.tv_main_payinfo);
        tvMemberNum = (TextView) rootView.findViewById(R.id.tv_keyboard_member_num);
        tvMemberJifeng = (TextView) rootView.findViewById(R.id.tv_keyboard_member_jifeng);
        tvMemberYouhui = (TextView) rootView.findViewById(R.id.tv_keyboard_member_youhui);
        tvMemberAmount = (TextView) rootView.findViewById(R.id.tv_keyboard_member_amount);
        tvMemberDeposit = (TextView) rootView.findViewById(R.id.deposit);
        tvMemberAmountcard = (TextView) rootView.findViewById(R.id.amountcard);
        tvSpentAmount = (TextView) rootView.findViewById(R.id.tv_spent_amount);
        mRefreshMember = (FrameLayout) rootView.findViewById(R.id.id_for_member_info_fl);
        ivProgressbar = (ImageView) rootView.findViewById(R.id.userinfo_refresh);

        {
            listView = (ListView) rootView.findViewById(R.id.list_pay_info);
            adapter = new PayInfoAdapter(mContext, datas);
            listView.setAdapter(adapter);
        }

        initEvents();
    }

    private void initEvents() {
        operatingAnim = AnimationUtils.loadAnimation(mContext, R.anim.rotate_animation);
        LinearInterpolator lin = new LinearInterpolator();
        operatingAnim.setInterpolator(lin);

        mRefreshMember.setOnClickListener(new View.OnClickListener() { //点击刷新
            @Override
            public void onClick(View view) {
                OrderHelper orderHelper = OrderHelper.getInstance(mActivity);
                OrderInfoData orderData = orderHelper.getOrderData();
                if (orderData != null) {
                    getAmounts(orderData.getCustomerId());
                }
            }
        });
    }

    /**
     * 已收金额
     *
     * @param orderNo
     * @return
     */
    public float refershList(String orderNo) {
        datas = LitepalHelper.getInstance().queryPaysData(orderNo);//DataSupport.where("orderNo=?", orderNo).find(PayAmountData.class);
        float amout = 0.00f;
        if (datas != null && datas.size() > 0) {
            adapter.setData(datas);
            {
                float moling = 0;
                for (PayAmountData payData : datas) {
                    if (payData.getPayChanel() == PayChanel.PAY_BY_DISCOUNT) {
                        moling = payData.getPayAmount();
                    } else if (payData.getPayChanel() != PayChanel.PAY_BY_KEEPACCCOUNT) {//记账金额不记入实收
                        amout += payData.getPayAmount();
                    }
                }
                float change = 0.00f;
                if (amout > datas.get(0).getOrderAmount()) {
                    change = amout - datas.get(0).getOrderAmount();
                }
                if (moling > 0) {
                    payInfo.setText(String.format(Constant.ORDER_PAY_INFOB, Util.subFloat(amout) + "", Util.subFloat(moling) + ""));
                } else {
                    payInfo.setText(String.format(Constant.ORDER_PAY_INFOA, Util.subFloat(amout) + "", Util.subFloat(change) + ""));
                }
            }
        } else {
            adapter.setData(new ArrayList<PayAmountData>());
            payInfo.setText("");
        }
        return amout;
    }

    public void setOrderSize(int size) {
        tvOrderSize.setText(size + "");
    }

    public void setOrderAmount(float amount) {
        if (amount > 0) {
            String amountStr = Util.subFloat(amount) + "";
            tvOrderAmount.setText(amountStr);
            tvNeedPay.setText(amountStr);
            LayoutSpendAmount.setVisibility(View.VISIBLE);
            tvSpentAmount.setText(amountStr);
        } else {
            LayoutSpendAmount.setVisibility(View.GONE);
            tvOrderAmount.setText("0");
            tvNeedPay.setText("0");
        }
    }

    public void resetInfo() {
        OrderHelper orderHelper = OrderHelper.getInstance(mActivity);
        OrderInfoData orderData = orderHelper.getOrderData();
        datas = new ArrayList<PayAmountData>();
        adapter.setData(datas);
        tvOrderSize.setText("0");
        tvOrderAmount.setText("0");
        tvNeedPay.setText("0");
        tvSpentAmount.setText("");
        payInfo.setText("");
        refershMemberInfo(orderData.getCustomerId());
    }

    public void refershMemberInfo(int customerid) {
        List<CustomerList> customerLists = DataSupport.where(" CustomerId=?", "" + customerid).find(CustomerList.class);
        if (customerLists != null && customerLists.size() > 0) {
            tvMemberNum.setText(String.format("会员: %1$s", customerLists.get(0).getCardNumber()));
            tvMemberJifeng.setText(String.format("积分: %1$s", customerLists.get(0).getScore()));
            tvMemberYouhui.setText(String.format("等级: %1$s", customerLists.get(0).getCardTypeName()));

            if (SelectMemberUtil.getInstance().getDataInfo() != null) {
                AuthModel.DataInfo.AnonymousCustomerInfo customerIonfo = OAuthUtils.getInstance().getAnonyCustomInfo(mContext);
                if (customerid != customerIonfo.getAnonymousCustomerID()) {
                    tvMemberAmount.setText(String.format("账户余额: %1$s", SelectMemberUtil.getInstance().getDataInfo().getAccount()));
                    tvMemberAmountcard.setText(String.format("卡内余额: %1$s", SelectMemberUtil.getInstance().getDataInfo().getMemberCard()));
                    tvMemberDeposit.setText(String.format("押金: %1$s", SelectMemberUtil.getInstance().getDataInfo().getPrePay()));
                } else {
                    tvMemberAmount.setText(String.format("账户余额: %1$s", "0"));
                    tvMemberAmountcard.setText(String.format("卡内余额:%1$s", "0"));
                    tvMemberDeposit.setText(String.format("押金:%1$s", "0"));
                }
            } else {
                tvMemberAmount.setText(String.format("账户余额: %1$s", "0"));
                tvMemberAmountcard.setText(String.format("卡内余额:%1$s", "0"));
                tvMemberDeposit.setText(String.format("押金:%1$s", "0"));
            }

        } else {
            tvMemberNum.setText(String.format("会员: %1$s", ""));
            tvMemberJifeng.setText(String.format("积分: %1$s", "0"));
            tvMemberYouhui.setText(String.format("等级: %1$s", ""));
            tvMemberAmount.setText(String.format("账户余额: %1$s", "0"));
            tvMemberAmountcard.setText(String.format("卡内余额: %1$s", "0"));
            tvMemberDeposit.setText(String.format("押金: %1$s", "0"));
        }
    }

    public void showAnimation() {
        ivProgressbar.setVisibility(View.VISIBLE);

        if (operatingAnim != null) {
            ivProgressbar.startAnimation(operatingAnim);
        }
    }

    public void clearAnimation() {
        ivProgressbar.setVisibility(View.GONE);

        if (operatingAnim != null) {
            ivProgressbar.clearAnimation();
        }
    }

    public void refreshAmount(AmountsModel.DataInfo dataInfo, int customerid) {
        List<CustomerList> customerLists = DataSupport.where(" CustomerId=?", "" + customerid).find(CustomerList.class);
        if (customerLists != null && customerLists.size() > 0) {
            tvMemberNum.setText(String.format("会员: %1$s", customerLists.get(0).getCardNumber()));
            tvMemberJifeng.setText(String.format("积分: %1$s", customerLists.get(0).getScore()));
            tvMemberYouhui.setText(String.format("等级: %1$s", customerLists.get(0).getCardTypeName()));

            if (dataInfo != null) {
                String amount = String.valueOf(dataInfo.getAccount());
                String cardAmount = String.valueOf(dataInfo.getMemberCard());
                String prePay = String.valueOf(dataInfo.getPrePay());

                tvMemberAmount.setText(String.format("账户余额: %1$s", amount));
                tvMemberAmountcard.setText(String.format("卡内余额: %1$s", cardAmount));
                tvMemberDeposit.setText(String.format("押金: %1$s", prePay));

                LogUtil.error("refreshAmount getAccount " + String.valueOf(dataInfo.getAccount()));
                LogUtil.error("refreshAmount getMemberCard " + String.valueOf(dataInfo.getMemberCard()));
                LogUtil.error("refreshAmount getPrePay " + String.valueOf(dataInfo.getPrePay()));

            } else {
                tvMemberAmount.setText(String.format("账户余额: %1$s", "0"));
                tvMemberAmountcard.setText(String.format("卡内余额: %1$s", "0"));
                tvMemberDeposit.setText(String.format("押金: %1$s", "0"));
            }

        } else {
            tvMemberNum.setText(String.format("会员: %1$s", ""));
            tvMemberJifeng.setText(String.format("积分: %1$s", "0"));
            tvMemberYouhui.setText(String.format("等级: %1$s", ""));
            tvMemberAmount.setText(String.format("账户余额: %1$s", "0"));
            tvMemberAmountcard.setText(String.format("卡内余额: %1$s", "0"));
            tvMemberDeposit.setText(String.format("押金: %1$s", "0"));
        }
    }

    //用户账户金额信息
    public void checkMemberAmount() {
        OrderInfoData orderData = OrderHelper.getInstance(mActivity).getOrderData();
        if (orderData == null) {
            return;
        }
        AmountsModel.DataInfo amountInfo = SelectMemberUtil.getInstance().getDataInfo();
        if (amountInfo != null && orderData.getCustomerId() == amountInfo.getCustomerid()) {
            refreshAmount(amountInfo, amountInfo.getCustomerid());
        } else if (orderData.getCustomerId() > 0) {
            getAmounts(orderData.getCustomerId());
        }
    }

    public void getAmounts(final int customerid) {
        showAnimation();
        new GetCustomeAmontProto(mContext, new GetCustomeAmontProto.ProtoListener() {
            @Override
            public void onCompleted(AmountsModel.DataInfo dataInfo) {
                refreshAmount(dataInfo, dataInfo.getCustomerid());
                clearAnimation();
            }
        }, customerid);
    }
}
