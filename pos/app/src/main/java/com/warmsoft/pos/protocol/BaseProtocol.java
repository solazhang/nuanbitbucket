//package com.warmsoft.pos.protocol;
//
//import com.warmsoft.pos.util.Trace;
//import com.zhy.http.okhttp.OkHttpUtils;
//import com.zhy.http.okhttp.callback.StringCallback;
//
//import org.json.JSONObject;
//
//import java.util.Map;
//
///**
// * 作者: lijinliu on 2016/8/5.
// * 邮箱：jinliu.li@warmsoft.com
// * 描述: 协议基础类
// */
//
//public class BaseProtocol {
//    private final String mBaseUrl = "http://120.25.61.99:21234/api/WarmPos/SOAProduct/ProductCore/";
//
//    private static BaseProtocol mProtocol;
//
//    private BaseProtocol() {
//
//    }
//
//    public static BaseProtocol getInstance() {
//        if (mProtocol == null) {
//            mProtocol = new BaseProtocol();
//        }
//        return mProtocol;
//    }
//
//    public void PostString(String action, Map<String, String> params, final ProtocolPostListener listener) {
//        JSONObject obj = new JSONObject(params);
//        Trace.e(obj.toString());
//        OkHttpUtils.postString().mediaType(MediaType.parse("application/json")).url(mBaseUrl + action).content("[" + obj.toString() + "]").build().execute(new StringCallback() {
//            @Override
//            public void onError(Call call, Exception e) {
//                listener.onError(call, e);
//                Trace.e("Protocol Post Exception:");
//                Trace.e(e);
//            }
//
//            @Override
//            public void onResponse(String response) {
//                listener.onResponse(response);//返回数据不再做加密
//            }
//        });
//    }
//
//    public void Post(String action, Map<String, String> params, final ProtocolPostListener listener) {
//
//        OkHttpUtils.post().url(mBaseUrl + action).params(params).build().execute(new StringCallback() {
//            @Override
//            public void onError(Call call, Exception e) {
//                listener.onError(call, e);
//                Trace.e("Protocol Post Exception:");
//                Trace.e(e);
//            }
//
//            @Override
//            public void onResponse(String response) {
//                listener.onResponse(response);//返回数据不再做加密
//            }
//        });
//    }
//
//    public interface ProtocolPostListener {
//        public void onError(Call call, Exception e);
//
//        public void onResponse(String response);
//    }
//}
