package com.warmsoft.pos.layout.view;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.warmsoft.pos.R;
import com.warmsoft.pos.dialog.GoodRemarkEditDialog;
import com.warmsoft.pos.dialog.GoodSizeEditDialog;
import com.warmsoft.pos.dialog.NomelNoteDialog;
import com.warmsoft.pos.enumutil.OrderStatusEnum;
import com.warmsoft.pos.fragment.main.FragmentPos;
import com.warmsoft.pos.litepal.data.OrderGoodsData;
import com.warmsoft.pos.litepal.data.SettlementData;
import com.warmsoft.pos.util.Constant;
import com.warmsoft.pos.util.OrderHelper;
import com.warmsoft.pos.util.Util;

/**
 * 作者: lijinliu on 2016/8/9.
 * 邮箱：jinliu.li@warmsoft.com
 * 描述: 定单商品列表操作View
 */
public class LayoutOrderGood implements View.OnClickListener, GoodSizeEditDialog.SizeListener {

    private Activity mActivity;
    private FragmentPos fragmentPos;
    private Context mContext;
    private OrderGoodsData medicine;

    public Button btnClose;
    public ImageView ivGoodIssure;
    public TextView tvGoodSaveStatus;
    public TextView tvGoodName;
    public TextView tvGoodPrice;
    public View viewWeightLeft;
    public View viewWeightRight;
    public Button btnKeyboardDiaolog;
    public Button btnReduce;
    public TextView tvGoodsSize;
    public Button btnAdd;
    public RelativeLayout relayoutSize;
    public TextView tvGoodSureSize;
    public RelativeLayout relayoutRemarkInfo;
    public TextView tvGoodRemarksTitle;
    public TextView tvGoodRemarksDesc;
    public View viewRemarksLine;
    public TextView tvInputRemarks;
    public View viewLastLine;
    public TextView tvOrderInfo;
    public Button btnDeleteGoods;
    public Button btnConfirmEdit;


    public LayoutOrderGood(View rootView, Activity activity, FragmentPos fragment) {
        this.mActivity = activity;
        this.mContext = mActivity.getApplicationContext();
        this.fragmentPos = fragment;

        ivGoodIssure = (ImageView) rootView.findViewById(R.id.iv_good_issure);
        tvGoodSaveStatus = (TextView) rootView.findViewById(R.id.tv_good_save_status);
        tvGoodName = (TextView) rootView.findViewById(R.id.tv_good_name);
        tvGoodPrice = (TextView) rootView.findViewById(R.id.tv_good_price);
        viewWeightLeft = (View) rootView.findViewById(R.id.view_weight_left);
        viewWeightRight = (View) rootView.findViewById(R.id.view_weight_right);
        btnKeyboardDiaolog = (Button) rootView.findViewById(R.id.btn_keyboard_diaolog);
        btnReduce = (Button) rootView.findViewById(R.id.btn_reduce);
        tvGoodsSize = (TextView) rootView.findViewById(R.id.tv_goods_size);
        tvGoodsSize.setTag(0);
        btnAdd = (Button) rootView.findViewById(R.id.btn_add);
        relayoutSize = (RelativeLayout) rootView.findViewById(R.id.relayout_size);
        tvGoodSureSize = (TextView) rootView.findViewById(R.id.tv_good_sure_size);
        relayoutRemarkInfo = (RelativeLayout) rootView.findViewById(R.id.relayout_remark_info);
        tvGoodRemarksTitle = (TextView) rootView.findViewById(R.id.tv_good_remarks_title);
        tvGoodRemarksDesc = (TextView) rootView.findViewById(R.id.tv_good_remarks_desc);
        viewRemarksLine = (View) rootView.findViewById(R.id.view_remarks_line);
        tvInputRemarks = (TextView) rootView.findViewById(R.id.tv_input_remarks);
        viewLastLine = (View) rootView.findViewById(R.id.view_last_line);
        tvOrderInfo = (TextView) rootView.findViewById(R.id.tv_order_info);
        btnDeleteGoods = (Button) rootView.findViewById(R.id.btn_delete_goods);
        btnConfirmEdit = (Button) rootView.findViewById(R.id.btn_confirm_edit);
        btnClose = (Button) rootView.findViewById(R.id.btn_close_goodlayout);

        initEvents();
    }

    private void initEvents() {
        btnClose.setOnClickListener(this);
        btnDeleteGoods.setOnClickListener(this);
        btnConfirmEdit.setOnClickListener(this);
        btnKeyboardDiaolog.setOnClickListener(this);
        tvInputRemarks.setOnClickListener(this);
        btnReduce.setOnClickListener(this);
        btnAdd.setOnClickListener(this);
    }


    public void setData(OrderGoodsData goodInfo) {
        medicine = goodInfo;
        if (goodInfo.getSureCount() > 0) {
            ivGoodIssure.setImageResource(R.mipmap.warmpos_sure);
            tvGoodSaveStatus.setVisibility(View.GONE);
            viewWeightRight.setVisibility(View.GONE);
            btnKeyboardDiaolog.setVisibility(View.GONE);
            btnReduce.setVisibility(View.GONE);
            tvGoodsSize.setVisibility(View.GONE);
            tvGoodsSize.setTag(String.format(Constant.GOODSIZE, goodInfo.getCount() + ""));
            tvInputRemarks.setVisibility(View.GONE);
            viewLastLine.setVisibility(View.GONE);
            btnDeleteGoods.setVisibility(View.GONE);
            btnConfirmEdit.setVisibility(View.GONE);
            tvGoodName.setVisibility(View.VISIBLE);
            tvGoodPrice.setVisibility(View.VISIBLE);
            viewWeightLeft.setVisibility(View.VISIBLE);
            btnAdd.setVisibility(View.VISIBLE);
            relayoutSize.setVisibility(View.VISIBLE);
            tvGoodSureSize.setVisibility(View.VISIBLE);
            relayoutRemarkInfo.setVisibility(View.VISIBLE);
            tvGoodRemarksTitle.setVisibility(View.VISIBLE);
            tvGoodRemarksDesc.setVisibility(View.VISIBLE);
            viewRemarksLine.setVisibility(View.VISIBLE);
            tvOrderInfo.setVisibility(View.VISIBLE);

            tvGoodName.setText(goodInfo.getFeeItemName());
            tvGoodPrice.setText(String.format(Constant.GOODPRICE, Util.subFloat(goodInfo.getAmount()) + ""));
            tvGoodSureSize.setText(String.format(Constant.GOODSIZE, goodInfo.getCount() + ""));
            tvGoodRemarksDesc.setText(goodInfo.getGoodDesc() == null ? "无" : goodInfo.getGoodDesc());
            tvOrderInfo.setText("");

        } else {

            ivGoodIssure.setImageResource(R.mipmap.warmsoft_order_point);

            tvGoodSaveStatus.setVisibility(View.GONE);
            relayoutSize.setVisibility(View.GONE);
            tvGoodRemarksDesc.setVisibility(View.GONE);
            viewRemarksLine.setVisibility(View.GONE);
            tvOrderInfo.setVisibility(View.GONE);
            btnConfirmEdit.setVisibility(View.GONE);
            tvGoodName.setVisibility(View.VISIBLE);
            tvGoodPrice.setVisibility(View.VISIBLE);
            viewWeightLeft.setVisibility(View.VISIBLE);
            viewWeightRight.setVisibility(View.VISIBLE);
            btnKeyboardDiaolog.setVisibility(View.VISIBLE);
            btnReduce.setVisibility(View.VISIBLE);
            tvGoodsSize.setVisibility(View.VISIBLE);
            btnAdd.setVisibility(View.VISIBLE);
            tvGoodSureSize.setVisibility(View.VISIBLE);
            relayoutRemarkInfo.setVisibility(View.VISIBLE);
            tvGoodRemarksTitle.setVisibility(View.VISIBLE);
            tvInputRemarks.setVisibility(View.VISIBLE);
            viewLastLine.setVisibility(View.VISIBLE);
            btnDeleteGoods.setVisibility(View.VISIBLE);

            tvInputRemarks.setText(goodInfo.getGoodDesc() == null ? "" : goodInfo.getGoodDesc());
            tvGoodName.setText(goodInfo.getFeeItemName());
            tvGoodPrice.setText(String.format(Constant.GOODPRICE, Util.subFloat(goodInfo.getAmount()) + ""));
            tvGoodsSize.setText(String.format(Constant.GOODSIZE, goodInfo.getCount() + ""));
            tvGoodsSize.setTag(goodInfo.getCount());
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_close_goodlayout://关闭
                fragmentPos.closeGoodView();
               // fragmentPos.mLayoutMainPos.upDateText();
                break;
            case R.id.btn_delete_goods://删除商品
               // fragmentPos.mLayoutMainPos.upDateText();
                setGoodSize(0);
                break;
            case R.id.btn_confirm_edit://确认修改
                break;
            case R.id.btn_keyboard_diaolog://修改数量
                new GoodSizeEditDialog(mActivity, medicine.getCount(), this);
                break;
            case R.id.tv_input_remarks://添加备注
                new GoodRemarkEditDialog(mActivity, tvInputRemarks, medicine);
                break;
            case R.id.btn_reduce://减一份
                setGoodSize(getTvGoodSize() - 1);
                break;
            case R.id.btn_add://加一份
                if (medicine.getSureCount() > 0) {
                    new GoodSizeEditDialog(mActivity, medicine.getCount(), this);
                } else {
                    setGoodSize(getTvGoodSize() + 1);
                }
                break;
        }
    }

    @Override
    public void numKeyConfim(int value) {
        setGoodSize(value);
    }

    private void setGoodSize(float size) {
        OrderHelper orderHelper = OrderHelper.getInstance(mContext);
        if (orderHelper.getOrderStaus() == OrderStatusEnum.Bill || orderHelper.getOrderStaus() == OrderStatusEnum.HandIn || orderHelper.getOrderStaus() == OrderStatusEnum.ReOpen) {
            if (medicine.getSureCount() > 0 && size <= medicine.getSureCount()) {//最小size不能小于已下单的size
                size = medicine.getSureCount();
                medicine.setIsSure(1);
            } else if (medicine.getSureCount() > 0 && size > medicine.getSureCount()) {
                medicine.setIsSure(0);
            }
            tvGoodsSize.setText(String.format(Constant.GOODSIZE, size + ""));
            tvGoodsSize.setTag(size);
            fragmentPos.setGoodSize(medicine, size, size == 0);
        } else {
            new NomelNoteDialog(mActivity, "不能修改", "请重新开单操作!", "确定", null);
        }
    }

    private float getTvGoodSize() {
        float value = (Float) tvGoodsSize.getTag();
        return value;
    }

}
