package com.warmsoft.pos.bean;

import com.warmsoft.pos.litepal.data.SettlementData;

import java.io.Serializable;
import java.util.List;

/**
 * 作者: lijinliu on 2016/8/9.
 * 邮箱：jinliu.li@warmsoft.com
 * 描述: 客户未付款订单
 */
public class CustomerOrderModel implements Serializable {
    int code;
    String message;
    List<SettlementData> Data;


    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<SettlementData> getData() {
        return Data;
    }

    public void setData(List<SettlementData> data) {
        Data = data;
    }
}
