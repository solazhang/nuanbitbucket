package com.warmsoft.pos.bean;

import com.warmsoft.pos.litepal.data.PetCategory;

import java.io.Serializable;
import java.util.List;

/**
 * Created by brooks on 16/1/26.
 */
public class PetsCategoryModel implements Serializable {
    int code;
    String message;

    List<PetCategory> Data;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<PetCategory> getData() {
        return Data;
    }

    public void setData(List<PetCategory> data) {
        Data = data;
    }
}
