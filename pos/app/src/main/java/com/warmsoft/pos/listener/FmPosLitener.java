package com.warmsoft.pos.listener;

import com.warmsoft.pos.litepal.data.OrderInfoData;
import com.warmsoft.pos.litepal.data.PayAmountData;

/**
 * 作者: lijinliu on 2016/8/29.
 * 邮箱：jinliu.li@warmsoft.com
 * 描述: FragmentPost中常用监听事件
 */
public interface FmPosLitener {
    /**
     * 待付款点击事件
     */
    void settleListClick(OrderInfoData orderData);

    void swingMember();

    void payConfirm(String info);

    void newOrder();

    void clearOnePays(PayAmountData data);

    void clearAllPays();

    void settleOver();

    void keepingAccount();
}
