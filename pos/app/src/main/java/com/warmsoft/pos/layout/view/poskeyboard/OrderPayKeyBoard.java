package com.warmsoft.pos.layout.view.poskeyboard;

import android.app.Activity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.warmsoft.pos.R;
import com.warmsoft.pos.dialog.NomelNoteDialog;
import com.warmsoft.pos.enumutil.OrderStatusEnum;
import com.warmsoft.pos.layout.view.LayoutPosKeyBoard;
import com.warmsoft.pos.listener.FmPosLitener;
import com.warmsoft.pos.util.OrderHelper;
import com.warmsoft.pos.util.Util;

/**
 * 作者: lijinliu on 2016/8/10.
 * 邮箱：jinliu.li@warmsoft.com
 * 描述: view_pos_keyboder.xml 键盘按钮
 */
public class OrderPayKeyBoard implements View.OnClickListener, NomelNoteDialog.DialogListener {

    private Activity mActivity;
    private KeyClick keyListener;
    private FmPosLitener mListener;
    public Button btnKeyIsVip;
    public Button btnKeyPayCash;
    public Button btnKeyUseCard;
    public Button btnKeyClear;
    public Button btnKeySeven;
    public Button btnKeyEight;
    public Button btnKeyNie;
    public Button btnKeyDel;
    public Button btnKeyFour;
    public Button btnKeyFive;
    public Button btnKeySix;
    public Button btnKeyCancle;
    public Button btnKeyOne;
    public Button btnKeyTwo;
    public Button btnKeyThree;
    public Button btnKeyZero;
    public Button btnKeyDoubleZero;
    public Button btnKeyPoint;
    public Button btnPayConfirm;
    public ImageView ivOverInfo;

    public OrderPayKeyBoard(View rootView, Activity activity, KeyClick listener, FmPosLitener fmlistener) {
        this.mActivity = activity;
        this.keyListener = listener;
        this.mListener = fmlistener;

        btnKeyIsVip = (Button) rootView.findViewById(R.id.btn_keyboard_vip);
        btnKeyPayCash = (Button) rootView.findViewById(R.id.btn_keyboard_cash);
        btnKeyUseCard = (Button) rootView.findViewById(R.id.btn_keyboard_card);
        btnKeyClear = (Button) rootView.findViewById(R.id.btn_keyboard_clear);
        btnKeySeven = (Button) rootView.findViewById(R.id.btn_keyboard_seven);
        btnKeyEight = (Button) rootView.findViewById(R.id.btn_keyboard_eight);
        btnKeyNie = (Button) rootView.findViewById(R.id.btn_keyboard_nine);
        btnKeyDel = (Button) rootView.findViewById(R.id.btn_keyboard_delete);
        btnKeyFour = (Button) rootView.findViewById(R.id.btn_keyboard_four);
        btnKeyFive = (Button) rootView.findViewById(R.id.btn_keyboard_five);
        btnKeySix = (Button) rootView.findViewById(R.id.btn_keyboard_six);
        btnKeyCancle = (Button) rootView.findViewById(R.id.btn_keyboard_cancle);
        btnKeyOne = (Button) rootView.findViewById(R.id.btn_keyboard_one);
        btnKeyTwo = (Button) rootView.findViewById(R.id.btn_keyboard_two);
        btnKeyThree = (Button) rootView.findViewById(R.id.btn_keyboard_three);
        btnKeyZero = (Button) rootView.findViewById(R.id.btn_keyboard_zero);
        btnKeyDoubleZero = (Button) rootView.findViewById(R.id.btn_keyboard_doublezero);
        btnKeyPoint = (Button) rootView.findViewById(R.id.btn_keyboard_point);
        btnPayConfirm = (Button) rootView.findViewById(R.id.btn_keyboard_confirm);
        ivOverInfo = (ImageView) rootView.findViewById(R.id.iv_pos_cheques_over_info);
        initEvents();
    }

    private void initEvents() {
        btnKeyIsVip.setOnClickListener(this);
        btnKeyPayCash.setOnClickListener(this);
        btnKeyUseCard.setOnClickListener(this);
        btnKeyClear.setOnClickListener(this);
        btnKeySeven.setOnClickListener(this);
        btnKeyEight.setOnClickListener(this);
        btnKeyNie.setOnClickListener(this);
        btnKeyDel.setOnClickListener(this);
        btnKeyFour.setOnClickListener(this);
        btnKeyFive.setOnClickListener(this);
        btnKeySix.setOnClickListener(this);
        btnKeyCancle.setOnClickListener(this);
        btnKeyOne.setOnClickListener(this);
        btnKeyTwo.setOnClickListener(this);
        btnKeyThree.setOnClickListener(this);
        btnKeyZero.setOnClickListener(this);
        btnKeyDoubleZero.setOnClickListener(this);
        btnKeyPoint.setOnClickListener(this);
        btnPayConfirm.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_keyboard_vip:
                if (mListener != null) {
                    mListener.swingMember();
                }
                break;
            case R.id.btn_keyboard_cash:
                if (keyListener != null) {
                    keyListener.payByCash();
                }
                break;
            case R.id.btn_keyboard_card:
                if (keyListener != null) {
                    keyListener.payByCard();
                }
                break;
            case R.id.btn_keyboard_clear:
                if (keyListener != null) {
                    keyListener.clearPayed();
                }
                break;
            case R.id.btn_keyboard_seven:
                if (keyListener != null) {
                    keyListener.numKeyClick("7");
                }
                break;
            case R.id.btn_keyboard_eight:
                if (keyListener != null) {
                    keyListener.numKeyClick("8");
                }
                break;
            case R.id.btn_keyboard_nine:
                if (keyListener != null) {
                    keyListener.numKeyClick("9");
                }
                break;
            case R.id.btn_keyboard_delete:
                if (keyListener != null) {
                    keyListener.keyDelete();
                }
                break;
            case R.id.btn_keyboard_four:
                if (keyListener != null) {
                    keyListener.numKeyClick("4");
                }
                break;
            case R.id.btn_keyboard_five:
                if (keyListener != null) {
                    keyListener.numKeyClick("5");
                }
                break;
            case R.id.btn_keyboard_six:
                if (keyListener != null) {
                    keyListener.numKeyClick("6");
                }
                break;
            case R.id.btn_keyboard_cancle:
                if (keyListener != null) {
                    keyListener.keyCancle();
                }
                break;
            case R.id.btn_keyboard_one:
                if (keyListener != null) {
                    keyListener.numKeyClick("1");
                }
                break;
            case R.id.btn_keyboard_two:
                if (keyListener != null) {
                    keyListener.numKeyClick("2");
                }
                break;
            case R.id.btn_keyboard_three:
                if (keyListener != null) {
                    keyListener.numKeyClick("3");
                }
                break;
            case R.id.btn_keyboard_zero:
                if (keyListener != null) {
                    keyListener.numKeyClick("0");
                }
                break;
            case R.id.btn_keyboard_doublezero:
                if (keyListener != null) {
                    keyListener.numKeyClick("00");
                }
                break;
            case R.id.btn_keyboard_point:
                if (keyListener != null) {
                    keyListener.numKeyClick(".");
                }
                break;
            case R.id.btn_keyboard_confirm:
                settleOver();
                break;
        }
    }


    private void settleOver() {
        OrderHelper helper = OrderHelper.getInstance(mActivity);
        if (helper.getOrderStaus() == OrderStatusEnum.HandIn || helper.getOrderStaus() == OrderStatusEnum.KeepingAccounts) {
            if (helper.getNeedColletAmount() <= 0) {

                helper.settleOrder(mActivity);//结算
                if (mListener != null) {//结算成功
                    mListener.settleOver();
                }
                lessOrderAmount();
            } else {
                Util.showToast(mActivity, "实收金额小于应收金额");
            }
        } else {
            new NomelNoteDialog(mActivity, "不能结算", "无待结算数据!", "确定", null);
        }
    }

    /**
     * 付款金额大于等于定单金额,可结算
     */
    public void moreOrderAmount() {
        ivOverInfo.setVisibility(View.VISIBLE);
        btnPayConfirm.setBackgroundResource(R.drawable.warmsoft_keybord_payend_confirm);
        OrderHelper.getInstance(mActivity).orderPayed();//定单收款完毕
    }

    /**
     * 结算后/不可结算
     */
    public void lessOrderAmount() {
        ivOverInfo.setVisibility(View.GONE);
        btnPayConfirm.setBackgroundResource(R.drawable.warmsoft_keybord_confirm);
    }

    @Override
    public void noteDialogConfirm() {

    }

    public interface KeyClick {
        void payByCash();

        void payByCard();

        void keyDelete();

        void keyCancle();

        void numKeyClick(String keyValue);

        void clearPayed();
    }
}
