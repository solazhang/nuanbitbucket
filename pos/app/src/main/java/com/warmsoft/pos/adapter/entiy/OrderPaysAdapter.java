package com.warmsoft.pos.adapter.entiy;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.warmsoft.pos.R;
import com.warmsoft.pos.listener.FmPosLitener;
import com.warmsoft.pos.litepal.data.PayAmountData;

import java.util.List;

/**
 * 作者: lijinliu on 2016/8/9.
 * 邮箱：jinliu.li@warmsoft.com
 * 描述: 已支付列表(清空支付弹出窗口使用)
 */
public class OrderPaysAdapter extends BaseAdapter {
    protected Context mContext;
    protected List<PayAmountData> datas;
    private FmPosLitener mListener;

    public OrderPaysAdapter(Context context, List<PayAmountData> datas, FmPosLitener listener) {
        mContext = context;
        this.datas = datas;
        this.mListener = listener;
    }

    public void setData(List<PayAmountData> datas) {
        this.datas = datas;
        this.notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        if (datas == null) {
            return 0;
        } else {
            return datas.size();
        }
    }

    @Override
    public Object getItem(int position) {
        if (datas == null) {
            return null;
        } else {
            return datas.get(position);
        }
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @SuppressLint("ViewHolder")
    @Override
    public View getView(int position, View view, ViewGroup parent) {
        ViewHolder holde;
        if (view == null) {
            view = LayoutInflater.from(mContext).inflate(R.layout.item_order_payeds, null);
            holde = new ViewHolder(view);
        } else {
            holde = (ViewHolder) view.getTag();
            if (holde == null) {
                view = LayoutInflater.from(mContext).inflate(R.layout.item_order_payeds, null);
                holde = new ViewHolder(view);
            }
        }
        if (holde != null) {
            PayAmountData item = (PayAmountData) getItem(position);
            holde.bindData(item);
        }

        return view;
    }

    class ViewHolder {
        TextView tvPayChanel;
        TextView tvPayAmount;
        ImageView ivBtnClear;

        ViewHolder(View view) {
            tvPayChanel = (TextView) view.findViewById(R.id.tv_pay_chanel);
            tvPayAmount = (TextView) view.findViewById(R.id.tv_pay_value);
            ivBtnClear = (ImageView) view.findViewById(R.id.iv_btn_clear);
            view.setTag(this);
        }

        void bindData(final PayAmountData data) {
            if (data == null) {
                return;
            } else {
                tvPayChanel.setText(data.getPayChanelName() + "");
                tvPayAmount.setText(data.getPayAmount() + "");
                ivBtnClear.setTag(data);
                ivBtnClear.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        PayAmountData data = (PayAmountData) view.getTag();
                        if (mListener != null) {
                            datas.remove(data);
                            OrderPaysAdapter.this.notifyDataSetChanged();
                            mListener.clearOnePays(data);
                        }
                    }
                });
            }
        }
    }
}
