package com.warmsoft.pos.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.warmsoft.pos.R;
import com.warmsoft.pos.bean.AccountRechargeHistoryModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by laole918 on 2016/3/19.
 */
public class RechargeHistoryRecyclerViewAdapter extends RecyclerView.Adapter<RechargeHistoryRecyclerViewAdapter.RecyclerViewHolder> {

    private Context mContext;
    List<AccountRechargeHistoryModel.DataInfo> historyLists = new ArrayList<>();

    public RechargeHistoryRecyclerViewAdapter(Context context, List<AccountRechargeHistoryModel.DataInfo> list) {
        this.mContext = context;
        this.historyLists = list;
    }

    public void setData(List<AccountRechargeHistoryModel.DataInfo> list) {
        this.historyLists = list;
        notifyDataSetChanged();
    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerViewHolder holder = new RecyclerViewHolder(LayoutInflater.from(
                mContext).inflate(R.layout.item_for_recharge_history, parent,
                false));
        return holder;
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, final int position) {
        AccountRechargeHistoryModel.DataInfo data = historyLists.get(position);

        if (data != null) {
            holder.tvRechargeAmount.setText(String.valueOf(data.getReceivedAmount()));
            holder.tvGetAmount.setText(String.valueOf(data.getInAccountAmount()));
            holder.tvCurrentAmount.setText(String.valueOf(data.getCurrentTotalAmount()));
            holder.tvRechargeTime.setText(String.valueOf(data.getChargeDatetime()));
            holder.tvStatusDesc.setText(TextUtils.isEmpty(String.valueOf(data.getStatusText())) ? "" : data.getStatusText());
            holder.tvOperation.setText(TextUtils.isEmpty(String.valueOf(data.getOperatorName())) ? "" : data.getOperatorName());
        }
    }

    @Override
    public int getItemCount() {
        return historyLists.size();
    }

    class RecyclerViewHolder extends RecyclerView.ViewHolder {
        public TextView tvRechargeAmount;
        public TextView tvGetAmount;
        public TextView tvCurrentAmount;
        public TextView tvRechargeTime;
        public TextView tvStatusDesc;
        public TextView tvOperation;

        public RecyclerViewHolder(View itemView) {
            super(itemView);
            tvRechargeAmount = (TextView) itemView.findViewById(R.id.id_for_recharge_history_amount);
            tvGetAmount = (TextView) itemView.findViewById(R.id.id_for_recharge_history_get_amount);
            tvCurrentAmount = (TextView) itemView.findViewById(R.id.id_for_recharge_history_current_amount);
            tvRechargeTime = (TextView) itemView.findViewById(R.id.id_for_recharge_history_time);
            tvStatusDesc = (TextView) itemView.findViewById(R.id.id_for_recharge_history_status_desc);
            tvOperation = (TextView) itemView.findViewById(R.id.id_for_recharge_history_operator);
        }
    }
}
