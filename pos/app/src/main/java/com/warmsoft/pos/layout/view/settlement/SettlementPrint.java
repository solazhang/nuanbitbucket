package com.warmsoft.pos.layout.view.settlement;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.widget.Button;

import com.warmsoft.pos.R;
import com.warmsoft.pos.dialog.NomelNoteDialog;
import com.warmsoft.pos.listener.LayoutSettleListener;

/**
 * 作者: lijinliu on 2016/8/10.
 * 邮箱：jinliu.li@warmsoft.com
 * 描述: view_seetlement_print.xml
 */
public class SettlementPrint implements View.OnClickListener {

    private Activity mActivity;
    private Context mContext;
    private LayoutSettleListener mListner;

    public Button btnAlldiscountp;//整单折扣
    public Button btnPartdiscount;//部分折扣
    public Button btnPlandiscount;//方案打折
    public Button btnPrintticket;//打印客户联

    public SettlementPrint(View rootView, Activity activity, LayoutSettleListener listener) {
        this.mActivity = activity;
        this.mContext = mActivity.getApplicationContext();
        this.mListner = listener;

        btnAlldiscountp = (Button) rootView.findViewById(R.id.btn_alldiscountp);
        btnPartdiscount = (Button) rootView.findViewById(R.id.btn_partdiscount);
        btnPlandiscount = (Button) rootView.findViewById(R.id.btn_plandiscount);
        btnPrintticket = (Button) rootView.findViewById(R.id.btn_printticket);
        initEvents();
    }

    private void initEvents() {
        btnAlldiscountp.setOnClickListener(this);
        btnPartdiscount.setOnClickListener(this);
        btnPlandiscount.setOnClickListener(this);
        btnPrintticket.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_alldiscountp:
                new NomelNoteDialog(mActivity, "开发中", "功能开发中，请关注产品升级!", "确定", null);
                break;
            case R.id.btn_partdiscount:
                new NomelNoteDialog(mActivity, "开发中", "功能开发中，请关注产品升级!", "确定", null);
                break;
            case R.id.btn_plandiscount:
                new NomelNoteDialog(mActivity, "开发中", "功能开发中，请关注产品升级!", "确定", null);
                break;
            case R.id.btn_printticket://打印客户联
                if (mListner != null) {
                    mListner.print();
                }
                break;
        }
    }
}
