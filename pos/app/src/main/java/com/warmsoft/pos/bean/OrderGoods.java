package com.warmsoft.pos.bean;

import com.warmsoft.pos.litepal.data.CustomerList;
import com.warmsoft.pos.litepal.data.GoodItemInfo;
import com.warmsoft.pos.litepal.data.LitepalHelper;
import com.warmsoft.pos.litepal.data.OrderGoodsData;
import com.warmsoft.pos.litepal.data.OrderInfoData;
import com.warmsoft.pos.litepal.data.PetData;
import com.warmsoft.pos.litepal.data.SettlementData;
import com.warmsoft.pos.util.Trace;

import org.litepal.crud.DataSupport;

import java.util.ArrayList;
import java.util.List;

/**
 * 作者: lijinliu on 2016/8/26.
 * 邮箱：jinliu.li@warmsoft.com
 * 描述: 包括所有数据 ，已下单，和未下单数据
 */
public class OrderGoods {

    private List<OrderGoodsData> allGoodList = new ArrayList<OrderGoodsData>();
    private List<SettlementData> unSureGoodList = new ArrayList<SettlementData>();

    /**
     * 添加商品
     *
     * @param goodInfo
     * @param orderData
     */
    public void addGoods(GoodItemInfo goodInfo, OrderInfoData orderData) {
        boolean allContain = false;
        boolean unSureContain = false;

        for (OrderGoodsData medicine : allGoodList) {
            if (goodInfo.getGoodId() == medicine.getFeeItemId()) {
                medicine.setCount(medicine.getCount() + 1);
                medicine.setIsSure(0);
                medicine.setTotalAmount(medicine.getAmount() * medicine.getCount());
                orderData.setCountAmount(orderData.getCountAmount() + medicine.getAmount());
                orderData.setCountGoods(orderData.getCountGoods() + 1);
                allContain = true;
                break;
            }
        }

        if (!allContain) {
            SettlementData medicine = createSettleData(goodInfo, orderData);
            OrderGoodsData goodData = createGoodData(goodInfo, orderData);
            allGoodList.add(goodData);
            unSureGoodList.add(medicine);
            orderData.setCountAmount(orderData.getCountAmount() + medicine.getAmount());
            orderData.setCountGoods(orderData.getCountGoods() + 1);
        } else {
            for (SettlementData medicine : unSureGoodList) {
                if (goodInfo.getGoodId() == medicine.getFeeItemId()) {
                    medicine.setCount(medicine.getCount() + 1);
                    medicine.setTotalAmount(medicine.getAmount() * medicine.getCount());
                    unSureContain = true;
                    break;
                }
            }
            if (!unSureContain) {
                SettlementData medicine = createSettleData(goodInfo, orderData);
                unSureGoodList.add(medicine);
            }
        }
    }


    /**
     * 商品数量修改，负减正加
     *
     * @param orderData
     * @param goodInfo
     * @param newSize
     */
    public void setGoodSize(OrderInfoData orderData, OrderGoodsData goodInfo, float newSize) {
        float sureSize = 0;
        for (OrderGoodsData medicine : allGoodList) {
            if (goodInfo.getFeeItemId() == medicine.getFeeItemId()) {
                float oldSize = medicine.getCount();
                if (newSize > 0) {//删除部分
                    medicine.setCount(newSize);
                    medicine.setTotalAmount(medicine.getAmount() * newSize);
                    float newAmount = orderData.getCountAmount() + medicine.getAmount() * newSize - medicine.getAmount() * oldSize;
                    orderData.setCountAmount(newAmount);
                    orderData.setCountGoods(orderData.getCountGoods() - oldSize + newSize);
                } else {//全部删除
                    allGoodList.remove(medicine);
                    orderData.setCountAmount(orderData.getCountAmount() - medicine.getAmount() * oldSize);
                    orderData.setCountGoods(orderData.getCountGoods() - oldSize);
                }
                sureSize = medicine.getSureCount();
                break;
            }
        }

        boolean unSureContain = false;
        for (SettlementData medicine : unSureGoodList) {
            if (goodInfo.getFeeItemId() == medicine.getFeeItemId()) {
                if (newSize > sureSize) {//删除部分
                    medicine.setCount(newSize - sureSize);
                    medicine.setTotalAmount(medicine.getAmount() * medicine.getCount());
                } else {//全部删除
                    unSureGoodList.remove(medicine);
                }
                unSureContain = true;
                break;
            }
        }
        if (!unSureContain) {
            unSureGoodList.add(createSettleData(goodInfo, orderData, newSize - sureSize));
        }
        //Trace.e("******************sureSize:" + sureSize + " newSize" + newSize);
    }

    //更换会员信息
    public void changeMemberInfo(CustomerList custom, PetData pet) {
        for (SettlementData medicine : unSureGoodList) {
            medicine.setPetId(pet.getPetId());
            medicine.setPetName(pet.getPetName());
            medicine.setCustomerId(Integer.parseInt(custom.getCustomerId()));
            medicine.setCustomerName(custom.getName());
        }
        for (OrderGoodsData medicine : allGoodList) {
            medicine.setPetId(pet.getPetId());
            medicine.setPetName(pet.getPetName());
            medicine.setCustomerId(Integer.parseInt(custom.getCustomerId()));
            medicine.setCustomerName(custom.getName());
        }
    }

    public int getUnSureize() {
        if (unSureGoodList != null) {
            return unSureGoodList.size();
        } else {
            return 0;
        }
    }

    /**
     * 提交定单:数据写入本地
     *
     * @return
     */
    public boolean handInOrder(long batchNo) {
        if (unSureGoodList != null && unSureGoodList.size() > 0) {
            try {
                for (SettlementData medicine : unSureGoodList) {
                    medicine.setBatchNo(batchNo);
                }
                DataSupport.saveAll(unSureGoodList);
                unSureGoodList = new ArrayList<SettlementData>();//清空
                for (OrderGoodsData medicine : allGoodList) {//标记商品已确认,并更新确认商品数量
                    medicine.setIsSure(1);
                    medicine.setSureCount(medicine.getCount());
                }
                return true;
            } catch (Exception e) {
                Trace.e(e);
                return false;
            }
        }
        return false;
    }

    /**
     * 定单收款完成
     */
    public void orderPayed(int orderNo) {
        if (allGoodList != null && allGoodList.size() > 0) {
            for (OrderGoodsData data : allGoodList) {
                data.setActlyPayed(data.getTotalAmount());//实付金额
            }
            LitepalHelper.getInstance().updateActlyPayed(orderNo);//更新订单实付金额
        }
    }

    public List<OrderGoodsData> getAllGoodList() {
        return allGoodList;
    }

    public float getNeedAmount() {
        float amount = 0;
        if (allGoodList != null && allGoodList.size() > 0) {
            for (OrderGoodsData medicine : allGoodList) {
                amount = amount + medicine.getAmount() * medicine.getSureCount();
            }
        }
        return amount;
    }

    public List<SettlementData> getUnsureGoodList() {
        return unSureGoodList;
    }

    public boolean allIsSure() {
        for (OrderGoodsData medicine : allGoodList) {
            if (medicine.getIsSure() == 0) {
                return false;
            }
        }
        return true;
    }

    public void setAllGoodList(List<SettlementData> goodsDatas) {
        if (goodsDatas != null && goodsDatas.size() > 0) {
            for (SettlementData settleData : goodsDatas) {
                boolean allContain = false;
                for (OrderGoodsData medicine : allGoodList) {
                    if (settleData.getFeeItemId() == medicine.getFeeItemId()) {
                        medicine.setCount(medicine.getCount() + settleData.getCount());
                        medicine.setSureCount(medicine.getCount());
                        medicine.setTotalAmount(medicine.getAmount() * medicine.getCount());
                        allContain = true;
                        break;
                    }
                }
                if (!allContain) {
                    allGoodList.add(createGoodData(settleData));
                }
            }
        }
    }

    private SettlementData createSettleData(GoodItemInfo goodInfo, OrderInfoData orderData) {
        SettlementData medicine = new SettlementData();
        medicine.setOrderNo(orderData.getOrderNo());
        medicine.setFeeItemId(goodInfo.getGoodId());
        medicine.setOrgId(Integer.parseInt(goodInfo.getOrgId()));
        medicine.setCount(1);
        medicine.setAmount(Float.parseFloat(goodInfo.getOutstorePrice()));//单价,默认是卖出价
        medicine.setOutstorePrice(Float.parseFloat(goodInfo.getOutstorePrice()));
        medicine.setFeeItemName(goodInfo.getDrugsName());
        medicine.setDrugType(Integer.parseInt(goodInfo.getDrugType()));
        medicine.setInstorePrice(Float.parseFloat(goodInfo.getInstorePrice()));
        medicine.setMemberPrice(Float.parseFloat(goodInfo.getMemberPrice()));
        medicine.setTotalAmount(medicine.getAmount());
        medicine.setCustomerId(orderData.getCustomerId());
        medicine.setCustomerName(orderData.getCustomerName());
        medicine.setPetId(orderData.getPetId());
        medicine.setPetName(orderData.getPetName());
        medicine.setDiscountRate(1);//折扣率
        medicine.setDiscountAmount(0);//折扣金额
        return medicine;
    }

    private SettlementData createSettleData(OrderGoodsData goodInfo, OrderInfoData orderData, float size) {
        SettlementData medicine = new SettlementData();
        medicine.setOrderNo(orderData.getOrderNo());
        medicine.setFeeItemId(goodInfo.getFeeItemId());
        medicine.setOrgId(goodInfo.getOrgId());
        medicine.setCount(size);
        medicine.setAmount(goodInfo.getOutstorePrice());//单价,默认是卖出价
        medicine.setOutstorePrice(goodInfo.getOutstorePrice());
        medicine.setFeeItemName(goodInfo.getFeeItemName());
        medicine.setDrugType(goodInfo.getDrugType());
        medicine.setInstorePrice(goodInfo.getInstorePrice());
        medicine.setMemberPrice(goodInfo.getMemberPrice());
        medicine.setTotalAmount(medicine.getAmount() * size);
        medicine.setCustomerId(orderData.getCustomerId());
        medicine.setCustomerName(orderData.getCustomerName());
        medicine.setPetId(orderData.getPetId());
        medicine.setPetName(orderData.getPetName());
        medicine.setDiscountRate(1);//折扣率
        medicine.setDiscountAmount(0);//折扣金额
        medicine.setGoodDesc(goodInfo.getGoodDesc());
        return medicine;
    }

    private OrderGoodsData createGoodData(GoodItemInfo goodInfo, OrderInfoData orderData) {
        OrderGoodsData goodData = new OrderGoodsData();
        goodData.setOrderNo(orderData.getOrderNo());
        goodData.setFeeItemId(goodInfo.getGoodId());
        goodData.setOrgId(Integer.parseInt(goodInfo.getOrgId()));
        goodData.setCount(1);
        goodData.setAmount(Float.parseFloat(goodInfo.getOutstorePrice()));//单价,默认是卖出价
        goodData.setOutstorePrice(Float.parseFloat(goodInfo.getOutstorePrice()));
        goodData.setFeeItemName(goodInfo.getDrugsName());
        goodData.setDrugType(Integer.parseInt(goodInfo.getDrugType()));
        goodData.setInstorePrice(Float.parseFloat(goodInfo.getInstorePrice()));
        goodData.setMemberPrice(Float.parseFloat(goodInfo.getMemberPrice()));
        goodData.setTotalAmount(goodData.getAmount());
        goodData.setCustomerId(orderData.getCustomerId());
        goodData.setCustomerName(orderData.getCustomerName());
        goodData.setPetId(orderData.getPetId());
        goodData.setPetName(orderData.getPetName());
        goodData.setDiscountRate(1);//默认无折扣率
        goodData.setDiscountAmount(0);//默认折扣金额为0
        return goodData;
    }

    private OrderGoodsData createGoodData(SettlementData settleData) {
        OrderGoodsData goodsData = new OrderGoodsData();
        goodsData.setOrderNo(settleData.getOrderNo());
        goodsData.setAmount(settleData.getAmount());
        goodsData.setCount(settleData.getCount());
        goodsData.setPetId(settleData.getPetId());
        goodsData.setPetName(settleData.getPetName());
        goodsData.setCustomerId(settleData.getCustomerId());
        goodsData.setCustomerName(settleData.getCustomerName());
        goodsData.setFeeItemId(settleData.getFeeItemId());
        goodsData.setFeeItemName(settleData.getFeeItemName());
        goodsData.setDrugType(settleData.getDrugType());
        goodsData.setFeeItemEnglishName(settleData.getFeeItemEnglishName());
        goodsData.setOrgId(settleData.getOrgId());
        goodsData.setSellerId(settleData.getSellerId());
        goodsData.setCost(settleData.getCost());
        goodsData.setChargeDatetime(settleData.getChargeDatetime());
        goodsData.setDataChannel(settleData.getDataChannel());
        goodsData.setDiscountRate(settleData.getDiscountRate());
        goodsData.setCellPhone(settleData.getCellPhone());
        goodsData.setDiscountAmount(settleData.getDiscountAmount());
        goodsData.setPayedDate(settleData.getPayedDate());
        goodsData.setActlyPayed(settleData.getActlyPayed());
        goodsData.setTotalAmount(settleData.getTotalAmount());
        goodsData.setIsSure(settleData.getIsSure());
        goodsData.setOutstorePrice(settleData.getOutstorePrice());
        goodsData.setInstorePrice(settleData.getInstorePrice());
        goodsData.setMemberPrice(settleData.getMemberPrice());
        goodsData.setSureCount(settleData.getCount());
        goodsData.setGoodDesc(settleData.getGoodDesc());
        goodsData.setIsSure(1);
        return goodsData;
    }

    // 商品备注记录
    public void setGoodDesc(int goodId, String goodDesc) {
        if (unSureGoodList != null && unSureGoodList.size() > 0) {
            for (SettlementData medicine : unSureGoodList) {
                if (medicine.getFeeItemId() == goodId) {
                    medicine.setGoodDesc(goodDesc);
                }
            }
        }
    }

}
