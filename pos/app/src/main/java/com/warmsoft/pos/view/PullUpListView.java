package com.warmsoft.pos.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.ListView;
import android.widget.TextView;

import com.warmsoft.pos.R;
import com.warmsoft.pos.util.Trace;

/**
 * 作者: lijinliu on 2016/9/18.
 * 邮箱：jinliu.li@warmsoft.com
 * 描述: 上拉刷新ListView
 */
public class PullUpListView extends ListView implements OnScrollListener {

    private LayoutInflater inflater;
    private View footer;

    private TextView noData;
    private TextView loadFull;
    private TextView more;

    private boolean loadEnable = true;// 开启或者关闭加载更多功能
    private boolean isLoadFull;
    private int pageSize = 10;

    private OnLoadListener onLoadListener;

    public PullUpListView(Context context) {
        super(context);
        initView(context);
    }

    public PullUpListView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView(context);
    }

    public PullUpListView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        initView(context);
    }

    // 加载更多监听
    public void setOnLoadListener(OnLoadListener onLoadListener) {
        this.loadEnable = true;
        this.onLoadListener = onLoadListener;
    }

    public boolean isLoadEnable() {
        return loadEnable;
    }

    // 这里的开启或者关闭加载更多，并不支持动态调整
    public void setLoadEnable(boolean loadEnable) {
        this.loadEnable = loadEnable;
        this.removeFooterView(footer);
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    // 初始化组件
    private void initView(Context context) {

        inflater = LayoutInflater.from(context);
        footer = inflater.inflate(R.layout.listview_loadbar, null);
        loadFull = (TextView) footer.findViewById(R.id.loadFull);
        noData = (TextView) footer.findViewById(R.id.noData);
        more = (TextView) footer.findViewById(R.id.more);

        this.addFooterView(footer);
        this.setOnScrollListener(this);
    }

    public void onLoad() {
        if (onLoadListener != null) {
            onLoadListener.onLoad();
        }
    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {
        ifNeedLoad(view, scrollState);
    }

    // 根据listview滑动的状态判断是否需要加载更多
    private void ifNeedLoad(AbsListView view, int scrollState) {
        Trace.e(loadEnable + "");
        if (!loadEnable) {
            return;
        }
        try {
            Trace.e(isLoadFull + "");
            if (scrollState == OnScrollListener.SCROLL_STATE_IDLE &&view.getLastVisiblePosition() == view.getPositionForView(footer) && !isLoadFull) {
                onLoad();
            }
        } catch (Exception e) {
        }
    }

    /**
     * 这个方法是根据结果的大小来决定footer显示的。
     * <p>
     * 这里假定每次请求的条数为10。如果请求到了10条。则认为还有数据。如过结果不足10条，则认为数据已经全部加载，这时footer显示已经全部加载
     * </p>
     *
     * @param resultSize
     */
    public void setResultSize(int resultSize) {
        if (resultSize == 0) {
            isLoadFull = true;
            loadFull.setVisibility(View.GONE);
            more.setVisibility(View.GONE);
            noData.setVisibility(View.VISIBLE);
        } else if (resultSize > 0 && resultSize < pageSize) {
            isLoadFull = true;
            loadFull.setVisibility(View.VISIBLE);
            more.setVisibility(View.GONE);
            noData.setVisibility(View.GONE);
        } else if (resultSize == pageSize) {
            isLoadFull = false;
            loadFull.setVisibility(View.GONE);
            more.setVisibility(View.VISIBLE);
            noData.setVisibility(View.GONE);
        }
    }

    /*
     * 定义加载更多接口
     */
    public interface OnLoadListener {
        public void onLoad();
    }

}
