package com.warmsoft.pos.update;

import java.io.Serializable;

/**
 * Created by brooks on 16/6/28.
 */
public class UpgradeModel implements Serializable {
    int code;
    String message;

    DataInfo data;

    public static class DataInfo implements Serializable {
        int type;
        String download;
        String feature;

        public int getType() {
            return type;
        }

        public void setType(int type) {
            this.type = type;
        }

        public String getDownload() {
            return download;
        }

        public void setDownload(String download) {
            this.download = download;
        }

        public String getFeature() {
            return feature;
        }

        public void setFeature(String feature) {
            this.feature = feature;
        }
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataInfo getData() {
        return data;
    }

    public void setData(DataInfo data) {
        this.data = data;
    }
}
