package com.warmsoft.pos.bean;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 作者: lijinliu on 2016/8/17.
 * 邮箱：jinliu.li@warmsoft.com
 * 描述: 订单打印信息
 */
public class OrderPrintModel {
    private String orgName;//机构名称
    private int orderNo;//地定单号
    private Date orderDate;//定单日期
    private Date printDate;//打印日期
    private String opName;//收银员
    private float countGoods;//数量
    private float countAmount;//总计
    private float cashAmount;//现金
    private float bankCardAmount;//银行卡
    private float webChatAmount;//微信
    private float aliPayAmount;//支付宝
    private float accountAmount;//账户支付
    private float prePayAmount;//押金支付
    private float memberCardAmount;//会员卡支付
    private float changeAmount;//找零
    private float discountAmount;//抹零金额
    private String advWords;//广告语
    private float accountBalance;//账户余额
    private float prePayBalance;//押金余额
    private float memberCardBalance;//会员卡余额

    private List<GoodInfo> goodInfoList = new ArrayList<GoodInfo>();

    public static class GoodInfo {
        private String name;//名称
        private float count;//数量
        private float price;//单价
        private float amount;//总金额

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public float getCount() {
            return count;
        }

        public void setCount(float count) {
            this.count = count;
        }

        public float getPrice() {
            return price;
        }

        public void setPrice(float price) {
            this.price = price;
        }

        public float getAmount() {
            return amount;
        }

        public void setAmount(float amount) {
            this.amount = amount;
        }
    }

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    public int getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(int orderNo) {
        this.orderNo = orderNo;
    }

    public Date getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(Date orderDate) {
        this.orderDate = orderDate;
    }

    public Date getPrintDate() {
        return printDate;
    }

    public void setPrintDate(Date printDate) {
        this.printDate = printDate;
    }

    public String getOpName() {
        return opName;
    }

    public void setOpName(String opName) {
        this.opName = opName;
    }

    public float getCountGoods() {
        return countGoods;
    }

    public void setCountGoods(float countGoods) {
        this.countGoods = countGoods;
    }

    public float getCountAmount() {
        return countAmount;
    }

    public void setCountAmount(float countAmount) {
        this.countAmount = countAmount;
    }

    public float getCashAmount() {
        return cashAmount;
    }

    public void setCashAmount(float cashAmount) {
        this.cashAmount = cashAmount;
    }

    public float getBankCardAmount() {
        return bankCardAmount;
    }

    public void setBankCardAmount(float bankCardAmount) {
        this.bankCardAmount = bankCardAmount;
    }

    public float getWebChatAmount() {
        return webChatAmount;
    }

    public void setWebChatAmount(float webChatAmount) {
        this.webChatAmount = webChatAmount;
    }

    public float getAliPayAmount() {
        return aliPayAmount;
    }

    public void setAliPayAmount(float aliPayAmount) {
        this.aliPayAmount = aliPayAmount;
    }

    public float getChangeAmount() {
        return changeAmount;
    }

    public void setChangeAmount(float changeAmount) {
        this.changeAmount = changeAmount;
    }

    public float getDiscountAmount() {
        return discountAmount;
    }

    public void setDiscountAmount(float discountAmount) {
        this.discountAmount = discountAmount;
    }

    public List<GoodInfo> getGoodInfoList() {
        return goodInfoList;
    }

    public void setGoodInfoList(List<GoodInfo> goodInfoList) {
        this.goodInfoList = goodInfoList;
    }

    public String getAdvWords() {
        return advWords;
    }

    public void setAdvWords(String advWords) {
        this.advWords = advWords;
    }

    public float getAccountAmount() {
        return accountAmount;
    }

    public void setAccountAmount(float accountAmount) {
        this.accountAmount = accountAmount;
    }

    public float getPrePayAmount() {
        return prePayAmount;
    }

    public void setPrePayAmount(float prePayAmount) {
        this.prePayAmount = prePayAmount;
    }

    public float getMemberCardAmount() {
        return memberCardAmount;
    }

    public void setMemberCardAmount(float memberCardAmount) {
        this.memberCardAmount = memberCardAmount;
    }

    public float getAccountBalance() {
        return accountBalance;
    }

    public void setAccountBalance(float accountBalance) {
        this.accountBalance = accountBalance;
    }

    public float getPrePayBalance() {
        return prePayBalance;
    }

    public void setPrePayBalance(float prePayBalance) {
        this.prePayBalance = prePayBalance;
    }

    public float getMemberCardBalance() {
        return memberCardBalance;
    }

    public void setMemberCardBalance(float memberCardBalance) {
        this.memberCardBalance = memberCardBalance;
    }
}
