package com.warmsoft.pos.net;

import retrofit.GsonConverterFactory;
import retrofit.Retrofit;
import retrofit.RxJavaCallAdapterFactory;

/**
 * Created by brooks on 16/1/13.
 */
public class RetrofitManager {
    private static RetrofitManager instance = null;
    private APIClient service;
    private APIClient phpService;
    private Retrofit retrofit;

    public static RetrofitManager getInstance() {
        if (instance == null) {
            synchronized (RetrofitManager.class) {
                instance = new RetrofitManager();
            }
        }

        return instance;
    }

    public APIClient initAPIService() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(APIClient.url)
                .client(OkHttpClientManager.getInstance())
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .build();

        setRetrofit(retrofit);
        service = retrofit.create(APIClient.class);

        return service;
    }

    public APIClient getAPIService() {
        if (service != null) {
            return service;
        }

        return initAPIService();
    }

    public Retrofit getRetrofit() {
        return retrofit;
    }

    public void setRetrofit(Retrofit retrofit) {
        this.retrofit = retrofit;
    }

    //提供给升级使用
    public APIClient initPhpAPIService() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(APIClient.upgrade_url)
                .client(OkHttpClientManager.getInstance())
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .build();

        setRetrofit(retrofit);
        phpService = retrofit.create(APIClient.class);

        return phpService;
    }

    public APIClient getPhpAPIService() {
        if (phpService != null) {
            return phpService;
        }

        return initPhpAPIService();
    }
}


