package com.warmsoft.pos.net;

import com.warmsoft.pos.bean.AccountInfoModel;
import com.warmsoft.pos.bean.AccountRechargeHistoryModel;
import com.warmsoft.pos.bean.AccountRechargeModel;
import com.warmsoft.pos.bean.AddOrderModel;
import com.warmsoft.pos.bean.AmountsModel;
import com.warmsoft.pos.bean.AuthModel;
import com.warmsoft.pos.bean.BackCardModel;
import com.warmsoft.pos.bean.CardKindModel;
import com.warmsoft.pos.bean.CardListModel;
import com.warmsoft.pos.bean.CustomerOrderModel;
import com.warmsoft.pos.bean.DeletePaymentModel;
import com.warmsoft.pos.bean.DepositRefundModel;
import com.warmsoft.pos.bean.GetPetModel;
import com.warmsoft.pos.bean.GetPetOldModel;
import com.warmsoft.pos.bean.GoodDataModel;
import com.warmsoft.pos.bean.MemberRechargeModel;
import com.warmsoft.pos.bean.MemberReturnModel;
import com.warmsoft.pos.bean.MembersModel;
import com.warmsoft.pos.bean.PetsCategoryModel;
import com.warmsoft.pos.bean.PrePledgeRechargeModel;
import com.warmsoft.pos.bean.RemoveRechargeModel;
import com.warmsoft.pos.bean.ResultModel;
import com.warmsoft.pos.bean.SettlementModel;
import com.warmsoft.pos.update.UpgradeModel;
import com.warmsoft.pos.wxpay.WePaySettingsModel;

import java.util.Map;

import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.QueryMap;
import rx.Observable;

/**
 * Created by brooks on 16/1/13.
 */
public interface APIClient {
//        String url = "http://192.168.31.197:27607";
    String url = "http://padpos.warmsoft.com";
    String upgrade_url = "http://pos.doctorwarm.com";

    /**
     * App_User.PreLogin
     * 该接口无需授权
     *
     * @return
     */
    @POST("/Api/WarmPos/Api/ProductCore/PMedicinesByOrgId")
    Observable<GoodDataModel> getGoodData(@Body String json);

    /**
     * App_User.PreLogin
     * 该接口无需授权
     *
     * @return
     */
    @POST("/Api/WarmPos/Api/CustomerCore/CustomerList")
    Observable<MembersModel> getCustomerList(@Body String json);

    /**
     * App_User.PreLogin
     * 该接口无需授权
     *
     * @return
     */
    @POST("/Api/WarmPos/Api/SystemCore/UserLogin")
    Observable<AuthModel> login(@Body String json);

    /**
     * App_User.PreLogin
     * 该接口无需授权O
     *
     * @return
     */
    @POST("/Api/WarmPos/Api/SystemCore/AddOrder")
    Observable<AddOrderModel> addOrer(@Body String json);

    /**
     * 结算
     * 该接口无需授权
     *
     * @return
     */
    @POST("/Api/WarmPos/Api/SystemCore/UpdatePaymentAndDetalis")
    Observable<SettlementModel> updatePaymentAndDetalis(@Body String json);

    /**
     * 商品数据直接提交结算(for 离线数据)
     * 该接口无需授权
     *
     * @return
     */
    @POST("/Api/WarmPos/Api/SystemCore/InsertPaymentAndDetalis")
    Observable<SettlementModel> insertPaymentAndDetalis(@Body String json);

    /**
     * App_User.PreLogin
     * 该接口无需授权
     *
     * @return
     */
    @POST("/Api/WarmPos/Api/SystemCore/SystemUpgrade")
    Observable<UpgradeModel> checkUpdate(@Body String json);

    /**
     * 获取宠物信息
     *
     * @return
     */
    @POST("/Api/WarmPos/Api/CustomerCore/GetPetListByCutomerId")
    Observable<GetPetOldModel> getPetListByCutomerId(@Body String json);

    /**
     * 获取宠物信息
     *
     * @return
     */
    @POST("/Api/WarmPos/Api/CustomerCore/GetPetListModelByCutomerId")
    Observable<GetPetModel> GetPetListModelByCutomerId(@Body String json);

    /**
     * 获取机构所有订单 status -1 代表所有订单，0未付款订单，1已付款订单
     *
     * @return
     */
    @POST("/Api/WarmPos/Api/SystemCore/CpaymentGetByOrgId")
    Observable<CustomerOrderModel> getCpaymentByOrgId(@Body String json);

    /**
     * 获取客户未付款订单
     *
     * @return
     */
    @POST("/Api/WarmPos/Api/SystemCore/GetOrderbyCustomer")
    Observable<CustomerOrderModel> getOrderbyCustomer(@Body String json);

    /**
     * 未结算订单退端接口
     *
     * @return
     */
    @POST("/Api/WarmPos/Api/SystemCore/DeletedPaymentDetalisById")
    Observable<DeletePaymentModel> deletedPaymentDetalisById(@Body String json);

    /**
     * 已结算订单退端接口
     *
     * @return
     */
    @POST("/Api/WarmPos/Api/SystemCore/DeletePaymentById")
    Observable<DeletePaymentModel> deletePaymentById(@Body String json);

    /**
     * 记账
     *
     * @return
     */
    @POST("/Api/WarmPos/Api/SystemCore/CheckOutForKeepAccount")
    Observable<DeletePaymentModel> keepAccount(@Body String json);

    /**
     * 获取会员卡及打折
     *
     * @return
     */
    @POST("/Api/WarmPos/Api/CustomerCore/GetCardKind")
    Observable<CardKindModel> getCardKind(@Body String json);

    /**
     * 获取宠物品种
     *
     * @return
     */
    @POST("/Api/WarmPos/Api/SystemCore/GetPetsKind")
    Observable<ResultModel> getPetsKind(@Body String json);

    /**
     * 获取宠物种类
     *
     * @return
     */
    @POST("/Api/WarmPos/Api/SystemCore/GetPetsCategory")
    Observable<PetsCategoryModel> getPetsCategory(@Body String json);

    /**
     * 增加会员
     *
     * @return
     */
    @POST("/Api/WarmPos/Api/CustomerCore/SaveCustomer")
    Observable<MemberReturnModel> saveCustomer(@Body String json);


    /**
     * App_My.chkVer
     * 该接口需授权
     *
     * @param map
     * @return
     */
    @GET("/")
    Observable<UpgradeModel> chkVer(@QueryMap Map<String, String> map);

    /**
     * 用户卡内金额
     *
     * @return
     */
    @POST("/Api/WarmPos/Api/CustomerCore/GetAmountsByCustomerId")
    Observable<AmountsModel> amount(@Body String json);

    /**
     * 用户卡信息
     *
     * @return
     */
    @POST("/Api/WarmPos/Api/CustomerCore/GetCMembersCardListByCustomerId")
    Observable<CardListModel> membersCardList(@Body String json);

    /**
     * 用户卡信息
     *
     * @return
     */
    @POST("/api/WarmPos/Api/SystemCore/GetWePaySettings")
    Observable<WePaySettingsModel> GetWePaySettings(@Body String json);

    /**
     * 3.1.1	账户充值记录
     *
     * @return
     */
    @POST("/api/WarmPos/Api/SystemCore/GetAmountHistoires")
    Observable<AccountRechargeHistoryModel> getAmountHistoires(@Body String json);

    /**
     * 3.1.1	获取账户信息
     *
     * @return
     */
    @POST("/api/WarmPos/Api/SystemCore/GetUserMoneyInFo")
    Observable<AccountInfoModel> getUserMoneyInFo(@Body String json);

    /**
     * 3.1.1	账户充值
     *
     * @return
     */
    @POST("/api/WarmPos/Api/SystemCore/AccountRecharge")
    Observable<AccountRechargeModel> accountRecharge(@Body String json);

    /**
     * 3.1.2	账户退款
     *
     * @return
     */
    @POST("/api/WarmPos/Api/SystemCore/RetirementAccount")
    Observable<RemoveRechargeModel> retirementAccount(@Body String json);

    /**
     * 3.1.1	押金充值
     *
     * @return
     */
    @POST("/api/WarmPos/Api/SystemCore/DepositRecharge")
    Observable<PrePledgeRechargeModel> depositRecharge(@Body String json);

    /**
     * 3.3.2	押金退款
     *
     * @return
     */
    @POST("/api/WarmPos/Api/SystemCore/DepositRefund")
    Observable<DepositRefundModel> depositRefund(@Body String json);

    /**
     * 3.2.4	会员卡充值,即可以充值,也可以退款
     * 账单类型 0:消费 1：充值 2：退款
     *
     * @return
     */
    @POST("/api/WarmPos/Api/SystemCore/CardRecharge")
    Observable<MemberRechargeModel> cardRecharge(@Body String json);


    @POST("/api/WarmPos/Api/SystemCore/BackCard")
    Observable<BackCardModel> backCard(@Body String json);
}
