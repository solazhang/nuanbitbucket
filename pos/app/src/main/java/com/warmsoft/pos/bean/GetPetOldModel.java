package com.warmsoft.pos.bean;

import com.warmsoft.pos.litepal.data.PetData;

import java.io.Serializable;
import java.util.List;

/**
 * 作者: lijinliu on 2016/8/9.
 * 邮箱：jinliu.li@warmsoft.com
 * 描述: 老接口--宠物信息
 */
public class GetPetOldModel implements Serializable {
    int code;
    String message;
    List<PetData> Data;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<PetData> getData() {
        return Data;
    }

    public void setData(List<PetData> data) {
        Data = data;
    }
}
