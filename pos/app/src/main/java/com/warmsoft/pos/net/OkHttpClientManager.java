package com.warmsoft.pos.net;

import com.squareup.okhttp.Interceptor;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;
import com.squareup.okhttp.ResponseBody;
import com.warmsoft.base.MyApplication;
import com.warmsoft.pos.util.LogUtil;
import com.warmsoft.pos.util.OAuthUtils;

import java.io.IOException;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.util.concurrent.TimeUnit;

import okio.Buffer;

/**
 * Created by brooks on 16/1/26.
 */
public class OkHttpClientManager {
    private static final String TAG = "OkHttpClientManager";
    private static OkHttpClient sInstance;

    private static final int READ_TIME_OUT = 30; //建议15s
    private static final int CONNECT_TIME_OUT = 30;

    public static OkHttpClient getInstance() {
        if (sInstance == null) {
            synchronized (OkHttpClientManager.class) {
                if (sInstance == null) {
                    sInstance = new OkHttpClient();
                    //cookie enabled
                    sInstance.setCookieHandler(new CookieManager(null, CookiePolicy.ACCEPT_ORIGINAL_SERVER));
                    //从主机读取数据超时
                    sInstance.setReadTimeout(READ_TIME_OUT, TimeUnit.SECONDS);
                    //连接主机超时
                    sInstance.setConnectTimeout(CONNECT_TIME_OUT, TimeUnit.SECONDS);
                    sInstance.interceptors().add(new LoggingInterceptor());
                }
            }
        }
        return sInstance;
    }

    /**
     * see http://stackoverflow.com/questions/24952199/okhttp-enable-logs
     */
    static class LoggingInterceptor implements Interceptor {

        @Override
        public Response intercept(Chain chain) throws IOException {
            Request original = chain.request();

            Request request = original.newBuilder()
                    .header("Content-Type", "application/json;charset=utf-8")
                    .header("Token", OAuthUtils.getInstance().getToken(MyApplication.getContext()))
                    .header("Content-Encoding", "gzip")
                    .method(original.method(), original.body())
                    .build();

            LogUtil.error("/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////");
            LogUtil.error("request info:");

            if (!request.method().equalsIgnoreCase("GET")) { //Get方法不使用
                LogUtil.error(bodyToString(request));
            }

            long t1 = System.nanoTime();
            LogUtil.error("request url:" + String.valueOf(request.url()));
            LogUtil.error("header:" + String.valueOf(request.headers()));
            LogUtil.error("\\n\\n");
            Response response = chain.proceed(request);

            long t2 = System.nanoTime();
            LogUtil.error("total time:" + String.valueOf((t2 - t1) / 1e6d) + "ms");
            LogUtil.error("okhttpurl" + String.valueOf(response.request().url()));

            String bodyString = response.body().string();

            LogUtil.error("response info:");
            LogUtil.error(bodyString);
            LogUtil.error("/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////");

            LogUtil.error("okhttpurl" + "\n" + bodyString);

            return response.newBuilder()
                    .body(ResponseBody.create(response.body().contentType(), bodyString))
                    .build();
        }
    }

    private static String bodyToString(final Request request) {

        try {
            final Request copy = request.newBuilder().build();
            final Buffer buffer = new Buffer();
            copy.body().writeTo(buffer);
            return buffer.readUtf8();
        } catch (final IOException e) {
            return "did not work";
        }
    }
}
