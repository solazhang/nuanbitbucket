package com.warmsoft.pos.print;

import android.app.Activity;
import android.content.res.Resources;
import android.os.RemoteException;
import android.widget.Toast;

import com.android.print.demo.sunmi.AidlUtil;
import com.android.print.demo.sunmi.BytesUtil;
import com.android.print.demo.sunmi.ESCUtil;
import com.android.print.sdk.PrinterConstants;
import com.android.print.sdk.PrinterInstance;
import com.android.print.sdk.Table;
import com.warmsoft.pos.R;
import com.warmsoft.pos.bean.OrderPrintModel;
import com.warmsoft.pos.util.LogUtil;
import com.warmsoft.pos.util.OrderHelper;
import com.warmsoft.pos.util.TimeUtils;

import woyou.aidlservice.jiuiv5.IWoyouService;

/**
 * Created by Sola on 2018/1/29.
 */

public class SunMiPrintUtils {

    private boolean is58mm = false;

    private static SunMiPrintUtils instance = null;

    public static SunMiPrintUtils getInstance() {
        if (instance == null) {
            synchronized (SunMiPrintUtils.class) {
                if(instance==null){
                    instance = new SunMiPrintUtils();
                }
            }
        }

        return instance;
    }

        public boolean getConnectStatus() {
        return AidlUtil.getInstance().isConnect();
    }

    public void initPrinter() {
        AidlUtil.getInstance().initPrinter();
    }

    public void printOrder(Activity mContext) {
        LogUtil.error("开始打印");
        try {

            SunMiPrintUtils.printNoteTest(mContext.getResources(), AidlUtil.getInstance().getService(), is58mm);
//            OrderPrintModel model = OrderHelper.getInstance(mContext).getPrintInfo();
//            if (model.getGoodInfoList().size() <= 0) {
//                Toast.makeText(mContext, "空单不能打印~~", Toast.LENGTH_SHORT).show();
//                return;
//            }
//            LogUtil.error("开始打印");
//            if (AidlUtil.getInstance().getService() != null) {
//                initPrinter();
//                LogUtil.error("开始打印");
//                SunMiPrintUtils.printNote(mContext.getResources(), AidlUtil.getInstance().getService(), is58mm,model);
//            }
        } catch (Exception e) {
        }
    }

    public static void printNote(Resources resources, IWoyouService mPrinter,
                                 boolean is58mm, OrderPrintModel model) {

        try {
            mPrinter.sendRAWData(ESCUtil.boldOff(), null);
            mPrinter.sendRAWData(ESCUtil.alignCenter(),null);
            mPrinter.printTextWithFont(model.getOpName(),null,24,null);
            mPrinter.lineWrap(3,null);
            mPrinter.sendRAWData(ESCUtil.alignLeft(),null);

            StringBuffer sb = new StringBuffer();
            //单号
            sb.append("单    号:" + model.getOrderNo() + "\n");
            sb.append("收 银 员:" + model.getOpName() + "\n");
            sb.append("单据日期:" + TimeUtils.getInstance().formatYMD(model.getOrderDate()) + "\n");
            sb.append("打印日期:" + TimeUtils.getInstance().formatYMDHMS(model.getPrintDate()) + "\n");

            mPrinter.printTextWithFont(sb.toString(),null,18,null);
            mPrinter.lineWrap(1,null);
            printTable(resources, mPrinter, is58mm, model); // 打印表格
            sb = new StringBuffer();
            if (is58mm) {
                sb.append("数量:"
                        + "" + model.getCountGoods() + "\n");
                sb.append("总计:"
                        + "" + model.getCountAmount() + "\n");

                sb.append("付款:");

                if (model.getCashAmount() > 0) {
                    sb.append(
                            "\n " + "现  金:" + model.getCashAmount() + "");
                }

                if (model.getBankCardAmount() > 0) {
                    sb.append("\n"
                            + "" + " 银行卡:" + model.getBankCardAmount());
                }

                if (model.getMemberCardAmount() > 0) {
                    sb.append("\n" + " 会员卡:" + model.getMemberCardAmount());
                }

                if (model.getPrePayAmount() > 0) {
                    sb.append("\n" + " 押  金:" + model.getPrePayAmount());
                }

                if (model.getAccountAmount() > 0) {
                    sb.append("\n" + " 账  户:" + model.getAccountAmount());
                }

                if (model.getWebChatAmount() > 0) {
                    sb.append("\n"
                            + "" + " 微  信:" + model.getWebChatAmount());
                }

                if (model.getAliPayAmount() > 0) {
                    sb.append("\n"
                            + "" + " 支付宝:" + model.getAliPayAmount());
                }

                if (model.getDiscountAmount() > 0) {
                    sb.append("\n"
                            + "抹  零:"
                            + "" + model.getDiscountAmount());
                }

                sb.append("\n"
                        + "找  零:"
                        + "" + model.getChangeAmount());

                sb.append("\n" + "余额:"
                        + "" + model.getAccountBalance() + "\n");
            } else {
                sb.append("数量:"
                        + "" + model.getCountGoods() + "\n");
                sb.append("总计:"
                        + "" + model.getCountAmount() + "\n");

                sb.append("付款:");

                if (model.getCashAmount() > 0) {
                    sb.append("\n " + "现  金:" + model.getCashAmount() + "");
                }

                if (model.getBankCardAmount() > 0) {
                    sb.append("\n "
                            + "" + " 银行卡:" + model.getBankCardAmount());
                }

                if (model.getMemberCardAmount() > 0) {
                    sb.append("\n" + " 会员卡:" + model.getMemberCardAmount());
                }

                if (model.getPrePayAmount() > 0) {
                    sb.append("\n" + " 押  金:" + model.getPrePayAmount());
                }

                if (model.getAccountAmount() > 0) {
                    sb.append("\n" + " 账  户:" + model.getAccountAmount());
                }

                if (model.getWebChatAmount() > 0) {
                    sb.append("\n"
                            + "" + " 微  信:" + model.getWebChatAmount());
                }

                if (model.getAliPayAmount() > 0) {
                    sb.append("\n"
                            + "" + " 支付宝:" + model.getAliPayAmount());
                }

                if (model.getDiscountAmount() > 0) {
                    sb.append("\n"
                            + "抹  零:"
                            + "" + model.getDiscountAmount());
                }

                sb.append("\n"
                        + "找  零:"
                        + "" + model.getChangeAmount());

                sb.append("\n"
                        + "余额:"
                        + "" + model.getAccountBalance() + "\n");
            }

            if (is58mm) {
                sb.append("-------------------------------\n");
            } else {
                sb.append("----------------------------------------------\n");
            }
            mPrinter.printTextWithFont(sb.toString(),null,18,null);

            mPrinter.sendRAWData(ESCUtil.alignCenter(),null);
            mPrinter.printTextWithFont(resources.getString(com.android.print.demo.R.string.shop_thanks) + "\n",null,18,null);
            mPrinter.lineWrap(3,null);
            mPrinter.cutPaper(null);
        } catch (RemoteException e) {
            e.printStackTrace();
        }


    }


    public static void printNoteTest(Resources resources, IWoyouService mPrinter,
                                 boolean is58mm) {

        try {
            mPrinter.sendRAWData(ESCUtil.boldOff(), null);
            mPrinter.sendRAWData(ESCUtil.alignCenter(),null);
            mPrinter.printTextWithFont(resources.getString(com.android.print.demo.R.string.shop_company_title),null,24,null);
            mPrinter.lineWrap(3,null);
            mPrinter.sendRAWData(ESCUtil.alignLeft(),null);

            StringBuffer sb = new StringBuffer();
            //单号
            sb.append(resources.getString(com.android.print.demo.R.string.shop_num) + "574001\n");
            sb.append(resources.getString(com.android.print.demo.R.string.shop_receipt_num)
                    + "S00003169\n");
            sb.append(resources.getString(com.android.print.demo.R.string.shop_cashier_num)
                    + "s004_s004\n");
            sb.append(resources.getString(com.android.print.demo.R.string.shop_receipt_date)
                    + "2012-06-17\n");
            sb.append(resources.getString(com.android.print.demo.R.string.shop_print_time)
                    + "2012-06-17 13:37:24\n");

            mPrinter.printTextWithFont(sb.toString(),null,18,null);
            mPrinter.lineWrap(1,null);
            printTableTest(resources, mPrinter, is58mm); // 打印表格
            sb = new StringBuffer();
            if (is58mm) {
                sb.append(resources.getString(com.android.print.demo.R.string.shop_goods_number)
                        + "                6.00\n");
                sb.append(resources.getString(com.android.print.demo.R.string.shop_goods_total_price)
                        + "                35.00\n");

                sb.append(resources.getString(com.android.print.demo.R.string.shop_payment)
                        + "                100.00\n");

                    sb.append(resources.getString(com.android.print.demo.R.string.shop_change)
                            + "                65.00\n");

            } else {
                sb.append(resources.getString(com.android.print.demo.R.string.shop_goods_number)
                        + "                                6.00\n");
                sb.append(resources.getString(com.android.print.demo.R.string.shop_goods_total_price)
                        + "                                35.00\n");
                sb.append(resources.getString(com.android.print.demo.R.string.shop_payment)
                        + "                                100.00\n");
                sb.append(resources.getString(com.android.print.demo.R.string.shop_change)
                        + "                                65.00\n");
            }

            sb.append(resources.getString(com.android.print.demo.R.string.shop_company_name) + "\n");
            sb.append(resources.getString(com.android.print.demo.R.string.shop_company_site)
                    + "www.jiangsuxxxx.com\n");
            sb.append(resources.getString(com.android.print.demo.R.string.shop_company_address) + "\n");
            sb.append(resources.getString(com.android.print.demo.R.string.shop_company_tel)
                    + "0574-12345678\n");
            sb.append(resources.getString(com.android.print.demo.R.string.shop_Service_Line)
                    + "4008-123-456 \n");

            if (is58mm) {
                sb.append("==============================\n");
            } else {
                sb.append("==============================================\n");
            }

            mPrinter.printTextWithFont(sb.toString(),null,18,null);

            mPrinter.sendRAWData(ESCUtil.alignCenter(),null);
            mPrinter.printTextWithFont(resources.getString(com.android.print.demo.R.string.shop_thanks) + "\n",null,16,null);
            mPrinter.lineWrap(3,null);
            mPrinter.cutPaper(null);
        } catch (RemoteException e) {
            e.printStackTrace();
        }


    }

    public static void printTable(Resources resources,
                                  IWoyouService mPrinter, boolean is58mm, OrderPrintModel model) throws RemoteException {
//        String column = resources.getString(R.string.note_title);//品名;数量;单价;金额
        String[] column=new String[]{"品名","数量","单价","金额"};
        Table table;
        if (is58mm) {
//            table = new Table(column, ";", new int[]{12, 6, 7, 7});
            mPrinter.printColumnsText(column, new int[]{12, 6, 7, 7}, new int[]{0, 0, 0, 0}, null);
        } else {
//            table = new Table(column, ";", new int[]{18, 10, 10, 12});
            mPrinter.printColumnsText(column, new int[]{18, 10, 10, 12}, new int[]{0, 0, 0, 0}, null);
        }

        for (int i = 0; i < model.getGoodInfoList().size(); i++) {
            OrderPrintModel.GoodInfo info = model.getGoodInfoList().get(i);
//            table.addRow("" + info.getName() + ";" + "" + String.valueOf("" + info.getCount()) + ";" + info.getPrice() + ";" + info.getAmount());
            mPrinter.printColumnsText(new String[]{info.getName() , String.valueOf("" + info.getCount()) ,String.valueOf(info.getPrice()+""),String.valueOf(info.getAmount()+"")},new int[]{18, 10, 10, 12}, new int[]{0, 0, 0, 0}, null);
            mPrinter.lineWrap(1, null);
        }

//        mPrinter.printTable(table);
    }

    public static void printTableTest(Resources resources,
                                  IWoyouService mPrinter, boolean is58mm) throws RemoteException {
//        String column = resources.getString(R.string.note_title);//品名;数量;单价;金额
        String[] column=new String[]{"品名","数量","单价","金额"};
        Table table;
        if (is58mm) {
//            table = new Table(column, ";", new int[]{12, 6, 7, 7});
            mPrinter.printColumnsText(column, new int[]{12, 6, 7, 7}, new int[]{0, 0, 0, 0}, null);
        } else {
//            table = new Table(column, ";", new int[]{18, 10, 10, 12});
            mPrinter.printColumnsText(column, new int[]{18, 10, 10, 12}, new int[]{0, 0, 0, 0}, null);
        }

//            table.addRow("" + info.getName() + ";" + "" + String.valueOf("" + info.getCount()) + ";" + info.getPrice() + ";" + info.getAmount());
            mPrinter.printColumnsText(new String[]{"Test" , "Test"  ,"Test" ,"Test" },new int[]{18, 10, 10, 12}, new int[]{0, 0, 0, 0}, null);
            mPrinter.lineWrap(1, null);

//        mPrinter.printTable(table);
    }
}
