package com.warmsoft.pos.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.warmsoft.pos.R;
import com.warmsoft.pos.adapter.AddPetRecyclerViewAdapter;
import com.warmsoft.pos.adapter.OrderSelecterAdapter;
import com.warmsoft.pos.bean.MemberInfoModel;
import com.warmsoft.pos.bean.PetInfoModel;
import com.warmsoft.pos.litepal.data.CardKind;
import com.warmsoft.pos.litepal.data.CustomerList;
import com.warmsoft.pos.litepal.data.LitepalHelper;
import com.warmsoft.pos.litepal.data.OrderInfoData;
import com.warmsoft.pos.litepal.data.PetCategory;
import com.warmsoft.pos.litepal.data.PetData;
import com.warmsoft.pos.util.AppUtils;
import com.warmsoft.pos.util.LogUtil;
import com.warmsoft.pos.util.MemberUtil;
import com.warmsoft.pos.util.OAuthUtils;
import com.warmsoft.pos.util.OrderHelper;
import com.warmsoft.pos.util.SelectMemberUtil;
import com.warmsoft.pos.view.SpaceItemDecoration;

import org.angmarch.views.NiceSpinner;
import org.litepal.crud.DataSupport;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 作者: lijinliu on 2016/8/5.
 * 邮箱：jinliu.li@warmsoft.com
 * 描述: 当前会员待付款
 */
public class MemberAddMemberDialog implements View.OnClickListener {
    private Dialog mDialog;
    private Activity mActivity;
    private OrderHelper mOrderHelper;
    private LayoutInflater mInflater;
    private int mCustomeId;

    private TextView tvBtnCancle;
    private TextView tvBtnCofirm;
    private TextView tvNoRecoredNote;
    private TextView tvWaitpayTitle;

    private ListView listview;
    OrderSelecterAdapter mAdapter;
    List<OrderInfoData> mOrderList;

//    private CustomerList customer;//选中的待付款

    private NiceSpinner mDialogMemberCard;
    private ImageView ivClose;
    private TextView tvClose;

    private ImageView ivPetAdd;

    private RecyclerView mRecyclerViews;
    private AddPetRecyclerViewAdapter recyclerViewAdapter;

    List<PetCategory> petCategories;
    List<PetInfoModel> petInfoModelList = new ArrayList<>();

    private TextView tvCommit;
    private EditText etName;
    private EditText etMobile;

    private LitepalHelper mLitepal;
    MemberInfoModel memberInfoModel = new MemberInfoModel();

    MemberAddInterface memberAddInterface;

    public MemberAddMemberDialog(Activity activity, MemberAddMemberDialog.MemberAddInterface addInterface) {
        this.mActivity = activity;
        this.mInflater = LayoutInflater.from(mActivity);
        this.mOrderHelper = OrderHelper.getInstance(mActivity);
        this.memberAddInterface = addInterface;

        createLoadingDialog(mActivity);

        init(mActivity);
    }

    /**
     * 得到自定义的progressDialog
     *
     * @param mContext
     * @return
     */
    private void createLoadingDialog(Activity mContext) {
        initDatas();

        View rootView = mInflater.inflate(R.layout.dialog_member_add, null);
        LinearLayout layout = (LinearLayout) rootView.findViewById(R.id.dialog_view);
        initView(rootView, mContext);
        initEvents();
        mDialog = new Dialog(mContext, R.style.fullScreenDialog);
        mDialog.setCancelable(false);
        int MATH_PARENT = LinearLayout.LayoutParams.MATCH_PARENT;
        mDialog.setContentView(layout, new LinearLayout.LayoutParams(MATH_PARENT, MATH_PARENT));// 设置布局
        mDialog.show();

        init(mContext);

//        initDate();//已选会员数据
    }

    private void initDatas() {

    }

    private void initView(View rootView, Activity mContext) {
        mDialogMemberCard = (NiceSpinner) rootView.findViewById(R.id.id_for_member_card_add);
        ivClose = (ImageView) rootView.findViewById(R.id.id_for_member_close_iv);
        tvClose = (TextView) rootView.findViewById(R.id.id_for_member_close);

        tvCommit = (TextView) rootView.findViewById(R.id.btn_confirm);
        etName = (EditText) rootView.findViewById(R.id.id_for_master_name);
        etMobile = (EditText) rootView.findViewById(R.id.id_for_master_mobile_number);

        mRecyclerViews = (RecyclerView) rootView.findViewById(R.id.id_for_add_pet_info);

        petCategories = MemberUtil.getInstance(mContext).getPetCategoryLists();

        //设置布局管理器
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext);
        linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);

        //设置间距
        mRecyclerViews.addItemDecoration(new SpaceItemDecoration(AppUtils.dip2px(mContext, 20)));

        mRecyclerViews.setLayoutManager(linearLayoutManager);
        recyclerViewAdapter = new AddPetRecyclerViewAdapter(mContext, petInfoModelList, petCategories);
        mRecyclerViews.setAdapter(recyclerViewAdapter);

        PetInfoModel model = new PetInfoModel();
        model.setPetName("");
        model.setAge("");
        model.setID(String.valueOf(petInfoModelList.size()));
        petInfoModelList.add(model);

        recyclerViewAdapter.setData(petInfoModelList);

        ivPetAdd = (ImageView) rootView.findViewById(R.id.id_for_member_add_pet_icon);
        ivPetAdd.setOnClickListener(this);

//        tvBtnCancle = (TextView) rootView.findViewById(R.id.tv_btn_cancle);
//        tvBtnCofirm = (TextView) rootView.findViewById(R.id.btn_select_confirm);
//        tvNoRecoredNote = (TextView) rootView.findViewById(R.id.tv_no_recored);
//        tvWaitpayTitle = (TextView) rootView.findViewById(R.id.tv_member_waitpay_title);
//
//        listview = (ListView) rootView.findViewById(R.id.listview_orders);
//        mAdapter = new OrderSelecterAdapter(mActivity, mOrderList);
//        listview.setAdapter(mAdapter);
    }

    private void initEvents() {
        ivClose.setOnClickListener(this);
        tvClose.setOnClickListener(this);
        tvCommit.setOnClickListener(this);
//        listview.setOnItemClickListener(petListClick);
    }

    public void initDate() {
        CustomerList custom = LitepalHelper.getInstance().queryCustomInfo(mCustomeId + "");
        if (custom != null) {
            tvWaitpayTitle.setText(custom.getName() + "的待付款");
        } else {
            tvWaitpayTitle.setText("无相应会员");
        }

        mOrderList = LitepalHelper.getInstance().getOrderListByCustomId(mCustomeId, mOrderHelper.getOrderData().getOrderNo());
        if (mOrderList != null && mOrderList.size() > 0) {
            listview.setVisibility(View.VISIBLE);
            tvNoRecoredNote.setVisibility(View.GONE);

            mAdapter.setData(mOrderList);
            for (int i = 0; i < mOrderList.size(); i++) {
                if (mOrderList.get(i).getOrderNo() == mOrderHelper.getOrderData().getOrderNo()) {
                    mAdapter.setSelect(i);
                    break;
                }
            }

        } else {
            listview.setVisibility(View.GONE);
            tvNoRecoredNote.setVisibility(View.VISIBLE);
        }
    }


    AdapterView.OnItemClickListener petListClick = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
            mAdapter.setSelect(i);
        }
    };


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.id_for_member_close_iv:
            case R.id.id_for_member_close:
                dismiss();
                break;

            case R.id.id_for_member_add_pet_icon:
                for (int i = 0; i < petInfoModelList.size(); i++) {
                    PetInfoModel petInfoModel = petInfoModelList.get(i);
                    if (TextUtils.isEmpty(petInfoModel.getPetName()) || TextUtils.isEmpty(petInfoModel.getAge())) {
                        Toast.makeText(mActivity, "宠物信息不能有空的情况~~~", Toast.LENGTH_SHORT).show();
                        return;
                    }
                }

                PetInfoModel model = new PetInfoModel();
                model.setPetName("");
                model.setAge("");
                model.setID(String.valueOf(petInfoModelList.size()));
                petInfoModelList.add(model);

                recyclerViewAdapter.setData(petInfoModelList);
                break;

            case R.id.btn_confirm:
                if (isLegalPetInfo()) {
                    insertMember();
                }
                break;
        }

//        if (view == tvBtnCofirm) {
//            if (mAdapter.getSelected() == null) {
//                Util.showToastTop(mActivity, "请选择待一笔付款!");
//            } else {
//                dismiss();
//                OrderInfoData selected = mAdapter.getSelected();
//                if (selected.getOrderNo() != mOrderHelper.getOrderData().getOrderNo()) {
//                    mFragmentPos.settleListClick(selected);
//                }
//            }
//        } else if (view == tvBtnCancle) {
//            dismiss();
//        }
    }

    private void insertMember() {
        memberInfoModel.setName(etName.getText().toString().trim());
        memberInfoModel.setMobile(etMobile.getText().toString().trim());

        int bachNo = (int) new Date().getTime();

        //会员插入表
        CustomerList customer = new CustomerList();
        customer.setName(memberInfoModel.getName());
        customer.setCardTypeName(memberInfoModel.getLevel());
        customer.setCellPhone(memberInfoModel.getMobile());
        customer.setVIPLevel(memberInfoModel.getVIPLevel());
        customer.setCustomerId(String.valueOf(bachNo));
        customer.setCardNumber(String.valueOf(bachNo));
        customer.setOrgId(OAuthUtils.getInstance().getUserinfo(mActivity).getOrgId());
        customer.setIsNew(1);//新增
        customer.save(); //保存

//        customeList.add(0, customer);
//        cleanOtherSelectStatus(customeList);
//        customeAdapter.setData(customeList);//刷新会员列表

        SelectMemberUtil.getInstance().setCustomer(customer); //用来在商品列表中显示

        List<PetData> datas = new ArrayList<>();
        for (int i = 0; i < petInfoModelList.size(); i++) {
            PetData petData = new PetData();
            petData.setCustomerID(bachNo);
            petData.setKindOF(Integer.parseInt(petInfoModelList.get(i).getKindOF() == null ? "" : petInfoModelList.get(i).getKindOF()));
            petData.setAge(Integer.parseInt(petInfoModelList.get(i).getAge()));

            petData.setPetName(petInfoModelList.get(i).getPetName());
            petData.setOrgId(OAuthUtils.getInstance().getUserinfo(mActivity).getOrgId());

            petData.save();
            datas.add(petData);

            LogUtil.error("petInfoModelList getPetName " + DataSupport.findLast(PetData.class).getPetName());
        }

        SelectMemberUtil.getInstance().setPetDatas(datas);

        //插入本地数据库
        MemberUtil.getInstance(mActivity).postAddMembers(bachNo, memberInfoModel, petInfoModelList);

        if (memberAddInterface != null) {
            memberAddInterface.onAdd(customer);
        }

        //隐藏键盘
        hideKeyboard(mActivity);

        //关闭对话框
        dismiss();
    }

    private boolean isLegalPetInfo() {
        //提交增加会员信息
        if (TextUtils.isEmpty(etName.getText().toString().trim())) {
            Toast.makeText(mActivity, "姓名不能为空~~~", Toast.LENGTH_SHORT).show();
            return false;
        }

        if (TextUtils.isEmpty(etMobile.getText().toString().trim())) {
            Toast.makeText(mActivity, "手机号码不能为空~~~", Toast.LENGTH_SHORT).show();
            return false;
        }

        mLitepal = LitepalHelper.getInstance();
        CustomerList customerList = mLitepal.queryCustomInfoByMobile(etMobile.getText().toString().trim());
        if (customerList != null) {
            Toast.makeText(mActivity, "手机号码已存在~~~", Toast.LENGTH_SHORT).show();
            return false;
        }

        if (petInfoModelList == null || petInfoModelList.size() <= 0) {
            Toast.makeText(mActivity, "必须添加宠物信息~~~", Toast.LENGTH_SHORT).show();
            return false;
        }

        for (int i = 0; i < petInfoModelList.size(); i++) {
            PetInfoModel petInfoModel = petInfoModelList.get(i);
            if (TextUtils.isEmpty(petInfoModel.getPetName()) || TextUtils.isEmpty(petInfoModel.getAge())) {
                Toast.makeText(mActivity, "宠物信息必须全部填写~~~", Toast.LENGTH_SHORT).show();
                return false;
            }
        }

        return true;
    }

    public void dismiss() {
        if (mDialog != null) {
            mDialog.dismiss();
            mDialog = null;
        }
    }

    private void init(Context mContext) {
        final ArrayList<CardKind> cardKinds = (ArrayList) MemberUtil.getInstance(mContext).getCardKindLists();

        if (cardKinds.size() > 0) {
            LogUtil.error("init ");
            memberInfoModel.setVIPLevel(cardKinds.get(0).getId());
            memberInfoModel.setLevel(cardKinds.get(0).getName());

            mDialogMemberCard.setSelectedIndex(0);
            mDialogMemberCard.attachDataSource(parseSpinnerDisplay(cardKinds));
            mDialogMemberCard.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    memberInfoModel.setVIPLevel(cardKinds.get(i).getId());
                    memberInfoModel.setLevel(cardKinds.get(i).getName());
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });
        }
    }

    private List<String> parseSpinnerDisplay(List<CardKind> list) {

        List<String> l = new ArrayList<>();
        for (CardKind s : list) {
            l.add(s.getName());
        }

        return l;
    }

    public interface MemberAddInterface {
        void onAdd(CustomerList customerList);
    }

    public void hideKeyboard(Context mContext) {
        InputMethodManager imm = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
        // 隐藏软键盘
        imm.hideSoftInputFromWindow(mActivity.getWindow().getDecorView().getWindowToken(), 0);
    }
}
