package com.warmsoft.pos.litepal.data;

import org.litepal.crud.DataSupport;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by brooks on 16/8/15.
 */
public class CatgoryItemInfo extends DataSupport {
    private int id;
    private String Category;
    private float choose;
    private int type;
    public List<GoodItemInfo> info = new ArrayList<>();

    public String getCategory() {
        return Category;
    }

    public void setCategory(String category) {
        Category = category;
    }

    public List<GoodItemInfo> getInfo() {
        return info;
    }

    public void setInfo(List<GoodItemInfo> info) {
        this.info = info;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public float getChoose() {
        return choose;
    }

    public void setChoose(float choose) {
        this.choose = choose;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}
