package com.warmsoft.pos.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.warmsoft.pos.R;
import com.warmsoft.pos.bean.AccountRechargeHistoryModel;

import java.util.List;

/**
 * 作者: lijinliu on 2016/8/9.
 * 邮箱：jinliu.li@warmsoft.com
 * 描述: 开单时弹出窗中会员列表
 */
public class MemberAddListAdapter extends BaseAdapter {
    protected Context mContext;
    protected List<AccountRechargeHistoryModel.DataInfo> datas;

    public MemberAddListAdapter(Context context, List<AccountRechargeHistoryModel.DataInfo> datas) {
        mContext = context;
        this.datas = datas;
    }

    public void setData(List<AccountRechargeHistoryModel.DataInfo> datas) {
        this.datas = datas;
        this.notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        if (datas == null) {
            return 0;
        } else {
            return datas.size();
        }
    }

    @Override
    public Object getItem(int position) {
        if (datas == null) {
            return null;
        } else {
            return datas.get(position);
        }
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @SuppressLint("ViewHolder")
    @Override
    public View getView(int position, View view, ViewGroup parent) {
        ViewHolder holde;
        if (view == null) {
            view = LayoutInflater.from(mContext).inflate(R.layout.view_item_for_member_add, null);
            holde = new ViewHolder(view);
        } else {
            holde = (ViewHolder) view.getTag();
            if (holde == null) {
                view = LayoutInflater.from(mContext).inflate(R.layout.view_item_for_member_add, null);
                holde = new ViewHolder(view);
            }
        }
        if (holde != null) {
            AccountRechargeHistoryModel.DataInfo item = (AccountRechargeHistoryModel.DataInfo) getItem(position);
            holde.bindData(item);
        }

        return view;
    }

    class ViewHolder {
        TextView tvAccountRechargeAmount;
        TextView tvAccountRechargeGetAmount;
        TextView tvAccountRechargeCurrentAmount;
        TextView tvAccountRechargeTime;
        TextView tvAccountRechargeStatus;
        TextView tvAccountRechargeOperator;
        LinearLayout llCustomerBackground;

        ViewHolder(View view) {
            tvAccountRechargeAmount = (TextView) view.findViewById(R.id.id_for_account_recharge_amount);
            tvAccountRechargeGetAmount = (TextView) view.findViewById(R.id.id_for_account_recharge_get_amount);
            tvAccountRechargeCurrentAmount = (TextView) view.findViewById(R.id.id_for_account_recharge_current_amount);
            tvAccountRechargeTime = (TextView) view.findViewById(R.id.id_for_account_recharge_time);
            tvAccountRechargeStatus = (TextView) view.findViewById(R.id.id_for_account_recharge_status);
            tvAccountRechargeOperator = (TextView) view.findViewById(R.id.id_for_account_recharge_operator);
            llCustomerBackground = (LinearLayout) view.findViewById(R.id.recy_bg_id);
            view.setTag(this);
        }

        void bindData(final AccountRechargeHistoryModel.DataInfo data) {
            if (data == null) {
                return;
            } else {
                tvAccountRechargeAmount.setText(String.valueOf(data.getReceivedAmount()) + "");
                tvAccountRechargeGetAmount.setText(String.valueOf(data.getInAccountAmount()) + "");
                tvAccountRechargeCurrentAmount.setText(String.valueOf(data.getCurrentTotalAmount()) + "");
                tvAccountRechargeTime.setText(String.valueOf(data.getChargeDatetime()) + "");
                tvAccountRechargeStatus.setText(String.valueOf(data.getStatusText()) + "");
                tvAccountRechargeOperator.setText(String.valueOf(data.getOperatorName()) + "");
            }
        }
    }
}
