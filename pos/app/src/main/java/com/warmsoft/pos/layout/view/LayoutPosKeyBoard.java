package com.warmsoft.pos.layout.view;

import android.app.Activity;
import android.view.View;

import com.warmsoft.pos.dialog.ClearPayedsDialog;
import com.warmsoft.pos.dialog.LoadingDialog;
import com.warmsoft.pos.dialog.NomelNoteDialog;
import com.warmsoft.pos.enumutil.OrderStatusEnum;
import com.warmsoft.pos.enumutil.PayChanel;
import com.warmsoft.pos.layout.view.poskeyboard.OrderPayChannel;
import com.warmsoft.pos.layout.view.poskeyboard.OrderPayInfo;
import com.warmsoft.pos.layout.view.poskeyboard.OrderPayKeyBoard;
import com.warmsoft.pos.listener.FmPosLitener;
import com.warmsoft.pos.litepal.data.OrderInfoData;
import com.warmsoft.pos.util.OrderHelper;
import com.warmsoft.pos.util.Util;

/**
 * 作者: lijinliu on 2016/8/9.
 * 邮箱：jinliu.li@warmsoft.com
 * 描述: 对应 view_pos_keyboder.xml
 */
public class LayoutPosKeyBoard implements OrderPayKeyBoard.KeyClick, OrderPayChannel.PayListener {

    private LoadingDialog mDialog;
    private OrderHelper orderHelper;
    private int payChanel = PayChanel.PAY_BY_CASH;//默认现金
    private boolean isFirstClick = false;
    private Activity mActivity;
    public OrderPayChannel layoutPayChannel;
    public OrderPayInfo layoutPayInfo;
    public OrderPayKeyBoard layoutKeyBoard;

    protected FmPosLitener mListener;

    public LayoutPosKeyBoard(View rootView, Activity activity, FmPosLitener listener) {
        this.mActivity = activity;
        this.mListener = listener;
        layoutPayChannel = new OrderPayChannel(rootView, mActivity, this);
        layoutPayInfo = new OrderPayInfo(rootView, mActivity);
        layoutKeyBoard = new OrderPayKeyBoard(rootView, mActivity, this, listener);
    }

    @Override
    public void payByCash() {
        orderHelper = OrderHelper.getInstance(mActivity);
        if (orderHelper.getOrderStaus() == OrderStatusEnum.HandIn || orderHelper.getOrderStaus() == OrderStatusEnum.KeepingAccounts) {
            payChanel = PayChanel.PAY_BY_CASH;
            isFirstClick = true;
            layoutPayInfo.layoutMain.setVisibility(View.GONE);
            layoutPayChannel.payByCash(orderHelper.getNeedColletAmountForText());
        } else if (orderHelper.getUnSureGoodSize() > 0) {
            new NomelNoteDialog(mActivity, "不能结算", "定单中有未确认商品!", "确定", null);
        }
    }

    @Override
    public void payByCard() {
        orderHelper = OrderHelper.getInstance(mActivity);
        if (orderHelper.getOrderStaus() == OrderStatusEnum.HandIn || orderHelper.getOrderStaus() == OrderStatusEnum.KeepingAccounts) {
            isFirstClick = true;
            payChanel = PayChanel.PAY_BY_BANKCARD;
            layoutPayInfo.layoutMain.setVisibility(View.GONE);
            layoutPayChannel.payByCard(orderHelper.getNeedColletAmountForText());
        } else if (orderHelper.getUnSureGoodSize() > 0) {
            new NomelNoteDialog(mActivity, "不能结算", "定单中有未确认商品!", "确定", null);
        }
    }

    @Override
    public void numKeyClick(String keyValue) {
        if (layoutPayChannel.layoutMain.getVisibility() == View.VISIBLE) {

            if (orderHelper.getOrderStaus() == OrderStatusEnum.KeepingAccounts) {
                new NomelNoteDialog(mActivity, "不能修改", "记账仅支持单一方式支付!", "确定", null);
                return;
            }

            String pay = layoutPayChannel.tvCollectAmount.getText().toString();
            if (!Util.isNullStr(pay)) {
                if (isFirstClick && !".".equals(keyValue)) {
                    isFirstClick = false;
                    pay = "";
                }
                if ("0".equals(pay) && !".".equals(keyValue)) {
                    pay = "";
                    if ("00".equals(keyValue)) {
                        keyValue = "0";
                    }
                }
                if (".".equals(keyValue) && pay.contains(".")) {
                    keyValue = "";
                }
                if (pay.contains(".")) {
                    int index = pay.length() - pay.indexOf(".");
                    if (index > 2) {
                        keyValue = "";
                    }
                    if (index == 2 && "00".equals(keyValue)) {
                        keyValue = "0";
                    }
                }
            }
            orderHelper = OrderHelper.getInstance(mActivity);
            float changeAmount = Float.parseFloat(pay + keyValue) - orderHelper.getNeedColletAmount();

            if (changeAmount > 0) {
                layoutPayChannel.tvChangeAmount.setText(Util.subFloat(changeAmount) + "");
            } else {
                layoutPayChannel.tvChangeAmount.setText("0");
                changeAmount = 0;
            }
            orderHelper.setChangeAmoutn(changeAmount);

            layoutPayChannel.tvCollectAmount.setText(pay + keyValue);
        }

    }

    //清空已支付信息
    @Override
    public void clearPayed() {
        new ClearPayedsDialog(mActivity, mListener);
    }

    @Override
    public void keyDelete() {
        if (layoutPayChannel.layoutMain.getVisibility() == View.VISIBLE) {

            if (orderHelper.getOrderStaus() == OrderStatusEnum.KeepingAccounts) {
                new NomelNoteDialog(mActivity, "不能修改", "记账仅支持单一方式支付!", "确定", null);
                return;
            }

            String pay = layoutPayChannel.tvCollectAmount.getText().toString();
            if (Util.isNullStr(pay) || pay.length() == 1) {
                layoutPayChannel.tvCollectAmount.setText("0");
            } else {
                pay = pay.substring(0, pay.length() - 1);
                layoutPayChannel.tvCollectAmount.setText(pay);
            }

            orderHelper = OrderHelper.getInstance(mActivity);
            if (PayChanel.PAY_BY_CASH == payChanel || PayChanel.PAY_BY_BANKCARD == payChanel) {//现金找零
                float changeAmount = Float.parseFloat(pay) - orderHelper.getNeedColletAmount();
                if (changeAmount > 0) {
                    layoutPayChannel.tvChangeAmount.setText(Util.subFloat(changeAmount) + "");
                } else {
                    changeAmount = 0;
                    layoutPayChannel.tvChangeAmount.setText("0");
                }
                orderHelper.setChangeAmoutn(changeAmount);
            }
        }
    }

    @Override
    public void keyCancle() {
        cancelPay();
    }

    @Override
    public void cancelPay() {
        layoutPayInfo.layoutMain.setVisibility(View.VISIBLE);
        layoutPayChannel.layoutMain.setVisibility(View.GONE);
    }

    @Override
    public void payConfirm() {
        orderHelper = OrderHelper.getInstance(mActivity);
        OrderStatusEnum orderStatus = orderHelper.getOrderStaus();

        if (orderStatus == OrderStatusEnum.HandIn || orderStatus == OrderStatusEnum.KeepingAccounts) {
            String pay = layoutPayChannel.tvCollectAmount.getText().toString();
            float payAmount = Float.parseFloat(pay);

            orderHelper.saveCollectAmount(payAmount, payChanel);//保存收款信息

            layoutPayInfo.refershList(orderHelper.getOrderData().getOrderNo() + "");
            if (mListener != null) {
                mListener.payConfirm(orderHelper.getOrderData().getOrderNo() + "");
            }
            if ((orderHelper.getOrderStaus() == OrderStatusEnum.HandIn || orderHelper.getOrderStaus() == OrderStatusEnum.KeepingAccounts) && orderHelper.getNeedColletAmount() <= 0) {
                layoutKeyBoard.moreOrderAmount();
            } else {
                layoutKeyBoard.lessOrderAmount();
            }
            layoutPayInfo.layoutMain.setVisibility(View.VISIBLE);
            layoutPayChannel.layoutMain.setVisibility(View.GONE);

        } else {
            Util.showToast(mActivity, "无应收费用");
        }
    }

    //更新定单商品数据
    public void setOrderSize(int size) {
        layoutPayInfo.setOrderSize(size);
    }

    //更新应收
    public void setNeedPayAmount(float amount) {
        cancelPay();
        String amountStr = Util.subFloat(amount) + "";
        layoutPayChannel.tvNeedPayAmout.setText(amountStr);
        layoutPayChannel.tvCollectAmount.setText(amountStr);
        layoutPayInfo.setOrderAmount(amount);
    }

    //初始数据
    public void initData() {
        orderHelper = OrderHelper.getInstance(mActivity);
        OrderInfoData orderData = orderHelper.getOrderData();
        layoutPayInfo.setOrderSize(orderHelper.getOrderSize());
        setNeedPayAmount(orderHelper.getNeedAmount());

        layoutPayInfo.refershMemberInfo(orderData.getCustomerId());
        layoutPayInfo.refershList(orderData.getOrderNo() + "");

        if ((orderHelper.getOrderStaus() == OrderStatusEnum.HandIn || orderHelper.getOrderStaus() == OrderStatusEnum.KeepingAccounts) && orderHelper.getNeedColletAmount() <= 0) {
            layoutKeyBoard.moreOrderAmount();
        } else {
            layoutKeyBoard.lessOrderAmount();
        }
        layoutPayInfo.checkMemberAmount();
    }

    //刷新会员 信息
    public void refershMemberInfo(int customerid) {
        layoutPayInfo.getAmounts(customerid);
    }
}
