package com.warmsoft.pos.protocol;

import com.warmsoft.pos.bean.CustomerOrderModel;

/**
 * 作者: lijinliu on 2016/9/13.
 * 邮箱：jinliu.li@warmsoft.com
 * 描述: 网络返回监听
 */
public interface ProtocolListener {
    void onCompleted();
    void onError();
    void onNext();
}
