package com.warmsoft.pos.util;

import android.content.Context;

import com.bumptech.glide.Glide;

/**
 * Created by brooks on 16/6/22.
 */
public class RecyclingUtil {
    private static RecyclingUtil instance = null;

    public static RecyclingUtil getInstance() {
        if (instance == null) {
            synchronized (RecyclingUtil.class) {
                instance = new RecyclingUtil();
            }
        }

        return instance;
    }

    //清理所有
    public void recyclingAll(Context mContext) {
        recyclingOAuth(mContext);
        recyclingCache(mContext);
    }

    //清理账号
    public void recyclingOAuth(Context mContext) {
//        OAuthUtils.getInstance().cleanToken(mContext);//清空token
    }

    //清理缓存
    public void recyclingCache(final Context mContext) {
        Glide.get(mContext).clearMemory();

        new Thread(new Runnable() {
            @Override
            public void run() {
                Glide.get(mContext).clearDiskCache();
            }
        }).start();
    }
}
