package com.warmsoft.pos.enumutil;

/**
 * 作者: lijinliu on 2016/8/16.
 * 邮箱：jinliu.li@warmsoft.com
 * 描述: 支付方式
 */
public class PayChanel {
    public final static int PAY_BY_CASH = 1;//现金
    public final static int PAY_BY_BANKCARD = 2;//银行卡
    public final static int PAY_BY_VIPCARD = 3;//会员卡
    public final static int PAY_BY_ACCOUNT = 4;//账号支付
    public final static int PAY_BY_ALIPAY = 5;//支付宝
    public final static int PAY_BY_WECHAT = 6;//微信
    public final static int PAY_BY_DEPOSIT = 7;//押金
    public final static int PAY_BY_KEEPACCCOUNT = 8;//记账
    public final static int PAY_BY_DISCOUNT = 9;//抹零
    public final static int PAY_BY_OTHER = 10;//其它

    public static String getPayChanelName(int chalen) {
        String payChaneCn = "现金";
        switch (chalen) {
            case PAY_BY_CASH:
                payChaneCn = "现金";
                break;
            case PAY_BY_BANKCARD:
                payChaneCn = "银行卡";
                break;
            case PAY_BY_VIPCARD:
                payChaneCn = "会员卡";
                break;
            case PAY_BY_ACCOUNT:
                payChaneCn = "账户";
                break;
            case PAY_BY_ALIPAY:
                payChaneCn = "支付宝";
                break;
            case PAY_BY_WECHAT:
                payChaneCn = "微信";
                break;
            case PAY_BY_DEPOSIT:
                payChaneCn = "押金";
                break;
            case PAY_BY_KEEPACCCOUNT:
                payChaneCn = "记账";
                break;
            case PAY_BY_OTHER:
                payChaneCn = "其它";
                break;
            case PAY_BY_DISCOUNT:
                payChaneCn = "抹零";
                break;
        }
        return payChaneCn;
    }
}
