package com.warmsoft.pos.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.warmsoft.pos.R;
import com.warmsoft.pos.litepal.data.CustomerList;

import java.util.List;

/**
 * 作者: lijinliu on 2016/8/9.
 * 邮箱：jinliu.li@warmsoft.com
 * 描述: 开单时弹出窗中会员列表
 */
public class CustomeSelecterAdapter extends BaseAdapter {
    protected Context mContext;
    protected List<CustomerList> datas;

    public CustomeSelecterAdapter(Context context, List<CustomerList> datas) {
        mContext = context;
        this.datas = datas;
    }

    public void setData(List<CustomerList> datas) {
        this.datas = datas;
        this.notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        if (datas == null) {
            return 0;
        } else {
            return datas.size();
        }
    }

    @Override
    public Object getItem(int position) {
        if (datas == null) {
            return null;
        } else {
            return datas.get(position);
        }
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @SuppressLint("ViewHolder")
    @Override
    public View getView(int position, View view, ViewGroup parent) {
        ViewHolder holde;
        if (view == null) {
            view = LayoutInflater.from(mContext).inflate(R.layout.item_member_inputs, null);
            holde = new ViewHolder(view);
        } else {
            holde = (ViewHolder) view.getTag();
            if (holde == null) {
                view = LayoutInflater.from(mContext).inflate(R.layout.item_member_inputs, null);
                holde = new ViewHolder(view);
            }
        }
        if (holde != null) {
            CustomerList item = (CustomerList) getItem(position);
            holde.bindData(item);
        }

        return view;
    }

    class ViewHolder {
        TextView tvCardNum;
        TextView tvMemberPhone;
        TextView tvMemberName;

        ViewHolder(View view) {
            tvCardNum = (TextView) view.findViewById(R.id.tv_card_num);
            tvMemberPhone = (TextView) view.findViewById(R.id.tv_member_phone);
            tvMemberName = (TextView) view.findViewById(R.id.tv_member_name);
            view.setTag(this);
        }

        void bindData(final CustomerList data) {
            if (data == null) {
                return;
            } else {
                tvCardNum.setText(data.getCardNumber() + "");
                tvMemberPhone.setText(data.getCellPhone() + "");
                tvMemberName.setText(data.getName() + "");
            }
        }
    }
}
