package com.warmsoft.base;

//import com.juwan.analytics.UMStatistics;

import android.app.Activity;
import android.content.Context;
import android.support.v4.app.Fragment;
import android.view.View;

/**
 * 作者: lijinliu on 2016/8/5.
 * 邮箱：jinliu.li@warmsoft.com
 * 描述: 基础fragment
 */
public class BaseFragment extends Fragment {
    protected Context mContext;
    protected Activity mActivity;
    protected View mLayoutView;

    // private final String mClassName = this.getClass().getSimpleName();

    @Override
    public void onPause() {
        super.onPause();
        // UMStatistics.onPageEnd(mClassName);
    }

    @Override
    public void onResume() {
        super.onResume();
        // UMStatistics.onPageStart(mClassName);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = activity;
        mContext = activity.getApplicationContext();
    }
}
