package com.warmsoft.base;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

import com.warmsoft.pos.permission.CheckPermission;
import com.warmsoft.pos.permission.PermissionActivity;
import com.zhy.http.okhttp.utils.L;

/**
 * 作者: lijinliu on 2016/8/5.
 * 邮箱：jinliu.li@warmsoft.com
 * 描述: 基础Activity
 */
public class BasicActivity extends FragmentActivity {

    private CheckPermission checkPermission;//检测权限器
    private static final int REQUEST_CODE = 10036;//请求码
    //配置需要取的权限
//    static final String[] PERMISSION = new String[]{
//            Manifest.permission.WRITE_EXTERNAL_STORAGE,// 写入权限
//            Manifest.permission.READ_EXTERNAL_STORAGE,  //读取权限
//    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppManager.getAppManager().addActivity(this);
        checkPermission = new CheckPermission(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        AppManager.getAppManager().removeActivity(this);
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        AppManager.getAppManager().removeActivity(this);
    }

    @Override
    protected void onStart() {
        // TODO Auto-generated method stub
        super.onStart();
        //initSystemBar(this);
    }

    public void checkPermission(String[] PERMISSION) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            //缺少权限时，进入权限设置页面
            if (checkPermission.permissionSet(PERMISSION)) {
                startPermissionActivity(PERMISSION);
            }else{
                nextEvent();
            }
        }else{
            nextEvent();
        }
    }

    //进入权限设置页面
    private void startPermissionActivity(String[] PERMISSION) {
        PermissionActivity.startActivityForResult(this, REQUEST_CODE, PERMISSION);
    }

    //返回结果回调
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //拒绝时，没有获取到主要权限，无法运行，关闭页面
        if (requestCode == REQUEST_CODE && resultCode == PermissionActivity.PERMISSION_GRANTED) {
//            finish();
            nextEvent();
        }
    }

    //权限申请返回操作
    protected void nextEvent(){

    }

}