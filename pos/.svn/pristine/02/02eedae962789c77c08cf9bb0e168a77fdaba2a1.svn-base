package com.warmsoft.pos.layout.view.settlement;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.warmsoft.pos.R;
import com.warmsoft.pos.bean.AmountsModel;
import com.warmsoft.pos.dialog.NomelNoteDialog;
import com.warmsoft.pos.enumutil.OrderStatusEnum;
import com.warmsoft.pos.enumutil.PayChanel;
import com.warmsoft.pos.listener.LayoutSettleListener;
import com.warmsoft.pos.litepal.data.OrderInfoData;
import com.warmsoft.pos.util.Constant;
import com.warmsoft.pos.util.OrderHelper;
import com.warmsoft.pos.util.SelectMemberUtil;
import com.warmsoft.pos.util.Util;

/**
 * 作者: lijinliu on 2016/8/10.
 * 邮箱：jinliu.li@warmsoft.com
 * 描述: view_seetlement_keyboard.xml
 */
public class SettlementKeyBoard implements View.OnClickListener {

    private Activity mActivity;
    private Context mContext;
    private LayoutSettleListener mListner;
    protected OrderHelper orderHelper;

    protected LinearLayout layoutPayend;
    protected LinearLayout layoutPaying;
    protected LinearLayout layoutPayChanel;

    protected TextView btnSettleConfirm;
    protected TextView tvNeedCollecAmount;
    protected TextView btnContinue;
    protected TextView btnMoling;

    protected TextView tvPayChanelTitle;
    protected TextView tvNeedPayAmount;
    protected TextView tvCollectAmount;
    protected LinearLayout layoutChange;

    protected TextView tvChangeAmount;
    protected TextView btnPayCancle;
    protected TextView btnPayConfirm;


    protected Button btnKeySeven;
    protected Button btnKeyEight;
    protected Button btnKeyNine;
    protected Button btnKeyDel;
    protected Button btnKeyFour;
    protected Button btnKeyFive;
    protected Button btnKeySix;
    protected Button btnKeyCancle;
    protected Button btnKeyOne;
    protected Button btnKeyTwo;
    protected Button btnKeyThree;
    protected Button btnKeyZero;
    protected Button btnKeyDoubleZero;
    protected Button btnKeyPoint;
    protected Button btnConfirm;

    protected ImageView ivSettleOver;

    private boolean isFirstClick = false;
    private int payChanel = PayChanel.PAY_BY_CASH;

    public SettlementKeyBoard(View rootView, Activity activity, LayoutSettleListener listener) {
        this.mActivity = activity;
        this.mContext = mActivity.getApplicationContext();
        this.mListner = listener;

        layoutPayend = (LinearLayout) rootView.findViewById(R.id.layout_settle_payend);
        layoutPaying = (LinearLayout) rootView.findViewById(R.id.layout_settle_paying);
        layoutPayChanel = (LinearLayout) rootView.findViewById(R.id.layout_settle_paychanel);

        btnSettleConfirm = (TextView) rootView.findViewById(R.id.tv_btn_settle_confirm);
        tvNeedCollecAmount = (TextView) rootView.findViewById(R.id.tv_settle_need_collection_amount);
        btnContinue = (TextView) rootView.findViewById(R.id.tv_btn_settle_continue);
        btnMoling = (TextView) rootView.findViewById(R.id.tv_btn_settle_moling);

        tvPayChanelTitle = (TextView) rootView.findViewById(R.id.tv_settle_paychanel_title);
        tvNeedPayAmount = (TextView) rootView.findViewById(R.id.tv_settle_paychanel_needpay);
        tvCollectAmount = (TextView) rootView.findViewById(R.id.tv_settle_paychanel_collect_amount);
        layoutChange = (LinearLayout) rootView.findViewById(R.id.layout_settle_pay_chanel_change);

        tvChangeAmount = (TextView) rootView.findViewById(R.id.tv_settle_paychanel_change_amount);
        btnPayCancle = (TextView) rootView.findViewById(R.id.tv_btn_settle_paycancle);
        btnPayConfirm = (TextView) rootView.findViewById(R.id.tv_btn_settle_payconfirm);


        btnKeySeven = (Button) rootView.findViewById(R.id.btn_settle_keyboard_seven);
        btnKeyEight = (Button) rootView.findViewById(R.id.btn_settle_keyboard_eight);
        btnKeyNine = (Button) rootView.findViewById(R.id.btn_settle_keyboard_nine);
        btnKeyDel = (Button) rootView.findViewById(R.id.btn_settle_keyboard_delete);
        btnKeyFour = (Button) rootView.findViewById(R.id.btn_settle_keyboard_four);
        btnKeyFive = (Button) rootView.findViewById(R.id.btn_settle_keyboard_five);
        btnKeySix = (Button) rootView.findViewById(R.id.btn_settle_keyboard_six);
        btnKeyCancle = (Button) rootView.findViewById(R.id.btn_settle_keyboard_cancle);
        btnKeyOne = (Button) rootView.findViewById(R.id.btn_settle_keyboard_one);
        btnKeyTwo = (Button) rootView.findViewById(R.id.btn_settle_keyboard_two);
        btnKeyThree = (Button) rootView.findViewById(R.id.btn_settle_keyboard_three);
        btnKeyZero = (Button) rootView.findViewById(R.id.btn_settle_keyboard_zero);
        btnKeyDoubleZero = (Button) rootView.findViewById(R.id.btn_settle_keyboard_doublezero);
        btnKeyPoint = (Button) rootView.findViewById(R.id.btn_settle_keyboard_point);
        btnConfirm = (Button) rootView.findViewById(R.id.btn_settle_keyboard_confirm);

        ivSettleOver = (ImageView) rootView.findViewById(R.id.iv_cheques_over_info);

        initEvents();
    }

    private void initEvents() {

        btnSettleConfirm.setOnClickListener(this);
        btnContinue.setOnClickListener(this);
        btnMoling.setOnClickListener(this);

        btnPayCancle.setOnClickListener(this);
        btnPayConfirm.setOnClickListener(this);

        btnKeySeven.setOnClickListener(this);
        btnKeyEight.setOnClickListener(this);
        btnKeyNine.setOnClickListener(this);
        btnKeyDel.setOnClickListener(this);
        btnKeyFour.setOnClickListener(this);
        btnKeyFive.setOnClickListener(this);
        btnKeySix.setOnClickListener(this);
        btnKeyCancle.setOnClickListener(this);
        btnKeyOne.setOnClickListener(this);
        btnKeyTwo.setOnClickListener(this);
        btnKeyThree.setOnClickListener(this);
        btnKeyZero.setOnClickListener(this);
        btnKeyDoubleZero.setOnClickListener(this);
        btnKeyPoint.setOnClickListener(this);
        btnConfirm.setOnClickListener(this);
    }

    public void initData() {
        orderHelper = OrderHelper.getInstance(mContext);
        OrderInfoData orderData = orderHelper.getOrderData();
        if (orderData != null) {
            layoutPayend.setVisibility(View.GONE);
            layoutPayChanel.setVisibility(View.GONE);
            layoutPaying.setVisibility(View.VISIBLE);
            if (orderHelper.getOrderStaus() == OrderStatusEnum.Null || orderHelper.getOrderStaus() == OrderStatusEnum.Bill) {
                initDefaultData();
            } else {
                setNeedCollectAmount(orderHelper.getNeedAmount());
            }
        } else {
            initDefaultData();
        }
    }

    public void initDefaultData() {
        payCancle();
        tvNeedCollecAmount.setText(String.format(Constant.ORDER_NEED_COLLECTION_AMOUNT, "0"));
    }


    public void collectMoney(int payChanel) {
        isFirstClick = true;
        this.payChanel = payChanel;

        tvPayChanelTitle.setText(PayChanel.getPayChanelName(payChanel));
        float needPayAmount = orderHelper.getNeedColletAmountForText();
        layoutPayend.setVisibility(View.GONE);
        layoutPayChanel.setVisibility(View.VISIBLE);
        layoutPaying.setVisibility(View.GONE);
        tvCollectAmount.setText(Util.subFloat(needPayAmount) + "");
        tvNeedPayAmount.setText(Util.subFloat(needPayAmount) + "");
        tvChangeAmount.setText("0");

        OrderInfoData orderData = orderHelper.getOrderData();
        AmountsModel.DataInfo amountInfo = SelectMemberUtil.getInstance().getDataInfo();
        switch (payChanel) {
            case PayChanel.PAY_BY_CASH://现金";
                layoutChange.setVisibility(View.VISIBLE);
                break;
            case PayChanel.PAY_BY_BANKCARD://银行卡";
                layoutChange.setVisibility(View.VISIBLE);
                break;
            case PayChanel.PAY_BY_VIPCARD://会员卡
                if (orderData != null && amountInfo != null && orderData.getCustomerId() == amountInfo.getCustomerid() && amountInfo.getMemberCard() < needPayAmount) {
                    tvCollectAmount.setText(Util.subFloat((float) amountInfo.getMemberCard()) + "");
                }
                layoutChange.setVisibility(View.GONE);
                break;
            case PayChanel.PAY_BY_ACCOUNT://账户
                if (orderData != null && amountInfo != null && orderData.getCustomerId() == amountInfo.getCustomerid() && amountInfo.getAccount() < needPayAmount) {
                    tvCollectAmount.setText(Util.subFloat((float) amountInfo.getAccount()) + "");
                }
                layoutChange.setVisibility(View.GONE);
                break;
            case PayChanel.PAY_BY_ALIPAY://支付宝
                layoutChange.setVisibility(View.GONE);
                break;
            case PayChanel.PAY_BY_WECHAT://微信
                layoutChange.setVisibility(View.GONE);
                break;
            case PayChanel.PAY_BY_DEPOSIT://押金
                if (orderData != null && amountInfo != null && orderData.getCustomerId() == amountInfo.getCustomerid() && amountInfo.getPrePay() < needPayAmount) {
                    tvCollectAmount.setText(Util.subFloat((float) amountInfo.getPrePay()) + "");
                }
                layoutChange.setVisibility(View.GONE);
                break;
        }

    }

    public void numKeyClick(String keyValue) {
        if (layoutPayChanel.getVisibility() == View.VISIBLE) {
            if (PayChanel.PAY_BY_ACCOUNT == payChanel||PayChanel.PAY_BY_VIPCARD == payChanel||PayChanel.PAY_BY_DEPOSIT == payChanel)
            {//账号，押金，会员卡支付，金额不能修改
                new NomelNoteDialog(mActivity, "不能修改", PayChanel.getPayChanelName(payChanel) + "支付，金额不能修改!", "确定", null);
                return;
            }
            if(orderHelper.getOrderStaus()==OrderStatusEnum.KeepingAccounts){
                new NomelNoteDialog(mActivity, "不能修改", "记账仅支持单一方式支付!", "确定", null);
                return;
            }

            String pay = tvCollectAmount.getText().toString();
            if (!Util.isNullStr(pay)) {
                if (isFirstClick && !".".equals(keyValue)) {
                    isFirstClick = false;
                    pay = "";
                }
                if ("0".equals(pay) && !".".equals(keyValue)) {
                    pay = "";
                    if ("00".equals(keyValue)) {
                        keyValue = "0";
                    }
                }
                if (".".equals(keyValue) && pay.contains(".")) {
                    keyValue = "";
                }
                if (pay.contains(".")) {
                    int index = pay.length() - pay.indexOf(".");
                    if (index > 2) {
                        keyValue = "";
                    }
                    if (index == 2 && "00".equals(keyValue)) {
                        keyValue = "0";
                    }
                }
            }
            orderHelper = OrderHelper.getInstance(mActivity);

            float payAmount = Float.parseFloat(pay + keyValue);
            float changeAmount = payAmount - orderHelper.getNeedColletAmount();

            if (changeAmount > 0) {
                tvChangeAmount.setText(Util.subFloat(changeAmount) + "");
            } else {
                tvChangeAmount.setText("0");
                changeAmount = 0;
            }
            orderHelper.setChangeAmoutn(changeAmount);
            tvCollectAmount.setText(pay + keyValue);

            OrderInfoData orderData = orderHelper.getOrderData();
            AmountsModel.DataInfo amountInfo = SelectMemberUtil.getInstance().getDataInfo();

            if (PayChanel.PAY_BY_WECHAT == payChanel || PayChanel.PAY_BY_ALIPAY == payChanel) {//微信支付宝 最大金额不能大于应付金额
                if (payAmount > orderHelper.getNeedColletAmount()) {
                    tvCollectAmount.setText(orderHelper.getNeedColletAmount() + "");
                }
            } else if (PayChanel.PAY_BY_ACCOUNT == payChanel) {//账号支付最大金额
                if (orderData != null && amountInfo != null && orderData.getCustomerId() == amountInfo.getCustomerid()) {
                    if (payAmount > orderHelper.getNeedColletAmount() && payAmount <= amountInfo.getAccount()) {//超过应付金额
                        tvCollectAmount.setText(orderHelper.getNeedColletAmount() + "");
                    }
                    if (payAmount < orderHelper.getNeedColletAmount() && payAmount > amountInfo.getAccount()) {//余额不足
                        tvCollectAmount.setText(amountInfo.getAccount() + "");
                    }
                }
            } else if (PayChanel.PAY_BY_VIPCARD == payChanel) {//会员卡支付最大金额
                if (orderData != null && amountInfo != null && orderData.getCustomerId() == amountInfo.getCustomerid()) {
                    if (payAmount > orderHelper.getNeedColletAmount() && payAmount <= amountInfo.getMemberCard()) {//超过应付金额
                        tvCollectAmount.setText(orderHelper.getNeedColletAmount() + "");
                    }
                    if (payAmount < orderHelper.getNeedColletAmount() && payAmount > amountInfo.getMemberCard()) {//余额不足
                        tvCollectAmount.setText(amountInfo.getMemberCard() + "");
                    }
                }
            } else if (PayChanel.PAY_BY_DEPOSIT == payChanel) {//押金支付最大金额
                if (orderData != null && amountInfo != null && orderData.getCustomerId() == amountInfo.getCustomerid()) {
                    if (payAmount > orderHelper.getNeedColletAmount() && payAmount <= amountInfo.getPrePay()) {//超过应付金额
                        tvCollectAmount.setText(orderHelper.getNeedColletAmount() + "");
                    }
                    if (payAmount < orderHelper.getNeedColletAmount() && payAmount > amountInfo.getPrePay()) {//余额不足
                        tvCollectAmount.setText(amountInfo.getPrePay() + "");
                    }
                }
            }
        }
    }

    private void collectionConfirm() {
        orderHelper = OrderHelper.getInstance(mActivity);
        OrderStatusEnum orderStatus = orderHelper.getOrderStaus();

        if (orderStatus == OrderStatusEnum.HandIn||orderStatus == OrderStatusEnum.KeepingAccounts) {
            String pay = tvCollectAmount.getText().toString();
            float payAmount = Float.parseFloat(pay);

            orderHelper.saveCollectAmount(payAmount, payChanel);//保存收款信息

            layoutPayend.setVisibility(View.GONE);
            layoutPayChanel.setVisibility(View.GONE);
            layoutPaying.setVisibility(View.VISIBLE);
            setNeedCollectAmount(orderHelper.getNeedAmount());

            if (mListner != null) {
                mListner.payConfirm(orderHelper.getOrderData().getOrderNo() + "");
            }
        } else {
            Util.showToast(mActivity, "无应收费用");
        }
    }

    private void setNeedCollectAmount(float amount) {
        amount = amount - orderHelper.getPayedAmout();
        ivSettleOver.setVisibility(View.GONE);
        btnConfirm.setBackgroundResource(R.drawable.warmsoft_keybord_confirm);
        if (amount > 0 || orderHelper.getOrderSize() == 0) {
            layoutPayend.setVisibility(View.GONE);
            layoutPayChanel.setVisibility(View.GONE);
            layoutPaying.setVisibility(View.VISIBLE);
            tvNeedCollecAmount.setText(String.format(Constant.ORDER_NEED_COLLECTION_AMOUNT, amount + ""));
        } else {
            layoutPayend.setVisibility(View.VISIBLE);
            layoutPayChanel.setVisibility(View.GONE);
            layoutPaying.setVisibility(View.GONE);
            if (orderHelper.getOrderStaus() == OrderStatusEnum.HandIn||orderHelper.getOrderStaus() == OrderStatusEnum.KeepingAccounts) {//下单未结算
                ivSettleOver.setVisibility(View.VISIBLE);
                btnConfirm.setBackgroundResource(R.drawable.warmsoft_keybord_payend_confirm);
            }
        }
    }

    private void payCancle() {
        layoutPayend.setVisibility(View.GONE);
        layoutPayChanel.setVisibility(View.GONE);
        layoutPaying.setVisibility(View.VISIBLE);
    }

    public void keyDelete() {
        if (layoutPayChanel.getVisibility() == View.VISIBLE) {

            if(orderHelper.getOrderStaus()==OrderStatusEnum.KeepingAccounts){
                new NomelNoteDialog(mActivity, "不能修改", "记账仅支持单一方式支付!", "确定", null);
                return;
            }

            String pay = tvCollectAmount.getText().toString();
            if (Util.isNullStr(pay) || pay.length() == 1) {
                tvCollectAmount.setText("0");
            } else {
                pay = pay.substring(0, pay.length() - 1);
                tvCollectAmount.setText(pay);
            }

            orderHelper = OrderHelper.getInstance(mActivity);
            if (payChanel == PayChanel.PAY_BY_CASH || PayChanel.PAY_BY_BANKCARD == payChanel) {//找零
                float changeAmount = Float.parseFloat(pay) - orderHelper.getNeedColletAmount();
                if (changeAmount > 0) {
                    tvChangeAmount.setText(Util.subFloat(changeAmount) + "");
                } else {
                    changeAmount = 0;
                    tvChangeAmount.setText("0");
                }
                orderHelper.setChangeAmoutn(changeAmount);
            }
        }
    }

    private void settleOver() {
        OrderHelper helper = OrderHelper.getInstance(mActivity);
        if (helper.getOrderStaus() == OrderStatusEnum.HandIn || helper.getOrderStaus() == OrderStatusEnum.ReOpen|| helper.getOrderStaus() == OrderStatusEnum.KeepingAccounts) {
            if (helper.getNeedColletAmount() <= 0) {
                helper.settleOrder(mActivity);//结算
                if (mListner != null) {//结算成功
                    mListner.settleOrder();
                }
                ivSettleOver.setVisibility(View.GONE);
                btnConfirm.setBackgroundResource(R.drawable.warmsoft_keybord_confirm);
            } else {
                Util.showToast(mActivity, "实收金额小于应收金额");
            }
        } else {
            new NomelNoteDialog(mActivity, "不能结算", "无待结算数据!", "确定", null);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tv_btn_settle_confirm:
                settleOver();
                break;
            case R.id.tv_btn_settle_continue:
                if (mListner != null) {
                    mListner.continueCollect();
                }
                break;
            case R.id.tv_btn_settle_moling:
                if (mListner != null) {
                    mListner.moling();
                }
                break;
            case R.id.tv_btn_settle_paycancle:
                payCancle();
                break;
            case R.id.tv_btn_settle_payconfirm:
                collectionConfirm();
                break;
            case R.id.btn_settle_keyboard_seven:
                numKeyClick("7");
                break;
            case R.id.btn_settle_keyboard_eight:
                numKeyClick("8");
                break;
            case R.id.btn_settle_keyboard_nine:
                numKeyClick("9");
                break;
            case R.id.btn_settle_keyboard_delete:
                keyDelete();
                break;
            case R.id.btn_settle_keyboard_four:
                numKeyClick("4");
                break;
            case R.id.btn_settle_keyboard_five:
                numKeyClick("5");
                break;
            case R.id.btn_settle_keyboard_six:
                numKeyClick("6");
                break;
            case R.id.btn_settle_keyboard_cancle:
                payCancle();
                break;
            case R.id.btn_settle_keyboard_one:
                numKeyClick("1");
                break;
            case R.id.btn_settle_keyboard_two:
                numKeyClick("2");
                break;
            case R.id.btn_settle_keyboard_three:
                numKeyClick("3");
                break;
            case R.id.btn_settle_keyboard_zero:
                numKeyClick("0");
                break;
            case R.id.btn_settle_keyboard_doublezero:
                numKeyClick("00");
                break;
            case R.id.btn_settle_keyboard_point:
                numKeyClick(".");
                break;
            case R.id.btn_settle_keyboard_confirm:
                settleOver();
                break;
        }
    }
}
