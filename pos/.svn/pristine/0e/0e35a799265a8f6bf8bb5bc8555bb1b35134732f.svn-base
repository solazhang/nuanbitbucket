package com.warmsoft.pos.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.warmsoft.pos.R;
import com.warmsoft.pos.litepal.data.OrderGoodsData;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * 作者: lijinliu on 2016/8/9.
 * 邮箱：jinliu.li@warmsoft.com
 * 描述:
 */
public class OrderAdapter extends BaseAdapter {
    protected Context mContext;
    protected List<OrderGoodsData> datas;

    protected int drawableSure;
    protected int drawableUnSure;

    private int bgWhite;
    private int bgYellow;
    protected List<OrderGoodsData> checkedList = new ArrayList<OrderGoodsData>();
    private static Boolean myVisiblecheck = false;


    public OrderAdapter(Context context, List<OrderGoodsData> datas, Boolean visiblecheck) {
        mContext = context;
        bgWhite = mContext.getResources().getColor(R.color.white);
        bgYellow = mContext.getResources().getColor(R.color.orderbg_yellow);
        drawableSure = R.mipmap.warmpos_sure;
        drawableUnSure = R.mipmap.warmsoft_order_point;
        this.datas = datas;

    }

    public void setFlag(Boolean visiblecheck) {
        myVisiblecheck = visiblecheck;

    }

    public boolean getFlag() {
        return myVisiblecheck;

    }

    public List<OrderGoodsData> getCheckList() {
        return checkedList;
    }

    public void removeCheckList() {
        checkedList = new ArrayList<OrderGoodsData>();
    }

    public void setData(List<OrderGoodsData> datas) {
        this.datas = datas;
        this.notifyDataSetChanged();
    }

    public OrderGoodsData getSelected(int position) {
        OrderGoodsData om = null;
        if (datas != null && position < datas.size()) {
            int newPosition = datas.size() - position - 1;//倒序位置
            for (int i = 0; i < datas.size(); i++) {
                if (newPosition == i) {
                    om = datas.get(i);
                    om.isSelected = true;
                } else {
                    datas.get(i).isSelected = false;
                }
            }
            this.notifyDataSetChanged();
        }
        return om;
    }

    @Override
    public int getCount() {
        if (datas == null) {
            return 0;
        } else {
            return datas.size();
        }
    }

    @Override
    public Object getItem(int position) {
        if (datas == null) {
            return null;
        } else {
            return datas.get(position);
        }
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @SuppressLint("ViewHolder")
    @Override
    public View getView(int position, View view, ViewGroup parent) {
        ViewHolder holde;
        if (view == null) {
            view = LayoutInflater.from(mContext).inflate(R.layout.item_order_list, null);
            holde = new ViewHolder(view);
        } else {
            holde = (ViewHolder) view.getTag();
            if (holde == null) {
                view = LayoutInflater.from(mContext).inflate(R.layout.item_order_list, null);
                holde = new ViewHolder(view);
            }
        }
        if (holde != null) {
            int newPosition = datas.size() - position;
            OrderGoodsData item = (OrderGoodsData) getItem(newPosition - 1);
            holde.bindData(item, newPosition);
        }
        return view;
    }

    class ViewHolder {
        TextView tvOrderNum;
        TextView tvProductName;
        TextView tvOrderSize;
        TextView tvSumAmount;
        ImageView ivOrderSure;
        LinearLayout layoutBg;
        View firstLine;
        View bottomLine;
        CheckBox ckBox;
        FrameLayout selectLayot;

        ViewHolder(View view) {
            layoutBg = (LinearLayout) view.findViewById(R.id.layout_bg);
            tvOrderNum = (TextView) view.findViewById(R.id.tv_list_num);
            tvProductName = (TextView) view.findViewById(R.id.tv_product_name);
            tvOrderSize = (TextView) view.findViewById(R.id.tv_order_size);
            tvSumAmount = (TextView) view.findViewById(R.id.tv_sum_amount);
            ivOrderSure = (ImageView) view.findViewById(R.id.iv_order_sure);
            firstLine = view.findViewById(R.id.view_first_line);
            bottomLine = view.findViewById(R.id.view_bottom_line);
            selectLayot = (FrameLayout)view.findViewById(R.id.layout_selectid);

            ckBox = (CheckBox) view.findViewById(R.id.checkbox_id);
//            ckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//                @Override
//                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                    OrderGoodsData data = (OrderGoodsData) ckBox.getTag();
//                    if (isChecked) {
//                        ckBox.setBackgroundResource(R.mipmap.warmpos_batch);
//                        if (checkedList.contains(data)) {
//                            checkedList.remove(data);
//                        }
//                    } else {
//                        ckBox.setBackgroundResource(R.mipmap.warmpos_batch_sure);
//                        if (!checkedList.contains(data)) {
//                            checkedList.add(data);
//                        }
//                    }
//                }
//            });

            selectLayot.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(myVisiblecheck){
                    OrderGoodsData data = (OrderGoodsData) ckBox.getTag();
                    if (ckBox.isChecked()) {
                        ckBox.setChecked(false);
                        ckBox.setBackgroundResource(R.mipmap.warmpos_batch);
                        if (checkedList.contains(data)) {
                            checkedList.remove(data);
                        }
                    } else {
                        ckBox.setChecked(true);
                        ckBox.setBackgroundResource(R.mipmap.warmpos_batch_sure);
                        if (!checkedList.contains(data)) {
                            checkedList.add(data);
                        }
                    }
                 }
                }
            });
            view.setTag(this);
        }

        void bindData(final OrderGoodsData data, final int position) {
            if (data == null) {
                return;
            } else {
                tvOrderNum.setText(position + "");
                tvProductName.setText(data.getFeeItemName() + "");
                tvSumAmount.setText(data.getTotalAmount() + "");
                ckBox.setTag(data);

                if (data.getIsSure() == 1) {
                    ivOrderSure.setImageResource(drawableSure);
                } else {
                    ivOrderSure.setImageResource(drawableUnSure);
                }

                layoutBg.setBackgroundColor(bgWhite);
                ckBox.setChecked(false);
                ckBox.setBackgroundResource(R.mipmap.warmpos_batch);
                if (myVisiblecheck) {
                    float unSureSize = data.getCount() - data.getSureCount();
                    if (unSureSize > 0) {
                        tvOrderSize.setText(unSureSize + "");
                        tvOrderNum.setVisibility(View.GONE);
                        firstLine.setVisibility(View.GONE);
                        ivOrderSure.setVisibility(View.GONE);
                        ckBox.setVisibility(View.VISIBLE);
                        selectLayot.setVisibility(View.VISIBLE);
                        layoutBg.setVisibility(View.VISIBLE);
                        bottomLine.setVisibility(View.VISIBLE);
                        tvSumAmount.setText(data.getAmount() * unSureSize + "");
                    } else {
                        layoutBg.setVisibility(View.GONE);
                        bottomLine.setVisibility(View.GONE);
                    }
                } else {
                    tvOrderSize.setText(data.getCount() + "");
                    ckBox.setVisibility(View.GONE);
                    selectLayot.setVisibility(View.GONE);
                    tvOrderNum.setVisibility(View.VISIBLE);
                    firstLine.setVisibility(View.VISIBLE);
                    ivOrderSure.setVisibility(View.VISIBLE);
                    layoutBg.setVisibility(View.VISIBLE);
                    bottomLine.setVisibility(View.VISIBLE);
                    if (data.isSelected) {
                        layoutBg.setBackgroundColor(bgYellow);
                    }
                }

            }
        }
    }
}
