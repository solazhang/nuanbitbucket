package org.litepal;

import android.content.Context;
import android.os.Environment;

import java.io.File;

/**
 * 作者: lijinliu on 2016/8/5.
 * 邮箱：jinliu.li@warmsoft.com
 * 描述: 数据缓存路径设置
 */
public class SqliteFilePath {
    /**
     * 数据存放路径
     **/
    public static final String ROOT_PATH = "/Android/";
    public static final String DAT_PATH = ROOT_PATH + "data/";
//    public static final String PACKAGE_PATH = DAT_PATH + "com.warmsoft.warmpos/";
public static final String PACKAGE_PATH = DAT_PATH + "com.warmsoft.pos/";
    public static final String DBCACHE_PATH = PACKAGE_PATH + "database/";


    /**
     * 数据库路径获取
     *
     * @param DB_NAME
     * @param context
     * @return
     */
    public static String getDatabaseName(String DB_NAME, Context context) {
        String databasename = DB_NAME;
        boolean isSdcardEnable = false;
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {// SDCard是否插入
            isSdcardEnable = true;
        }
        String dbPath = null;
        if (isSdcardEnable) {
            dbPath = Environment.getExternalStorageDirectory().getPath() + DBCACHE_PATH;
        } else {// 未插入SDCard，建在内存中
            dbPath = context.getFilesDir().getPath() + "/database/";
        }
        File dbp = new File(dbPath);
        if (!dbp.exists()) {
            dbp.mkdirs();
        }
        databasename = dbPath + DB_NAME;
        return databasename;
    }
}
